# PricingEngine
Pricing engine for financial derivatives in C++ (C++11). It has support classes that interface with the blpapi library (Bloomberg) and uses the quantlib and boost libraries. CMake is used for the build.

CMakeLists files are based on the wonderful tutorial by https://www.johnlamp.net

## Settings:
- path to **blpapi** must be specified in **main CMakeLists.txt**
- paths to **boost** should be specified in **main CMakeLists.txt**
- paths to **quantlib** must be specified in **main CMakeLists.txt**
- path to **gmock** must be specified in **cmake/Modules/gmock.cmake**
- path to **gnuplot-iostream** must be specified in **main CMakeLists.txt** (**gnuplot** has to be installed)
- path to **soci** (C++ database access) must be specified in **main CMakeLists.txt**. The user must as well specify the soci libraries that she/he needs.
- path to **MySQL Connector/C** (or the corresponding API requested by soci based on database) must be specified in **main CMakeLists.txt**. For MySQL it was necessary to link to the import library, in this case the user has to make sure that the OS finds the dll. 
- path to **OTL** (more flexible C++ database access) must be specified in **main CMakeLists.txt**. The library is one header file.
- path to **MySQL Connector/ODBC** (or the corresponding API requested by OTL based on database) must be specified in **main CMakeLists.txt**. The user has to make sure the OS finds the dll (by for example setting the PATH environment variable accordingly).

## Download
Simply clone this repository:

```
git clone https://gitlab.com/giancarloGiuffra/PricingEngine.git PricingEngine
```

## Build
The build is done with CMake. Simply run, in the command prompt or terminal, the following:

```
cmake -G generator-name main-directory
```

where `generator-name` is the name of a supported CMake Generator and `main-directory` is the directory containing the main **CMakeLists.txt** file. All build files will be saved in the current working directory (I usually just create a temporary directory within my git repository). For a complete list of generators visit http://www.cmake.org/cmake/help/v3.0/manual/cmake-generators.7.html. This allows to build the code with any compiler and any operating system. For example if the generator **Unix Makefiles** was selected, there will be a **Makefile** within the build directory and to compile the code it will be necessary to simply run **make** in this directory. If **Visual Studio 12 2013 Win64**  was selected instead, there will be a **PricingEngine.sln** which you can open to later compile the code within **Visual Studio**.

## Documentation
To produce the documentation, doxygen (https://www.stack.nl/~dimitri/doxygen/) must be installed (latex and pdflatex as well - see the Getting started section in the doxygen website for instructions). Simply run the following, in the command prompt or terminal, from the doc directory:

```
doxygen Doxyfile
```

After running the command two directories will be created within the doc directory: html and latex. In the html directory **index.html** will contain the main page. In the latex directory run the command **make**, the file **refman.pdf** will contain the documentation.
Besides the main documentation produced with the above command there is some extra documentation within the **doc/extra** directory. The user can compile the **tex** files contained in the subdirectories to produce the relative documentation. This extra documentation regards more the mathematical models than the code.

## TO DO:
- gmock and gtest are currently built along with this project using the CMakeLists.txt file provided by gmock and gtest developers, so the targets gmock, gmock_main, gtest and gtest_main appear as well. It would be nice to use the gtest library + gmock framework directly.
- enable macros QL_ERROR_LINES and QL_ERROR_FUNCTION to make quantlib error messages more informative.
- Use mock objects in MarketDataTest instead of making real requests to the Bloomberg Data Center.
- Try gcov for coverage analysis. Unfortunately g++ does not work correctly in Windows OS, so the test must be done in another OS.
