#include <MarketData\include\OISCurveMktDataRequest.h>
#include <MarKetQuote\include\OISQuote.h>
#include <MarketData\include\EONIAMktDataRequest.h>
#include <MarketData\include\BaseMarketDataRequest.h>
#include <MarKetQuote\include\UtilityFunctions.h>
#include <CurveBootstrapping\include\OISBootstrap.h>
#include <MarketData\include\OptionsMktDataRequest.h>
#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarketData\include\UtilityFunctions.h>
#include <Utilities\include\ImpliedVolCalculations.h>
#include <MarKetQuote\include\OISQuote.h>
#include <CurveBootstrapping\include\OISCurve.h>
#include <MarketData\include\FilterByVolume.h>
#include <MarketData\include\FilterByOpenInterest.h>
#include <MarketData\include\FutureStripMktDataRequest.h>
#include <MarketData\include\FutureStripBrent.h>
#include <MarketData\include\FilterIfITM.h>
#include <MarketData\include\OISCurveUSD.h>
#include <Utilities\include\VolatilityStream.h>
#include <Utilities\include\DividendStream.h>
#include <Utilities\include\ConfigurationStream.h>
#include <MarketData\include\FilterIfMaturityBeforeThan.h>
#include <Utilities\include\OptionUtilities.h>
#include <Utilities\include\AmericanDividendsCostFunction.h>
#include <Utilities\include\DividendUtilities.h>
#include <MarKetQuote\include\FilterFwdOutliersByPercentile.h>
#include <Equity\include\MCDiscreteArithmeticEngineGeneric.h>
#include <Equity\include\MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric.h>
#include <Instruments\include\DiscreteAveragingMultiplePayoffsAsianOption.h>
#include <MarketData\include\FileMktDataRequest.h>
#include <DatabaseOTL\include\PricingDataDB.h>
#include <MarketData\include\OptionsFileMktDataRequest.h>

#include <ql\errors.hpp>
#include <ql\utilities\tracing.hpp>
#include <ql\currency.hpp>
#include <ql\currencies\all.hpp>
#include <ql\money.hpp>
#include <ql\exchangerate.hpp>
#include <ql\time\date.hpp>
#include <ql\time\period.hpp>
#include <ql\time\daycounters\actual360.hpp>
#include <ql\time\daycounters\thirty360.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\settings.hpp>
#include <ql\cashflows\simplecashflow.hpp>
#include <ql\cashflows\coupon.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\termstructures\yield\zeroyieldstructure.hpp>
#include <ql\termstructures\yield\piecewiseyieldcurve.hpp>
#include <ql\termstructures\volatility\equityfx\blackvariancesurface.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <ql\math\optimization\all.hpp>
#include <ql\instruments\makeois.hpp>
#include <ql\option.hpp>
#include <ql\instruments\payoffs.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>
#include <ql\processes\hestonprocess.hpp>
#include <ql\models\equity\hestonmodel.hpp>
#include <ql\models\equity\hestonmodelhelper.hpp>
#include <ql\pricingengines\vanilla\analytichestonengine.hpp>
#include <ql\math\optimization\differentialevolution.hpp>
#include <ql\math\optimization\simulatedannealing.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\processes\blackscholesprocess.hpp>
#include <ql\pricingengines\asian\mc_discr_arith_av_price.hpp>

#include <boost\foreach.hpp>
#include <boost\range\irange.hpp>
#include <boost\tuple\tuple.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\pointer_cast.hpp>

#include <gnuplot-iostream.h>

#define OTL_ODBC
#include <otlv4.h>

#include <vector>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <memory>
#include <ctime>
#include <iostream>
#include <chrono>
#include <regex>

using namespace QuantLib;

int main(void)
{
	//QL_TRACE_ENABLE;
	try {

		/*DEBUG----------------------------------------------------*/

		std::string text =
			"underlying 1\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n"
			"underlying 2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n";

		std::ofstream fs("\\CorrectOptionsMktDataFile.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\CorrectOptionsMktDataFile.txt");
		request.run();
		std::remove("\\CorrectOptionsMktDataFile.txt"); //deletes file

		//tests
		QueryResult resultMktFile = request.optionDataForSecurity("underlying 1");

		/*----------------------------------------------------------*/

		std::cout << "evaluation date: " << Settings::instance().evaluationDate() << std::endl;

		auto start = std::chrono::high_resolution_clock::now();

		/*************************
		***** ENI EXAMPLE ******
		**************************/

		//utilities for reporting
		int width = 112;
		std::string separator = " | ";
		std::string rule(width + 6, '-');
		std::string vspace = "\n\n";

		/**********
		MARKET DATA
		**********/

		//spot
		boost::shared_ptr<Quote> spot(new SimpleQuote(13.15));
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << std::left << "SPOT" << separator << std::endl;
		std::cout << separator << std::setw(width) << std::left << spot->value() << separator << std::endl;
		std::cout << rule << std::endl;

		//options for synthetic forwards
		std::vector<VanillaOptionQuote> oQuotesForSyntheticFwds = {
			VanillaOptionQuote("ENI IM", "c", "american", 13, "2016-03-17", "EUR", 0.2945),
			VanillaOptionQuote("ENI IM", "c", "american", 13.5, "2016-03-17", "EUR", 0.0857),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-03-17", "EUR", 0.1),
			VanillaOptionQuote("ENI IM", "p", "american", 13, "2016-03-17", "EUR", 0.2065),
			VanillaOptionQuote("ENI IM", "c", "american", 12.5, "2016-03-18", "EUR", 0.731),
			VanillaOptionQuote("ENI IM", "c", "american", 13, "2016-03-18", "EUR", 0.3495),
			VanillaOptionQuote("ENI IM", "c", "american", 13.25, "2016-03-18", "EUR", 0.2128),
			VanillaOptionQuote("ENI IM", "c", "american", 13.5, "2016-03-18", "EUR", 0.1298),
			VanillaOptionQuote("ENI IM", "c", "american", 13.75, "2016-03-18", "EUR", 0.0683),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-03-18", "EUR", 0.0578),
			VanillaOptionQuote("ENI IM", "p", "american", 12.5, "2016-03-18", "EUR", 0.0835),
			VanillaOptionQuote("ENI IM", "p", "american", 13, "2016-03-18", "EUR", 0.2243),
			VanillaOptionQuote("ENI IM", "p", "american", 13.25, "2016-03-18", "EUR", 0.3448),
			VanillaOptionQuote("ENI IM", "p", "american", 13.5, "2016-03-18", "EUR", 0.5103),
			VanillaOptionQuote("ENI IM", "c", "american", 12.5, "2016-04-15", "EUR", 0.958),
			VanillaOptionQuote("ENI IM", "c", "american", 13, "2016-04-15", "EUR", 0.6108),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-04-14", "EUR", 0.1765),
			VanillaOptionQuote("ENI IM", "p", "american", 12, "2016-04-14", "EUR", 0.179),
			VanillaOptionQuote("ENI IM", "p", "american", 13, "2016-04-14", "EUR", 0.4795),
			VanillaOptionQuote("ENI IM", "p", "american", 13.5, "2016-04-14", "EUR", 0.7388),
			VanillaOptionQuote("ENI IM", "c", "american", 13.5, "2016-04-15", "EUR", 0.3722),
			VanillaOptionQuote("ENI IM", "c", "american", 14.5, "2016-04-15", "EUR", 0.1017),
			VanillaOptionQuote("ENI IM", "p", "american", 11.5, "2016-04-15", "EUR", 0.1223),
			VanillaOptionQuote("ENI IM", "p", "american", 12, "2016-04-15", "EUR", 0.1948),
			VanillaOptionQuote("ENI IM", "p", "american", 13.5, "2016-04-15", "EUR", 0.7355),
			VanillaOptionQuote("ENI IM", "c", "american", 13, "2016-05-20", "EUR", 0.8168),
			VanillaOptionQuote("ENI IM", "c", "american", 13.5, "2016-05-20", "EUR", 0.5478),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-05-20", "EUR", 0.3525),
			VanillaOptionQuote("ENI IM", "c", "american", 14.5, "2016-05-20", "EUR", 0.226),
			VanillaOptionQuote("ENI IM", "p", "american", 12, "2016-05-19", "EUR", 0.3323),
			VanillaOptionQuote("ENI IM", "p", "american", 13.5, "2016-05-20", "EUR", 0.9165),
			VanillaOptionQuote("ENI IM", "p", "american", 10, "2016-06-16", "EUR", 0.162),
			VanillaOptionQuote("ENI IM", "p", "american", 9, "2016-06-17", "EUR", 0.1243),
			VanillaOptionQuote("ENI IM", "c", "american", 13, "2016-06-17", "EUR", 0.842),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-06-17", "EUR", 0.3883),
			VanillaOptionQuote("ENI IM", "c", "american", 14.5, "2016-06-17", "EUR", 0.2643),
			VanillaOptionQuote("ENI IM", "p", "american", 12.5, "2016-06-17", "EUR", 0.738),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-09-15", "EUR", 0.624),
			VanillaOptionQuote("ENI IM", "c", "american", 16, "2016-09-15", "EUR", 0.1825),
			VanillaOptionQuote("ENI IM", "p", "american", 13, "2016-09-16", "EUR", 1.24),
			VanillaOptionQuote("ENI IM", "c", "american", 13.5, "2016-12-16", "EUR", 0.8938),
			VanillaOptionQuote("ENI IM", "c", "american", 14, "2016-12-16", "EUR", 0.7035),
			VanillaOptionQuote("ENI IM", "c", "american", 14.5, "2016-12-16", "EUR", 0.5417),
			VanillaOptionQuote("ENI IM", "c", "american", 16, "2016-12-16", "EUR", 0.2253),
			VanillaOptionQuote("ENI IM", "p", "american", 12, "2016-12-16", "EUR", 1.1168),
			VanillaOptionQuote("ENI IM", "p", "american", 12.5, "2016-12-16", "EUR", 1.3465),
			VanillaOptionQuote("ENI IM", "p", "american", 13, "2016-12-16", "EUR", 1.6065)
		};
		std::vector<std::shared_ptr<VanillaOptionQuote>> oQuotesForSyntheticFwdsPtr;
		for (auto const& quote : oQuotesForSyntheticFwds)
			oQuotesForSyntheticFwdsPtr.push_back(std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote(quote)
			));
		/********DEBUG****/
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width)
			<< "OPTIONS (" + std::to_string(oQuotesForSyntheticFwds.size()) + ")" << separator << std::endl;
		for (auto quote : oQuotesForSyntheticFwds)
		{
			std::stringstream ss;
			ss << quote;
			std::cout << separator << std::setw(width) << std::left << ss.str() << separator << std::endl;
		}
		std::cout << rule << std::endl;
		/********/

		/************
		OIS BOOTSTRAP
		*************/

		auto startBootstrap = std::chrono::high_resolution_clock::now();

		//ois
		std::vector<OISQuote> oisQuotes = {
			OISQuote("EUR", "1W", -0.0031625),
			OISQuote("EUR", "2W", -0.0032925),
			OISQuote("EUR", "3W", -0.00325),
			OISQuote("EUR", "1M", -0.003275),
			OISQuote("EUR", "2M", -0.003325),
			OISQuote("EUR", "3M", -0.0033575),
			OISQuote("EUR", "4M", -0.00343),
			OISQuote("EUR", "5M", -0.003435),
			OISQuote("EUR", "6M", -0.00346),
			OISQuote("EUR", "7M", -0.00351),
			OISQuote("EUR", "20Y", 0.00953),
			OISQuote("EUR", "25Y", 0.01013),
			OISQuote("EUR", "30Y", 0.01034),
			OISQuote("EUR", "4Y", -0.00273),
			OISQuote("EUR", "5Y", -0.00182),
			OISQuote("EUR", "6Y", -0.000755),
			OISQuote("EUR", "7Y", 0.00042),
			OISQuote("EUR", "8Y", 0.00165),
			OISQuote("EUR", "9Y", 0.002855),
			OISQuote("EUR", "10Y", 0.00395),
			OISQuote("EUR", "11Y", 0.004948),
			OISQuote("EUR", "12Y", 0.00585),
			OISQuote("EUR", "15Y", 0.0078525),
			OISQuote("EUR", "8M", -0.0035475),
			OISQuote("EUR", "9M", -0.003585),
			OISQuote("EUR", "10M", -0.00362),
			OISQuote("EUR", "11M", -0.003705),
			OISQuote("EUR", "1Y", -0.0036725),
			OISQuote("EUR", "15M", -0.003735),
			OISQuote("EUR", "18M", -0.00376),
			OISQuote("EUR", "21M", -0.00375),
			OISQuote("EUR", "2Y", -0.00376),
			OISQuote("EUR", "3Y", -0.003395)
		};
		std::vector<boost::shared_ptr<Quote>> quotes;
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "OIS" << separator << std::endl;
		for (auto q : oisQuotes)
		{
			quotes.push_back(boost::shared_ptr<Quote>(new OISQuote(q)));
			std::stringstream ss;
			ss << *boost::dynamic_pointer_cast<OISQuote>(quotes.back());
			std::cout << separator << std::setw(width) << std::left
				<< ss.str() << separator << std::endl;
		}
		std::cout << rule << std::endl;

		//bootstrap
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "Bootstrapping ..." << separator << std::endl;
		std::cout << rule << std::endl;
		boost::shared_ptr<YieldTermStructure> oisCurve(new
			OISCurve<Discount, LogLinear, OISBootstrap>(2, TARGET(), quotes, Actual365Fixed(), 1.0e-12));

		auto endBootstrap = std::chrono::high_resolution_clock::now();

		/*****************************************
		EUROPEAN OPTION PRICES & IMPLIED DIVIDENDS
		******************************************/

		auto startImpliedDividends = std::chrono::high_resolution_clock::now();

		//Handles
		Handle<Quote> spotH(spot);
		Handle<YieldTermStructure> oisCurveH(oisCurve);

		//implied dividends
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "Calculating implied dividends ..." << separator << std::endl;
		std::cout << rule << std::endl;
		auto result =
			utilities::stripDividendsFromAmericanOptions(spotH, oisCurveH, oQuotesForSyntheticFwdsPtr, 0.01, 2);
		auto allDividendTSH = Handle<YieldTermStructure>(std::get<0>(result));
		auto allEuropeanQuotes =
			utilities::calculateEuropeanFromAmericanOptionPrices(spotH, oisCurveH, allDividendTSH,
			oQuotesForSyntheticFwdsPtr);
		auto syntheticFwds = utilities::calculateSyntheticFwds(spotH, oisCurveH, allEuropeanQuotes,
			FilterFwdOutliersByPercentile(0.20));
		auto dividendTSH = Handle<YieldTermStructure>(
			utilities::stripDividendsFromFutures(spotH, oisCurveH, syntheticFwds,
			oisCurveH->dayCounter(),
			oisCurveH->calendar()
			));

		auto endImpliedDividends = std::chrono::high_resolution_clock::now();

		//european prices
		std::vector<VanillaOptionQuote> oQuotes;
		std::transform(allEuropeanQuotes.begin(), allEuropeanQuotes.end(), std::back_inserter(oQuotes),
			[](std::shared_ptr<VanillaOptionQuote> q){ return *q; });
		oQuotes.erase(
			std::remove_if(oQuotes.begin(), oQuotes.end(),
			[&spotH](VanillaOptionQuote q){
			return q.optionType() == Option::Call ? q.strike() < spotH->value() : q.strike() >= spotH->value(); }),
				oQuotes.end());
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width)
			<< "OPTIONS (" + std::to_string(oQuotes.size()) + ")" << separator << std::endl;
		for (auto quote : oQuotes)
		{
			std::stringstream ss;
			ss << quote;
			std::cout << separator << std::setw(width) << std::left << ss.str() << separator << std::endl;
		}
		std::cout << rule << std::endl;

		/**********
		CALIBRATION
		**********/

		//stochastic process
		Real volatility = 0.20;
		Real v0 = volatility*volatility;
		Real kappa = 1.0;
		Real theta = volatility*volatility;
		Real sigma = 0.50;
		Real rho = -0.70;
		boost::shared_ptr<HestonProcess> process(
			new HestonProcess(oisCurveH, dividendTSH, spotH, v0, kappa, theta, sigma, rho));

		//calibrated model
		boost::shared_ptr<HestonModel> model(new HestonModel(process));

		//calibration helpers
		boost::shared_ptr<PricingEngine> hestonEngine(new AnalyticHestonEngine(model, 1.0e-3, 3000));
		std::vector<boost::shared_ptr<CalibrationHelper>> helpers;
		std::vector<boost::shared_ptr<VanillaOption>> options;
		auto volsAndOptions = utilities::calculateImpliedVolatilities(oQuotes, spotH, dividendTSH, oisCurveH);
		auto volatilities = std::get<0>(volsAndOptions);
		auto optionForCalibration = std::get<1>(volsAndOptions);
		for (int i = 0; i < optionForCalibration.size(); ++i)
		{
			options.push_back(boost::shared_ptr<VanillaOption>(optionForCalibration[i].option()));
			helpers.push_back(boost::shared_ptr<CalibrationHelper>(
				new HestonModelHelper(
				Period(optionForCalibration[i].expiry() - oisCurveH->referenceDate(), Days),
				oisCurveH->calendar(),
				spotH->value(),
				optionForCalibration[i].strike(),
				Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(volatilities[i]))),
				oisCurveH,
				dividendTSH
				)));
			helpers[i]->setPricingEngine(hestonEngine);
		}

		//launch calibration
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "Calibrating ..." << separator << std::endl;
		std::cout << rule << std::endl;
		Simplex optimizer(0.001);
		EndCriteria endCriteria(10000, 200, 1.0e-9, 1.0e-9, 1.0e-9);
		model->calibrate(helpers, optimizer, endCriteria);

		//results
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "CALIBRATION RESULTS" << separator << std::endl;
		std::cout << rule << std::endl;
		std::stringstream ss;
		ss << model->endCriteria();
		std::cout << separator << std::setw(width / 2) << "End Criteria: "
			<< separator << std::setw(width / 2) << ss.str() << separator << std::endl;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width / 3) << "Market Price"
			<< separator << std::setw(width / 3) << "Model Price"
			<< separator << std::setw(width / 3) << "Difference"
			<< separator << std::endl;
		std::cout << rule << std::endl;
		for (int j = 0; j < optionForCalibration.size(); ++j)
		{
			options[j]->setPricingEngine(hestonEngine);
			std::cout << separator << std::setw(width / 3) << std::defaultfloat << optionForCalibration[j].value()
				<< separator << std::setw(width / 3) << std::defaultfloat << options[j]->NPV()
				<< separator << std::setw(width / 3) << std::scientific << optionForCalibration[j].value() - options[j]->NPV()
				<< separator << std::endl;
		}
		std::cout << rule << std::endl;

		//calibrated parameters
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "CALIBRATED PARAMETERS" << separator << std::endl;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width / 2) << "initial volatility: "
			<< separator << std::setw(width / 2) << std::sqrt(model->v0()) << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "long-term volatility: "
			<< separator << std::setw(width / 2) << std::sqrt(model->theta()) << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "mean reversion: "
			<< separator << std::setw(width / 2) << model->kappa() << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "vol of vol: "
			<< separator << std::setw(width / 2) << model->sigma() << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "correlation: "
			<< separator << std::setw(width / 2) << model->rho() << separator << std::endl;
		std::cout << rule << std::endl;

		/*************************
		EXTENDED VOLATILITY MATRIX
		**************************/

		//implied vol from market quotes
		Matrix impliedVolatilityMatrix = utilities::buildImpliedVolatilityMatrix(
			optionForCalibration,
			spotH,
			dividendTSH,
			oisCurveH);

		//prepare expiries: add quoted + desired
		std::vector<Date> expiries = utilities::extractOptionExpiries(optionForCalibration);
		expiries.insert(expiries.end(), {
			Date::todaysDate() + Period(1, Years),
			Date::todaysDate() + Period(2, Years),
			Date::todaysDate() + Period(3, Years),
			Date::todaysDate() + Period(4, Years) });
		std::sort(expiries.begin(), expiries.end());
		auto lastE = std::unique(expiries.begin(), expiries.end());
		expiries.erase(lastE, expiries.end());
		expiries.erase(std::remove_if(expiries.begin(), expiries.end(),
			[](Date d){ return d < Date::todaysDate() + Period(1, Months); }),
			expiries.end()); //exclude expiries < 1 Month because heston price might not correspond to a implied vol

		//prepare strikes: add quoted + desired
		std::vector<Real> strikes = utilities::extractOptionStrikes(optionForCalibration);
		Real semi = 0.1;
		Real lowestStrike = spotH->value()*(1 - semi);
		Real stepStrike = spotH->value()*0.02;
		std::size_t size = static_cast<std::size_t>(((1 + semi)*spotH->value() - lowestStrike) / stepStrike);
		std::vector<Real> desiredStrikes(size);
		std::generate(desiredStrikes.begin(), desiredStrikes.end(),
			[&lowestStrike, stepStrike]{ return lowestStrike + stepStrike; });
		strikes.insert(strikes.end(), desiredStrikes.begin(), desiredStrikes.end());
		std::sort(strikes.begin(), strikes.end());
		auto lastS = std::unique(strikes.begin(), strikes.end());
		strikes.erase(lastS, strikes.end());

		//matrix
		Matrix extendedVolatilityMatrix = utilities::buildExtendedVolatilityMatrix(
			expiries,
			strikes,
			hestonEngine,
			spotH,
			dividendTSH,
			oisCurveH);

		/*************
		OUTPUT TO FILE
		**************/

		//volatility surface
		BlackVarianceSurface volSurface = BlackVarianceSurface(
			oisCurve->referenceDate(),
			oisCurve->calendar(),
			expiries,
			strikes,
			extendedVolatilityMatrix,
			oisCurve->dayCounter());

		//kondor
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "WRITING TO FILES ..." << separator << std::endl;
		std::cout << rule << std::endl;

		VolatilityStream vs(volSurface);
		vs.writeKondor("C:\\volatility.txt",
			oisCurveH,
			dividendTSH,
			spotH,
			{ Date::todaysDate() + Period(4, Years) },
			{ 0.90, 0.95, 1.05, 1.10, 1.10 });
		vs.copyToArchive("C:\\volatility.txt", Date::todaysDate());

		DividendStream ds(dividendTSH);
		ds.write("C:\\dividendstrip.txt", expiries);
		ds.writeKondor("C:\\dividend.txt", Date::todaysDate() + Period(4, Years));
		ds.copyToArchive("C:\\dividend.txt", Date::todaysDate());

		ConfigurationStream cs("C:\\configuration.txt");
		cs.update(
			dividendTSH,
			Date::todaysDate() + Period(4, Years),
			"SX5E",
			"SX5EShortName",
			"SX5EVolatilityShortName"
			);

		/***********
		ELAPSED TIME
		************/

		auto end = std::chrono::high_resolution_clock::now();
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "ELAPSED TIME" << separator << std::endl;
		std::cout << separator << std::setw(width)
			<< "Total: " + std::to_string(std::chrono::duration<double, std::ratio<60>>(end - start).count()) + " min"
			<< separator << std::endl;
		std::cout << separator << std::setw(width)
			<< "Bootstrap: " + std::to_string(std::chrono::duration<double, std::ratio<60>>(endBootstrap - startBootstrap).count()) + " min"
			<< separator << std::endl;
		std::cout << separator << std::setw(width)
			<< "Implied Dividends: " + std::to_string(std::chrono::duration<double, std::ratio<60>>(endImpliedDividends - startImpliedDividends).count()) + " min"
			<< separator << std::endl;
		std::cout << rule << std::endl;

		/****************
		PLOT PREPARATIONS
		****************/

		//smiles
		std::vector<Date> maturities = utilities::extractOptionExpiries(optionForCalibration);
		std::map<Date, std::vector<VanillaOptionQuote>> smilesQuotes;
		std::map<Date, std::vector<boost::shared_ptr<VanillaOption>>> smilesOptions;
		std::map<Date, std::vector<Real>> smilesMktStrikes;
		std::map<Date, std::vector<Real>> smilesModelStrikes;
		std::map<Date, std::vector<Real>> smilesMktPrices;
		std::map<Date, std::vector<Real>> smilesModelPrices;
		for (auto maturity : maturities)
		{
			//quotes
			std::vector<VanillaOptionQuote> q;
			for (auto const& quote : optionForCalibration)
			{
				if (quote.expiry() == maturity) { q.push_back(quote); }
			}
			std::sort(q.begin(), q.end(), [](VanillaOptionQuote q1, VanillaOptionQuote q2){
				return q1.strike() < q2.strike();
			});
			smilesQuotes.insert(std::make_pair(maturity, q));

			//options
			std::vector<boost::shared_ptr<VanillaOption>> o;
			for (auto const& option : options)
			{
				if (option->exercise()->lastDate() == maturity) { o.push_back(option); }
			}
			std::sort(o.begin(), o.end(),
				[](boost::shared_ptr<VanillaOption> o1, boost::shared_ptr<VanillaOption> o2){
				return boost::dynamic_pointer_cast<StrikedTypePayoff>(o1->payoff())->strike() <
					boost::dynamic_pointer_cast<StrikedTypePayoff>(o2->payoff())->strike();
			});
			smilesOptions.insert(std::make_pair(maturity, o));

			//containers for plotting
			std::vector<Real> x1;
			std::vector<Real> y1;
			std::transform(q.begin(), q.end(), std::back_inserter(x1),
				[](VanillaOptionQuote q){ return q.strike(); });
			std::transform(q.begin(), q.end(), std::back_inserter(y1),
				[](VanillaOptionQuote q){ return q.value(); });
			std::vector<Real> x2;
			std::vector<Real> y2;
			std::transform(o.begin(), o.end(), std::back_inserter(x2),
				[](boost::shared_ptr<VanillaOption> o){
				return boost::dynamic_pointer_cast<StrikedTypePayoff>(o->payoff())->strike(); });
			std::transform(o.begin(), o.end(), std::back_inserter(y2),
				[](boost::shared_ptr<VanillaOption> o){ return o->NPV(); });

			smilesMktStrikes.insert(std::make_pair(maturity, x1));
			smilesMktPrices.insert(std::make_pair(maturity, y1));
			smilesModelStrikes.insert(std::make_pair(maturity, x2));
			smilesModelPrices.insert(std::make_pair(maturity, y2));
		}

		//ATM term structure
		std::vector<std::string> datesATMTermStructure;
		std::vector<Real> mktPricesATMTermStructure;
		std::vector<Real> modelPricesATMTermStructure;
		for (auto & pair : smilesQuotes)
		{
			//maturities
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << pair.first.dayOfMonth() << "/"
				<< pair.first.month() << "/"
				<< pair.first.year();
			datesATMTermStructure.push_back(ss.str());

			//quotes
			Real fwd = spot->value()*
				dividendTSH->discount(pair.first, true) / oisCurve->discount(pair.first, true);
			std::sort(pair.second.begin(), pair.second.end(),
				[fwd](VanillaOptionQuote q1, VanillaOptionQuote q2){
				return std::abs(fwd - q1.strike()) < std::abs(fwd - q2.strike());
			});
			mktPricesATMTermStructure.push_back(pair.second.front().value());

			//options
			std::vector<boost::shared_ptr<VanillaOption>> o = smilesOptions.at(pair.first);
			for (auto & option : o)
			{
				if (boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff())->strike()
					== pair.second.front().strike())
					modelPricesATMTermStructure.push_back(option->NPV());
			}
		}

		//synthetic fwds
		std::sort(syntheticFwds.begin(), syntheticFwds.end(),
			[](std::shared_ptr<FutureQuote> f1, std::shared_ptr<FutureQuote> f2){
			return f1->expiry() < f2->expiry();
		});
		std::vector<std::string> datesFutures;
		std::vector<Real> mktPricesFutures;
		std::vector<Real> modelFutures;
		for (auto const& quote : syntheticFwds)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << quote->expiry().dayOfMonth() << "/"
				<< quote->expiry().month() << "/"
				<< quote->expiry().year();
			datesFutures.push_back(ss.str());
			mktPricesFutures.push_back(quote->value());
			Real fwd = spot->value()*
				dividendTSH->discount(quote->expiry(), true) / oisCurve->discount(quote->expiry(), true);
			modelFutures.push_back(fwd);
		}

		//implied vs extended surface
		//implied
		std::vector<std::vector<boost::tuple<std::string, Real, Real>>> implied;
		BlackVarianceSurface impliedVolSurface = BlackVarianceSurface(
			oisCurve->referenceDate(),
			oisCurve->calendar(),
			utilities::extractOptionExpiries(optionForCalibration),
			utilities::extractOptionStrikes(optionForCalibration),
			impliedVolatilityMatrix,
			oisCurve->dayCounter());
		for (auto & quote : optionForCalibration)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << quote.expiry().dayOfMonth() << "/"
				<< quote.expiry().month() << "/"
				<< quote.expiry().year();
			implied.push_back({ boost::make_tuple(ss.str(), quote.strike(),
				impliedVolSurface.blackVol(quote.expiry(), quote.strike())) });
		}

		//extended
		std::vector<std::vector<boost::tuple<std::string, Real, Real>>> extended;
		for (auto & expiry : expiries)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << expiry.dayOfMonth() << "/"
				<< expiry.month() << "/"
				<< expiry.year();
			std::vector<boost::tuple<std::string, Real, Real>> smile;
			for (auto & strike : strikes)
			{
				smile.push_back(boost::make_tuple(ss.str(), strike, volSurface.blackVol(expiry, strike)));
			}
			extended.push_back(smile);
		}

		/****
		PLOTS
		*****/

		//terminal and layout
		Gnuplot gp;
		gp << "set terminal wxt 0 size 1200,800 title 'Calibration Results' enhanced font 'Verdana,8'\n";
		int rows = (int)std::ceil(maturities.size() / 2.0) + 1;
		gp << "set multiplot layout " << rows << ",2\n";

		//Synthetic forwards
		gp << "set title 'Synthetic Forwards'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['" << datesFutures.front() << "':'" << datesFutures.back() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		std::vector<std::pair<std::string, Real>> datesMktPricesFutures;
		std::transform(datesFutures.begin(), datesFutures.end(), mktPricesFutures.begin(),
			std::back_inserter(datesMktPricesFutures),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << "plot" << gp.file1d(datesMktPricesFutures) << " using 1:2 with linespoints pt 6 title 'market',";
		std::vector<std::pair<std::string, Real>> datesModelPricesFutures;
		std::transform(datesFutures.begin(), datesFutures.end(), modelFutures.begin(),
			std::back_inserter(datesModelPricesFutures),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << gp.file1d(datesModelPricesFutures) << " using 1:2 with linespoints pt 6 title 'model'\n";

		//ATM Term Structure
		gp << "set title 'ATM Prices'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['" << datesATMTermStructure.front() << "':'" << datesATMTermStructure.back() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		std::vector<std::pair<std::string, Real>> datesMktPricesATM;
		std::transform(datesATMTermStructure.begin(), datesATMTermStructure.end(),
			mktPricesATMTermStructure.begin(), std::back_inserter(datesMktPricesATM),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << "plot" << gp.file1d(datesMktPricesATM) << " using 1:2 with linespoints pt 6 title 'market',";

		std::vector<std::pair<std::string, Real>> datesModelPricesATM;
		std::transform(datesATMTermStructure.begin(), datesATMTermStructure.end(),
			modelPricesATMTermStructure.begin(), std::back_inserter(datesModelPricesATM),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << gp.file1d(datesModelPricesATM) << " using 1:2 with linespoints pt 6 title 'model'\n";

		//smiles
		gp << "set xdata\n";
		gp << "set format\n";
		gp << "set xrange [*:*]\n";
		gp << "set xtics norotate\n";
		for (auto maturity : maturities)
		{
			gp << "set title '" << maturity << "'\n";
			std::vector<std::pair<Real, Real>> strikesMktPrices;
			std::transform(smilesMktStrikes.at(maturity).begin(), smilesMktStrikes.at(maturity).end(),
				smilesMktPrices.at(maturity).begin(),
				std::back_inserter(strikesMktPrices),
				[](Real k, Real p){ return std::make_pair(k, p); });
			gp << "plot" << gp.file1d(strikesMktPrices) << " with linespoints pt 6 title 'market',";

			std::vector<std::pair<Real, Real>> strikesModelPrices;
			std::transform(smilesModelStrikes.at(maturity).begin(), smilesModelStrikes.at(maturity).end(),
				smilesModelPrices.at(maturity).begin(),
				std::back_inserter(strikesModelPrices),
				[](Real k, Real p){ return std::make_pair(k, p); });
			gp << gp.file1d(strikesModelPrices) << " with linespoints pt 6 title 'model'\n";
		}
		gp << "unset multiplot\n";

		//surface (new window)
		gp << "set terminal wxt 1 size 1200,800 title 'Volatility Surface' enhanced font 'Verdana,8'\n";
		gp << "set title 'Impled vs Extended'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		std::stringstream start_stream;
		start_stream << std::setfill('0') << std::setw(2) << expiries.front().dayOfMonth() << "/"
			<< expiries.front().month() << "/"
			<< expiries.front().year();
		std::stringstream end_stream;
		end_stream << std::setfill('0') << std::setw(2) << expiries.back().dayOfMonth() << "/"
			<< expiries.back().month() << "/"
			<< expiries.back().year();
		gp << "set xrange ['" << start_stream.str() << "':'" << end_stream.str() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		gp << "splot" << gp.file2d(extended) << " using 1:2:3 with lines title 'Extended Surface',";
		gp << gp.file2d(implied) << " using 1:2:3 with points title 'Implied Surface'\n";

	}
	catch (std::exception& e) {

		std::cerr << std::endl;
		std::cerr << e.what() << std::endl;
	}
	catch (otl_exception& p){ // intercept OTL exceptions
		std::cerr << p.msg << std::endl; // print out error message
		std::cerr << p.code << std::endl; // print out error code
		std::cerr << p.var_info << std::endl; // print out the variable that caused the error
		std::cerr << p.sqlstate << std::endl; // print out SQLSTATE message
		std::cerr << p.stm_text << std::endl; // print out SQL that caused the error
	}

	// wait for enter key to exit application
	std::cout << "Press ENTER to quit" << std::endl;
	char dummy[2];
	std::cin.getline(dummy, 2);
	return 0;
}