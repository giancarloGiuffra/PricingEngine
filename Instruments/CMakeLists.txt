# create variable marketData_SRC with all cpp and h files in the /src /include directories respectively
file(GLOB instruments_SRC RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" 
	"${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")

#include directory /include so cmake knows where to find the headers
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

#add subdirecty test to the build
add_subdirectory(test)

# define library marketData with dependencies given by the list above and link to the quantlib and the bboost library
add_library(Instruments ${instruments_SRC})
target_link_libraries(Instruments ${Boost_LIBRARIES} optimized quantlib debug quantlibDebug ) 

install(TARGETS Instruments DESTINATION lib EXPORT pricingengine)
