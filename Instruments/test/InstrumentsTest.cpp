#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <Instruments\include\VanillaOptionFuture.h>
#include <ql\exercise.hpp>

namespace Instruments
{
namespace testing
{
    class InstrumentsTest : public Test
    {
    protected:
        InstrumentsTest(){}
        ~InstrumentsTest(){}
		
		virtual void SetUp(){}
        virtual void TearDown(){}

    };

    TEST_F(InstrumentsTest, VanillaOptionFuture_Functionality)
    {
		Option::Type type(Option::Call);
		Real strike = 100;
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(Date::todaysDate()+Period(1,Years)));
		ASSERT_NO_THROW(
			VanillaOptionFuture europeanOption(payoff, europeanExercise, europeanExercise->lastDate() + Period(1, Months)));
		ASSERT_THROW(
			VanillaOptionFuture europeanOption(payoff, europeanExercise, europeanExercise->lastDate() - Period(1, Months)),
			std::exception);
    }

} //namespace testing
} //namespace MarketData