#include "UtilityFunctions.h"

/*operator<< for Exercise::Type*/
std::ostream& operator<<(std::ostream& os, const Exercise::Type type)
{
	static std::map<Exercise::Type, std::string> exercise;
	if (exercise.size() == 0)
	{
#define INSERT_ELEMENT(t) exercise[t] = #t
		INSERT_ELEMENT(Exercise::Type::American);
		INSERT_ELEMENT(Exercise::Type::Bermudan);
		INSERT_ELEMENT(Exercise::Type::European);
#undef INSERT_ELEMENT
	}
	return os << exercise[type];
}