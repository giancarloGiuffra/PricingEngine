#include "DiscreteAveragingMultiplePayoffsAsianOption.h"

/*DiscreteAveragingMultiplePayoffsAsianOption*/

/*constructor*/
DiscreteAveragingMultiplePayoffsAsianOption::DiscreteAveragingMultiplePayoffsAsianOption(
	Average::Type averageType,
	Real runningAccumulator,
	Size pastFixings,
	const std::vector<Date>& fixingDates,
	const std::vector<boost::shared_ptr<StrikedTypePayoff>>& payoffs,
	const boost::shared_ptr<Exercise>& exercise)
	: DiscreteAveragingAsianOption(averageType, runningAccumulator, pastFixings, fixingDates, payoffs[0], exercise),
	payoffs_(payoffs)
{}

/*set up arguments*/
void DiscreteAveragingMultiplePayoffsAsianOption::setupArguments(PricingEngine::arguments* args) const
{
	DiscreteAveragingAsianOption::setupArguments(args);

	DiscreteAveragingMultiplePayoffsAsianOption::arguments* moreArgs =
		dynamic_cast<DiscreteAveragingMultiplePayoffsAsianOption::arguments*>(args);
	QL_REQUIRE(moreArgs != 0, "DiscreteAveragingMultiplePayoffsAsianOption:: wrong argument type!");
	moreArgs->payoffs = payoffs_;
}

/*DiscreteAveragingMultiplePayoffsAsianOption*/

/*constructor*/
DiscreteAveragingMultiplePayoffsAsianOption::arguments::arguments()
	: DiscreteAveragingAsianOption::arguments()
{}