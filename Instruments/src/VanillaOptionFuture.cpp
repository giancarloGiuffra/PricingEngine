#include "VanillaOptionFuture.h"

#include <ql\errors.hpp>
#include <ql\exercise.hpp>

#include <map>
#include <string>

/*vanilla option on future*/

/*constructor*/
VanillaOptionFuture::VanillaOptionFuture(
	const boost::shared_ptr<StrikedTypePayoff>& payoff,
	const boost::shared_ptr<Exercise>& exercise,
	Date futureExpiry)
	: VanillaOption(payoff, exercise), futureExpiry_(futureExpiry)
{
	QL_REQUIRE( futureExpiry >= exercise->lastDate() , "future expiry must be equal or later than last option exercise");
}

/*set up arguments*/
void VanillaOptionFuture::setupArguments(PricingEngine::arguments* args) const
{
	VanillaOptionFuture::arguments* arguments = dynamic_cast<VanillaOptionFuture::arguments*>(args);
	QL_REQUIRE(arguments, "wrong type of arguments!, expected VanillaOptionFuture::arguments");

	//set up VanillaOption arguments
	VanillaOption::setupArguments(args);

	//set up VanillaOptionFuture arguments
	arguments->futureExpiry = this->futureExpiry_;
}

/*future expiry inspector*/
Date VanillaOptionFuture::futureExpiry() const
{
	return this->futureExpiry_;
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const VanillaOptionFuture& option)
{
	os << option.payoff_->description() << " - "
		<< option.exercise_->type() << "(" << option.exercise_->lastDate() << ")" << " - "
		<< option.futureExpiry_;
	return os;
}

/*vanilla option on future arguments*/

/*constructor*/
VanillaOptionFuture::arguments::arguments()
	:VanillaOption::arguments()
{}

/*validate*/
void VanillaOptionFuture::arguments::validate() const
{
	//validate VanillaOption
	VanillaOption::arguments::validate();

	/*no validation for futureExpiry_ in order to allow pricing of VanillaOptions as well*/
}