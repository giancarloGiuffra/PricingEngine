#ifndef VANILLA_OPTION_FUTURE_H
#define VANILLA_OPTION_FUTURE_H

#include "UtilityFunctions.h"

#include <ql\instruments\vanillaoption.hpp>
#include <iostream>

using namespace QuantLib;

/*! \brief Class that represents a Vanilla Option having as underlying a Future contract.

*/
class VanillaOptionFuture : public VanillaOption
{
public:

	/** Arguments class for Pricing Engine.
	*/
	class arguments;

	/** Pricing Engine class.
	*/
	class engine;

	/** Constructor.
	@param payoff
	@param exercise
	@param futureExpiry
	*/
	VanillaOptionFuture(
		const boost::shared_ptr<StrikedTypePayoff>& payoff,
		const boost::shared_ptr<Exercise>& exercise,
		Date futureExpiry);

	//! \name Instrument Interface.
	//@{
	/** Method that passes the arguments to the pricing engine.
	@param args PricingEngine::arguments*
	*/
	void setupArguments(PricingEngine::arguments* args) const;
	//@}

	/** Inspector for the expiry of the Future contract.
	*/
	Date futureExpiry() const;

	/** Overload of operator<<.
	@param os
	@param option
	@return std::ostream
	*/
	friend std::ostream& operator<<(std::ostream& os, const VanillaOptionFuture& option);

private:

	Date futureExpiry_; /**< Expiry of Future contract. */
};

/*! \brief Arguments class for Vanilla Option on Future.

*/
class VanillaOptionFuture::arguments : public VanillaOption::arguments
{
public:

	/** Constructor.
	*/
	arguments();

	//! \name PricingEngine::arguments Interface
	//@{
	/** Method to verify arguments are valid. 
	*/
	void validate() const;
	//@}

	Date futureExpiry; /**< Expiry of Future contract. */
	
};

/*! \brief Pricing Engine class for Vanilla Option on Future.

*/
class VanillaOptionFuture::engine : public GenericEngine < VanillaOptionFuture::arguments, VanillaOptionFuture::results > {};

#endif // !VANILLA_OPTION_FUTURE_H
