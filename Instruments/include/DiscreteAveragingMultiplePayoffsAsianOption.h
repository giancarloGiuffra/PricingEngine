#ifndef DISCRETE_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_OPTION_H
#define DISCRETE_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_OPTION_H

#include <ql\instruments\asianoption.hpp>

#include <vector>

using namespace QuantLib;

/*! \brief Class to facilitate the pricing of multiple discrete averaging asian options in one MC run.

The options share the the fixing dates and the exercise (european). The payoffs can be different, e.g. 
a strip of calls with different strikes.
*/
class DiscreteAveragingMultiplePayoffsAsianOption : public DiscreteAveragingAsianOption
{
public:

	class arguments;
	class engine;

	/** Constructor.
	@param averageType
	@param runningAccumulator
	@param pastFixings
	@param fixingDates
	@param payoffs the first element is used as payoff for the underlying DiscreteAveragingAsianOption
	@param exercise
	*/
	DiscreteAveragingMultiplePayoffsAsianOption(
		Average::Type averageType,
		Real runningAccumulator,
		Size pastFixings,
		const std::vector<Date>& fixingDates,
		const std::vector<boost::shared_ptr<StrikedTypePayoff>>& payoffs,
		const boost::shared_ptr<Exercise>& exercise);

	//! \name Instrument Interface.
	//@{

	/** Method to pass arguments to the pricing engine.
	@param args
	*/
	void setupArguments(PricingEngine::arguments* args) const;

	//@}

protected:

	std::vector<boost::shared_ptr<StrikedTypePayoff>> payoffs_; /**< payoffs to be priced */
};

/** \brief Class arguments for DiscreteAveragingMultiplePayoffsAsianOption.

*/
class DiscreteAveragingMultiplePayoffsAsianOption::arguments :
	public DiscreteAveragingAsianOption::arguments
{
public:

	/** Constructor.
	*/
	arguments();

	std::vector<boost::shared_ptr<StrikedTypePayoff>> payoffs; /**< payoffs to be priced */
};

/*! \brief Pricing Engine class for DiscreteAveragingMultiplePayoffsAsianOption.

*/
class DiscreteAveragingMultiplePayoffsAsianOption::engine :
	public GenericEngine < DiscreteAveragingMultiplePayoffsAsianOption::arguments,
						   DiscreteAveragingMultiplePayoffsAsianOption::results > {};

#endif // !DISCRETE_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_OPTION_H
