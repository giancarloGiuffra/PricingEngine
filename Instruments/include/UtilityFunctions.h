#ifndef INSTRUMENTS_UTILITY_FUNCTIONS_H
#define INSTRUMENTS_UTILITY_FUNCTIONS_H

#include <ql\exercise.hpp>

#include <iostream>
#include <map>

using namespace QuantLib;

/** operator<< for Exercise::Type.
@param os
@param type
@return os
*/
std::ostream& operator<<(std::ostream& os, const Exercise::Type type);

#endif // !INSTRUMENTS_UTILITY_FUNCTIONS_H
