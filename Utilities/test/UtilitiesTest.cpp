#include <Utilities\include\ImpliedVolCalculations.h>
#include <Utilities\include\VolatilityStream.h>
#include <Utilities\include\DateConversion.h>
#include <Utilities\include\StringUtilities.h>
#include <Utilities\include\OptionUtilities.h>
#include <Utilities\include\DividendUtilities.h>
#include <Utilities\include\AmericanDividendsCostFunction.h>

#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\termstructures\volatility\equityfx\blackvariancesurface.hpp>
#include <ql\processes\blackscholesprocess.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\pricingengines\vanilla\analyticeuropeanengine.hpp>
#include <ql\pricingengines\vanilla\baroneadesiwhaleyengine.hpp>
#include <ql\instruments\payoffs.hpp>

#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::ContainerEq;
  using ::testing::Le;
#include <gtest\gtest.h>
  using ::testing::Test;

namespace Utilities
{
namespace testing
{
    class UtilitiesTest : public Test
    {
    protected:
        UtilitiesTest(){}
        ~UtilitiesTest(){}
		
		virtual void SetUp(){

			//quotes
			optionQuotes_.clear();
			expiries_.clear();
			strikes_.clear();
			optionFutureQuotes_.clear();
			optionsSTL_.clear();

			optionQuotes_.push_back(boost::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 80, "2016-04-15", "EUR", 22)));
			optionQuotes_.push_back(boost::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 100, "2016-04-15", "EUR", 5)));
			optionQuotes_.push_back(boost::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 120, "2016-04-15", "EUR", 0.12)));
			optionQuotes_.push_back(boost::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 80, "2017-04-15", "EUR", 23)));
			optionQuotes_.push_back(boost::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 100, "2017-04-15", "EUR", 10)));

			optionsSTL_.push_back(std::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 80, "2016-04-15", "EUR", 22)));
			optionsSTL_.push_back(std::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 100, "2016-04-15", "EUR", 5)));
			optionsSTL_.push_back(std::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 120, "2016-04-15", "EUR", 0.12)));
			optionsSTL_.push_back(std::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 80, "2017-04-15", "EUR", 23)));
			optionsSTL_.push_back(std::shared_ptr<VanillaOptionQuote>(
				new VanillaOptionQuote("underlying", "Call", "European", 100, "2017-04-15", "EUR", 10)));

			optionFutureQuotes_.push_back(VanillaOptionFutureQuote(
				"underlying", "Call", "American", 80, "2016-04-15", "2016-04-15", "EUR", 22));
			optionFutureQuotes_.push_back(VanillaOptionFutureQuote(
				"underlying", "Call", "American", 100, "2016-04-15", "2016-04-15", "EUR", 5));
			optionFutureQuotes_.push_back(VanillaOptionFutureQuote(
				"underlying", "Call", "American", 120, "2016-04-15", "2016-04-15", "EUR", 0.12));
			optionFutureQuotes_.push_back(VanillaOptionFutureQuote(
				"underlying", "Call", "American", 80, "2017-04-15", "2017-04-15", "EUR", 23));
			optionFutureQuotes_.push_back(VanillaOptionFutureQuote(
				"underlying", "Call", "American", 100, "2017-04-15", "2017-04-15", "EUR", 10));

			expiries_.push_back(Date(15, Apr, 2016));
			expiries_.push_back(Date(15, Apr, 2017));

			strikes_.push_back(80);
			strikes_.push_back(100);
			strikes_.push_back(120);

			//spot
			spot_ = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(100)));
			//risk free
			Real riskFreeRate = 0.01;
			riskFreeTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
				new FlatForward(Date(1, Jan, 2016), riskFreeRate, Actual365Fixed())));
			//dividends
			Real dividend = 0.005;
			dividenTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
				new FlatForward(Date(1, Jan, 2016), dividend, Actual365Fixed())));
		}

        virtual void TearDown(){}

		std::vector<boost::shared_ptr<VanillaOptionQuote>> optionQuotes_;
		std::vector<std::shared_ptr<VanillaOptionQuote>> optionsSTL_;
		std::vector<VanillaOptionFutureQuote> optionFutureQuotes_;
		std::vector<Date> expiries_;
		std::vector<Real> strikes_;
		Handle<Quote> spot_;
		Handle<YieldTermStructure> dividenTS_;
		Handle<YieldTermStructure> riskFreeTS_;
    };

    TEST_F(UtilitiesTest, OptionUtilities_extractOptionExpiries)
    {
		EXPECT_THAT(utilities::extractOptionExpiries(optionQuotes_), ContainerEq(expiries_));
    }

	TEST_F(UtilitiesTest, OptionUtilities_extractOptionExpiriesSTL)
	{
		EXPECT_THAT(utilities::extractOptionExpiries(optionsSTL_), ContainerEq(expiries_));
	}

	TEST_F(UtilitiesTest, OptionUtilities_extractOptionStrikes)
	{
		EXPECT_THAT(utilities::extractOptionStrikes(optionQuotes_), ContainerEq(strikes_));
	}

	TEST_F(UtilitiesTest, OptionUtilities_extractOptionStrikesSTL)
	{
		EXPECT_THAT(utilities::extractOptionStrikes(optionsSTL_), ContainerEq(strikes_));
	}

	TEST_F(UtilitiesTest, OptionUtilities_groupOptionsByExpiry)
	{
		std::shared_ptr<VanillaOptionQuote> o1(
			new VanillaOptionQuote("underlying", "Call", "European", 80, "2017-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> o2(
			new VanillaOptionQuote("underlying", "Call", "European", 120, "2016-04-15", "EUR", 0.12));

		std::vector<std::shared_ptr<VanillaOptionQuote>> options;
		options.push_back(o1);
		options.push_back(o2);

		auto grouped = utilities::groupOptionsByExpiry(options);

		//tests
		ASSERT_THAT(grouped.size(), Eq(2));
		ASSERT_THAT(grouped[0].size(), Eq(1));
		ASSERT_THAT(grouped[1].size(), Eq(1));

		EXPECT_THAT(grouped[0][0], Eq(o2));
		EXPECT_THAT(grouped[1][0], Eq(o1));
		
	}

	TEST_F(UtilitiesTest, OptionUtilities_createPutCallParityPairsSameExpiry)
	{
		std::shared_ptr<VanillaOptionQuote> c1(
			new VanillaOptionQuote("underlying", "Call", "European", 80, "2016-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> c2(
			new VanillaOptionQuote("underlying", "Call", "European", 120, "2016-04-15", "EUR", 0.12));
		std::shared_ptr<VanillaOptionQuote> p1(
			new VanillaOptionQuote("underlying", "Put", "European", 80, "2016-04-15", "EUR", 23));

		std::vector<std::shared_ptr<VanillaOptionQuote>> options;
		options.push_back(c1);
		options.push_back(c2);
		options.push_back(p1);

		auto pairs = utilities::createPutCallParityPairsSameExpiry(options);

		//tests
		ASSERT_THAT(pairs.size(), Eq(1));
		EXPECT_THAT(pairs[0].first, Eq(*c1));
		EXPECT_THAT(pairs[0].second, Eq(*p1));
	}

	TEST_F(UtilitiesTest, OptionUtilities_createPutCallParityPairs)
	{
		std::shared_ptr<VanillaOptionQuote> c1(
			new VanillaOptionQuote("underlying", "Call", "European", 80, "2017-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> p1(
			new VanillaOptionQuote("underlying", "Put", "European", 80, "2017-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> c2(
			new VanillaOptionQuote("underlying", "Call", "European", 80, "2016-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> p2(
			new VanillaOptionQuote("underlying", "Put", "European", 80, "2016-04-15", "EUR", 23));
		std::shared_ptr<VanillaOptionQuote> c3(
			new VanillaOptionQuote("underlying", "Call", "European", 80, "2018-04-15", "EUR", 23));

		std::vector<std::shared_ptr<VanillaOptionQuote>> options;
		options.push_back(c1);
		options.push_back(c2);
		options.push_back(c3);
		options.push_back(p1);
		options.push_back(p2);

		auto pairs = utilities::createPutCallParityPairs(options);

		//tests
		ASSERT_THAT(pairs.size(), Eq(2));
		ASSERT_THAT(pairs[0].size(), Eq(1));
		ASSERT_THAT(pairs[1].size(), Eq(1));

		EXPECT_THAT(pairs[0][0], Eq(std::make_pair(*c2, *p2)));
		EXPECT_THAT(pairs[1][0], Eq(std::make_pair(*c1, *p1)));
	}

	TEST_F(UtilitiesTest, ImpliedVolCalculations_buildImpliedVolatilityMatrix)
	{
		std::vector<VanillaOptionQuote> quotes;
		for (auto q : optionQuotes_)
			quotes.push_back(*q);
		Matrix volMatrix = utilities::buildImpliedVolatilityMatrix(
			quotes,
			spot_,
			dividenTS_,
			riskFreeTS_);
		BlackVarianceSurface volSurface = BlackVarianceSurface(
			riskFreeTS_->referenceDate(),
			riskFreeTS_->calendar(),
			utilities::extractOptionExpiries(optionQuotes_),
			utilities::extractOptionStrikes(optionQuotes_),
			volMatrix,
			riskFreeTS_->dayCounter());
		for (auto q : quotes)
		{
			Handle<BlackVolTermStructure> constVol(boost::shared_ptr<BlackVolTermStructure>(
				new BlackConstantVol(
				riskFreeTS_->referenceDate(),
				riskFreeTS_->calendar(),
				volSurface.blackVol(q.expiry(), q.strike()),
				riskFreeTS_->dayCounter())));
			boost::shared_ptr<GeneralizedBlackScholesProcess> bsProcess =
				boost::shared_ptr<GeneralizedBlackScholesProcess>(
				new BlackScholesMertonProcess(
				spot_, dividenTS_, riskFreeTS_, constVol));
			boost::shared_ptr<VanillaOption> option = q.option();
			option->setPricingEngine(boost::shared_ptr<PricingEngine>(
				new AnalyticEuropeanEngine(bsProcess)));
			Real tolerance = 1.0e-5; //accuracy set by default in function
			EXPECT_THAT(std::abs(option->NPV() - q.value()), Le(tolerance));
		}
	}

	TEST_F(UtilitiesTest, ImpliedVolCalculations_buildImpliedVolatilityMatrixOptionsOnFutures)
	{
		Matrix volMatrix = utilities::buildImpliedVolatilityMatrix(
			optionFutureQuotes_,
			spot_,
			dividenTS_,
			riskFreeTS_);
		BlackVarianceSurface volSurface = BlackVarianceSurface(
			riskFreeTS_->referenceDate(),
			riskFreeTS_->calendar(),
			utilities::extractOptionExpiries(optionFutureQuotes_),
			utilities::extractOptionStrikes(optionFutureQuotes_),
			volMatrix,
			riskFreeTS_->dayCounter());
		for (auto q : optionFutureQuotes_)
		{
			Handle<BlackVolTermStructure> constVol(boost::shared_ptr<BlackVolTermStructure>(
				new BlackConstantVol(
				riskFreeTS_->referenceDate(),
				riskFreeTS_->calendar(),
				volSurface.blackVol(q.expiry(), q.strike()),
				riskFreeTS_->dayCounter())));
			boost::shared_ptr<GeneralizedBlackScholesProcess> bsProcess =
				boost::shared_ptr<GeneralizedBlackScholesProcess>(
				new BlackScholesMertonProcess(
				spot_, dividenTS_, riskFreeTS_, constVol));
			VanillaOption option = VanillaOption(
				boost::dynamic_pointer_cast	<StrikedTypePayoff>(q.optionFuture()->payoff()), 
				q.optionFuture()->exercise());
			option.setPricingEngine(boost::shared_ptr<PricingEngine>(
				new BaroneAdesiWhaleyApproximationEngine(bsProcess)));
			Real tolerance = 1.0e-1; //accuracy reduced by the FD methods used for American options
			EXPECT_THAT(std::abs(option.NPV() - q.value()), Le(tolerance));
		}
	}

	TEST_F(UtilitiesTest, ImpliedVolCalculations_buildExtendedVolatilityMatrix)
	{	
		//process
		Real constant = 0.20;
		Handle<BlackVolTermStructure> constVol(boost::shared_ptr<BlackVolTermStructure>(
			new BlackConstantVol(
			riskFreeTS_->referenceDate(),
			riskFreeTS_->calendar(),
			constant,
			riskFreeTS_->dayCounter())));
		boost::shared_ptr<GeneralizedBlackScholesProcess> bsProcess =
			boost::shared_ptr<GeneralizedBlackScholesProcess>(
			new BlackScholesMertonProcess(
			spot_, dividenTS_, riskFreeTS_, constVol));
		//engine
		boost::shared_ptr<PricingEngine> engine(
			new AnalyticEuropeanEngine(bsProcess));

		//calculate matrix
		Matrix volMatrix = utilities::buildExtendedVolatilityMatrix(
			expiries_,
			strikes_,
			engine,
			spot_,
			dividenTS_,
			riskFreeTS_
			);

		//test
		Real tolerance = 1.0e-6;
		for (auto i = volMatrix.begin(); i < volMatrix.end(); i++)
			EXPECT_THAT(std::abs(*i - constant), Le(tolerance));
	}

	TEST_F(UtilitiesTest, KondorExpiries_Functionality)
	{
		std::vector<KondorExpiry> kExpiries = {
			KondorExpiry::SD7,
			KondorExpiry::SM1,
			KondorExpiry::SM2,
			KondorExpiry::SM3,
			KondorExpiry::SM6,
			KondorExpiry::SM9,
			KondorExpiry::SM12,
			KondorExpiry::SM15,
			KondorExpiry::SM18,
			KondorExpiry::SM21,
			KondorExpiry::SM24,
			KondorExpiry::SM36,
			KondorExpiry::SM60,
			KondorExpiry::SM120
		};

		EXPECT_THAT(KondorExpiry::kondorExpiries(), ContainerEq(kExpiries));
	}

	TEST_F(UtilitiesTest, DateConversion_dateConversion)
	{
		std::string out = utilities::dateConversion("2016-04-15", "%Y-%m-%d", "%d/%m/%Y");
		EXPECT_THAT(out, Eq("15/04/2016"));
	}

	TEST_F(UtilitiesTest, DateConversion_dateStringToTM)
	{
		std::tm tm = utilities::dateStringToTM("2016-04-15", "%Y-%m-%d");
		EXPECT_THAT(tm.tm_year, Eq(116));
		EXPECT_THAT(tm.tm_mon, Eq(3));
		EXPECT_THAT(tm.tm_mday, Eq(15));
	}

	TEST_F(UtilitiesTest, DateConversion_dateTMToString)
	{
		std::tm tm = {};
		tm.tm_year = 116;
		tm.tm_mon = 3;
		tm.tm_mday = 15;
		EXPECT_THAT(utilities::dateTMToString(tm,"%Y-%m-%d"), Eq("2016-04-15"));
	}

	TEST_F(UtilitiesTest, StringUtilities_split)
	{
		std::vector<std::string> tokens = utilities::split("Exercise::Type::European", ':');
		std::vector<std::string> expected = { "Exercise", "", "Type", "", "European" };
		EXPECT_THAT(expected, ContainerEq(tokens));
	}

	TEST_F(UtilitiesTest, DividendUtilities_stripDividendFromOptions)
	{
		//options - imply future equal to strike
		std::shared_ptr<VanillaOptionQuote> c1(
			new VanillaOptionQuote("underlying", "Call", "European", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> p1(
			new VanillaOptionQuote("underlying", "Put", "European", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> c2(
			new VanillaOptionQuote("underlying", "Call", "European", 100, "2016-04-15", "EUR", 10));
		std::shared_ptr<VanillaOptionQuote> p2(
			new VanillaOptionQuote("underlying", "Put", "European", 100, "2016-04-15", "EUR", 10));

		std::vector<std::shared_ptr<VanillaOptionQuote>> options;
		options.push_back(c1);
		options.push_back(c2);
		options.push_back(p1);
		options.push_back(p2);

		//spot
		auto spot = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(100)));
		//risk free TS
		auto riskFreeTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(1, Jan, 2016), 0.0, Actual365Fixed())));

		//calculated dividend TS: with the data given it should be BackwardFlat with 2 points both zero
		auto dividendTS = utilities::stripDividendsFromOptions(spot, riskFreeTS, options);

		//tests
		EXPECT_THAT(dividendTS->discount(c1->expiry()), Eq(1));
		EXPECT_THAT(dividendTS->discount(c2->expiry()), Eq(1));

		auto riskFreeTSOther = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(1, Jan, 2016), 0.02, Actual365Fixed())));
		auto dividendTSOther = utilities::stripDividendsFromOptions(spot, riskFreeTSOther, options);
		EXPECT_THAT(dividendTSOther->discount(c1->expiry()), Eq(riskFreeTSOther->discount(c1->expiry())));
		EXPECT_THAT(dividendTSOther->discount(c2->expiry()), Eq(riskFreeTSOther->discount(c2->expiry())));
		//using tolerance because there are calculations involved
		Real tolerance = 1.0e-12;
		EXPECT_THAT(std::abs(dividendTSOther->discount(Date(15, April, 2020),true) -
			riskFreeTSOther->discount(Date(15, April, 2020), true)), Le(tolerance));
	}

	TEST_F(UtilitiesTest, AmericanDividendsCostFunction_Constructor)
	{
		//options european
		std::shared_ptr<VanillaOptionQuote> ce(
			new VanillaOptionQuote("underlying", "Call", "European", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> pe(
			new VanillaOptionQuote("underlying", "Put", "European", 100, "2017-04-15", "EUR", 20));

		std::vector<std::shared_ptr<VanillaOptionQuote>> europeans;
		europeans.push_back(ce);
		europeans.push_back(pe);

		//options american
		std::shared_ptr<VanillaOptionQuote> ca(
			new VanillaOptionQuote("underlying", "Call", "american", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> pa(
			new VanillaOptionQuote("underlying", "Put", "american", 130, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> paPaired(
			new VanillaOptionQuote("underlying", "Put", "american", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> paAlone(
			new VanillaOptionQuote("underlying", "Put", "american", 100, "2018-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> caExtra(
			new VanillaOptionQuote("underlying", "Call", "american", 100, "2016-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> paExtra(
			new VanillaOptionQuote("underlying", "Put", "american", 100, "2016-04-15", "EUR", 20));

		std::vector<std::shared_ptr<VanillaOptionQuote>> americans;
		americans.push_back(ca);
		americans.push_back(pa);
		americans.push_back(caExtra);
		americans.push_back(paExtra);
		americans.push_back(paPaired);
		americans.push_back(paAlone);

		//spot
		auto spot = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(100)));
		//risk free TS
		auto riskFreeTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(1, Jan, 2016), 0.01, Actual365Fixed())));

		//tests
		EXPECT_THROW(AmericanDividendsCostFunction cf(spot, riskFreeTS, europeans), std::exception);
		EXPECT_NO_THROW(AmericanDividendsCostFunction cf(spot, riskFreeTS, americans));
		AmericanDividendsCostFunction cf(spot, riskFreeTS, americans);
		std::vector<std::shared_ptr<VanillaOptionQuote>> filteredOptions;
		filteredOptions.push_back(caExtra);
		filteredOptions.push_back(paExtra);
		filteredOptions.push_back(ca);
		filteredOptions.push_back(paPaired);
		auto cfOptions = cf.options();
		for (std::size_t i = 0; i < cfOptions.size(); i++)
			EXPECT_THAT(*cfOptions[i], Eq(*filteredOptions[i])); //order is important
		std::vector<Date> expiries = { Date(15, April, 2016), Date(15, April, 2017) };
		EXPECT_THAT(cf.expiries(), ContainerEq(expiries));
	}

	TEST_F(UtilitiesTest, AmericanDividendsCostFunction_values)
	{
		//options - imply future equal to strike
		std::shared_ptr<VanillaOptionQuote> c1(
			new VanillaOptionQuote("underlying", "Call", "american", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> p1(
			new VanillaOptionQuote("underlying", "Put", "american", 100, "2017-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> c2(
			new VanillaOptionQuote("underlying", "Call", "american", 100, "2016-04-15", "EUR", 20));
		std::shared_ptr<VanillaOptionQuote> p2(
			new VanillaOptionQuote("underlying", "Put", "american", 100, "2016-04-15", "EUR", 20));

		std::vector<std::shared_ptr<VanillaOptionQuote>> options;
		options.push_back(c1);
		options.push_back(p1);
		options.push_back(c2);
		options.push_back(p2);

		//spot
		auto spot = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(100)));
		//risk free TS
		auto riskFreeTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(1,Jan,2016), 0.01, Actual365Fixed())));

		//cost function
		AmericanDividendsCostFunction cf(spot, riskFreeTS, options);

		//tests
		Array x(2);
		x[0] = 0.03;
		x[1] = 0.03;
		EXPECT_NO_THROW(Array result = cf.values(x));
	}

	TEST_F(UtilitiesTest, DividendUtilities_stripDividendsFromAmericanOptions)
	{
		//options ENI (ENI IM) date 4-Mar-2016
		auto options = {
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13, "2016-04-15", "EUR", 0.852)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13.5, "2016-04-15", "EUR", 0.558)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 14, "2016-04-15", "EUR", 0.3305)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13, "2016-04-15", "EUR", 0.36)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13.5, "2016-04-15", "EUR", 0.5485)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 14, "2016-04-15", "EUR", 0.8175)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13, "2016-06-16", "EUR", 1.081)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13.5, "2016-06-16", "EUR", 0.7995)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 14, "2016-06-16", "EUR", 0.567)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13, "2016-06-16", "EUR", 0.8055)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13.5, "2016-06-16", "EUR", 1.05)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 14, "2016-06-16", "EUR", 1.3405))
		};

		//spot
		auto spot = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(13.53)));
		//risk free TS
		auto riskFreeTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(4, Mar, 2016), -0.00135, Actual365Fixed())));

		//test
		EXPECT_NO_THROW(utilities::stripDividendsFromAmericanOptions(spot, riskFreeTS, options, 0.01));

	}

	TEST_F(UtilitiesTest, OptionUtilities_calculateEuropeanFromAmericanOptionPrices)
	{
		//options ENI (ENI IM) date 4-Mar-2016
		std::vector<std::shared_ptr<VanillaOptionQuote>> options = {
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13.5, "2016-04-15", "EUR", 0.558)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13.5, "2016-04-15", "EUR", 0.5485)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "C", "american", 13.5, "2016-06-16", "EUR", 0.7995)),
			std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote("ENI", "P", "american", 13.5, "2016-06-16", "EUR", 1.05)),
		};

		//spot
		auto spot = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(13.53)));
		//risk free TS
		auto riskFreeTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date(4, Mar, 2016), -0.00135, Actual365Fixed())));

		//calculate europeans
		auto result = utilities::stripDividendsFromAmericanOptions(spot, riskFreeTS, options, 0.01);
		auto dividendTs = Handle<YieldTermStructure>(std::get<0>(result));
		std::vector<std::shared_ptr<VanillaOptionQuote>> europeans =
			utilities::calculateEuropeanFromAmericanOptionPrices(spot, riskFreeTS, dividendTs, options);

		//print
		std::vector<Date> expiries = { Date(15, Apr, 2016), Date(16, Jun, 2016) };
		for (auto const & d : expiries)
			std::cout << d << ": " << dividendTs->zeroRate(d, dividendTs->dayCounter(), Continuous).rate()
			<< std::endl;

		//test
		for (std::size_t i = 0; i < options.size(); i++)
		{
			EXPECT_THAT(europeans[i]->value(), Le(options[i]->value()));
		}
	}

} //namespace testing
} //namespace MarketData