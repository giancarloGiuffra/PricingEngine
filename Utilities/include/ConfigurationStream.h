#ifndef CONFIGURATION_STREAM_H
#define CONFIGURATION_STREAM_H

#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\handle.hpp>

#include <string>

using namespace QuantLib;

/*! \brief Class to facilitate the updating of the configuration file for kondor.

*/
class ConfigurationStream
{
public:

	/** Constructor.
	@param file
	*/
	ConfigurationStream(
		std::string file = "\\\\192.168.83.69\\mb-import-certificates\\configuration.txt");

	/** Updates the configuration file.
	@param dividendTS
	@param date
	@param underlying
	@param kShortName
	@param kVolatilityShortName
	@param spotFile
	@throws if file can not opened/created
	*/
	void update(
		const Handle<YieldTermStructure>& dividendTS,
		Date date,
		std::string underlying,
		std::string kShortName,
		std::string kVolatilityShortName,
		std::string spotFile = ""
		) const;

private:

	std::string file_; /**< configuration file */

};

#endif // !CONFIGURATION_STREAM_H
