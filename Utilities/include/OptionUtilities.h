#ifndef OPTION_UTILITIES_H
#define OPTION_UTILITIES_H

#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarKetQuote\include\VanillaOptionFutureQuote.h>

#include <ql\time\date.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\handle.hpp>

#include <vector>
#include <memory>
#include <utility>

using namespace QuantLib;

/*! \brief It represents calls and puts paired by strike and expiry, furthermore pairs are grouped by expiry.

*/
typedef std::vector<std::vector<std::pair<VanillaOptionQuote, VanillaOptionQuote>>> PutCallParityOptions;

namespace utilities
{
	/** It extracts the expiries from the option quotes.
	The returned vector contains sorted unique values.
	@param optionQuotes
	@return std::vector<Date>
	*/
	std::vector<Date> extractOptionExpiries(
		std::vector<boost::shared_ptr<VanillaOptionQuote>> optionQuotes);

	/** It extracts the expiries from the option quotes.
	The returned vector contains sorted unique values.
	@param optionQuotes
	@return std::vector<Date>
	*/
	std::vector<Date> extractOptionExpiries(
		std::vector<std::shared_ptr<VanillaOptionQuote>> optionQuotes);

	/** It extracts the expiries from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<Date>
	*/
	std::vector<Date> extractOptionExpiries(
		std::vector<VanillaOptionQuote> optionQuotes);

	/** It extracts the expiries from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<Date>
	*/
	std::vector<Date> extractOptionExpiries(
		std::vector<VanillaOptionFutureQuote> optionQuotes);

	/** It extracts the strikes from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<double>
	*/
	std::vector<Real> extractOptionStrikes(
		std::vector<boost::shared_ptr<VanillaOptionQuote>> optionQuotes);

	/** It extracts the strikes from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<double>
	*/
	std::vector<Real> extractOptionStrikes(
		std::vector<std::shared_ptr<VanillaOptionQuote>> optionQuotes);

	/** It extracts the strikes from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<double>
	*/
	std::vector<Real> extractOptionStrikes(
		std::vector<VanillaOptionQuote> optionQuotes);

	/** It extracts the strikes from the option quotes.
	The returned vector contains unique values.
	@param optionQuotes
	@return std::vector<double>
	*/
	std::vector<Real> extractOptionStrikes(
		std::vector<VanillaOptionFutureQuote> optionQuotes);

	/** It pairs calls and puts with the same expiry and strike, furthermore pairs are grouped by expiry.
	Outer vector is ordered by expiry and inner vector by strike.
	@param options
	@return PutCallParityOptions
	*/
	PutCallParityOptions createPutCallParityPairs(
		const std::vector<std::shared_ptr<VanillaOptionQuote>>& options);

	/** It groups options by expiry.
	Outer vector is ordered by expiry.
	@param options
	@return std::vector<std::vector<std::shared_ptr<VanillaOptionQuote>>>
	*/
	std::vector<std::vector<std::shared_ptr<VanillaOptionQuote>>>
		groupOptionsByExpiry(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options);

	/** It pairs calls and puts with the same expiry and strike.
	It discards unpaired options.
	@param options
	@return std::vector<std::pair<VanillaOptionQuote, VanillaOptionQuote>>
	@throws std::exception if options don't have the same expiry
	*/
	std::vector<std::pair<VanillaOptionQuote, VanillaOptionQuote>>
		createPutCallParityPairsSameExpiry(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options);

	/** It extracts the options with the given option type.
	@param options
	@param type
	@return std::vector<std::shared_ptr<VanillaOptionQuote>>
	*/
	std::vector<std::shared_ptr<VanillaOptionQuote>>
		extractOptionByType(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options, Option::Type type);

	/** It extracts european option prices from american ones.
	There may be less option quotes in the output wrt the input because american option prices that quote less
	than their intrinsic value are filtered out.
	@param spot
	@param riskFreeRate
	@param dividendTS
	@param options
	@return std::vector<std::shared_ptr<VanillaOptionQuote>>
	@see utilities::stripDividendsFromAmericanOptions
	*/
	std::vector<std::shared_ptr<VanillaOptionQuote>> calculateEuropeanFromAmericanOptionPrices(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		Handle<YieldTermStructure> dividendTS,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options
		);
}

#endif // !OPTION_UTILITIES_H
