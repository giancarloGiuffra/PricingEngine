#ifndef DATE_CONVERSION_H
#define DATE_CONVERSION_H

#include <string>
#include <ctime>

namespace utilities
{
	/** It converts a date in one format to another one.
	@param date
	@param formatIn
	@param formatOut
	@return std::string
	@see std::get_time and std::put_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	std::string dateConversion(std::string date, std::string formatIn, std::string formatOut);

	/** It returns the corresponding std::tm.
	@param date
	@param format
	@return std::tm
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	std::tm dateStringToTM(std::string date, std::string format);

	/** It returns the corresponding string according to the specified format.
	@param date
	@param format
	@return std::string
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	std::string dateTMToString(std::tm date, std::string format);
}

#endif // !DATE_CONVERSION_H
