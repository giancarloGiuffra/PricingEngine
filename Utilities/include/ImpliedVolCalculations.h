#ifndef IMPLIED_VOL_CALCULATIONS_H
#define IMPLIED_VOL_CALCULATIONS_H

#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarKetQuote\include\VanillaOptionFutureQuote.h>

#include <ql\instruments\vanillaoption.hpp>
#include <ql\time\date.hpp>
#include <ql\math\matrix.hpp>
#include <boost\shared_ptr.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\handle.hpp>

#include <vector>
#include <tuple>

using namespace QuantLib;

/*! \brief Namespace that contains global utility functions.

*/
namespace utilities
{

	/** It returns implied volatilites for each quote.
	The Black Scholes Merton process is used.
	@param quotes,
	@param spot
	@param dividendTS
	@param riskFreeTS
	@return std::tuple<std::vector<Volatility>,std::vector<VanillaOptionQuote>>
	*/
	std::tuple<std::vector<Volatility>,std::vector<VanillaOptionQuote>> calculateImpliedVolatilities(
		std::vector<VanillaOptionQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS);

	/** It creates the matrix of Implied Volatilities from quotes.
	Points for which a quote is not provided have as default a zero value.
	If for a strike and expiry there is more than one quote the implied volatility corresponding to the last one
	is used.
	The Black Scholes Merton process is used.
	@param quotes,
	@param spot
	@param dividendTS
	@param riskFreeTS
	@return Matrix
	*/
	Matrix buildImpliedVolatilityMatrix(
		std::vector<VanillaOptionQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS);

	/** It creates the matrix of Implied Volatilities from quotes.
	Points for which a quote is not provided have as default a zero value.
	The Black process is used.
	@param quotes,
	@param spot
	@param dividendTS
	@param riskFreeTS
	@return Matrix
	*/
	Matrix buildImpliedVolatilityMatrix(
		std::vector<VanillaOptionFutureQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS);

	/** It creates a volatility matrix (BSM model) corresponding to european call prices calculated with engine.
	It is left to the user to ensure that the pricing engine is using the correct spot, dividendTS and
	riskFreeTS.
	@param expiries
	@param strikea
	@param engine
	@param spot
	@param dividendTS
	@param riskFreeTS
	@return Matrix
	*/
	Matrix buildExtendedVolatilityMatrix(
		std::vector<Date> expiries,
		std::vector<Real> strikes,
		boost::shared_ptr<PricingEngine> engine,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS);
}

#endif // !IMPLIED_VOL_CALCULATIONS_H
