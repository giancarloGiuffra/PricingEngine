#ifndef STRING_UTILITIES_H
#define STRING_UTILITIES_H

#include <string>
#include <vector>

namespace utilities
{
	/** It splits a std::string around the specified delimiter and returns the tokens.
	@param s
	@param delimiter
	@return std::vector<std::string>
	*/
	std::vector<std::string> split(const std::string& s, char delimiter);
}

#endif // !STRING_UTILITIES_H
