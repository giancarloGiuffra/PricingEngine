#ifndef DIVIDEND_STREAM_H
#define DIVIDEND_STREAM_H

#include <ql\termstructures\yieldtermstructure.hpp>

using namespace QuantLib;

/*! \brief Class to facilitate writing the dividend term structure to files.

*/
class DividendStream
{
public:

	/** Constructor.
	@param dividendTS
	*/
	DividendStream(const Handle<YieldTermStructure> dividendTS);

	/** Default method to write to file.
	@param file
	@param dates
	*/
	void write(std::string file, std::vector<Date> dates) const;

	/** Method to write to file using format for kondor+ feed.
	@param file
	@param date
	*/
	void writeKondor(std::string file, Date date) const;

	/** Static method to generate a copy of the file.
	The new file will have the name formatted according to the archive specifications.
	@param file
	@param referenceDate
	@throws if file does not exist
	*/
	static void copyToArchive(std::string file, Date referenceDate);

private:

	Handle<YieldTermStructure> dividendTS_; /**< dividend term structure */
};

#endif // !DIVIDEND_STREAM_H
