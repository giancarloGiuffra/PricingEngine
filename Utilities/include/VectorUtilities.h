#ifndef VECTOR_UTILITIES_H
#define VECTOR_UTILITIES_H

#include <vector>
#include <algorithm>
#include <numeric>

namespace utilities
{
	/** It extracts the permutation associated to the sorting of vec according to compare.
	It sorts vec as well.
	@param vec
	@param compare
	@return std::vector<std::size_t>
	*/
	template <typename T, typename Compare>
	std::vector<std::size_t> sort_permutation(std::vector<T>& vec, Compare compare)
	{
		std::vector<std::size_t> permutation(vec.size());
		std::iota(permutation.begin(), permutation.end(), 0); //sequence
		std::sort(permutation.begin(), permutation.end(),
			[&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
		std::sort(vec.begin(), vec.end()); //sort vec as well
		return permutation;
	}

	/** It applies the permutation p to vector vec.
	@param vec
	@param p
	*/
	template <typename T>
	void apply_permutation(std::vector<T>& vec, const std::vector<std::size_t>& p)
	{
		std::vector<T> sorted(p.size());
		std::transform(p.begin(), p.end(), sorted.begin(),
			[&](std::size_t i){ return vec[i]; });
		vec = sorted;
	}
}

#endif // !VECTOR_UTILITIES_H
