#ifndef AMERICAN_DIVIDENDS_COST_FUNCTION_H
#define AMERICAN_DIVIDENDS_COST_FUNCTION_H

#include <MarKetQuote\include\VanillaOptionQuote.h>

#include <ql\math\optimization\costfunction.hpp>
#include <ql\handle.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\time\date.hpp>

#include <vector>
#include <memory>

/*! \brief Cost Function class for the problem of stripping dividends from American Option prices.

Options with a price less than its intrinsic value are discarded.
*/
class AmericanDividendsCostFunction : public CostFunction
{
public:

	/** Constructor.
	Options with a price less than its intrinsic value are discarded.
	Expiry is considered if it has at least minimumPairsPerExpiry pairs.
	@param spot
	@param riskFreeRate
	@param options
	@param minimumPairsPerExpiry
	@throws std::exception if not all options are american
	*/
	AmericanDividendsCostFunction(
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& riskFreeRate,
		const std::vector<std::shared_ptr<VanillaOptionQuote>>& options,
		Natural minimumPairsPerExpiry = 1
		);

	/** Inspector for options.
	@return options actually used in the cost function (after filtering the ones with price < intrinsic value)
	*/
	const std::vector<std::shared_ptr<VanillaOptionQuote>>& options() const;

	/** Inspector for expiries.
	@return expiries actually used in the cost function (<= option expiries because not necessarily all paired)
	*/
	const std::vector<Date>& expiries() const;

	//! \name CostFunction interface
	//@{
	
	Real value(const Array& x) const;

	Disposable<Array> values(const Array& x) const;

	//}@

private:

	Handle<Quote> spot_; /**< spot */
	Handle <YieldTermStructure> riskFreeRate_; /**< risk free discounts */
	std::vector<std::shared_ptr<VanillaOptionQuote>> options_; /**< american options */
	std::vector<Date> expiries_; /**< expiries for the forwards (<= expiries of options) */
	Natural minimumPairsPerExpiry_; /**< expiry is considered if there's at least this number of pairs */
};

#endif // !AMERICAN_DIVIDENDS_COST_FUNCTION_H
