#ifndef VOLATILITY_STREAM_H
#define VOLATILITY_STREAM_H

#include <ql\termstructures\volatility\equityfx\blackvariancesurface.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>

#include <vector>
#include <string>
#include <memory>

using namespace QuantLib;

/*! \brief Class to emulate enum for Kondor expiries.

*/
class KondorExpiry
{
public:

	static const KondorExpiry SD7;
	static const KondorExpiry SM1;
	static const KondorExpiry SM2;
	static const KondorExpiry SM3;
	static const KondorExpiry SM6;
	static const KondorExpiry SM9;
	static const KondorExpiry SM12;
	static const KondorExpiry SM15;
	static const KondorExpiry SM18;
	static const KondorExpiry SM21;
	static const KondorExpiry SM24;
	static const KondorExpiry SM36;
	static const KondorExpiry SM60;
	static const KondorExpiry SM120;

	/** It returns the expiry as a string.
	@return std::string
	*/
	std::string to_string() const;

	/** It returns the expiry as a date.
	@param referenceDate
	@return Date
	*/
	Date to_date(Date referenceDate) const;

	/** Returns all the kondor expiries.
	@return std::vector<KondorExpiry>
	*/
	static std::vector<KondorExpiry> kondorExpiries();

private:

	/** Private Constructor.
	@param expiry 
	*/
	KondorExpiry(std::string expiry, Period period);

	std::string expiry_; /**< expiry */
	Period period_; /**< period corresponding to expiry*/
	static std::vector<KondorExpiry> kondorExpiries_; /**< contains all kondor expiries*/

	/** operator==.
	@param lhs
	@param rhs
	*/
	friend bool operator==(const KondorExpiry& lhs, const KondorExpiry& rhs);

	/** operator!=.
	@param lhs
	@param rhs
	*/
	friend bool operator!=(const KondorExpiry& lhs, const KondorExpiry& rhs);
};

/*---------------------------------------------------------------------------------------------------------*/

/*! \brief Class to facilitate writing the surface volatility to files.

*/
class VolatilityStream
{
public:

	/** Constructor.
	@param surface
	*/
	VolatilityStream(BlackVarianceSurface surface);

	/** Method to write to file.
	@param file
	@param expiries
	@param strikes
	@throws if writing fails
	*/
	void write(std::string file, std::vector<Date> expiries, std::vector<Real> strikes) const;

	/** Method to write to file using format for Kondor+ feed.
	It includes the required kondor expiries and the ATM forward moneyness.
	@param file
	@param riskFreeTS
	@param dividendTS
	@param spot
	@param expiries
	@param moneynessFwd
	@throws if writing fails
	*/
	void writeKondor(
		std::string file,
		const Handle<YieldTermStructure>& riskFreeTS,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<Quote>& spot,
		std::vector<Date> expiries,
		std::vector<Real> moneynessFwd) const;

	/** Static method to generate a copy of the file.
	The new file will have the name formatted according to the archive specifications.
	@param file
	@param referenceDate
	@throws if file does not exist
	*/
	static void copyToArchive(std::string file, Date referenceDate);

private:

	/** It incorporates the kondor expiries to the given ones.
	The result is sorted.
	@param referenceDate
	@param expiries
	@param expiriesString post-condition: it contains the dates in the required format by kondor+.
	@return std::vector<Date>
	*/
	static std::vector<Date> generateKondorExpiries(
		Date referenceDate, 
		std::vector<Date> expiries,
		std::vector<std::string>& expiriesString);

	/** It generates the strikes correspondent to the forward moneynesses.
	@param expiry
	@param moneynessFwd
	@param riskFreeTS
	@param dividendTS
	@param spot
	*/
	static std::vector<Real> generateKondorStrikes(
		Date expiry,
		std::vector<Real> moneynessFwd,
		const Handle<YieldTermStructure>& riskFreeTS,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<Quote>& spot
		);

	BlackVarianceSurface surface_; /**< surface to write to file */
};

#endif // !VOLATILITY_STREAM_H
