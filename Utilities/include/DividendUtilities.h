#ifndef DIVIDEND_UTILITIES_H
#define DIVIDEND_UTILITIES_H

#include "OptionUtilities.h"
#include "AmericanDividendsCostFunction.h"

#include <MarKetQuote\include\FutureQuote.h>
#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarKetQuote\include\SyntheticFwd.h>
#include <MarKetQuote\include\FilterFwd.h>
#include <MarKetQuote\include\FilterFwdOutliersByPercentile.h>

#include <boost\shared_ptr.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\handle.hpp>
#include <ql\quote.hpp>
#include <ql\time\calendar.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\time\daycounter.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\math\interpolations\backwardflatinterpolation.hpp>
#include <ql\termstructures\yield\zerocurve.hpp>
#include <ql\errors.hpp>
#include <ql\math\optimization\constraint.hpp>
#include <ql\math\optimization\method.hpp>
#include <ql\math\optimization\differentialevolution.hpp>
#include <ql\math\optimization\simplex.hpp>
#include <ql\math\optimization\endcriteria.hpp>
#include <ql\math\optimization\problem.hpp>

#include <algorithm>
#include <vector>
#include <numeric>
#include <memory>
#include <iterator>
#include <sstream>
#include <tuple>

using namespace QuantLib;

namespace utilities
{
	/** It strips dividend yields from FutureQuotes.
	@param spot
	@param riskFreeRate
	@param futures
	@param dc daycounter for the produced curve.
	@param calendar for the produced curve.
	@return boost::shared_ptr<QuantLib::YieldTermStructure> to a InterpolatedZeroCure<Interpolator>
	*/
	template<class Interpolator = BackwardFlat>
	boost::shared_ptr<QuantLib::YieldTermStructure> stripDividendsFromFutures(
		QuantLib::Handle<QuantLib::Quote> spot,
		QuantLib::Handle<QuantLib::YieldTermStructure> riskFreeRate,
		std::vector<std::shared_ptr<FutureQuote>> futures,
		QuantLib::DayCounter dc = Actual365Fixed(),
		QuantLib::Calendar calendar = TARGET())
	{
		std::vector<Date> dates;
		std::vector<Rate> yields;
		dates.push_back(riskFreeRate->referenceDate());
		yields.push_back(0.0);
		for (auto future : futures)
		{
			if (future->expiry() > riskFreeRate->referenceDate())
			{
				dates.push_back(future->expiry());
				Rate q = -1 * std::log(future->value() / spot->value()*riskFreeRate->discount(dates.back())) /
					dc.yearFraction(riskFreeRate->referenceDate(), dates.back());
				yields.push_back(q);
			}
		}

		//make sure vectors are ordered otherwise Constructor throws error
		std::vector<std::size_t> p(dates.size());
		std::iota(p.begin(), p.end(), 0);
		std::sort(p.begin(), p.end(), [&dates](std::size_t i, std::size_t j){ return dates[i] < dates[j]; });
		std::sort(dates.begin(), dates.end());
		std::vector<Rate> sortedYields(yields.size());
		std::transform(p.begin(), p.end(), sortedYields.begin(),
			[&yields](std::size_t i){ return yields[i]; });

		boost::shared_ptr<YieldTermStructure> dividendTS(
			new InterpolatedZeroCurve<Interpolator>(dates, sortedYields, dc, calendar));

		return dividendTS;
	}

	/** It strips dividend yields from european option quotes.
	@param spot
	@param riskFreeRate
	@param options
	@param dc
	@param calendar
	@return boost::shared_ptr<YieldTermStructure>
	*/
	template<class Interpolator = BackwardFlat>
	boost::shared_ptr<YieldTermStructure> stripDividendsFromOptions(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options,
		DayCounter dc = Actual365Fixed(),
		Calendar calendar = TARGET(),
		std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd())
		)
	{
		return  stripDividendsFromFutures<Interpolator>(spot, riskFreeRate, 
			calculateSyntheticFwds(spot, riskFreeRate, options, filter),
			dc, calendar);
	}

	/** It calculates Synthetic Forwards from Option prices.
	After applying the filter the average is taken for each expiry.
	@param spot
	@param riskFreeRate
	@param options
	@return std::vector<std::shared_ptr<FutureQuote>>
	*/
	std::vector<std::shared_ptr<FutureQuote>> calculateSyntheticFwds(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options,
		std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd()));

	/** It strips dividends from american option quotes.
	It solves a minimization problem derived from a fixed point one. American option prices that quote less
	than their intrinsic value are filtered out. Expiry is considered if it has at least 
	minimumPairsPerExpiry pairs.
	@param spot
	@param riskFreeRate
	@param options
	@param initialValue
	@param minimunPairsPerExpiry
	@param endCriteria
	@param solver
	@param constraint
	@param dc
	@param calendar
	@return tuple contains the term structure, the end criteria  and the minimization problem in that order.
	The problem contains methods to extract important values regarding minimization and end criteria describes
	why the iterative algorithm exited.
	*/
	template<class Interpolator = BackwardFlat>
	std::tuple<boost::shared_ptr<YieldTermStructure>, EndCriteria::Type, Problem>
		stripDividendsFromAmericanOptions(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options,
		Rate initialValue,
		Natural minimunPairsPerExpiry = 1,
		EndCriteria endCriteria = EndCriteria(20, 4, 1.0e-4, 1.0e-8, 1.0e-12),
		std::shared_ptr<OptimizationMethod> solver = 
			std::shared_ptr<OptimizationMethod>(new Simplex(0.1)),
		Constraint constraint = BoundaryConstraint(-0.10,0.10),
		DayCounter dc = Actual365Fixed(),
		Calendar calendar = TARGET()
		)
	{
		//cost function
		AmericanDividendsCostFunction costFunction(spot, riskFreeRate, options, minimunPairsPerExpiry);

		//problem
		Array x0(costFunction.expiries().size(), initialValue);
		Problem problem(costFunction, constraint, x0);

		//solve
		EndCriteria::Type criteria = solver->minimize(problem, endCriteria);

		//return
		Array minimum = problem.currentValue();
		std::vector<Rate> yields(minimum.size());
		for (std::size_t i = 0; i < minimum.size(); i++)
			yields[i] = minimum[i];
		auto expiries = costFunction.expiries();
		expiries.insert(expiries.begin(), riskFreeRate->referenceDate());
		yields.insert(yields.begin(), 0.0);
		boost::shared_ptr<YieldTermStructure> dividendTS(new InterpolatedZeroCurve<Interpolator>(
			expiries, yields, dc, calendar));
		return std::make_tuple(dividendTS, criteria, problem);
	}

}

#endif // !DIVIDEND_UTILITIES_H
