#include "DateConversion.h"

#include <ql\errors.hpp>

#include <ctime>
#include <sstream>
#include <iomanip>

namespace utilities
{
	/*date conversion*/
	std::string dateConversion(std::string date, std::string formatIn, std::string formatOut)
	{
		//to date
		std::tm tm = dateStringToTM(date, formatIn);

		//output
		return dateTMToString(tm, formatOut);
	}

	/*date string to tm*/
	std::tm dateStringToTM(std::string date, std::string format)
	{
		std::tm tm = {};
		std::stringstream ss(date);
		ss >> std::get_time(&tm, format.c_str());
		QL_ASSERT(!ss.fail(), "Parsing of the string " << date << " into format " << format << " failed!");
		return tm;
	}

	/*date tm to string*/
	std::string dateTMToString(std::tm date, std::string format)
	{
		std::stringstream os;
		os << std::put_time(&date, format.c_str());

		return os.str();
	}
}