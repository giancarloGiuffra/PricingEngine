#include "StringUtilities.h"

#include <sstream>

namespace utilities
{
	/*split*/
	std::vector<std::string> split(const std::string& s, char delimiter)
	{
		std::stringstream ss(s);
		std::string item;
		std::vector<std::string> tokens;
		while (std::getline(ss, item, delimiter))
		{
			tokens.push_back(item);
		}
		return tokens;
	}
}
