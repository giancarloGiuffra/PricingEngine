#include "ImpliedVolCalculations.h"
#include "OptionUtilities.h"

#include <ql\instruments\vanillaoption.hpp>
#include <ql\processes\blackscholesprocess.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\currencies\europe.hpp>
#include <ql\utilities\tracing.hpp>

#include <algorithm>

namespace utilities
{

	/*calculate implied volatilities*/
	std::tuple<std::vector<Volatility>, std::vector<VanillaOptionQuote>> calculateImpliedVolatilities(
		std::vector<VanillaOptionQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS)
	{
		std::vector<Volatility> volatilities;
		std::vector<VanillaOptionQuote> options;
		for (auto & q : quotes)
		{
			boost::shared_ptr<VanillaOption> option = q.option();
			Handle<BlackVolTermStructure> tempVol(boost::shared_ptr<BlackVolTermStructure>(
				new BlackConstantVol(
				riskFreeTS->referenceDate(),
				riskFreeTS->calendar(),
				0.10,
				riskFreeTS->dayCounter())));
			//enable extrapolation to avoid error if expiries are beyond
			dividendTS->enableExtrapolation(true);
			riskFreeTS->enableExtrapolation(true);
			std::stringstream ss;
			ss << q.expiry();
			QL_TRACE_VARIABLE(ss.str());
			QL_TRACE_VARIABLE(q.strike());
			Volatility vol;
			try{
				vol = option->impliedVolatility(
					q.value(),
					boost::shared_ptr<GeneralizedBlackScholesProcess>(
					new BlackScholesMertonProcess(spot, dividendTS, riskFreeTS, tempVol)),
					1.0e-6);
			}
			catch (std::exception& e){
				std::cerr << std::endl;
				std::cerr << e.what() << std::endl;
				std::cerr << "calculateImpliedVolatilities: " << q << std::endl;
				std::cerr << "... skipping this option" << std::endl;
				continue;
			}
			volatilities.push_back(vol);
			options.push_back(q);
		}
		QL_ENSURE(options.size() == volatilities.size(),
			"number of implied volatilities is different to number of option quotes!");
		return std::make_tuple(volatilities, options);
	}
	
	/*build implied volatility matrix*/
	Matrix buildImpliedVolatilityMatrix(
		std::vector<VanillaOptionQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS)
	{
		QL_TRACE_ENTER_FUNCTION;
		// expiries and strikes
		std::vector<Date> expiries = extractOptionExpiries(quotes);
		std::vector<Real> strikes = extractOptionStrikes(quotes);

		//define matrix
		Matrix volMatrix = Matrix(strikes.size(), expiries.size(), 0);
		auto result = calculateImpliedVolatilities(quotes, spot, dividendTS, riskFreeTS);
		auto volatilities = std::get<0>(result);
		for (std::size_t k = 0; k < volatilities.size(); k++)
		{
			auto j = std::distance(expiries.begin(), std::find(expiries.begin(), expiries.end(), quotes[k].expiry()));
			auto i = std::distance(strikes.begin(), std::find(strikes.begin(), strikes.end(), quotes[k].strike()));
			volMatrix[i][j] = volatilities[k];
		}

		QL_TRACE_EXIT_FUNCTION;
		return volMatrix;
	}

	/*build implied volatility matrix (options on futures)*/
	Matrix buildImpliedVolatilityMatrix(
		std::vector<VanillaOptionFutureQuote> quotes,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS)
	{
		
		// expiries and strikes
		std::vector<Date> expiries = extractOptionExpiries(quotes);
		std::vector<Real> strikes = extractOptionStrikes(quotes);

		//define matrix
		Matrix volMatrix = Matrix(strikes.size(), expiries.size(), 0);
		for (auto q : quotes)
		{
			//we pass the correct forward but we still need a vanilla option for the engines used in impliedVolatility 
			VanillaOption option = VanillaOption(
				boost::dynamic_pointer_cast	<StrikedTypePayoff>(q.optionFuture()->payoff()), 
				q.optionFuture()->exercise());
			Handle<BlackVolTermStructure> tempVol(boost::shared_ptr<BlackVolTermStructure>(
				new BlackConstantVol(
				riskFreeTS->referenceDate(),
				riskFreeTS->calendar(),
				0,
				riskFreeTS->dayCounter())));
			//calculate correct forward - depends on future expiry and not option expiry
			Handle<Quote> fwd = Handle<Quote>(boost::shared_ptr<Quote>(
				new SimpleQuote(
				spot->value()*dividendTS->discount(q.futureExpiry(),true)/riskFreeTS->discount(q.futureExpiry(), true))));
			//enable extrapolation to avoid error if expiries are beyond
			riskFreeTS->enableExtrapolation(true);
			Volatility vol = option.impliedVolatility(
				q.value(),
				boost::shared_ptr<GeneralizedBlackScholesProcess>(
				new BlackProcess(fwd, riskFreeTS, tempVol)),
				1.0e-6, //accuracy
				300); // max evaluations
			auto j = std::distance(expiries.begin(), std::find(expiries.begin(), expiries.end(), q.expiry()));
			auto i = std::distance(strikes.begin(), std::find(strikes.begin(), strikes.end(), q.strike()));
			volMatrix[i][j] = vol;
		}

		return volMatrix;
	}

	/*build extended volatility matrix*/
	Matrix buildExtendedVolatilityMatrix(
		std::vector<Date> expiries,
		std::vector<Real> strikes,
		boost::shared_ptr<PricingEngine> engine,
		const Handle<Quote>& spot,
		const Handle<YieldTermStructure>& dividendTS,
		const Handle<YieldTermStructure>& riskFreeTS)
	{
		//build options and set pricing engine
		std::vector<VanillaOption> options;
		for (auto exp : expiries)
			for (auto str : strikes)
			{
				boost::shared_ptr<StrikedTypePayoff> payoff = boost::shared_ptr<StrikedTypePayoff>(
					new PlainVanillaPayoff(Option::Call, str));
				boost::shared_ptr<Exercise> exercise = boost::shared_ptr<Exercise>(
					new EuropeanExercise(exp));
				options.push_back(VanillaOption(payoff, exercise));
				options.back().setPricingEngine(engine);
			}

		//extract option prices and create quotes
		std::vector<VanillaOptionQuote> quotes;
		for (auto opt : options)
		{
			quotes.push_back(VanillaOptionQuote("JustForImpliedVolCalculation",
				boost::dynamic_pointer_cast<StrikedTypePayoff>(opt.payoff()),
				opt.exercise(),
				EURCurrency(),
				opt.NPV()));
		}

		//create matrix
		return buildImpliedVolatilityMatrix(quotes, spot, dividendTS, riskFreeTS);

	}
}