#include "AmericanDividendsCostFunction.h"
#include "OptionUtilities.h"
#include "DividendUtilities.h"

#include <ql\errors.hpp>
#include <ql\math\interpolations\backwardflatinterpolation.hpp>
#include <ql\termstructures\yield\zerocurve.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\processes\blackscholesprocess.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\pricingengines\vanilla\analyticeuropeanengine.hpp>

#include <algorithm>
#include <iostream>
#include <sstream>

/*constructor*/
AmericanDividendsCostFunction::AmericanDividendsCostFunction(
	const Handle<Quote>& spot,
	const Handle<YieldTermStructure>& riskFreeRate,
	const std::vector<std::shared_ptr<VanillaOptionQuote>>& options,
	Natural minimumPairsPerExpiry)
	: spot_(spot), riskFreeRate_(riskFreeRate), minimumPairsPerExpiry_(minimumPairsPerExpiry)
{
	QL_REQUIRE(std::all_of(options.begin(), options.end(),
		[](std::shared_ptr<VanillaOptionQuote> q){ return q->exerciseType() == Exercise::American; }),
		"not all options are american!");

	//filter options with value less than intrinsic value (mkt is wrong)
	for (auto const& o : options)
	{
		if (o->value() >= 
			(o->optionType() == Option::Call ? spot_->value() - o->strike() : o->strike() - spot_->value()))
			options_.push_back(o);
	}

	//save expiries and paired options - 
	PutCallParityOptions pairs = utilities::createPutCallParityPairs(options_);
	options_.clear();
	for (auto & strip : pairs)
	{
		if (strip.size() >= minimumPairsPerExpiry_)
		{
			expiries_.push_back(strip.front().first.expiry());
			for (auto & pair : strip)
			{
				options_.push_back(std::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(pair.first)));
				options_.push_back(std::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(pair.second)));
			}
		}
	}

	QL_ENSURE(expiries_.size() >= 1, "there must be at least 1 expiry");
}

/*inspector options*/
const std::vector<std::shared_ptr<VanillaOptionQuote>>& AmericanDividendsCostFunction::options() const
{
	return this->options_;
}

/*inspector expiries*/
const std::vector<Date>& AmericanDividendsCostFunction::expiries() const
{
	return this->expiries_;
}

/*value*/
Real AmericanDividendsCostFunction::value(const Array& x) const
{
	Array vals = values(x);
	auto maximumAbsoluteValue = std::max_element(vals.begin(), vals.end(), [](Real x1, Real x2){
		return std::abs(x1) < std::abs(x2); });
	return std::abs(*maximumAbsoluteValue);
}

/*values*/
Disposable<Array> AmericanDividendsCostFunction::values(const Array& x) const
{
	QL_REQUIRE(x.size() == expiries_.size(),
		"array x must have the same size as expiries (" << expiries_.size() << ")");

	//build yield termstructure
	std::vector<Rate> yields(x.size());
	for (std::size_t i = 0; i < x.size(); i++)
		yields[i] = x[i];
	std::vector<Date> expiries = expiries_;
	Handle<YieldTermStructure> dividendTS;
	if (expiries_.size() >= 2)
		dividendTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new InterpolatedZeroCurve<BackwardFlat>(
			expiries, yields, riskFreeRate_->dayCounter(), riskFreeRate_->calendar())));
	else
		dividendTS = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(
			riskFreeRate_->referenceDate(), yields[0], riskFreeRate_->dayCounter())));

	//build european option quotes
	std::vector<std::shared_ptr<VanillaOptionQuote>> europeans;
	for (auto const& o : options_)
	{
		//extract implied vol
		Handle<BlackVolTermStructure> tempVol(boost::shared_ptr<BlackVolTermStructure>(
			new BlackConstantVol(
			riskFreeRate_->referenceDate(),
			riskFreeRate_->calendar(),
			0.10,
			riskFreeRate_->dayCounter())));
		//enable extrapolation to avoid error if expiries are beyond
		dividendTS->enableExtrapolation(true);
		riskFreeRate_->enableExtrapolation(true);
		Volatility vol;
		try {
			vol = o->option()->impliedVolatility(
				o->value(),
				boost::shared_ptr<GeneralizedBlackScholesProcess>(
				new BlackScholesMertonProcess(spot_, dividendTS, riskFreeRate_, tempVol)),
				1.0e-6);
		}
		catch (std::exception& e){
			std::cerr << std::endl;
			std::cerr << e.what() << std::endl;
			std::cerr << "AmericanDividendsCostFunction: " << *o << std::endl;
			std::cerr << "... continue minimization excluding this option" << std::endl;
			continue;
		}

		//build european option quote
		std::string type = o->optionType() == Option::Call ? "C" : "P";
		std::stringstream expiry;
		expiry << io::iso_date(o->expiry());
		std::shared_ptr<VanillaOptionQuote> europeanQuote(
			new VanillaOptionQuote(
			o->underlying(),
			type,
			"european",
			o->strike(),
			expiry.str(),
			o->currency().code()
			));
		boost::shared_ptr<VanillaOption> europeanOption = europeanQuote->option();
		Handle<BlackVolTermStructure> impliedVol(boost::shared_ptr<BlackVolTermStructure>(
			new BlackConstantVol(
			riskFreeRate_->referenceDate(),
			riskFreeRate_->calendar(),
			vol,
			riskFreeRate_->dayCounter())));
		boost::shared_ptr<PricingEngine> engine(
			new AnalyticEuropeanEngine(boost::shared_ptr<GeneralizedBlackScholesProcess>(
			new BlackScholesMertonProcess(spot_, dividendTS, riskFreeRate_, impliedVol))));
		europeanOption->setPricingEngine(engine);
		europeanQuote->setValue(europeanOption->NPV());

		//insert in europeans
		europeans.push_back(europeanQuote);
	}

	//calculate updated yields
	auto newDividendTS = utilities::stripDividendsFromOptions(
		spot_, riskFreeRate_, europeans, riskFreeRate_->dayCounter(), riskFreeRate_->calendar());

	//build result array
	Array results(expiries_.size());
	for (std::size_t i = 0; i < results.size(); i++)
		results[i] =
		newDividendTS->zeroRate(expiries_[i], riskFreeRate_->dayCounter(), Compounding::Continuous).rate() -
		x[i];

	return results;
}