#include "DividendUtilities.h"

namespace utilities
{
	/*calculate synthetic fwds*/
	std::vector<std::shared_ptr<FutureQuote>> calculateSyntheticFwds(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options,
		std::shared_ptr<FilterFwdData> filter)
	{
		//checks
		std::string underlying = options.front()->underlying();
		QL_REQUIRE(std::all_of(options.begin(), options.end(),
			[underlying](std::shared_ptr<VanillaOptionQuote> q){ return q->underlying() == underlying; }),
			"not all options have the same underlying!");
		Currency currency = options.front()->currency();
		QL_REQUIRE(std::all_of(options.begin(), options.end(),
			[currency](std::shared_ptr<VanillaOptionQuote> q){ return q->currency() == currency; }),
			"not all options have the same currency!");
		QL_REQUIRE(std::all_of(options.begin(), options.end(),
			[](std::shared_ptr<VanillaOptionQuote> q){ return q->exerciseType() == Exercise::European; }),
			"not all options are european!");
		
		//extract put call parity pairs
		PutCallParityOptions pairs = utilities::createPutCallParityPairs(options);

		//transform pairs in synthetic forwards
		GroupedSyntheticFwds fwds(pairs.size());
		for (size_t i = 0; i < pairs.size(); i++)
		{
			std::transform(pairs[i].begin(), pairs[i].end(), std::back_inserter(fwds[i]),
				[riskFreeRate](std::pair<VanillaOptionQuote, VanillaOptionQuote> pair){
				return SyntheticFwd(pair.first, pair.second, riskFreeRate);
			});
		}

		//filter
		GroupedSyntheticFwds filteredFwds = filter->filter(fwds);

		//build "future" quotes
		std::vector<std::shared_ptr<FutureQuote>> syntheticFutures;
		std::transform(filteredFwds.begin(), filteredFwds.end(), std::back_inserter(syntheticFutures),
			[underlying, currency](SyntheticFwds strip){
			//mean fwd value
			Real value = 0.0;
			for (auto const& sfwd : strip)
				value += sfwd.value;
			value /= strip.size();
			//return future quote
			std::stringstream expiry;
			expiry << io::iso_date(strip.front().expiry);
			return std::shared_ptr<FutureQuote>(new FutureQuote(
				underlying,
				expiry.str(),
				currency.code(),
				value
				));
		});

		//return
		return syntheticFutures;
	}
}