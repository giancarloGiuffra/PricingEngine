#include "VolatilityStream.h"
#include "DateConversion.h"

#include <fstream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <numeric>

/*----------------------------------------KondorExpiry-------------------------------------------------------*/

/*initialization of static vector*/
std::vector<KondorExpiry> KondorExpiry::kondorExpiries_ = {};

/*constructor*/
KondorExpiry::KondorExpiry(std::string expiry, Period period)
	: expiry_(expiry), period_(period)
{
	KondorExpiry::kondorExpiries_.push_back(*this);
}

/*"enum"*/
const KondorExpiry KondorExpiry::SD7 = KondorExpiry("SD7", Period(7, Days));
const KondorExpiry KondorExpiry::SM1 = KondorExpiry("SM1", Period(1, Months));
const KondorExpiry KondorExpiry::SM2 = KondorExpiry("SM2", Period(2, Months));
const KondorExpiry KondorExpiry::SM3 = KondorExpiry("SM3", Period(3, Months));
const KondorExpiry KondorExpiry::SM6 = KondorExpiry("SM6", Period(6, Months));
const KondorExpiry KondorExpiry::SM9 = KondorExpiry("SM9", Period(9, Months));
const KondorExpiry KondorExpiry::SM12 = KondorExpiry("SM12", Period(12, Months));
const KondorExpiry KondorExpiry::SM15 = KondorExpiry("SM15", Period(15, Months));
const KondorExpiry KondorExpiry::SM18 = KondorExpiry("SM18", Period(18, Months));
const KondorExpiry KondorExpiry::SM21 = KondorExpiry("SM21", Period(21, Months));
const KondorExpiry KondorExpiry::SM24 = KondorExpiry("SM24", Period(24, Months));
const KondorExpiry KondorExpiry::SM36 = KondorExpiry("SM36", Period(36, Months));
const KondorExpiry KondorExpiry::SM60 = KondorExpiry("SM60", Period(60, Months));
const KondorExpiry KondorExpiry::SM120 = KondorExpiry("SM120", Period(120, Months));

/*to string*/
std::string KondorExpiry::to_string() const
{
	return this->expiry_;
}

/*to date*/
Date KondorExpiry::to_date(Date referenceDate) const
{
	return referenceDate + period_;
}

/*kondor expiries*/
std::vector<KondorExpiry> KondorExpiry::kondorExpiries()
{
	return KondorExpiry::kondorExpiries_;
}

/*operator==*/
bool operator==(const KondorExpiry& lhs, const KondorExpiry& rhs)
{
	return lhs.period_ == rhs.period_;
}

/*operator!=*/
bool operator!=(const KondorExpiry& lhs, const KondorExpiry& rhs)
{
	return !(lhs == rhs);
}

/*----------------------------------------VolatilityStream--------------------------------------------------*/

/*constructor*/
VolatilityStream::VolatilityStream(BlackVarianceSurface surface)
	: surface_(surface)
{}

/*write*/
void VolatilityStream::write(std::string file, std::vector<Date> expiries, std::vector<Real> strikes) const
{
	//define stream and open/create file discarding content
	std::ofstream fs(file, std::ios::out | std::ios::trunc);

	//check if file was opened successfully
	if (!fs.is_open()) QL_FAIL("file " << file << " could not be opened/created.");

	//write to file
	int width = 12;
	fs << std::setw(width) << " ";
	//strikes
	for (auto strike : strikes)
		fs << std::setw(width) << std::fixed << std::setprecision(2) << strike;
	fs << std::endl;
	//surface - first column is expiry
	for (auto expiry : expiries)
	{
		std::stringstream ss;
		ss << detail::iso_date_holder(expiry);
		fs << std::setw(width) << ss.str();
		for (auto strike : strikes)
		{
			fs << std::setw(width) << std::fixed 
				<< std::setprecision(2) << surface_.blackVol(expiry, strike, true);
		}
		fs << std::endl;
	}
}

/*write kondor*/
void VolatilityStream::writeKondor(
	std::string file,
	const Handle<YieldTermStructure>& riskFreeTS,
	const Handle<YieldTermStructure>& dividendTS,
	const Handle<Quote>& spot,
	std::vector<Date> expiries,
	std::vector<Real> moneynessFwd) const
{
	//define stream and open/create file discarding content
	std::ofstream fs(file, std::ios::out | std::ios::trunc);

	//check if file was opened successfully
	if (!fs.is_open()) QL_FAIL("file " << file << " could not be opened/created.");

	//write to file
	fs << "FileType:VolatCurve 2.6" << std::endl;
	fs << "CurveType:INDEX_ATM" << std::endl;
	fs << "\t" << "TENOR" << "\t" << "FWD_PRICE" << "\t" << "INTERPOLATED" << "\t"
		<< "VOLAT_BID" << "\t" << "VOLAT_ASK" << "\t";
	//moneyness : sort + unique
	std::sort(moneynessFwd.begin(), moneynessFwd.end());
	moneynessFwd.erase(std::unique(moneynessFwd.begin(), moneynessFwd.end()), moneynessFwd.end());
	for (auto moneyness : moneynessFwd)
	{
		if (moneyness != 1) //skip ATM forward
			fs << "\t" << std::fixed << std::setprecision(6) << (moneyness - 1)*100 << "\t" << "SMILE";
	}
	fs << std::endl;
	//surface - first column is expiry
	std::vector<std::string> expiriesString;
	std::vector<Date> allExpiries = VolatilityStream::generateKondorExpiries(riskFreeTS->referenceDate(),
		expiries, expiriesString);
	for (int i = 0; i < allExpiries.size(); i++)
	{
		//expiry
		fs << "\t" << expiriesString[i];
		//fwd
		Real fwd = spot->value()*
			dividendTS->discount(allExpiries[i], true) / riskFreeTS->discount(allExpiries[i], true);
		fs << "\t" << std::fixed << std::setprecision(6) << fwd << "\t" << "-";
		//ATM fwd volatility
		Real atmVol = surface_.blackVol(allExpiries[i], fwd, true);
		fs << "\t" << std::fixed << std::setprecision(6) << atmVol * 100;
		fs << "\t" << std::fixed << std::setprecision(6) << atmVol * 100;
		std::vector<Real> strikes = VolatilityStream::generateKondorStrikes(
			allExpiries[i], moneynessFwd, riskFreeTS, dividendTS, spot);
		for (auto strike : strikes)
		{
			if (std::abs(strike - fwd) > 1.0e-6) //skip ATM fwd strike
			{
				fs << "\t" << std::fixed << std::setprecision(6)
					<< (surface_.blackVol(allExpiries[i], strike, true) - atmVol) * 100;
				fs << "\t" << "*";
			}
		}
		fs << std::endl;
	}
}

/*copy to archive*/
void VolatilityStream::copyToArchive(std::string file, Date referenceDate)
{
	//date
	std::stringstream ss;
	ss << detail::iso_date_holder(referenceDate);
	std::string date = utilities::dateConversion(ss.str(), "%Y-%m-%d", "%Y%m%d");

	//streams
	std::ifstream source(file, std::ios::binary | std::ios::in);
	//check if file was opened successfully
	if (!source.is_open()) QL_FAIL("file " << file << " could not be opened.");
	std::ofstream destination(file + date + "K", std::ios::binary | std::ios::out );

	//copy
	destination << source.rdbuf();
}

/* generate kondor expiries*/
std::vector<Date> VolatilityStream::generateKondorExpiries(
	Date referenceDate,
	std::vector<Date> expiries,
	std::vector<std::string>& expiriesString)
{
	//extract kondor expiries
	std::vector<KondorExpiry> kExpiries = KondorExpiry::kondorExpiries();
	
	//extract dates
	std::vector<Date> dates;
	std::transform(kExpiries.begin(), kExpiries.end(), std::back_inserter(dates),
		[referenceDate](KondorExpiry k){ return k.to_date(referenceDate); });
	//include given expiries + extract permutation + sort + unique
	dates.insert(dates.end(), expiries.begin(), expiries.end());
	std::vector<std::size_t> p(dates.size());
	std::iota(p.begin(), p.end(), 0);
	std::sort(p.begin(), p.end(), [&dates](std::size_t i, std::size_t j){ return dates[i] < dates[j]; });
	std::sort(dates.begin(), dates.end());
	dates.erase(std::unique(dates.begin(), dates.end()), dates.end());
	
	// extract strings
	std::vector<std::string> datesStrings;
	std::transform(kExpiries.begin(), kExpiries.end(), std::back_inserter(datesStrings),
		[referenceDate](KondorExpiry k){ return k.to_string(); });
	//include given expiries in kondor+ format
	std::vector<std::string> formattedExpiries;
	std::transform(expiries.begin(), expiries.end(), std::back_inserter(formattedExpiries),
		[](Date e){ 
		std::stringstream ss;
		ss << detail::iso_date_holder(e);
		return utilities::dateConversion(ss.str(), "%Y-%m-%d", "%d/%m/%Y");
	});
	datesStrings.insert(datesStrings.end(), formattedExpiries.begin(), formattedExpiries.end());
	// sort using permutation + unique on std::vector passed by reference
	expiriesString.clear();
	std::transform(p.begin(), p.end(), std::back_inserter(expiriesString), 
		[&datesStrings](std::size_t i){ return datesStrings[i]; });
	expiriesString.erase(std::unique(expiriesString.begin(), expiriesString.end()), expiriesString.end());

	QL_ENSURE(dates.size() == expiriesString.size(),
		"somehow dates and corresponding strings don't have the same size");
	return dates;
}

/*generate kondor strikes*/
std::vector<Real> VolatilityStream::generateKondorStrikes(
	Date expiry,
	std::vector<Real> moneynessFwd,
	const Handle<YieldTermStructure>& riskFreeTS,
	const Handle<YieldTermStructure>& dividendTS,
	const Handle<Quote>& spot
	)
{
	//calculate fwd
	Real fwd = spot->value()*dividendTS->discount(expiry, true) / riskFreeTS->discount(expiry, true);
	
	//generate strikes
	std::vector<Real> strikes;
	std::transform(moneynessFwd.begin(), moneynessFwd.end(), std::back_inserter(strikes), 
		[fwd](Real m){ return m*fwd; });

	return strikes;
}