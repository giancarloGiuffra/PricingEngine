#include "OptionUtilities.h"
#include "DividendUtilities.h"

#include <ql\errors.hpp>
#include <ql\processes\blackscholesprocess.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\pricingengines\vanilla\analyticeuropeanengine.hpp>

#include <iterator>
#include <algorithm>

namespace utilities
{
	/*extract option expiries*/
	std::vector<Date> extractOptionExpiries(
		std::vector<boost::shared_ptr<VanillaOptionQuote>> optionQuotes)
	{
		std::vector<Date> expiries(optionQuotes.size());
		std::transform(optionQuotes.begin(), optionQuotes.end(), expiries.begin(),
			[](boost::shared_ptr<VanillaOptionQuote> quote){ return quote->expiry(); });
		std::sort(expiries.begin(), expiries.end());
		auto last = std::unique(expiries.begin(), expiries.end());
		expiries.erase(last, expiries.end());
		return expiries;
	}

	std::vector<Date> extractOptionExpiries(
		std::vector<std::shared_ptr<VanillaOptionQuote>> optionQuotes)
	{
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(*q)));
		return extractOptionExpiries(pQuotes);
	}

	std::vector<Date> extractOptionExpiries(
		std::vector<VanillaOptionQuote> optionQuotes)
	{
		//build pointers
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(q)));
		return extractOptionExpiries(pQuotes);
	}

	std::vector<Date> extractOptionExpiries(
		std::vector<VanillaOptionFutureQuote> optionQuotes)
	{
		//build pointers
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionFutureQuote(q)));
		return extractOptionExpiries(pQuotes);
	}

	/*extract option strikes*/
	std::vector<Real> extractOptionStrikes(
		std::vector<boost::shared_ptr<VanillaOptionQuote>> optionQuotes)
	{
		std::vector<Real> strikes(optionQuotes.size());
		std::transform(optionQuotes.begin(), optionQuotes.end(), strikes.begin(),
			[](boost::shared_ptr<VanillaOptionQuote> quote){ return quote->strike(); });
		std::sort(strikes.begin(), strikes.end());
		auto last = std::unique(strikes.begin(), strikes.end());
		strikes.erase(last, strikes.end());
		return strikes;
	}

	std::vector<Real> extractOptionStrikes(
		std::vector<std::shared_ptr<VanillaOptionQuote>> optionQuotes)
	{
		//build pointers
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(*q)));
		return extractOptionStrikes(pQuotes);
	}

	std::vector<Real> extractOptionStrikes(
		std::vector<VanillaOptionQuote> optionQuotes)
	{
		//build pointers
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionQuote(q)));
		return extractOptionStrikes(pQuotes);
	}

	std::vector<Real> extractOptionStrikes(
		std::vector<VanillaOptionFutureQuote> optionQuotes)
	{
		//build pointers
		std::vector<boost::shared_ptr<VanillaOptionQuote>> pQuotes;
		for (auto q : optionQuotes)
			pQuotes.push_back(boost::shared_ptr<VanillaOptionQuote>(new VanillaOptionFutureQuote(q)));
		return extractOptionStrikes(pQuotes);
	}

	/*extract options by type*/
	std::vector<std::shared_ptr<VanillaOptionQuote>>
		extractOptionByType(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options, Option::Type type)
	{
		std::vector<std::shared_ptr<VanillaOptionQuote>> results;
		for (auto const& o : options)
		{
			if (o->optionType() == type)
				results.push_back(o);
		}
		return results;
	}

	/*group options by expiry*/
	std::vector<std::vector<std::shared_ptr<VanillaOptionQuote>>>
		groupOptionsByExpiry(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options)
	{
		std::vector<Date> expiries = extractOptionExpiries(options);

		std::vector<std::vector<std::shared_ptr<VanillaOptionQuote>>> grouped(expiries.size());
		for (auto const& o : options)
		{
			//find index corresponding to expiry
			size_t index = std::distance(expiries.begin(), 
				std::find(expiries.begin(), expiries.end(), o->expiry()));
			//insert in the right place
			grouped[index].push_back(o);
		}

		return grouped;
	}

	/*create put call parity pairs same expiry*/
	std::vector<std::pair<VanillaOptionQuote, VanillaOptionQuote>>
		createPutCallParityPairsSameExpiry(const std::vector<std::shared_ptr<VanillaOptionQuote>>& options)
	{
		//check same expiry
		Date expiry = options.front()->expiry();
		QL_REQUIRE(std::all_of(options.begin(), options.end(),
			[expiry](std::shared_ptr<VanillaOptionQuote> q){ return q->expiry() == expiry; }),
			"not all options have the same expiry!");
		
		std::vector<std::pair<VanillaOptionQuote, VanillaOptionQuote>> putCallParityOptions;
		auto calls = extractOptionByType(options, Option::Call);
		auto puts = extractOptionByType(options, Option::Put);
		for (auto const& call : calls)
		{
			auto iterator = std::find_if(puts.begin(), puts.end(),
				[&call](std::shared_ptr<VanillaOptionQuote> put){ return put->strike() == call->strike(); });
			if (iterator != puts.end())
				putCallParityOptions.push_back(std::make_pair(*call,**iterator));
		}

		//sort by strike
		std::sort(putCallParityOptions.begin(), putCallParityOptions.end(),
			[](std::pair<VanillaOptionQuote, VanillaOptionQuote> p1, std::pair<VanillaOptionQuote, VanillaOptionQuote> p2){
			return p1.first.strike() < p2.first.strike();
		});

		return putCallParityOptions;
	}

	/*create put call parity pairs*/
	PutCallParityOptions createPutCallParityPairs(
		const std::vector<std::shared_ptr<VanillaOptionQuote>>& options)
	{
		// group by expiry
		auto grouped = groupOptionsByExpiry(options);

		PutCallParityOptions putCallParityPairs;
		for (auto const& group : grouped)
		{
			auto putCallParityPairsSameExpiry = createPutCallParityPairsSameExpiry(group);
			if (!putCallParityPairsSameExpiry.empty())
				putCallParityPairs.push_back(putCallParityPairsSameExpiry);
		}

		return putCallParityPairs;
	}

	/*calculate european from american*/
	std::vector<std::shared_ptr<VanillaOptionQuote>> calculateEuropeanFromAmericanOptionPrices(
		Handle<Quote> spot,
		Handle<YieldTermStructure> riskFreeRate,
		Handle<YieldTermStructure> dividendTS,
		std::vector<std::shared_ptr<VanillaOptionQuote>> options)
	{
		//cycle through option quotes
		std::vector<std::shared_ptr<VanillaOptionQuote>> europeans;
		for (auto const& o : options)
		{
			//skip if american price < intrinsic value
			if (o->value() >=
				(o->optionType() == Option::Call ? spot->value() - o->strike() : o->strike() - spot->value()))
			{
				//extract implied vol
				Handle<BlackVolTermStructure> tempVol(boost::shared_ptr<BlackVolTermStructure>(
					new BlackConstantVol(
					riskFreeRate->referenceDate(),
					riskFreeRate->calendar(),
					0.10,
					riskFreeRate->dayCounter())));
				//enable extrapolation to avoid error if expiries are beyond
				dividendTS->enableExtrapolation(true);
				riskFreeRate->enableExtrapolation(true);
				Volatility vol;
				try{
					vol = o->option()->impliedVolatility(
						o->value(),
						boost::shared_ptr<GeneralizedBlackScholesProcess>(
						new BlackScholesMertonProcess(spot, dividendTS, riskFreeRate, tempVol)),
						1.0e-6);
				}
				catch (std::exception& e){
					std::cerr << std::endl;
					std::cerr << e.what() << std::endl;
					std::cerr << "calculateEuropeanFromAmericanOptionPrices: " << *o << std::endl;
					std::cerr << "... skipping option quote" << std::endl;
					continue;
				}

				//build european option quote
				std::string type = o->optionType() == Option::Call ? "C" : "P";
				std::stringstream expiry;
				expiry << io::iso_date(o->expiry());
				std::shared_ptr<VanillaOptionQuote> europeanQuote(
					new VanillaOptionQuote(
					o->underlying(),
					type,
					"european",
					o->strike(),
					expiry.str(),
					o->currency().code()
					));
				boost::shared_ptr<VanillaOption> europeanOption = europeanQuote->option();
				Handle<BlackVolTermStructure> impliedVol(boost::shared_ptr<BlackVolTermStructure>(
					new BlackConstantVol(
					riskFreeRate->referenceDate(),
					riskFreeRate->calendar(),
					vol,
					riskFreeRate->dayCounter())));
				boost::shared_ptr<PricingEngine> engine(
					new AnalyticEuropeanEngine(boost::shared_ptr<GeneralizedBlackScholesProcess>(
					new BlackScholesMertonProcess(spot, dividendTS, riskFreeRate, impliedVol))));
				europeanOption->setPricingEngine(engine);
				europeanQuote->setValue(europeanOption->NPV());

				//insert in europeans
				europeans.push_back(europeanQuote);
			}
		}

		return europeans;
	}
}