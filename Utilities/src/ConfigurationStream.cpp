#include "ConfigurationStream.h"

#include <ql\errors.hpp>

#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>

/*constructor*/
ConfigurationStream::ConfigurationStream(std::string file)
	: file_(file)
{}

/*update*/
void ConfigurationStream::update(
	const Handle<YieldTermStructure>& dividendTS,
	Date date,
	std::string underlying,
	std::string kShortName,
	std::string kVolatilityShortName,
	std::string spotFile
	) const
{
	//define stream and open/create file for read/write, append content
	std::fstream fs(file_, std::ios::out | std::ios::in );

	//check if file was opened successfully
	if (!fs.is_open()) QL_FAIL("file " << file_ << " could not be opened/created.");

	//input position indicator at start
	fs.seekg(0, std::ios_base::beg);
	//dump content in string
	std::stringstream ss;
	ss << fs.rdbuf();
	//check if there is content for underlying
	if (ss.str().find(underlying) == std::string::npos)
	{
		//there is no content for underlying
		//go to the eof
		fs.seekp(0, std::ios_base::end);
		//write
		fs << underlying << ".VolatilityCurve_Data" << "=" << "VolatCurves." << underlying << std::endl;
		fs << underlying << ".ShortName" << "=" << kShortName << std::endl;
		fs << underlying << ".VolatilityCurve_ShortName" << "=" << kVolatilityShortName << std::endl;
		fs << underlying << ".DividendYield" << "=" << std::fixed << std::setprecision(4)
			<< dividendTS->zeroRate(
			date,
			dividendTS->dayCounter(),
			Compounding::Continuous,
			Frequency::Annual,
			true
			).rate() 
			<< std::endl;
		if (!spotFile.empty()) { fs << underlying << ".Spot" << "=" << spotFile << std::endl; }
	}
	else
	{
		//there is content for the underlying
		//cycle to find the line to be replaced
		std::string line;
		fs.seekg(0, std::ios_base::beg);
		int startLinePosition = (int)fs.tellg();
		while (std::getline(fs, line))
		{
			if (line.find(underlying + ".DividendYield") != std::string::npos) { break; }
			startLinePosition = (int)fs.tellg();
		}
		//position and replace
		fs.seekp(startLinePosition);
		fs << underlying << ".DividendYield" << "=" << std::fixed << std::setprecision(4)
			<< dividendTS->zeroRate(
			date,
			dividendTS->dayCounter(),
			Compounding::Continuous,
			Frequency::Annual,
			true
			).rate()
			<< std::endl;
	}
}