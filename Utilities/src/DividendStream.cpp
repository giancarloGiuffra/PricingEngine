#include "DividendStream.h"
#include "DateConversion.h"

#include <fstream>
#include <sstream>
#include <iomanip>
#include <regex>

/*constructor*/
DividendStream::DividendStream(const Handle<YieldTermStructure> dividendTS)
	: dividendTS_(dividendTS)
{}

/*write*/
void DividendStream::write(std::string file, std::vector<Date> dates) const
{
	//define stream and open/create file discarding content
	std::ofstream fs(file, std::ios::out | std::ios::trunc);

	//check if file was opened successfully
	if (!fs.is_open()) QL_FAIL("file " << file << " could not be opened/created.");

	//write to file
	int width = 12;
	for (auto date : dates)
	{
		std::stringstream datestr;
		datestr << detail::iso_date_holder(date);
		fs << std::setw(width) << datestr.str()
			<< std::setw(width) << std::fixed << std::setprecision(4)
			<< this->dividendTS_->zeroRate(
			date,
			dividendTS_->dayCounter(),
			Compounding::Continuous,
			Frequency::Annual,
			true
			).rate()
			<< std::endl;
	}
}

/*write kondor*/
void DividendStream::writeKondor(std::string file, Date date) const
{
	//define stream and open/create file discarding content
	std::ofstream fs(file, std::ios::out | std::ios::trunc);

	//check if file was opened successfully
	if (!fs.is_open()) QL_FAIL("file " << file << " could not be opened/created.");

	//write to file
	fs << this->dividendTS_->zeroRate(
		date,
		dividendTS_->dayCounter(),
		Compounding::Continuous,
		Frequency::Annual,
		true
		).rate()
		<< std::endl;
}

/*copy to archive*/
void DividendStream::copyToArchive(std::string file, Date referenceDate)
{
	//date
	std::stringstream ss;
	ss << detail::iso_date_holder(referenceDate);
	std::string date = utilities::dateConversion(ss.str(), "%Y-%m-%d", "%Y%m%d");

	//streams
	std::ifstream source(file, std::ios::binary | std::ios::in);
	//check if file was opened successfully
	if (!source.is_open()) QL_FAIL("file " << file << " could not be opened.");
	//capture path and name
	std::regex capture_path_name_file(R"((.+?)\\([^\\]+)\.[^\\]+$)");
	std::smatch m;
	std::regex_search(file, m, capture_path_name_file);
	std::ofstream destination(
		std::string(m[1]) + "\\" + std::string(m[2]) + "Div" + date + "K.div",
		std::ios::binary | std::ios::out);

	//copy
	destination << source.rdbuf();
}