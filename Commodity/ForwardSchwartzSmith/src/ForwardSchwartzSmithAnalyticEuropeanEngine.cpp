#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithAnalyticEuropeanEngine.h>
#include <ql\pricingengines\blackcalculator.hpp>
#include <ql\exercise.hpp>

/*constructor*/
ForwardSchwartzSmithAnalyticEuropeanEngine::ForwardSchwartzSmithAnalyticEuropeanEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve)
	: process_(process), discountCurve_(discountCurve)
{
	registerWith(process_);
	registerWith(discountCurve_);
}

/*constructor*/
ForwardSchwartzSmithAnalyticEuropeanEngine::ForwardSchwartzSmithAnalyticEuropeanEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process)
	: ForwardSchwartzSmithAnalyticEuropeanEngine(process, process->riskFreeTS())
{}

/*calculate*/
void ForwardSchwartzSmithAnalyticEuropeanEngine::calculate() const
{
	//require european option
	QL_REQUIRE(arguments_.exercise->type() == Exercise::European,
		"not an European option");

	//require striked payoff
	boost::shared_ptr<StrikedTypePayoff> payoff =
		boost::dynamic_pointer_cast<StrikedTypePayoff>(arguments_.payoff);
	QL_REQUIRE(payoff, "non-striked payoff given");

	//set future maturity - check if it is VanillaOption or VanillaFutureOption
	arguments_.futureExpiry == Null<Date>() ?
		this->process_->setFutureMaturity(arguments_.exercise->lastDate()) :
		this->process_->setFutureMaturity(arguments_.futureExpiry);

	//parameters for black calculator
	Real variance =
		process_->logReturnVariance(0, process_->time(arguments_.exercise->lastDate()));
	DiscountFactor riskFreeDiscount =
		discountCurve_->discount(arguments_.exercise->lastDate());
	Real spot = process_->spot()->value();
	QL_REQUIRE(spot > 0.0, "negative or null underlying given");
	Real forwardPrice = process_->initialFuturePrice();

	BlackCalculator black(payoff, forwardPrice, std::sqrt(variance),
		riskFreeDiscount);

	// price and greeks
	results_.value = black.value();
	results_.delta = black.delta(spot);
	results_.deltaForward = black.deltaForward();
	results_.elasticity = black.elasticity(spot);
	results_.gamma = black.gamma(spot);

	DayCounter rfdc = discountCurve_->dayCounter();
	DayCounter divdc = process_->dividendTS()->dayCounter();
	Time t = rfdc.yearFraction(discountCurve_->referenceDate(),
		arguments_.exercise->lastDate());
	results_.rho = black.rho(t);

	t = divdc.yearFraction(process_->dividendTS()->referenceDate(),
		arguments_.exercise->lastDate());
	results_.dividendRho = black.dividendRho(t);

	results_.vega = black.vega(t);

	try {
		results_.theta = black.theta(spot, t);
		results_.thetaPerDay =
			black.thetaPerDay(spot, t);
	}
	catch (Error&) {
		results_.theta = Null<Real>();
		results_.thetaPerDay = Null<Real>();
	}

	results_.strikeSensitivity = black.strikeSensitivity();
	results_.itmCashProbability = black.itmCashProbability();
}

/*inspector process*/
const boost::shared_ptr<ForwardSchwartzSmithProcess> ForwardSchwartzSmithAnalyticEuropeanEngine::process() const
{
	return this->process_;
}

/*inspector discountCurve*/
const Handle<YieldTermStructure> ForwardSchwartzSmithAnalyticEuropeanEngine::discountCurve() const
{
	return this->discountCurve_;
}