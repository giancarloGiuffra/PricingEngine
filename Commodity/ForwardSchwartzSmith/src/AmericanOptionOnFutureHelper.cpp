#include <Commodity\ForwardSchwartzSmith\include\AmericanOptionOnFutureHelper.h>

#include <ql\errors.hpp>
#include <ql\exercise.hpp>

/*constructor*/
AmericanOptionOnFutureHelper::AmericanOptionOnFutureHelper(
	const boost::shared_ptr<VanillaOptionFuture>& option,
	const Handle<Quote>& price,
	const Handle<YieldTermStructure>& discountCurve,
	CalibrationHelper::CalibrationErrorType errorType)
	: CalibrationHelper(price, discountCurve, errorType),
	option_(option)
{
	
	//Intended for american options
	boost::shared_ptr<AmericanExercise> exercise =
		boost::dynamic_pointer_cast<AmericanExercise>(option->exercise());
	QL_REQUIRE(exercise, "Option is not American!");
	
	/*NB: The CalibrationHelper interface assumes market prices are quoted in terms of volatility,
	that's why there is a field  Handle<Quote> volatility_ and a pure virtual method blackPrice that updates the marketValue_
	field.
	The implementation in this class assumes that market prices are quoted in currency, which is the reason for naming
	the quote "price".*/
	this->marketValue_ = this->blackPrice(price->value());
}

/*model value*/
Real AmericanOptionOnFutureHelper::modelValue() const
{
	option_->setPricingEngine(engine_);
	return option_->NPV();
}

/*black price - updates marketValue_*/
Real AmericanOptionOnFutureHelper::blackPrice(Real volatility) const
{
	/*NB: The implementation in this class assumes that market prices are quoted in currency.
	See http://www.implementingquantlib.com/2013/08/chapter-5-part-2-on-n-example.html */
	return volatility;
}

/*add times to - not used, necessary only for tree-based models*/
void AmericanOptionOnFutureHelper::addTimesTo(std::list<Time>&) const
{}

/*option*/
boost::shared_ptr<VanillaOptionFuture> AmericanOptionOnFutureHelper::option() const
{
	return option_;
}