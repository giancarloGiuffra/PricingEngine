#include <Commodity\ForwardSchwartzSmith\include\MCAmericanForwardSchwartzSmithEngine.h>

/*constructor*/
AmericanMultiPathPricer::AmericanMultiPathPricer(
	const boost::shared_ptr<Payoff>& payoff,
	Size dim,
	Size polynomOrder,
	LsmBasisSystem::PolynomType polynomType)
	: scalingValue_(1.0),
	dim_(dim),
	payoff_(payoff),
	v_(LsmBasisSystem::multiPathBasisSystem(dim,polynomOrder,polynomType))
{
	QL_REQUIRE(polynomType == LsmBasisSystem::Monomial
		|| polynomType == LsmBasisSystem::Laguerre
		|| polynomType == LsmBasisSystem::Hermite
		|| polynomType == LsmBasisSystem::Hyperbolic
		|| polynomType == LsmBasisSystem::Chebyshev2nd,
		"insufficient polynom type");

	// the payoff gives an additional value
	v_.push_back(boost::bind(&AmericanMultiPathPricer::payoff, this, _1));

	const boost::shared_ptr<StrikedTypePayoff> strikePayoff
		= boost::dynamic_pointer_cast<StrikedTypePayoff>(payoff_);

	if (strikePayoff) {
		// FLOATING_POINT_EXCEPTION
		scalingValue_ /= strikePayoff->strike();
	}
}

/*payoff*/
Real AmericanMultiPathPricer::payoff(Array state) const {
	return (*payoff_)(state[0] / scalingValue_);
}

/*operator()*/
Real AmericanMultiPathPricer::operator()(const MultiPath& path, Size t) const {
	return payoff(state(path, t));
}

/*state*/
Array AmericanMultiPathPricer::state(const MultiPath& path, Size t) const {
	// scale values of the underlying
	// to increase numerical stability
	Array a(1, path[0][t] * scalingValue_);
	return a;
}

/*basis system*/
std::vector<boost::function1<Real, Array> >
AmericanMultiPathPricer::basisSystem() const {
	return v_;
}