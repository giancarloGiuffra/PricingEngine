#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithBaroneAdesiWhaleyEngine.h>

#include <ql\math\distributions\normaldistribution.hpp>
#include <ql/math/comparison.hpp>
#include <ql/pricingengines/blackformula.hpp>
#include <ql/pricingengines/blackcalculator.hpp>
#include <ql/exercise.hpp>

/*constructor*/
ForwardSchwartzSmithBaroneAdesiWhaleyEngine::ForwardSchwartzSmithBaroneAdesiWhaleyEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve)
	: process_(process), discountCurve_(discountCurve)
{
	registerWith(process_);
	registerWith(discountCurve_);
}

/*constructor*/
ForwardSchwartzSmithBaroneAdesiWhaleyEngine::ForwardSchwartzSmithBaroneAdesiWhaleyEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process)
	: ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, process->riskFreeTS())
{}

/*critical price - free boundary point*/
Real ForwardSchwartzSmithBaroneAdesiWhaleyEngine::criticalPrice(
	const boost::shared_ptr<StrikedTypePayoff>& payoff,
	DiscountFactor riskFreeDiscount,
	Real variance,
	Real tolerance)
{
	// Calculation of seed value, Si
	Real dividendDiscount = riskFreeDiscount; //case when underlying is the forward, i.e. b == 0
	Real n = 2.0*std::log(dividendDiscount / riskFreeDiscount) / (variance);
	Real m = -2.0*std::log(riskFreeDiscount) / (variance);
	Real bT = std::log(dividendDiscount / riskFreeDiscount);

	Real qu, Su, h, Si;
	switch (payoff->optionType()) {
	case Option::Call:
		qu = (-(n - 1.0) + std::sqrt(((n - 1.0)*(n - 1.0)) + 4.0*m)) / 2.0;
		Su = payoff->strike() / (1.0 - 1.0 / qu);
		h = -(bT + 2.0*std::sqrt(variance)) * payoff->strike() /
			(Su - payoff->strike());
		Si = payoff->strike() + (Su - payoff->strike()) *
			(1.0 - std::exp(h));
		break;
	case Option::Put:
		qu = (-(n - 1.0) - std::sqrt(((n - 1.0)*(n - 1.0)) + 4.0*m)) / 2.0;
		Su = payoff->strike() / (1.0 - 1.0 / qu);
		h = (bT - 2.0*std::sqrt(variance)) * payoff->strike() /
			(payoff->strike() - Su);
		Si = Su + (payoff->strike() - Su) * std::exp(h);
		break;
	default:
		QL_FAIL("unknown option type");
	}

	// Newton Raphson algorithm for finding critical price Si
	Real Q, LHS, RHS, bi;
	Real forwardSi = Si * dividendDiscount / riskFreeDiscount; //case when underlying is the forward, i.e. b == 0
	Real d1 = (std::log(forwardSi / payoff->strike()) + 0.5*variance) /
		std::sqrt(variance);
	CumulativeNormalDistribution cumNormalDist;
	Real K = (!close(riskFreeDiscount, 1.0, 1000))
		? -2.0*std::log(riskFreeDiscount)
		/ (variance*(1.0 - riskFreeDiscount))
		: 2.0 / variance; // K here refers to M/K in the original paper
	Real temp = blackFormula(payoff->optionType(), payoff->strike(),
		forwardSi, std::sqrt(variance))*riskFreeDiscount; //price of the european option
	switch (payoff->optionType()) {
	case Option::Call:
		Q = (-(n - 1.0) + std::sqrt(((n - 1.0)*(n - 1.0)) + 4 * K)) / 2;
		LHS = Si - payoff->strike();
		RHS = temp + (1 - dividendDiscount * cumNormalDist(d1)) * Si / Q;
		bi = dividendDiscount * cumNormalDist(d1) * (1 - 1 / Q) +
			(1 - dividendDiscount *
			cumNormalDist.derivative(d1) / std::sqrt(variance)) / Q; //slope
		while (std::fabs(LHS - RHS) / payoff->strike() > tolerance) {
			Si = (payoff->strike() + RHS - bi * Si) / (1 - bi);
			forwardSi = Si * dividendDiscount / riskFreeDiscount; //case when underlying is the forward, i.e. b == 0
			d1 = (std::log(forwardSi / payoff->strike()) + 0.5*variance)
				/ std::sqrt(variance);
			LHS = Si - payoff->strike();
			Real temp2 = blackFormula(payoff->optionType(), payoff->strike(),
				forwardSi, std::sqrt(variance))*riskFreeDiscount;
			RHS = temp2 + (1 - dividendDiscount * cumNormalDist(d1)) * Si / Q;
			bi = dividendDiscount * cumNormalDist(d1) * (1 - 1 / Q)
				+ (1 - dividendDiscount *
				cumNormalDist.derivative(d1) / std::sqrt(variance))
				/ Q;
		}
		break;
	case Option::Put:
		Q = (-(n - 1.0) - std::sqrt(((n - 1.0)*(n - 1.0)) + 4 * K)) / 2;
		LHS = payoff->strike() - Si;
		RHS = temp - (1 - dividendDiscount * cumNormalDist(-d1)) * Si / Q;
		bi = -dividendDiscount * cumNormalDist(-d1) * (1 - 1 / Q)
			- (1 + dividendDiscount * cumNormalDist.derivative(-d1)
			/ std::sqrt(variance)) / Q;
		while (std::fabs(LHS - RHS) / payoff->strike() > tolerance) {
			Si = (payoff->strike() - RHS + bi * Si) / (1 + bi);
			forwardSi = Si * dividendDiscount / riskFreeDiscount; //case when underlying is the forward, i.e. b == 0
			d1 = (std::log(forwardSi / payoff->strike()) + 0.5*variance)
				/ std::sqrt(variance);
			LHS = payoff->strike() - Si;
			Real temp2 = blackFormula(payoff->optionType(), payoff->strike(),
				forwardSi, std::sqrt(variance))*riskFreeDiscount;
			RHS = temp2 - (1 - dividendDiscount * cumNormalDist(-d1)) * Si / Q;
			bi = -dividendDiscount * cumNormalDist(-d1) * (1 - 1 / Q)
				- (1 + dividendDiscount * cumNormalDist.derivative(-d1)
				/ std::sqrt(variance)) / Q;
		}
		break;
	default:
		QL_FAIL("unknown option type");
	}

	return Si;
}

/*calculate*/
void ForwardSchwartzSmithBaroneAdesiWhaleyEngine::calculate() const
{
	//checks and setup
	QL_REQUIRE(arguments_.exercise->type() == Exercise::American,
		"not an American Option");
	boost::shared_ptr<AmericanExercise> ex =
		boost::dynamic_pointer_cast<AmericanExercise>(arguments_.exercise);
	QL_REQUIRE(ex, "non-American exercise given");
	QL_REQUIRE(!ex->payoffAtExpiry(),
		"payoff at expiry not handled");

	boost::shared_ptr<StrikedTypePayoff> payoff =
		boost::dynamic_pointer_cast<StrikedTypePayoff>(arguments_.payoff);
	QL_REQUIRE(payoff, "non-striked payoff given");

	process_->setFutureMaturity(arguments_.futureExpiry); //set future maturity
	Real variance = process_->logReturnVariance(0,process_->time(ex->lastDate()));
	DiscountFactor riskFreeDiscount = discountCurve_->discount(
		ex->lastDate());
	DiscountFactor dividendDiscount = riskFreeDiscount; //case when underlying is the forward, i.e. b == 0
	Real spot = process_->spot()->value();
	QL_REQUIRE(spot > 0.0, "negative or null underlying given");
	Real forwardPrice = process_->initialFuturePrice();
	BlackCalculator black(payoff, forwardPrice, std::sqrt(variance),
		riskFreeDiscount);

	//null variance case - reference date of curves equal to maturity of option
	if (close(variance, 0.0, 1000))
	{
		//price equal to payoff
		results_.value = payoff->operator()(forwardPrice);
		return;
	}

	if (dividendDiscount >= 1.0 && payoff->optionType() == Option::Call) {
		// early exercise never optimal, thus european price
		results_.value = black.value();
		results_.delta = black.delta(spot);
		results_.deltaForward = black.deltaForward();
		results_.elasticity = black.elasticity(spot);
		results_.gamma = black.gamma(spot);

		DayCounter rfdc = discountCurve_->dayCounter();
		DayCounter divdc = process_->dividendTS()->dayCounter();
		DayCounter voldc = process_->dividendTS()->dayCounter();
		Time t =
			rfdc.yearFraction(discountCurve_->referenceDate(),
			arguments_.exercise->lastDate());
		results_.rho = black.rho(t);

		t = divdc.yearFraction(process_->dividendTS()->referenceDate(),
			arguments_.exercise->lastDate());
		results_.dividendRho = black.dividendRho(t);

		t = voldc.yearFraction(process_->dividendTS()->referenceDate(),
			arguments_.exercise->lastDate());
		results_.vega = black.vega(t);
		results_.theta = black.theta(spot, t);
		results_.thetaPerDay = black.thetaPerDay(spot, t);

		results_.strikeSensitivity = black.strikeSensitivity();
		results_.itmCashProbability = black.itmCashProbability();
	}
	else {
		// early exercise can be optimal
		CumulativeNormalDistribution cumNormalDist;
		Real tolerance = 1e-6;
		Real Sk = criticalPrice(payoff, riskFreeDiscount,
			 variance, tolerance);
		Real forwardSk = Sk * dividendDiscount / riskFreeDiscount; //Sk is already the forward critical price by construction
		Real d1 = (std::log(forwardSk / payoff->strike()) + 0.5*variance)
			/ std::sqrt(variance);
		Real n = 2.0*std::log(dividendDiscount / riskFreeDiscount) / variance;
		Real K = (!close(riskFreeDiscount, 1.0, 1000))
			? -2.0*std::log(riskFreeDiscount)
			/ (variance*(1.0 - riskFreeDiscount))
			: 2.0 / variance; //K here referes to M/K in the paper
		Real Q, a;
		switch (payoff->optionType()) {
		case Option::Call:
			Q = (-(n - 1.0) + std::sqrt(((n - 1.0)*(n - 1.0)) + 4.0*K)) / 2.0;
			a = (Sk / Q) * (1.0 - dividendDiscount * cumNormalDist(d1));
			if (forwardPrice<Sk) { //case when underlying is the forward, i.e. b == 0
				results_.value = black.value() +
					a * std::pow((forwardPrice / Sk), Q);
			}
			else {
				results_.value = forwardPrice - payoff->strike();
			}
			break;
		case Option::Put:
			Q = (-(n - 1.0) - std::sqrt(((n - 1.0)*(n - 1.0)) + 4.0*K)) / 2.0;
			a = -(Sk / Q) *
				(1.0 - dividendDiscount * cumNormalDist(-d1));
			if (forwardPrice>Sk) { //case when underlying is the forward, i.e. b == 0
				results_.value = black.value() +
					a * std::pow((forwardPrice / Sk), Q);
			}
			else {
				results_.value = payoff->strike() - forwardPrice;
			}
			break;
		default:
			QL_FAIL("unknown option type");
		}
	} // end of "early exercise can be optimal"
}

/*inspector process*/
const boost::shared_ptr<ForwardSchwartzSmithProcess> ForwardSchwartzSmithBaroneAdesiWhaleyEngine::process() const
{
	return this->process_;
}

/*inspector discountCurve*/
const Handle<YieldTermStructure> ForwardSchwartzSmithBaroneAdesiWhaleyEngine::discountCurve() const
{
	return this->discountCurve_;
}
