#include "ForwardSchwartzSmithProcess.h"

#include <ql\errors.hpp>
#include <ql/math/comparison.hpp>
#include <Utilities\include\DividendUtilities.h>

/*constructor - dividends*/
ForwardSchwartzSmithProcess::ForwardSchwartzSmithProcess(
	const Handle<Quote> spot,
	const Handle<YieldTermStructure> riskFreeTS,
	const Handle<YieldTermStructure> dividendTS,
	Real sigma_S,
	Real sigma_L,
	Real beta,
	Real rho) 
	: spot_(spot), riskFreeTS_(riskFreeTS), dividendTS_(dividendTS), sigma_S_(sigma_S), sigma_L_(sigma_L), beta_(beta), rho_(rho)
{
	QL_REQUIRE(spot_->value() > 0, "spot price " << spot_->value() << " must be > 0!");
	QL_REQUIRE(sigma_S_ > 0, "sigma_S " << sigma_S_ << " must be > 0!");
	QL_REQUIRE(sigma_L_ > 0, "sigma_L " << sigma_L_ << " must be > 0!");
	QL_REQUIRE(beta_ > 0, "beta " << beta_ << " must be > 0!");
	QL_REQUIRE(-1 <= rho_ && rho_ <= 1, "rho " << rho_ << " must be a correlation!");

	registerWith(spot_);
	registerWith(dividendTS_);
	registerWith(riskFreeTS_);
}

/*constructor - futures*/
ForwardSchwartzSmithProcess::ForwardSchwartzSmithProcess(
	const Handle<Quote> spot,
	const Handle<YieldTermStructure> riskFreeTS,
	std::vector<std::shared_ptr<FutureQuote>> futures,
	Real sigma_S,
	Real sigma_L,
	Real beta,
	Real rho
	)
	: ForwardSchwartzSmithProcess(spot, riskFreeTS,
	Handle<YieldTermStructure>(utilities::stripDividendsFromFutures(spot, riskFreeTS, futures)),
	sigma_S, sigma_L, beta, rho)
{}

/*destructor*/
ForwardSchwartzSmithProcess::~ForwardSchwartzSmithProcess()
{}

/*sets the future maturity*/
void ForwardSchwartzSmithProcess::setFutureMaturity(const Date& maturity)
{
	QL_REQUIRE(maturity > dividendTS_->referenceDate(), "Future Maturity " << maturity << 
		" must be equal or later than reference date " << dividendTS_->referenceDate() << " !");
	this->futureMaturity_ = maturity;
	this->T_ = dividendTS_->timeFromReference(this->futureMaturity_);
}

/*inspector futureMaturity*/
Date ForwardSchwartzSmithProcess::futureMaturity() const
{
	return this->futureMaturity_;
}

/*inspector spot*/
const Handle<Quote>& ForwardSchwartzSmithProcess::spot() const
{
	return this->spot_;
}

/*inspector risk free TS*/
const Handle<YieldTermStructure>& ForwardSchwartzSmithProcess::riskFreeTS() const
{
	return this->riskFreeTS_;
}

/*inspector futures*/
const Handle<YieldTermStructure>& ForwardSchwartzSmithProcess::dividendTS() const
{
	return this->dividendTS_;
}

/*inspector sigma_S*/
Real ForwardSchwartzSmithProcess::sigma_S() const
{
	return this->sigma_S_;
}

/*inspector sigma_L*/
Real ForwardSchwartzSmithProcess::sigma_L() const
{
	return this->sigma_L_;
}

/*inspector beta*/
Real ForwardSchwartzSmithProcess::beta() const
{
	return this->beta_;
}

/*inspector rho*/
Real ForwardSchwartzSmithProcess::rho() const
{
	return this->rho_;
}

/*******************************StochasticProcess Interface******************************************/

/*size*/
Size ForwardSchwartzSmithProcess::size() const
{
	return 1;
}

/*factors*/
Size ForwardSchwartzSmithProcess::factors() const
{
	return 2;
}

/*initial values*/
Disposable<Array> ForwardSchwartzSmithProcess::initialValues() const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	Array a(1, initialFuturePrice());
	return a;
}

/*drift*/
Real ForwardSchwartzSmithProcess::drift(Time t, Real x) const
{
	return 0;
}

/*diffusion*/
Disposable<Matrix> ForwardSchwartzSmithProcess::diffusion(Time t, Real x) const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE(T_ >= t, "Future Maturity " << T_ << " has to be equal or greater than time " << t << ".");
	Matrix m(1, 2);
	Real eBeta = exp(-beta_*(T_ - t));
	m[0][0] = x*sigma_S_*eBeta + rho_*sigma_L_*(1 - eBeta);
	m[0][1] = x*sigma_L_*(1 - eBeta)*sqrt(1 - rho_*rho_);
	return m;
}

/*expectation*/
Real ForwardSchwartzSmithProcess::expectation(Time t0, Real x0, Time dt) const
{
	QL_REQUIRE( T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE( t0+dt <= T_, "Future Maturity " << T_ << " has to be equal or greater than t0 + dt " << t0+dt << ".");
	return x0; //future prices are martingales.
}

/*standard deviation*/
Real ForwardSchwartzSmithProcess::stdDeviation(Time t0, Real x0, Time dt) const
{
	return sqrt(covariance(t0, x0, dt));
}

/*covariance*/
Real ForwardSchwartzSmithProcess::covariance(Time t0, Real x0, Time dt) const
{
	Real sigma2 = integralSigmaS(t0, t0 + dt) + integralSigmaL(t0, t0 + dt) + integralRho(t0, t0 + dt);
	return x0*x0*(exp(sigma2)-1);
}

/*evolve*/
Real ForwardSchwartzSmithProcess::evolve(Time t0, Real x0, Time dt, const Array& dw) const
{
	QL_REQUIRE( dw.size() == 2, "gaussian variable's dimension must be 2 (it's 2 factor model), and not " << dw.size() << ".");
	//integrals
	Real integralSigmaS_ = integralSigmaS(t0, t0 + dt);
	Real integralSigmaL_ = integralSigmaL(t0, t0 + dt);
	Real integralRho_ = integralRho(t0, t0 + dt);
	//mean
	Real mu = -0.5*(integralSigmaS_ + integralSigmaL_ + integralRho_);
	/*variables from square root matrix R:
	Rdw = [X Y] where V[X] = integralSigmaS_, V[Y] = integralSigmaL_, Cov[X,Y] = 0.5*integralRho_ */
	Real s = sqrt(integralSigmaS_*integralSigmaL_ - 0.25*integralRho_*integralRho_);
	Real t = sqrt(integralSigmaS_ + integralSigmaL_+2*s);

	return x0*exp(mu + 1 / t*((integralSigmaS_ + s + 0.5*integralRho_)*dw[0] + (integralSigmaL_ + s + 0.5*integralRho_)*dw[1]));
}

/*apply*/
Real ForwardSchwartzSmithProcess::apply(Real x0, Real dx) const
{
	return x0 + dx;
}

/***************************Private StochasticProcess Interface**********************************/

/*drift*/
Disposable<Array> ForwardSchwartzSmithProcess::drift(Time t, const Array& x) const
{
	QL_REQUIRE(x.size() == 1, "1-D array required");
	Array a(1, drift(t, x[0]));
	return a;

}

/*diffusion*/
Disposable<Matrix> ForwardSchwartzSmithProcess::diffusion(Time t, const Array& x) const
{
	QL_REQUIRE(x.size() == 1, "1-D array required");
	return diffusion(t, x[0]);
}

/*expectation*/
Disposable<Array> ForwardSchwartzSmithProcess::expectation(Time t0, const Array& x0, Time dt) const
{
	QL_REQUIRE(x0.size() == 1, "1-D array required");
	Array a(1, expectation(t0, x0[0], dt));
	return a;
}

/*standard deviation*/
Disposable<Matrix> ForwardSchwartzSmithProcess::stdDeviation(Time t0, const Array& x0, Time dt) const
{
	QL_REQUIRE(x0.size() == 1, "1-D array required");
	Matrix m(1, 1, stdDeviation(t0, x0[0], dt));
	return m;
}

/*covariance*/
Disposable<Matrix> ForwardSchwartzSmithProcess::covariance(Time t0, const Array& x0, Time dt) const
{
	QL_REQUIRE(x0.size() == 1, "1-D array required");
	Matrix m(1, 1, covariance(t0, x0[0], dt));
	return m;
}

/*evolve*/
Disposable<Array> ForwardSchwartzSmithProcess::evolve(Time t0, const Array& x0, Time dt, const Array& dw) const
{
	QL_REQUIRE(x0.size() == 1, "1-D array required");
	Array a(1, evolve(t0, x0[0], dt, dw));
	return a;
}

/*apply*/
Disposable<Array> ForwardSchwartzSmithProcess::apply(const Array& x0, const Array& dx) const
{
	QL_REQUIRE(x0.size() == 1, "x0: 1-D array required");
	QL_REQUIRE(dx.size() == 1, "dx: 1-D array required");
	Array a(1, apply(x0[0], dx[0]));
	return a;
}

/*time*/
Time ForwardSchwartzSmithProcess::time(const Date& date) const
{
	return dividendTS_->timeFromReference(date);
}

/*************************************************************************************************/

/*************************************Observer Interface******************************************/
/*update*/
void ForwardSchwartzSmithProcess::update()
{
	notifyObservers();
}
/*************************************************************************************************/

/*logreturn variance*/
Real ForwardSchwartzSmithProcess::logReturnVariance(Time t, Time S) const
{
	return integralSigmaS(t, S) + integralSigmaL(t, S) + integralRho(t, S);
}

/*logreturn standard deviation*/
Real ForwardSchwartzSmithProcess::logReturnStdDeviation(Time t, Time S) const
{
	return sqrt(logReturnVariance(t,S));
}

/*logreturn mean*/
Real ForwardSchwartzSmithProcess::logReturnMean(Time t, Time S) const
{
	return -0.5*logReturnVariance(t, S);
}

/*instantaneous variance*/
Real ForwardSchwartzSmithProcess::instantaneousVariance(Time t) const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE(T_ >= t, "Future Maturity " << T_ << " has to be equal or greater than time " << t << ".");
	Real eBeta = exp(-beta_*(T_ - t));
	Real e2Beta = exp(-2 * beta_*(T_ - t));
	return sigma_S_*sigma_S_*e2Beta +
		2 * rho_*sigma_S_*sigma_L_*eBeta*(1 - eBeta) +
		sigma_L_*sigma_L_*(1 - 2 * eBeta + e2Beta);
}

/*initial future price*/
Real ForwardSchwartzSmithProcess::initialFuturePrice() const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	return spot_->value() * dividendTS_->discount(T_, true) / riskFreeTS_->discount(T_, true); /*allow extrapolation to simulate future prices dynamics after
	last maturity*/
}

/* Method to calculate the integral coming from the brownian motion associated to sigma_S.*/
Real ForwardSchwartzSmithProcess::integralSigmaS(Time t, Time S) const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE(t <= S, "extremes of integral must be ordered: t <= S.");
	QL_REQUIRE(S <= T_, "Future Maturity " << T_ << " has to be equal or greater than S " << S << ".");
	return (!close(beta_, 0.0, 1000)) ?
		0.5*sigma_S_*sigma_S_ / beta_*(exp(-2 * beta_*(T_ - S)) - exp(-2 * beta_*(T_ - t))) :
		sigma_S_*sigma_S_*(S - t); //taylor al primo ordine
}

/* Method to calculate the integral coming from the brownian motion associated to sigma_L.*/
Real ForwardSchwartzSmithProcess::integralSigmaL(Time t, Time S) const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE(t <= S, "extremes of integral must be ordered: t <= S.");
	QL_REQUIRE(S <= T_, "Future Maturity " << T_ << " has to be equal or greater than S " << S << ".");
	return (!close(beta_, 0.0, 1000)) ?
		sigma_L_*sigma_L_ / beta_*(
		beta_*(S - t) -
		2 * (exp(-beta_*(T_ - S)) - exp(-beta_*(T_ - t))) +
		0.5*(exp(-2 * beta_*(T_ - S)) - exp(-2 * beta_*(T_ - t)))
		) :
		2 * sigma_L_*sigma_L_ * beta_* beta_*(std::pow((T_ - t), 3) - std::pow((T_ - S), 3)); //taylor al terzo ordine
}

/* Method to calculate the integral coming from correlation between the two brownian motions.*/
Real ForwardSchwartzSmithProcess::integralRho(Time t, Time S) const
{
	QL_REQUIRE(T_ > 0, "Future Maturity hasn't been set.");
	QL_REQUIRE(t <= S, "extremes of integral must be ordered: t <= S.");
	QL_REQUIRE(S <= T_, "Future Maturity " << T_ << " has to be equal or greater than S " << S << ".");
	return (!close(beta_, 0.0, 1000)) ?
		2 * rho_*sigma_S_*sigma_L_ / beta_*(
		(exp(-beta_*(T_ - S)) - exp(-beta_*(T_ - t))) -
		0.5*(exp(-2 * beta_*(T_ - S)) - exp(-2 * beta_*(T_ - t)))
		) :
		2 * rho_*sigma_S_*sigma_L_*beta_*(S - t)*(2 * T_ - S - t); //taylor al secondo ordine
}