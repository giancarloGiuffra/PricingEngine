#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithModel.h>

/*constructor*/
ForwardSchwartzSmithModel::ForwardSchwartzSmithModel(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve)
	: CalibratedModel(4), process_(process), discountCurve_(discountCurve)
{
	//build parameters to calibrate, initial values taken from the process
	arguments_[0] = ConstantParameter(process->sigma_S(),
		PositiveConstraint());
	arguments_[1] = ConstantParameter(process->sigma_L(),
		PositiveConstraint());
	arguments_[2] = ConstantParameter(process->beta(),
		PositiveConstraint());
	arguments_[3] = ConstantParameter(process->rho(),
		BoundaryConstraint(-1.0, 1.0));

	//update inner process
	generateArguments();

	//register with observables
	registerWith(process_);
	registerWith(discountCurve_);
}

/*constructor*/
ForwardSchwartzSmithModel::ForwardSchwartzSmithModel(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process)
	: ForwardSchwartzSmithModel(process, process->riskFreeTS())
{}

/*sigma_S*/
Real ForwardSchwartzSmithModel::sigma_S() const
{
	return this->arguments_[0](0.0);
}

/*sigma_L*/
Real ForwardSchwartzSmithModel::sigma_L() const
{
	return this->arguments_[1](0.0);
}

/*beta*/
Real ForwardSchwartzSmithModel::beta() const
{
	return this->arguments_[2](0.0);
}

/*rho*/
Real ForwardSchwartzSmithModel::rho() const
{
	return this->arguments_[3](0.0);
}

/*process*/
boost::shared_ptr<ForwardSchwartzSmithProcess> ForwardSchwartzSmithModel::process() const
{
	return this->process_;
}

/*generate arguments*/
void ForwardSchwartzSmithModel::generateArguments()
{
	/*this class is friend of ForwardSchwartzSmithProcess*/
	process_->sigma_S_ = sigma_S();
	process_->sigma_L_ = sigma_L();
	process_->beta_ = beta();
	process_->rho_ = rho();
}