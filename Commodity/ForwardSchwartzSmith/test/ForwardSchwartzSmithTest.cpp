#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>
#include <Commodity\ForwardSchwartzSmith\include\MCEuropeanForwardSchwartzSmithEngine.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithAnalyticEuropeanEngine.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithBaroneAdesiWhaleyEngine.h>
#include <Commodity\ForwardSchwartzSmith\include\MCAmericanForwardSchwartzSmithEngine.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithModel.h>
#include <Commodity\ForwardSchwartzSmith\include\AmericanOptionOnFutureHelper.h>
#include <Instruments\include\VanillaOptionFuture.h>

#include <ql\termstructures\yield\zerocurve.hpp>
#include <ql\quotes\simplequote.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\time\calendars\target.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\range\irange.hpp>
#include <ql\math\interpolations\loginterpolation.hpp>
#include <ql\math\interpolations\backwardflatinterpolation.hpp>
#include <ql\option.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\instruments\payoffs.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\pointer_cast.hpp>
#include <ql\math\distributions\normaldistribution.hpp>
#include <ql\math\optimization\levenbergmarquardt.hpp>
#include <ql\math\optimization\simplex.hpp>

#include <functional>
#include <iomanip>
#include <io.h>

#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::Le;
#include <gtest\gtest.h>
  using ::testing::Test;

//custom matchers
MATCHER_P2(IsBetween, imin, imax, std::string(negation ? "isn't" : "is") + " between " + std::to_string(imin) + " and " + std::to_string(imax))
{
	return imin <= arg && arg <= imax;
}

//test class
namespace ForwardSchwartzSmith
{
namespace testing
{
    class ForwardSchwartzSmithTest : public Test
    {
    protected:
        ForwardSchwartzSmithTest(){}
        ~ForwardSchwartzSmithTest(){}
		
		virtual void SetUp(){
			//spot
			boost::shared_ptr<Quote> quote(new SimpleQuote(46.66));
			spot_.linkTo(quote);
			//futures
			std::vector<Rate> yields;
			futures_ = {quote->value(), 46.71, 47.22, 47.94, 48.61, 49.14, 49.57, 50.02, 50.33, 50.75,
			50.94, 50.83, 52.47};
			Date today = Date::todaysDate() + Period(2,Days);
			DayCounter dc = Actual365Fixed();
			//risk free rate
			Rate riskFreeRate = 0.01;
			riskFreeTS_.linkTo(
				boost::shared_ptr<YieldTermStructure>(
				new FlatForward(today, riskFreeRate, dc)));
			Rate q;
			for (auto i : boost::irange(0, (int)futures_.size()))
			{
				dates_.push_back(today + Period(i, Months));
				q = (i == 0) ? 0 : -1*log(futures_[i] / spot_->value() * riskFreeTS_->discount(dates_.back(), true)) 
					/ dc.yearFraction(today, dates_.back());
				yields.push_back(q);
			}
			boost::shared_ptr<YieldTermStructure> dividends(new InterpolatedZeroCurve<BackwardFlat>(dates_, yields, dc, TARGET()));
			dividendTS_.linkTo(dividends);
			//options - prices given, 3 strikes per maturity around future price, maturities up to 4 months, future expiry + 1 month
			prices_ = {1.88, 1.63, 1.27, 3.10, 2.45, 2.25, 3.30, 3.29, 3.03, 4.05, 3.80, 3.56};
			int nro_strikes = 3;
			double delta_strike = 0.5;
			strikes_ = { 46.71 - delta_strike, 46.71, 46.71 + delta_strike ,
				47.22 - delta_strike, 47.22, 47.22 + delta_strike ,
				47.94 - delta_strike, 47.94, 47.94 + delta_strike ,
				48.61 - delta_strike, 48.61, 48.61 + delta_strike };
			for (auto i : boost::irange(0, (int)prices_.size()))
			{
				boost::shared_ptr<StrikedTypePayoff> payoff = boost::shared_ptr<StrikedTypePayoff>(
					new PlainVanillaPayoff(Option::Type::Call, strikes_[i]));
				boost::shared_ptr<Exercise> exercise = boost::shared_ptr<Exercise>(
					new AmericanExercise(today, today + Period(1 + i / nro_strikes, Months)));
				options_.push_back(boost::shared_ptr<VanillaOptionFuture>(
					new VanillaOptionFuture(
					payoff,
					exercise,
					exercise->lastDate() + Period(1, Months)
					)));
			}
		}
        virtual void TearDown(){}

		RelinkableHandle<Quote> spot_;
		std::vector<Date> dates_;
		std::vector<double> futures_;
		std::vector<double> strikes_;
		std::vector<double> prices_;
		std::vector<boost::shared_ptr<VanillaOptionFuture>> options_;
		RelinkableHandle<YieldTermStructure> dividendTS_;
		RelinkableHandle<YieldTermStructure> riskFreeTS_;
		Real sigma_S = 0.20;
		Real sigma_L = 0.20;
		Real beta = 0.05;
		Real rho = 0.5;
    };

	TEST_F(ForwardSchwartzSmithTest, SanityCheck_VanillaOptionFuture)
	{
		ASSERT_THAT(prices_.size(), Eq(options_.size()));
		for (auto i : boost::irange(0, (int)options_.size()))
		{
			std::cout << std::left << *options_[i];
			std::cout << std::setw(10) << std::right << prices_[i] << std::endl;
		}
	}

	TEST_F(ForwardSchwartzSmithTest, SanityCheck_FuturePricesDividendTS)
	{
		//future quotes
		std::vector<std::shared_ptr<FutureQuote>> futureQuotes;
		for (auto i : boost::irange(1, (int)futures_.size()))
		{
			std::ostringstream oss;
			oss << ::io::iso_date(dates_[i]);
			futureQuotes.push_back(
				std::shared_ptr<FutureQuote>(new FutureQuote("CO",oss.str(),"USD",futures_[i]))
				);
		}
		//process
		ForwardSchwartzSmithProcess process =
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, futureQuotes, sigma_S, sigma_L, beta, rho);
		
		Real tolerance = 1.0e-12;
		for (auto i : boost::irange(1, (int)futures_.size()))
		{
			process.setFutureMaturity(dates_[i]);
			ASSERT_THAT(std::abs(process.initialFuturePrice() - futures_[i]), Le(tolerance));
		}
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_ConstructorThrows)
	{
		Real sigma_S = -0.20;
		Real sigma_L = -0.20;
		Real beta = -0.05;
		Real spot = -10;
		Real rho = 2;
		ASSERT_THROW(ForwardSchwartzSmithProcess(
			Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(spot))), riskFreeTS_, dividendTS_, 0.10, 0.10, 0.02, 0.10),
			std::exception);
		ASSERT_THROW(ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, 0.10, 0.02, 0.10),
			std::exception);
		ASSERT_THROW(ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, 0.10, sigma_L, 0.02, 0.10),
			std::exception);
		ASSERT_THROW(ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, 0.10, 0.10, beta, 0.10),
			std::exception);
		ASSERT_THROW(ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, 0.10, 0.10, 0.02, rho),
			std::exception);
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_SetFutureMaturityThrows)
	{
		Date T = dividendTS_->referenceDate() - Period(1, Weeks);
		ForwardSchwartzSmithProcess process = 
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho);
		ASSERT_THROW(process.setFutureMaturity(T), std::exception);
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_Inspectors)
	{
		ForwardSchwartzSmithProcess process = 
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho);
		ASSERT_THAT(process.spot()->value(), Eq(spot_->value()));
		ASSERT_THAT(process.sigma_S(), Eq(sigma_S));
		ASSERT_THAT(process.sigma_L(), Eq(sigma_L));
		ASSERT_THAT(process.beta(), Eq(beta));
		ASSERT_THAT(process.rho(), Eq(rho));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_InitialFuturePrices)
	{
		ForwardSchwartzSmithProcess process = 
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho);
		Real tolerance = 1.0e-12;
		for (auto i : boost::irange(1, (int)futures_.size()))
		{
			process.setFutureMaturity(dates_[i]);
			ASSERT_THAT(std::abs( process.initialFuturePrice() - futures_[i]), Le(tolerance));
		}
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_DriftDiffusion)
	{
		ForwardSchwartzSmithProcess process = 
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho);
		for (auto date : dates_)
		{
			ASSERT_THAT(process.drift(dividendTS_->timeFromReference(date), spot_->value()), Eq(0.0));
		}
		
		Matrix m;
		Real first;
		Real second;
		Time t;
		Real x = spot_->value();
		Time T = dividendTS_->timeFromReference(dates_.back());
		process.setFutureMaturity(dates_.back());
		for (auto i : boost::irange(0, (int)dates_.size()))
		{
			t = dividendTS_->timeFromReference(dates_[i]);
			m = process.diffusion(t, x);
			ASSERT_THAT(m.rows(), Eq(1));
			ASSERT_THAT(m.columns(), Eq(2));
			first = x*process.sigma_S()*exp(-process.beta()*(T - t)) +
				process.rho()*process.sigma_L()*(1 - exp(-process.beta()*(T - t)));
			second = x*process.sigma_L()*(1 - exp(-process.beta()*(T - t)))*sqrt(1 - std::pow(process.rho(), 2));
			ASSERT_THAT(m[0][0], Eq(first));
			ASSERT_THAT(m[0][1], Eq(second));
		}
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_InstantaneousVariance)
	{
		ForwardSchwartzSmithProcess process = 
			ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho);
		Time t;
		Real instantaneousVariance;
		Time T = dividendTS_->timeFromReference(dates_.back());
		process.setFutureMaturity(dates_.back());
		for (auto i : boost::irange(0, (int)dates_.size()))
		{
			t = dividendTS_->timeFromReference(dates_[i]);
			instantaneousVariance = process.sigma_S()*process.sigma_S()*exp(-2 * process.beta()*(T - t)) +
				2 * process.rho()*process.sigma_S()*process.sigma_L()*exp(-process.beta()*(T - t))*(1 - exp(-process.beta()*(T - t))) +
				process.sigma_L()*process.sigma_L()*(1 - 2 * exp(-process.beta()*(T - t)) + exp(-2 * process.beta()*(T - t)));
			ASSERT_THAT(process.instantaneousVariance(t), Eq(instantaneousVariance));
		}
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithProcess_AnalyticVersusMC)
	{
		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//option
		Option::Type type(Option::Call);
		Real strike = spot_->value();
		Rate riskFreeRate = 0.01;
		Date maturity = dividendTS_->referenceDate() + Period(5, Years);
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(maturity));
		VanillaOptionFuture europeanOption(payoff, europeanExercise, europeanExercise->lastDate() + Period(1,Months));
		//pricing
		Size timeSteps = 1;
		Size mcSeed = 42;
		boost::shared_ptr<PricingEngine> mcengine;
		process->setFutureMaturity(europeanOption.futureExpiry());
		Real tolerance = 0.1;
		mcengine = MakeMCEuropeanForwardSchwartzSmithEngine<PseudoRandom>(process, flatTermStructure)
			.withSteps(timeSteps)
			.withAbsoluteTolerance(tolerance)
			.withMaxSamples(1000000)
			.withSeed(mcSeed);
		europeanOption.setPricingEngine(mcengine);
		//launch simulation
		europeanOption.NPV();
		//statistics
		SequenceStatisticsArrayInspectors statistics =
			boost::dynamic_pointer_cast<MCEuropeanForwardSchwartzSmithEngine<PseudoRandom, SequenceStatisticsArrayInspectors>>(mcengine)->sampleAccumulator();
		//tests - see path pricer used in engine for meaning of Array components
		//for standard deviations 1% relative variations are considered acceptable - MC tolerance guarantees only for the mean
		ASSERT_THAT(std::abs(statistics.mean()[1] - process->expectation(0, process->initialFuturePrice(), process->time(europeanExercise->lastDate()))), Le(tolerance));
		ASSERT_THAT(std::abs(statistics.mean()[2] - process->logReturnMean(0, process->time(europeanExercise->lastDate()))), Le(tolerance));
		ASSERT_THAT(std::max(process->stdDeviation(0, process->initialFuturePrice(), process->time(europeanExercise->lastDate())) / statistics.standardDeviation()[1],
			statistics.standardDeviation()[1] / process->stdDeviation(0, process->initialFuturePrice(), process->time(europeanExercise->lastDate()))), Le(1.01));
		ASSERT_THAT(std::max(process->logReturnStdDeviation(0, process->time(europeanExercise->lastDate())) / statistics.standardDeviation()[2],
			statistics.standardDeviation()[2] / process->logReturnStdDeviation(0, process->time(europeanExercise->lastDate()))), Le(1.01));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithAnalyticEuropeanEngine_VanillaOption)
	{
		
		//options
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(Option::Call, 100));
		Date expiry = Date::todaysDate() + Period(1, Years);
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(expiry));
		VanillaOptionFuture optFuture = VanillaOptionFuture(payoff, europeanExercise, expiry);
		VanillaOption opt = VanillaOption(payoff, europeanExercise);

		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//engine
		boost::shared_ptr<PricingEngine> analyticEngine(new ForwardSchwartzSmithAnalyticEuropeanEngine(process));
		
		//pricing
		optFuture.setPricingEngine(analyticEngine);
		opt.setPricingEngine(analyticEngine);
		EXPECT_THAT(opt.NPV(), Eq(optFuture.NPV()));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithAnalyticEuropeanEngine_PriceAndDelta)
	{
		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//option
		Option::Type type(Option::Call);
		Real strike = spot_->value();
		Rate riskFreeRate = 0.01;
		Date maturity = dividendTS_->referenceDate() + Period(5, Years);
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(maturity));
		VanillaOptionFuture europeanOption(payoff, europeanExercise, europeanExercise->lastDate() + Period(1, Months));
		
		//MC engine and pricing
		Size timeSteps = 1;
		Size mcSeed = 42;
		boost::shared_ptr<PricingEngine> mcengine;
		process->setFutureMaturity(europeanOption.futureExpiry());
		Real tolerance = 0.1;
		mcengine = MakeMCEuropeanForwardSchwartzSmithEngine<PseudoRandom>(process, flatTermStructure)
			.withSteps(timeSteps)
			.withAbsoluteTolerance(tolerance)
			.withMaxSamples(1000000)
			.withSeed(mcSeed);
		europeanOption.setPricingEngine(mcengine);
		//launch simulation
		Real mcNPV = europeanOption.NPV();
		//statistics
		SequenceStatisticsArrayInspectors statistics =
			boost::dynamic_pointer_cast<MCEuropeanForwardSchwartzSmithEngine<PseudoRandom, SequenceStatisticsArrayInspectors>>(mcengine)->sampleAccumulator();
		
		//Analytic engine and pricing
		boost::shared_ptr<PricingEngine> analyticEngine(new ForwardSchwartzSmithAnalyticEuropeanEngine(process, flatTermStructure));
		europeanOption.setPricingEngine(analyticEngine);
		Real analyticNPV = europeanOption.NPV();

		//price formula
		CumulativeNormalDistribution f;
		Real stdDev = process->logReturnStdDeviation(0, process->time(europeanExercise->lastDate()));
		Real d1 = std::log(process->initialFuturePrice() / payoff->strike()) / stdDev + 0.5*stdDev;
		Real d2 = d1 - stdDev;
		Real priceFormula = flatTermStructure->discount(process->time(europeanExercise->lastDate()))*
			(process->initialFuturePrice()*f(d1) - payoff->strike()*f(d2));

		//delta forward formula
		Real deltaFormula = flatTermStructure->discount(process->time(europeanExercise->lastDate()))*f(d1);

		//tests
		Real deltaTolerance = 1.0e-12;
		ASSERT_THAT(analyticNPV, Eq(priceFormula));
		ASSERT_THAT(std::abs(analyticNPV - mcNPV), Le(tolerance));
		ASSERT_THAT(std::abs(europeanOption.deltaForward() - deltaFormula), Le(deltaTolerance));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithBaroneAdesiWhaleyEngine_UsesEuropean)
	{
		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//european and american put option
		Option::Type type(Option::Call);
		Real strike = spot_->value();
		Rate riskFreeRate = 0.0; // to force BAW to use european price
		Date maturity = dividendTS_->referenceDate() + Period(3, Years);
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(maturity));
		boost::shared_ptr<Exercise> americanExercise(
			new AmericanExercise(dividendTS_->referenceDate(), maturity));
		VanillaOptionFuture europeanOption(payoff, europeanExercise, europeanExercise->lastDate() + Period(1, Months)); //european
		VanillaOptionFuture americanOption(payoff, americanExercise, europeanExercise->lastDate() + Period(1, Months)); //american

		// analytic european engine
		boost::shared_ptr<PricingEngine> europeanEngine(new ForwardSchwartzSmithAnalyticEuropeanEngine(process, flatTermStructure));
		europeanOption.setPricingEngine(europeanEngine);
		Real europeanNPV = europeanOption.NPV();

		//analytic BAW american engine
		boost::shared_ptr<PricingEngine> americanEngine(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, flatTermStructure));
		americanOption.setPricingEngine(americanEngine);
		Real americanNPV = americanOption.NPV();

		//test
		Real tolerance = 1.0e-6; //value used in newton method inside BAW approximation
		ASSERT_THAT(std::abs(americanNPV - europeanNPV), Le(tolerance));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithBaroneAdesiWhaleyEngine_NullVariance)
	{
		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//european option
		Option::Type type(Option::Call);
		Real strike = spot_->value();
		Rate riskFreeRate = 0.01;
		Date maturity = dividendTS_->referenceDate();
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> americanExercise(
			new AmericanExercise(dividendTS_->referenceDate(), maturity));
		VanillaOptionFuture americanOption(payoff, americanExercise, americanExercise->lastDate() + Period(1, Months)); //american

		//analytic BAW american engine
		boost::shared_ptr<PricingEngine> americanEngine(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, flatTermStructure));
		americanOption.setPricingEngine(americanEngine);
		Real americanNPV = americanOption.NPV();

		//test
		process->setFutureMaturity(americanOption.futureExpiry());
		Real fwd = process->initialFuturePrice();
		EXPECT_THAT(americanNPV, Eq(payoff->operator()(fwd)));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithBaroneAdesiWhaleyEngine_AnalyticVersusMC)
	{
		//process
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_, sigma_S, sigma_L, beta, rho));
		//american put option
		Option::Type type(Option::Put);
		Real strike = spot_->value();
		Rate riskFreeRate = 0.01;
		Date maturity = dividendTS_->referenceDate() + Period(3, Years);
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> americanExercise(
			new AmericanExercise(dividendTS_->referenceDate(), maturity));
		VanillaOptionFuture americanOption(payoff, americanExercise, americanExercise->lastDate() + Period(1, Months));

		//analytic BAW american engine
		boost::shared_ptr<PricingEngine> americanEngine(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, flatTermStructure));
		americanOption.setPricingEngine(americanEngine);
		Real analyticNPV = americanOption.NPV();

		//MC engine and pricing
		Size timeStepsPerYear = 26; //every two weeks
		Size mcSeed = 42;
		boost::shared_ptr<PricingEngine> mcengine;
		//process->setFutureMaturity(americanExercise->lastDate());
		Real tolerance = 0.05;
		mcengine = MakeMCAmericanForwardSchwartzSmithEngine<PseudoRandom>(process, flatTermStructure)
			.withStepsPerYear(timeStepsPerYear)
			.withAbsoluteTolerance(tolerance)
			.withMaxSamples(1000000)
			.withSeed(mcSeed);
		americanOption.setPricingEngine(mcengine);
		//launch simulation
		Real mcNPV = americanOption.NPV();
		//statistics
		Statistics statistics =
			boost::dynamic_pointer_cast<MCAmericanForwardSchwartzSmithEngine<PseudoRandom, Statistics, VanillaOptionFuture>>(mcengine)->sampleAccumulator();

		//test
		Real alpha = 0.01;
		InverseCumulativeNormal ic;
		ASSERT_THAT(std::abs(analyticNPV - mcNPV), Le(tolerance)); //not sure if this should be tested, the one below looks better
		ASSERT_THAT(analyticNPV, IsBetween(mcNPV + ic(0.5*alpha)*statistics.errorEstimate(), mcNPV - ic(0.5*alpha)*statistics.errorEstimate()));
	}

	TEST_F(ForwardSchwartzSmithTest, AmericanOptionOnFutureHelper_ThrowsIfNotAmerican)
	{
		//american put option
		Option::Type type(Option::Put);
		Real strike = spot_->value();
		Date maturity = dividendTS_->referenceDate() + Period(3, Years);
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(maturity));
		boost::shared_ptr<Exercise> americanExercise(
			new AmericanExercise(dividendTS_->referenceDate(), maturity));
		boost::shared_ptr<VanillaOptionFuture> americanOption(
			new VanillaOptionFuture(payoff, americanExercise, americanExercise->lastDate() + Period(1, Months))); //american
		boost::shared_ptr<VanillaOptionFuture> europeanOption(
			new VanillaOptionFuture(payoff, europeanExercise, europeanExercise->lastDate() + Period(1, Months))); //european

		//discounts
		Rate riskFreeRate = 0.01;
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));

		//option market price
		Handle<Quote> price(
			boost::shared_ptr<Quote>(
			new SimpleQuote(1.5)));

		//tests
		ASSERT_THROW(AmericanOptionOnFutureHelper helper(europeanOption, price, flatTermStructure), std::exception);
		ASSERT_NO_THROW(AmericanOptionOnFutureHelper helper(americanOption, price, flatTermStructure));
	}

	TEST_F(ForwardSchwartzSmithTest, VanillaOptionFuture_Constructor)
	{
		//option
		Option::Type type(Option::Put);
		Real strike = spot_->value();
		Date maturity = dividendTS_->referenceDate() + Period(3, Years);
		boost::shared_ptr<StrikedTypePayoff> payoff(
			new PlainVanillaPayoff(type, strike));
		boost::shared_ptr<Exercise> europeanExercise(
			new EuropeanExercise(maturity));

		//test
		ASSERT_THROW(VanillaOptionFuture option(payoff, europeanExercise, maturity - Period(1, Months)), std::exception);
		Date futureExpiry = maturity + Period(1, Months);
		ASSERT_NO_THROW(VanillaOptionFuture option(payoff, europeanExercise, futureExpiry));
		VanillaOptionFuture option(payoff, europeanExercise, futureExpiry);
		ASSERT_THAT(option.futureExpiry(), Eq(futureExpiry));
	}

	TEST_F(ForwardSchwartzSmithTest, ForwardSchwartzSmithModel_Calibration)
	{
		//process
		Real sigma_S_calibration = 0.40;
		Real sigma_L_calibration = 0.10;
		Real beta_calibration = 0.80;
		Real rho_calibration = 0.80;
		boost::shared_ptr<ForwardSchwartzSmithProcess> process(
			new ForwardSchwartzSmithProcess(spot_, riskFreeTS_, dividendTS_,
			sigma_S_calibration, sigma_L_calibration, beta_calibration, rho_calibration));
		//discounts
		Rate riskFreeRate = 0.01;
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS_->referenceDate(), riskFreeRate, dividendTS_->dayCounter())));

		//calibrated model
		boost::shared_ptr<ForwardSchwartzSmithModel> model(new ForwardSchwartzSmithModel(
			process, flatTermStructure));

		//calibration helpers
		boost::shared_ptr<PricingEngine> bawEngine(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, flatTermStructure));
		std::vector<boost::shared_ptr<CalibrationHelper>> helpers;
		for (int i = 0; i < options_.size(); ++i)
		{
			helpers.push_back(boost::shared_ptr<CalibrationHelper>(
				new AmericanOptionOnFutureHelper(
				options_[i],
				Handle<Quote>(boost::shared_ptr<Quote>( new SimpleQuote(prices_[i]) )),
				flatTermStructure
				)));
			helpers[i]->setPricingEngine(bawEngine);
		}

		//launch calibration
		Simplex optimizer(0.001);
		EndCriteria endCriteria(1000, 100, 1.0e-12, 1.0e-12, 1.0e-12);
		model->calibrate(helpers, optimizer, endCriteria);
		std::cout << "End Criteria: " << model->endCriteria() << std::endl;

		//test difference between market prices and calibrated prices
		//tolerance is given in terms of price of option as percentage of notional
		Real tolerance = 0.01;
		for (auto j : boost::irange(0, (int)helpers.size()))
		{
			options_[j]->setPricingEngine(bawEngine);
			std::cout << "Market Price: " << prices_[j] << ", Model Price: " << options_[j]->NPV() << std::endl;
			ASSERT_THAT(std::abs(prices_[j]-options_[j]->NPV())/strikes_[j],Le(tolerance));
		}
	}

} //namespace testing
} //namespace MarketData
