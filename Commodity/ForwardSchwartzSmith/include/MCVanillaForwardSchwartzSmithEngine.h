#ifndef MC_VANILLA_FORWARD_SCHWARTZ_SMITH_ENGINE_H
#define MC_VANILLA_FORWARD_SCHWARTZ_SMITH_ENGINE_H

#include <ql/pricingengines/mcsimulation.hpp>
#include <ql\math\statistics\sequencestatistics.hpp>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>
#include <MonteCarlo\include\SequenceStatisticsArrayInspectors.h>
#include <MonteCarlo\include\MonteCarloTraits.h>
#include <Instruments\include\VanillaOptionFuture.h>

using namespace QuantLib;

/*! \brief Monte Carlo ForwardSchwartzSmith-model engine for Vanilla options.

It's an abstract class, the pathPricer() method from McSimulation has to be implemented. The compatible path pricer class
is PathPricer<MultiPath,Array> and calculate() assumes the first element of the Array is the price of the instrument.
A base class for path pricer is provided: VanillaForwardSchwartzSmithPathPricer.
*/
template <class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors, class Inst = VanillaOptionFuture>
class MCVanillaForwardSchwartzSmithEngine : public Inst::engine,
	public McSimulation<MultiVariateWithArrayResults, RNG, S> {
public:

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	void calculate() const;
	//@}

protected:

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::path_generator_type
		path_generator_type;

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::path_pricer_type
		path_pricer_type;

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::stats_type
		stats_type;

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::result_type
		result_type;

	/** Constructor.
	@param process
	@param timeSteps
	@param timeStepsPerYear
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCVanillaForwardSchwartzSmithEngine(const boost::shared_ptr<ForwardSchwartzSmithProcess>&,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

	//! \name McSimulation interface
	//@{
	/** Method that constructs the Time Grid.
	@return TimeGrid
	*/
	TimeGrid timeGrid() const;

	/** Method that constructs the Path Generator.
	@return boost::shared_ptr<path_generator_type>
	*/
	boost::shared_ptr<path_generator_type> pathGenerator() const;
	//@}
	
	boost::shared_ptr<ForwardSchwartzSmithProcess> process_;
	Size timeSteps_, timeStepsPerYear_;
	Size requiredSamples_, maxSamples_;
	Real requiredTolerance_;
	bool brownianBridge_;
	BigNatural seed_;
};

/*! \brief Path Pricer for MCVanillaForwardSchwartzSmithEngine.

It's an abstract class. It's required to be use as base class for any path pricer for MCVanillaForwardSchwartzSmithEngine
with the convention that the first element of the Array is the price of the instrument.
*/
class VanillaForwardSchwartzSmithPathPricer : public PathPricer < MultiPath, Array > {};

// template definitions MCVanillaForwardSchwartzSmithEngine

/*constructor*/
template <class RNG, class S, class Inst>
inline MCVanillaForwardSchwartzSmithEngine<RNG, S, Inst>::MCVanillaForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	Size timeSteps,
	Size timeStepsPerYear,
	bool brownianBridge,
	bool antitheticVariate,
	bool controlVariate,
	Size requiredSamples,
	Real requiredTolerance,
	Size maxSamples,
	BigNatural seed)
	: McSimulation<MultiVariateWithArrayResults, RNG, S>(antitheticVariate, controlVariate),
	process_(process), timeSteps_(timeSteps),
	timeStepsPerYear_(timeStepsPerYear),
	requiredSamples_(requiredSamples), maxSamples_(maxSamples),
	requiredTolerance_(requiredTolerance),
	brownianBridge_(brownianBridge), seed_(seed) {
	QL_REQUIRE(timeSteps != Null<Size>() ||
		timeStepsPerYear != Null<Size>(),
		"no time steps provided");
	QL_REQUIRE(timeSteps == Null<Size>() ||
		timeStepsPerYear == Null<Size>(),
		"both time steps and time steps per year were provided");
	QL_REQUIRE(timeSteps != 0,
		"timeSteps must be positive, " << timeSteps <<
		" not allowed");
	QL_REQUIRE(timeStepsPerYear != 0,
		"timeStepsPerYear must be positive, " << timeStepsPerYear <<
		" not allowed");
	this->registerWith(process_);
}

/*time grid*/
template <class RNG, class S, class Inst>
inline TimeGrid MCVanillaForwardSchwartzSmithEngine<RNG, S, Inst>::timeGrid() const {
	Date lastExerciseDate = this->arguments_.exercise->lastDate();
	Time t = process_->time(lastExerciseDate);
	if (this->timeSteps_ != Null<Size>()) {
		return TimeGrid(t, this->timeSteps_);
	}
	else if (this->timeStepsPerYear_ != Null<Size>()) {
		Size steps = static_cast<Size>(this->timeStepsPerYear_*t);
		return TimeGrid(t, std::max<Size>(steps, 1));
	}
	else {
		QL_FAIL("time steps not specified");
	}
}

/*path generator*/
template <class RNG, class S, class Inst>
boost::shared_ptr<typename MCVanillaForwardSchwartzSmithEngine<RNG, S, Inst>::path_generator_type>
MCVanillaForwardSchwartzSmithEngine<RNG, S, Inst>::pathGenerator() const {
	Size dimensions = process_->factors();
	TimeGrid grid = this->timeGrid();
	typename RNG::rsg_type generator =
		RNG::make_sequence_generator(dimensions*(grid.size() - 1), seed_);
	return boost::shared_ptr<path_generator_type>(
		new path_generator_type(process_, grid,
		generator, brownianBridge_));
}

/*calculate*/
template <class RNG, class S, class Inst>
void MCVanillaForwardSchwartzSmithEngine<RNG, S, Inst>::calculate() const {
	//set future maturity
	process_->setFutureMaturity(arguments_.futureExpiry);
	//simulation
	McSimulation<MultiVariateWithArrayResults, RNG, S>::calculate(requiredTolerance_,
		requiredSamples_,
		maxSamples_);
	this->results_.value = this->mcModel_->sampleAccumulator().mean()[0]; //convention: first element is price
	if (RNG::allowsErrorEstimate)
		this->results_.errorEstimate =
		this->mcModel_->sampleAccumulator().errorEstimate()[0]; //convention: first element is price
}

#endif
