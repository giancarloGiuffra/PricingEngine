#ifndef FORWARD_SCHWARTZ_SMITH_ANALYTIC_EUROPEAN_ENGINE_H
#define FORWARD_SCHWARTZ_SMITH_ANALYTIC_EUROPEAN_ENGINE_H

#include <Instruments\include\VanillaOptionFuture.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>

/*! \brief Class that implements an Analytic European Pricing Engine for the Forward Schwartz-Smith Process.

It works for both VanillaOption and VanillaOptionFuture instruments.
*/
class ForwardSchwartzSmithAnalyticEuropeanEngine : public VanillaOptionFuture::engine
{
public:

	/** Constructor.
	@param process a ForwardSchwartzSmithProcess.
	@param discountCurve 
	*/
	ForwardSchwartzSmithAnalyticEuropeanEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve);

	/** Constructor.
	@param process a ForwardSchwartzSmithProcess.
	*/
	ForwardSchwartzSmithAnalyticEuropeanEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process);

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	void calculate() const;
	//@}

	/** Inspector for process.
	@return boost::shared_ptr<ForwardSchwartzSmithProcess>
	*/
	const boost::shared_ptr<ForwardSchwartzSmithProcess> process() const;

	/** Inspector for discountCurve.
	@return Handle<YieldTermStructure>
	*/
	const Handle<YieldTermStructure> discountCurve() const;

private:

	boost::shared_ptr<ForwardSchwartzSmithProcess> process_; /**< ForwardSchwartzSmithProcess */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
};

#endif // !FORWARD_SCHWARTZ_SMITH_ANALYTIC_EUROPEAN_ENGINE_H
