#ifndef MC_EUROPEAN_FORWARD_SCHWARTZ_SMITH_ENGINE
#define MC_EUROPEAN_FORWARD_SCHWARTZ_SMITH_ENGINE

#include <Commodity\ForwardSchwartzSmith\include\MCVanillaForwardSchwartzSmithEngine.h>
#include <ql\math\statistics\sequencestatistics.hpp>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>
#include <MonteCarlo\include\SequenceStatisticsArrayInspectors.h>

using namespace QuantLib;

/*! \brief Monte Carlo ForwardSchwartzSmith-model engine for European options.

*/
template <class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCEuropeanForwardSchwartzSmithEngine
	: public MCVanillaForwardSchwartzSmithEngine <RNG, S> {
public:

	typedef typename MCVanillaForwardSchwartzSmithEngine<RNG, S>::path_pricer_type
		path_pricer_type;

	/** Constructor.
	@param process
	@param discountCurve
	@param timeSteps
	@param timeStepsPerYear
	@param antitheticVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCEuropeanForwardSchwartzSmithEngine(const boost::shared_ptr<ForwardSchwartzSmithProcess>&,
		const Handle<YieldTermStructure>& discountCurve,
		Size timeSteps,
		Size timeStepsPerYear,
		bool antitheticVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

protected:

	/** It returns the PathPricer to use in simulations.
	@return boost::shared_ptr<path_pricer_type>
	@throws if payoff is not PlainVanillaPayoff or if process is not ForwardSchwartzSmithProcess.
	*/
	boost::shared_ptr<path_pricer_type> pathPricer() const;

	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
};

/*! \brief Monte Carlo ForwardSchwartzSmith European Engine Factory.

*/
template <class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MakeMCEuropeanForwardSchwartzSmithEngine {
public:
	/** Constructor.
	@param process
	@param discountCurve
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine(const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve);

	/** Constructor.
	@param process
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine(const boost::shared_ptr<ForwardSchwartzSmithProcess>& process);
	
	/** Method to initialize steps.
	@param steps
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	@throws if steps per year has already been set.
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withSteps(Size steps);

	/** Method to initialize steps per year.
	@param steps
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	@throws if steps has already been set.
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withStepsPerYear(Size steps);

	/** Method to initialize samples.
	@param samples
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	@throws if tolerance has been set.
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withSamples(Size samples);

	/** Method to initialize tolerance.
	@param tolerance
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	@throws if samples has been set or if RNG does not allow an error estimate.
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withAbsoluteTolerance(Real tolerance);

	/** Method to initialize max samples.
	@param samples
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withMaxSamples(Size samples);

	/** Method to initialize seed.
	@param seed
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withSeed(BigNatural seed);

	/** Method to indicate if the simulation should use antithetic variables.
	@param b
	@return MakeMCEuropeanForwardSchwartzSmithEngine&
	*/
	MakeMCEuropeanForwardSchwartzSmithEngine& withAntitheticVariate(bool b = true);
	
	/** Implicit conversion to PricingEngine.
	@return boost::shared_ptr<PricingEngine>
	@throws if steps has not been set.
	*/
	operator boost::shared_ptr<PricingEngine>() const;

private:
	boost::shared_ptr<ForwardSchwartzSmithProcess> process_; /**< ForwardSchwartzSmithProcess. */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve. */
	bool antithetic_; /**< Use or no antithetic variables.*/
	Size steps_; /**< Time steps. */
	Size stepsPerYear_; /**< Time steps per year. */
	Size samples_; /**< Requireed number of samples. */
	Size maxSamples_; /**< Maximum number of samples. */
	Real tolerance_; /**< Required Tolerance. */
	BigNatural seed_; /**< Seed for Random Number Generator. */
};

/*! \brief Path Pricer for MCEuropeanForwardSchwartzSmithEngine.

*/
class EuropeanForwardSchwartzSmithPathPricer : public VanillaForwardSchwartzSmithPathPricer {
public:

	/** Constructor.
	@param type Option::Type
	@param strike
	@param discount
	@param initialValue
	*/
	EuropeanForwardSchwartzSmithPathPricer(Option::Type type,
		Real strike,
		DiscountFactor discount,
		Real initialValue);

	/** Operator()
	@param Multipath
	@return Array
	*/
	Array operator()(const MultiPath& Multipath) const;

private:
	PlainVanillaPayoff payoff_; /**< PayOff */
	DiscountFactor discount_; /**< Discount */
	Real initialValue_; /**< Underlying's Initial Value */
};

/*--------------------------------------END OF DECLARATIONS------------------------------------------------*/

// template definitions MCEuropeanForwardSchwartzSmithEngine

/*constructor*/
template <class RNG, class S>
MCEuropeanForwardSchwartzSmithEngine<RNG, S>::MCEuropeanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve,
	Size timeSteps, Size timeStepsPerYear, bool antitheticVariate,
	Size requiredSamples, Real requiredTolerance,
	Size maxSamples, BigNatural seed)
	: MCVanillaForwardSchwartzSmithEngine <RNG, S>(process, timeSteps, timeStepsPerYear,
	false, antitheticVariate, false,
	requiredSamples, requiredTolerance,
	maxSamples, seed), discountCurve_(discountCurve)
{
	registerWith(discountCurve_);
}

/*path pricer*/
template <class RNG, class S>
boost::shared_ptr<
	typename MCEuropeanForwardSchwartzSmithEngine<RNG, S>::path_pricer_type>
	MCEuropeanForwardSchwartzSmithEngine<RNG, S>::pathPricer() const {

	boost::shared_ptr<PlainVanillaPayoff> payoff(
		boost::dynamic_pointer_cast<PlainVanillaPayoff>(
		this->arguments_.payoff));
	QL_REQUIRE(payoff, "non-plain payoff given");

	boost::shared_ptr<ForwardSchwartzSmithProcess> process =
		boost::dynamic_pointer_cast<ForwardSchwartzSmithProcess>(this->process_);
	QL_REQUIRE(process, "ForwardSchwartzSmith process required");

	return boost::shared_ptr<
		typename MCEuropeanForwardSchwartzSmithEngine<RNG, S>::path_pricer_type>(
		new EuropeanForwardSchwartzSmithPathPricer(
		payoff->optionType(),
		payoff->strike(),
		discountCurve_->discount(this->timeGrid().back()),
		process->initialFuturePrice()));
}

// template definitions MakeMCEuropeanForwardSchwartzSmithEngine

/*constructor*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::MakeMCEuropeanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve)
	: process_(process), 
	discountCurve_(discountCurve.currentLink()),
	antithetic_(false),
	steps_(Null<Size>()), stepsPerYear_(Null<Size>()),
	samples_(Null<Size>()), maxSamples_(Null<Size>()),
	tolerance_(Null<Real>()), seed_(0) {}

/*constructor*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::MakeMCEuropeanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process)
	: MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>(process, process->riskFreeTS())
{}

/*steps*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withSteps(Size steps) {
	QL_REQUIRE(stepsPerYear_ == Null<Size>(),
		"number of steps per year already set");
	steps_ = steps;
	return *this;
}

/*steps per year*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withStepsPerYear(Size steps) {
	QL_REQUIRE(steps_ == Null<Size>(),
		"number of steps already set");
	stepsPerYear_ = steps;
	return *this;
}

/*samples*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withSamples(Size samples) {
	QL_REQUIRE(tolerance_ == Null<Real>(),
		"tolerance already set");
	samples_ = samples;
	return *this;
}

/*tolerance*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withAbsoluteTolerance(Real tolerance) {
	QL_REQUIRE(samples_ == Null<Size>(),
		"number of samples already set");
	QL_REQUIRE(RNG::allowsErrorEstimate,
		"chosen random generator policy "
		"does not allow an error estimate");
	tolerance_ = tolerance;
	return *this;
}

/*max samples*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withMaxSamples(Size samples) {
	maxSamples_ = samples;
	return *this;
}

/*seed*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withSeed(BigNatural seed) {
	seed_ = seed;
	return *this;
}

/*antithetic*/
template <class RNG, class S>
inline MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>&
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::withAntitheticVariate(bool b) {
	antithetic_ = b;
	return *this;
}

/*conversion to boost::shared_ptr<PricingEngine>*/
template <class RNG, class S>
inline
MakeMCEuropeanForwardSchwartzSmithEngine<RNG, S>::
	operator boost::shared_ptr<PricingEngine>() const {
	QL_REQUIRE(steps_ != Null<Size>() || stepsPerYear_ != Null<Size>(),
		"number of steps not given");
	return boost::shared_ptr<PricingEngine>(
		new MCEuropeanForwardSchwartzSmithEngine<RNG, S>(process_,
		discountCurve_,
		steps_,
		stepsPerYear_,
		antithetic_,
		samples_, tolerance_,
		maxSamples_,
		seed_));
}

//template definitions EuropeanForwardSchwartzSmithPathPricer

/*constructor*/
inline EuropeanForwardSchwartzSmithPathPricer::EuropeanForwardSchwartzSmithPathPricer(
	Option::Type type,
	Real strike,
	DiscountFactor discount,
	Real initialValue)
	: payoff_(type, strike), discount_(discount), initialValue_(initialValue) {
	QL_REQUIRE(strike >= 0.0,
		"strike less than zero not allowed");
	QL_REQUIRE(initialValue >= 0.0,
		"initialValue less than zero not allowed");
}

/*operator()*/
inline Array EuropeanForwardSchwartzSmithPathPricer::operator()(
	const MultiPath& multiPath) const {
	const Path& path = multiPath[0];
	const Size n = multiPath.pathSize();
	QL_REQUIRE(n > 0, "the path cannot be empty");
	//results
	Array results(3);
	results[0] = payoff_(path.back()) * discount_; //price
	results[1] = path.back(); //underlying price
	results[2] = std::log(path.back()/initialValue_); //logreturn
	return results;
}

#endif
