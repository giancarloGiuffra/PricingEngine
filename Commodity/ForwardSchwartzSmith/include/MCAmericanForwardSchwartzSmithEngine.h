#ifndef MC_AMERICAN_FORWARD_SCHWARTZ_SMITH_ENGINE_H
#define MC_AMERICAN_FORWARD_SCHWARTZ_SMITH_ENGINE_H

#include <ql\pricingengines\mclongstaffschwartzengine.hpp>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>
#include <ql\pricingengines\vanilla\mcamericanengine.hpp>
#include <Instruments\include\VanillaOptionFuture.h>

using namespace QuantLib;

/*! \brief American Monte Carlo Engine for the Forward Schwartz Smith model.

*/
template <class RNG = PseudoRandom, class S = Statistics, class Inst = VanillaOptionFuture>
class MCAmericanForwardSchwartzSmithEngine
	: public MCLongstaffSchwartzEngine< typename Inst::engine, MultiVariate, RNG, S>{
public:

	/** Constructor
	@param process
	@param discountCurve
	@param timeSteps
	@param timeStepsPerYear
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	@param polynomOrder order of polynomy for LS regression
	@param polynomType type of polynomy for LS regression
	@param nCalibrationSamples
	*/
	MCAmericanForwardSchwartzSmithEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve,
		Size timeSteps,
		Size timeStepsPerYear,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed,
		Size polynomOrder,
		LsmBasisSystem::PolynomType polynomType,
		Size nCalibrationSamples = Null<Size>());

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	void calculate() const;
	//@}

protected:

	//! \name MCLongstaffSchwartzEngine interface
	//@{
	/** Method that returns the LSM Path Pricer.
	*/
	boost::shared_ptr<LongstaffSchwartzPathPricer<MultiPath> > lsmPathPricer() const;
	//@}

private:

	const Size polynomOrder_; /**< Order of Regression Polynomy */
	const LsmBasisSystem::PolynomType polynomType_; /**< Type of Regression Polynomy */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
};

/*! \brief LSM Path Pricer for the Forward Schwartz Smith model.

*/
class AmericanMultiPathPricer : public EarlyExercisePathPricer<MultiPath>  {
public:

	/** Constructor.
	@param payoff
	@param dim dimension of process
	@param polynomOrder
	@param polynomType
	*/
	AmericanMultiPathPricer(
		const boost::shared_ptr<Payoff>& payoff,
		Size dim,
		Size polynomOrder,
		LsmBasisSystem::PolynomType polynomType);

	//! \name EarlyExercisePathPricer<MultiPath> interface
	//@{
	/** Method that returns the value of the simulated process at an specific time.
	The value is scaled for numerical stability.
	@param path
	@param t
	@return Array scaled value of the simulated process at time \f$ t \f$.
	*/
	Array state(const MultiPath& path, Size t) const;

	/** Method to calculate the payoff value at an specific time.
	This method belongs to the PathPricer interface.
	@param path
	@param t
	@return Real payoff at time \f$ t \f$.
	*/
	Real operator()(const MultiPath& path, Size t) const;

	/** It returns the basis system used in the regression.
	@return std::vector<boost::function1<Real, Array> > vector of functions that represent the basis system.
	*/
	std::vector<boost::function1<Real, Array> > basisSystem() const;
	//@}

protected:

	/** Method that allows to scale the process for numerical stability.
	@param state
	@return Real payoff at corresponding state.
	*/
	Real payoff(Array state) const;

	Real scalingValue_; /**< Scale Factor used for numerical stability */
	Size dim_; /**< Dimension fo the process */
	const boost::shared_ptr<Payoff> payoff_; /**< Payoff */
	std::vector<boost::function1<Real, Array> > v_; /**< Basis System functions */
};

/*! \brief  Monte Carlo ForwardSchwartzSmith American Engine Factory.

*/
template <class RNG = PseudoRandom, class S = Statistics, class Inst = VanillaOptionFuture>
class MakeMCAmericanForwardSchwartzSmithEngine {
public:

	/** Constructor.
	@param process
	@param discountCurve
	*/
	MakeMCAmericanForwardSchwartzSmithEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve);

	/** Constructor.
	@param process
	*/
	MakeMCAmericanForwardSchwartzSmithEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process);

	/** Method to initialize steps.
	@param steps
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	@throws if steps per year has already been set.
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withSteps(Size steps);

	/** Method to initialize steps per year.
	@param steps
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	@throws if steps has already been set.
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withStepsPerYear(Size steps);

	/** Method to initialize samples.
	@param samples
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	@throws if tolerance has been set.
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withSamples(Size samples);

	/** Method to initialize tolerance.
	@param tolerance
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	@throws if samples has been set or if RNG does not allow an error estimate.
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withAbsoluteTolerance(Real tolerance);

	/** Method to initialize max samples.
	@param samples
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withMaxSamples(Size samples);

	/** Method to initialize seed.
	@param seed
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withSeed(BigNatural seed);

	/** Method to indicate if the simulation should use antithetic variables.
	@param b
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withAntitheticVariate(bool b = true);

	/** Method to indicate if the simulation should use control variables.
	To use control variables though appropriate methods, which have been omitted, must be declared and implemented in
	class MCAmericanForwardSchwartzSmithEngine. See class MCAmericanEngine for an example.
	@param b
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withControlVariate(bool b = true);

	/** Method to initialize the order of the regression polynomials.
	@param polynomOrder
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withPolynomOrder(Size polynomOrder);

	/** Method to initialize the type of the regression polynomials.
	@param polynomType
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withBasisSystem(LsmBasisSystem::PolynomType polynomType);

	/** Method to initialize the number of paths to calibrate the regression model.
	@param calibrationSamples
	@return MakeMCAmericanForwardSchwartzSmithEngine&
	*/
	MakeMCAmericanForwardSchwartzSmithEngine& withCalibrationSamples(Size calibrationSamples);

	/** Implicit conversion to PricingEngine.
	@return boost::shared_ptr<PricingEngine>
	@throws if steps has not been set.
	*/
	operator boost::shared_ptr<PricingEngine>() const;

private:
	boost::shared_ptr<ForwardSchwartzSmithProcess> process_; /**< ForwardSchwartzSmithProcess. */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
	bool antithetic_; /**< Use or no antithetic variables.*/
	bool controlVariate_; /**< Use or no control variables.*/
	Size steps_; /**< Time steps. */
	Size stepsPerYear_; /**< Time steps per year. */
	Size samples_; /**< Requireed number of samples. */
	Size maxSamples_; /**< Maximum number of samples. */
	Size calibrationSamples_; /**< Requireed number of samples for regression calibration. */
	Real tolerance_; /**< Required Tolerance. */
	BigNatural seed_; /**< Seed for Random Number Generator. */
	Size polynomOrder_; /**< Order of Regression Polynomials */
	LsmBasisSystem::PolynomType polynomType_; /**< Type of Regression Polynomials */
};

/*--------------------------------------END OF DECLARATIONS------------------------------------------------*/

//template definitions MCAmericanForwardSchwartzSmithEngine

/*constructor*/
template <class RNG, class S, class Inst>
inline MCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::MCAmericanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve,
	Size timeSteps, Size timeStepsPerYear,
	bool antitheticVariate, bool controlVariate,
	Size requiredSamples, Real requiredTolerance,
	Size maxSamples, BigNatural seed,
	Size polynomOrder, LsmBasisSystem::PolynomType polynomType,
	Size nCalibrationSamples)
	: MCLongstaffSchwartzEngine<VanillaOptionFuture::engine,
	MultiVariate, RNG, S>(
	process, timeSteps, timeStepsPerYear,
	false, antitheticVariate,
	controlVariate, requiredSamples,
	requiredTolerance, maxSamples,
	seed, nCalibrationSamples),
	polynomOrder_(polynomOrder),
	polynomType_(polynomType),
	discountCurve_(discountCurve)
{
	registerWith(discountCurve_);
}

/*calculate*/
template <class RNG, class S, class Inst>
inline void MCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::calculate() const {
	
	//set future maturity
	boost::shared_ptr<ForwardSchwartzSmithProcess> process =
		boost::dynamic_pointer_cast<ForwardSchwartzSmithProcess>(process_);
	process->setFutureMaturity(arguments_.futureExpiry);

	MCLongstaffSchwartzEngine<VanillaOptionFuture::engine,
		MultiVariate, RNG, S>::calculate();
	if (this->controlVariate_) {
		// control variate might lead to small negative
		// option values for deep OTM options
		this->results_.value = std::max(0.0, this->results_.value);
	}
}

/*LSM Path Pricer*/
template <class RNG, class S, class Inst>
inline boost::shared_ptr<LongstaffSchwartzPathPricer<MultiPath> >
MCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::lsmPathPricer() const {
	
	boost::shared_ptr<ForwardSchwartzSmithProcess> process =
		boost::dynamic_pointer_cast<ForwardSchwartzSmithProcess>(
		this->process_);
	QL_REQUIRE(process, "Forward Schwartz Smith process required");

	boost::shared_ptr<EarlyExercise> exercise =
		boost::dynamic_pointer_cast<EarlyExercise>(
		this->arguments_.exercise);
	QL_REQUIRE(exercise, "wrong exercise given");
	QL_REQUIRE(!exercise->payoffAtExpiry(),
		"payoff at expiry not handled");

	boost::shared_ptr<AmericanMultiPathPricer> earlyExercisePathPricer(
		new AmericanMultiPathPricer(
		this->arguments_.payoff,
		process_->size(),
		polynomOrder_, polynomType_));

	return boost::shared_ptr<LongstaffSchwartzPathPricer<MultiPath> >(
		new LongstaffSchwartzPathPricer<MultiPath>(
		this->timeGrid(),
		earlyExercisePathPricer,
		discountCurve_.currentLink()));
}

//template definitions for MakeMCAmericanForwardSchwartzSmithEngine

/*constructor*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::MakeMCAmericanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const Handle<YieldTermStructure>& discountCurve)
	: process_(process), discountCurve_(discountCurve),
	antithetic_(false), controlVariate_(false),
	steps_(Null<Size>()), stepsPerYear_(Null<Size>()),
	samples_(Null<Size>()), maxSamples_(Null<Size>()),
	calibrationSamples_(2048),
	tolerance_(Null<Real>()), seed_(0),
	polynomOrder_(2),
	polynomType_(LsmBasisSystem::Monomial)
{}

/*constructor*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::MakeMCAmericanForwardSchwartzSmithEngine(
	const boost::shared_ptr<ForwardSchwartzSmithProcess>& process)
	: MakeMCAmericanForwardSchwartzSmithEngine<RNG, S>(process, process->riskFreeTS())
{}

/*polynomial order*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withPolynomOrder(Size polynomOrder) {
	polynomOrder_ = polynomOrder;
	return *this;
}

/*polynomial type*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withBasisSystem(
LsmBasisSystem::PolynomType polynomType) {
	polynomType_ = polynomType;
	return *this;
}

/*steps*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withSteps(Size steps) {
	QL_REQUIRE(stepsPerYear_ == Null<Size>(),
		"number of steps per year already set");
	steps_ = steps;
	return *this;
}

/*steps per year*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withStepsPerYear(Size steps) {
	QL_REQUIRE(steps_ == Null<Size>(),
		"number of steps already set");
	stepsPerYear_ = steps;
	return *this;
}

/*samples*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withSamples(Size samples) {
	QL_REQUIRE(tolerance_ == Null<Real>(),
		"tolerance already set");
	samples_ = samples;
	return *this;
}

/*tolerance*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withAbsoluteTolerance(Real tolerance) {
	QL_REQUIRE(samples_ == Null<Size>(),
		"number of samples already set");
	QL_REQUIRE(RNG::allowsErrorEstimate,
		"chosen random generator policy "
		"does not allow an error estimate");
	tolerance_ = tolerance;
	return *this;
}

/*max number samples*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withMaxSamples(Size samples) {
	maxSamples_ = samples;
	return *this;
}

/*calibration samples - for LS regression*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withCalibrationSamples(Size samples) {
	calibrationSamples_ = samples;
	return *this;
}

/*seed*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withSeed(BigNatural seed) {
	seed_ = seed;
	return *this;
}

/*antithetic*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withAntitheticVariate(bool b) {
	antithetic_ = b;
	return *this;
}

/*control variate*/
template <class RNG, class S, class Inst>
inline MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>&
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::withControlVariate(bool b) {
	controlVariate_ = b;
	return *this;
}

/*conversion to boost::shared_ptr<PricingEngine>*/
template <class RNG, class S, class Inst>
inline
MakeMCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>::operator boost::shared_ptr<PricingEngine>()
const {
	QL_REQUIRE(steps_ != Null<Size>() || stepsPerYear_ != Null<Size>(),
		"number of steps not given");
	return boost::shared_ptr<PricingEngine>(new
		MCAmericanForwardSchwartzSmithEngine<RNG, S, Inst>(
		process_,
		discountCurve_,
		steps_,
		stepsPerYear_,
		antithetic_,
		controlVariate_,
		samples_, tolerance_,
		maxSamples_,
		seed_,
		polynomOrder_,
		polynomType_,
		calibrationSamples_));
}

#endif // !MC_AMERICAN_FORWARD_SCHWARTZ_SMITH_ENGINE_H
