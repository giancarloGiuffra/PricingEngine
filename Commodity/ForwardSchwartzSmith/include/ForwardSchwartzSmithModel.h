#ifndef FORWARD_SCHWARTZ_SMITH_MODEL_H
#define FORWARD_SCHWARTZ_SMITH_MODEL_H

#include <ql\models\model.hpp>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>

using namespace QuantLib;

/*! \brief Schwartz-Smith Model for Commodities.

This implementation follows a reformulation of the original paper by modelling the dynamics of future prices instead of that 
of the spot price.
\sa ForwardSchwartzSmithProcess for the dynamics and a link to the original paper.
*/
class ForwardSchwartzSmithModel : public CalibratedModel
{
public:

	/** Constructor.
	@param process
	@param discountCurve
	*/
	ForwardSchwartzSmithModel(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve);

	/** Constructor.
	@param process
	*/
	ForwardSchwartzSmithModel(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process);

	/** Inspector for the short-term volatility.
	@return Real
	*/
	Real sigma_S() const;

	/** Inspector for the long-term volatility.
	@return Real
	*/
	Real sigma_L() const;

	/** Inspector for beta.
	This parameter controls the interplay between short and lon-term volatility.
	@return Real
	*/
	Real beta() const;

	/** Inspector for rho.
	This parameter is the instantaneous correlation between the two brownian motions that drive the dynamics.
	@return Real
	*/
	Real rho() const;

	/** Inspector for the process.
	@return boost::shared_ptr<ForwardSchwartzSmithProcess>
	*/
	boost::shared_ptr<ForwardSchwartzSmithProcess> process() const;

protected:

	/** Method to update the parameters of the process.
	*/
	void generateArguments();

	boost::shared_ptr<ForwardSchwartzSmithProcess> process_; /**< ForwardSchwartzSmithProcess */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
};

#endif // !FORWARD_SCHWARTZ_SMITH_MODEL_H
