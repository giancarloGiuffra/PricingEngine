#ifndef AMERICAN_OPTION_ON_FUTURE_HELPER_H
#define AMERICAN_OPTION_ON_FUTURE_HELPER_H

#include <ql\models\calibrationhelper.hpp>
#include <Instruments\include\VanillaOptionFuture.h>

using namespace QuantLib;

/*! \brief Helper to calibrate to an American Option on a Future.

This class is intended to be use for the calibration of the Forward Schwartz Smith Model.
\sa ForwardSchwartzSmithModel
*/
class AmericanOptionOnFutureHelper : public CalibrationHelper
{
public:

	/** Constructor.
	@param option
	@param price
	@param discountCurve required by QuantLib::CalibrationHelper interface
	@param errorType CalibrationHelper::CalibrationErrorType
	@throws if option is not american.
	*/
	AmericanOptionOnFutureHelper(
		const boost::shared_ptr<VanillaOptionFuture>& option,
		const Handle<Quote>& price,
		const Handle<YieldTermStructure>& discountCurve,
		CalibrationHelper::CalibrationErrorType errorType = CalibrationHelper::RelativePriceError);

	//! \name CalibrationHelper interface
	//@{
	/** It returns the price of the option according to the model.
	@return Real
	*/
	Real modelValue() const;

	/** It returns the market price of the option.
	@param volatility the CalibrationHelper interface assumes prices are quoted in terms of volatility.
	@return Real
	@see marketValue()
	*/
	Real blackPrice(Real volatility) const;

	/** Method used in tree-based models.
	It is required though by the CalibrationHelper interface. It's implementation for this class is empty since it's not used.
	@param std::list<Time>
	*/
	void addTimesTo(std::list<Time>&) const;
	//@}

	/** Inspector for the option.
	@return boost::shared_ptr<VanillaOptionFuture>
	*/
	boost::shared_ptr<VanillaOptionFuture> option() const;

private:

	boost::shared_ptr<VanillaOptionFuture> option_; /**< American Option on Future. */
};

#endif // !AMERICAN_OPTION_ON_FUTURE_HELPER_H
