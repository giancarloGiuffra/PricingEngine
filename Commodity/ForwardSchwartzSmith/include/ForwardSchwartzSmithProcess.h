#ifndef FORWARD_SCHWARTZ_SMITH_PROCESS_H
#define FORWARD_SCHWARTZ_SMITH_PROCESS_H

#include <ql\stochasticprocess.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <MarKetQuote\include\FutureQuote.h>

using namespace QuantLib;

class ForwardSchwartzSmithModel;

/*! \brief Class that represents the Schwartz-Smith Stochastic Process for Commodities.

It is a 2 factor gaussian model and the implementation provided by this class follows a reformulation of the original paper
by modeling the dynamics of future prices instead of the spot price (somewhat similar to the relationship between short rate models and the HJM framework).
The dynamics are:

\f[
\frac{dF(t,T)}{F(t,T)} = \sigma_Se^{-\beta(T-t)}dW_{t}^{1} + \sigma_L\left(1-e^{-\beta(T-t)}\right)dW_{t}^{2}
\f]

where \f$W^1\f$ and \f$W^2\f$ are two brownian motions with instantaneous correlation equal to \f$\rho\f$.

See <a href="https://faculty.fuqua.duke.edu/~jes9/bio/Short-term_Long-term_Model.pdf">the original paper</a> for details.
*/
class ForwardSchwartzSmithProcess : public StochasticProcess 
{
public:
	/** Constructor.

	@param spot spot price of the commodity.
	@param riskFreeTS 
	@param dividendTS Handle to QuantLib::YieldTermStructure that represents the strip of futures at initial time.
	@param sigma_S short-term volatility.
	@param sigma_L long-term volatility.
	@param beta parameter that governs the exponential interplay between sigma_S and sigma_L.
	@param rho correlation between the two brownian motions.
	@throws if parameters are not valid
	*/
	ForwardSchwartzSmithProcess(
		const Handle<Quote> spot,
		const Handle<YieldTermStructure> riskFreeTS,
		const Handle<YieldTermStructure> dividendTS,
		Real sigma_S,
		Real sigma_L,
		Real beta,
		Real rho
		);

	/** Constructor.
	@param spot spot price of the commodity.
	@param riskFreeTS
	@param futures the strip of futures at initial time.
	@param sigma_S short-term volatility.
	@param sigma_L long-term volatility.
	@param beta parameter that governs the exponential interplay between sigma_S and sigma_L.
	@param rho correlation between the two brownian motions.
	@throws if parameters are not valid
	*/
	ForwardSchwartzSmithProcess(
		const Handle<Quote> spot,
		const Handle<YieldTermStructure> riskFreeTS,
		std::vector<std::shared_ptr<FutureQuote>> futures,
		Real sigma_S,
		Real sigma_L,
		Real beta,
		Real rho
		);

	/** Destructor.
	*/
	~ForwardSchwartzSmithProcess();

	/** Method to set the future maturity.
	The maturity specified identifies the future that is being modeled. It is considered in the calculations.
	@param maturity QuantLib::Date
	@throws if maturity is not greater than the reference date of dividendTS, the strip of current future prices.
	*/
	void setFutureMaturity(const Date& maturity);

	/** Inspector for the current maturity (and therefore future) being considered.
	@return QuantLib::Date.
	*/
	Date futureMaturity() const;

	/** Inspector for the spot price.
	@return const Handle<Quote>&
	*/
	const Handle<Quote>& spot() const;

	/** Inspector for the risk free TS.
	@return const Handle<YieldTermStructure>&
	*/
	const Handle<YieldTermStructure>& riskFreeTS() const;

	/** Inspector for the strip of futures.
	@return const Handle<YieldTermStructure>&
	*/
	const Handle<YieldTermStructure>& dividendTS() const;

	/** Inspector for the short-term volatility.
	@return Real
	*/
	Real sigma_S() const;

	/** Inspector for the long-term volatility.
	@return Real
	*/
	Real sigma_L() const;

	/** Inspector for beta.
	@return Real
	*/
	Real beta() const;

	/** Inspector for rho.
	@return Real
	*/
	Real rho() const;

	//! \name Schwartz-Smith StochasticProcess interface
	//@{

	/** It returns the dimensions of the stochastic process.
	@return Size
	*/
	Size size() const;

	/** It returns the number of independent factors of the process.
	@return Size
	*/
	Size factors() const;

	/** It returns the initial value of the future currently being considered.
	Its value depends on the future maturity \f$ T \f$ which therefore has to be previously set.
	@return Disposable<Array>
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set.
	*/
	Disposable<Array> initialValues() const;

	/** drift of the SDE describing the dynamics of \f$ F(t,T) \f$.
	@param t current time of simulation.
	@param x current value of the future in the simulation : \f$ F(t,T) \f$.
	@return Real
	*/
	Real drift(Time t, Real x) const;

	/** diffusion of the SDE describing the dynamics of \f$ F(t,T) \f$.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t current time of simulation.
	@param x current value of the future in the simulation : \f$ F(t,T) \f$.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or \f$ t > T \f$.
	*/
	Disposable<Matrix> diffusion(Time t, Real x) const;

	/** \f$ E\left[ F(t_0+dt,T) | F(t_0,T)=x_0  \right] \f$.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t0 initial time.
	@param x0 future price at t0 : \f$ F(t_0,T) \f$.
	@param dt time step.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or \f$ t_0+dt > T \f$.
	*/
	Real expectation(Time t0, Real x0, Time dt) const;

	/** \f$ \sqrt(V\left[ F(t_0+dt,T) | F(t_0,T)=x_0  \right]) \f$.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t0 initial time.
	@param x0 future price at t0 : \f$ F(t_0,T) \f$.
	@param dt time step.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or if the following is not respected \f$ t_0 < t_0+dt < T \f$.
	*/
	Real stdDeviation(Time t0, Real x0, Time dt) const;

	/** \f$ V\left[ F(t_0+dt,T) | F(t_0,T)=x_0  \right] \f$.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t0 initial time.
	@param x0 future price at t0 : \f$ F(t_0,T) \f$.
	@param dt time step.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or if the following is not respected \f$ t_0 < t_0+dt < T \f$.
	*/
	Real covariance(Time t0, Real x0, Time dt) const;

	/** \f$ F(t_0+dt,T) \f$.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t0 initial time.
	@param x0 future price at t0 : \f$ F(t_0,T) \f$.
	@param dt time step.
	@param dw two dimensional standard gaussian variable.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set, if the following is not respected \f$ t_0 < t_0+dt < T \f$ or if
	dw is not 2-dimensional.
	*/
	Real evolve(Time t0, Real x0, Time dt, const Array& dw) const;

	/** \f$ x_0 + dx \f$.
	It's required by the StochasticProcess interface.
	@param x0 future price at t0 : \f$ F(t_0,T) \f$.
	@param dx future price variation.
	@return Real
	*/
	Real apply(Real x0, Real dx) const;

	/** Method that calculates the time value corresponding to the given date in the reference system of the process.
	@param date to calculate the time value
	@return Time
	*/
	Time time(const Date& date) const;

	//@}

	//! \name Observer interface
	//@{
	void update();
	//@}

	/** Method to calculate the variance of the log returns of the future price.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t initial time
	@param S final time
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or if the following is not respected \f$ t < S < T \f$.
	*/
	Real logReturnVariance(Time t, Time S) const;

	/** Method to calculate the standard deviation of the log returns of the future price.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t initial time
	@param S final time
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or if the following is not respected \f$ t < S < T \f$.
	*/
	Real logReturnStdDeviation(Time t, Time S) const;

	/** Method to calculate the mean of the log returns of the future price.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@param t initial time
	@param S final time
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set or if the following is not respected \f$ t < S < T \f$.
	*/
	Real logReturnMean(Time t, Time S) const;

	/** Method to calculate the instantaneous variance appearing in the pricing PDE.
	The pricing PDE is the following:
	\f[
	\frac{1}{2}\sigma(t)F^2V_{FF} - rV + V_t = 0.
	\f]
	where \f$ \sigma(t) \f$ is the instantaneous variance. Its value depends on \f$ T \f$ 
	which therefore has to be previously set.
	@param t 
	@return Real
	*/
	Real instantaneousVariance(Time t) const;

	/** Method to calculate the future price at initial time.
	Its value depends on \f$ T \f$ which therefore has to be previously set.
	@return Real
	@see setFutureMaturity(const Date& maturity).
	@throws if future maturity has not yet been set.
	*/
	Real initialFuturePrice() const;

private:

	//! \name StochasticProcess interface
	//@{
	Disposable<Array> drift(Time t, const Array& x) const;

	Disposable<Matrix> diffusion(Time t, const Array& x) const;

	Disposable<Array> expectation(Time t0, const Array& x0, Time dt) const;

	Disposable<Matrix> stdDeviation(Time t0, const Array& x0, Time dt) const;

	Disposable<Matrix> covariance(Time t0, const Array& x0, Time dt) const;

	Disposable<Array> evolve(Time t0, const Array& x0, Time dt, const Array& dw) const;

	Disposable<Array> apply(const Array& x0, const Array& dx) const;
	//@}

	/** Method to calculate the integral coming from the brownian motion multiplied by sigma_S.
	*/
	Real integralSigmaS(Time t, Time S) const;

	/** Method to calculate the integral coming from the brownian motion multiplied by sigma_L.
	*/
	Real integralSigmaL(Time t, Time S) const;

	/** Method to calculate the integral coming from correlation between the two brownian motions.
	*/
	Real integralRho(Time t, Time S) const;

	Handle<Quote> spot_; /**< Spot price of commodity. */
	Handle<YieldTermStructure> riskFreeTS_; /**< Term Structure of risk free rate */
	Handle<YieldTermStructure> dividendTS_; /**< Term Structure of future prices. */
	Real sigma_S_; /**< Short-term volatility. */
	Real sigma_L_; /**< Long-term volatility. */
	Real beta_; /**< Governs the exponential interplay between sigma_S and sigma_L. */
	Real rho_; /**< Correlation between the two brownian motions. */
	Date futureMaturity_; /**< Maturity of the future that is being considered. */
	Time T_; /**< Maturity of the future that is being considered. To facilitate calculations.*/

	friend class ForwardSchwartzSmithModel;
};

#endif