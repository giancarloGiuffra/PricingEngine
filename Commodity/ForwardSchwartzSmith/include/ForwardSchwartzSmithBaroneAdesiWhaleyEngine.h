#ifndef FORWARD_SCHWARTZ_SMITH_BARONE_ADESI_WHALEY_ENGINE_H
#define FORWARD_SCHWARTZ_SMITH_BARONE_ADESI_WHALEY_ENGINE_H

#include <Instruments\include\VanillaOptionFuture.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>

/*! \brief Class that implements the Barone-Adesi/Whaley approximation for the Forward Schwartz-Smith Process.

Recall that the BAW approximation is used to price American Calls and Puts.
*/
class ForwardSchwartzSmithBaroneAdesiWhaleyEngine : public VanillaOptionFuture::engine
{
public:

	/** Constructor.
	@param process a ForwardSchwartzSmithProcess.
	@param discountCurve
	*/
	ForwardSchwartzSmithBaroneAdesiWhaleyEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
		const Handle<YieldTermStructure>& discountCurve);

	/** Constructor.
	@param process a ForwardSchwartzSmithProcess.
	*/
	ForwardSchwartzSmithBaroneAdesiWhaleyEngine(
		const boost::shared_ptr<ForwardSchwartzSmithProcess>& process);

	/** Method to calculate the free boundary point.
	@param payoff
	@param riskFreeDiscount
	@param variance
	@param tolerance
	@return Real
	*/
	static Real criticalPrice(
		const boost::shared_ptr<StrikedTypePayoff>& payoff,
		DiscountFactor riskFreeDiscount,
		Real variance,
		Real tolerance = 1e-6);

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	void calculate() const;
	//@}

	/** Inspector for process.
	@return boost::shared_ptr<ForwardSchwartzSmithProcess>
	*/
	const boost::shared_ptr<ForwardSchwartzSmithProcess> process() const;

	/** Inspector for discountCurve.
	@return Handle<YieldTermStructure>
	*/
	const Handle<YieldTermStructure> discountCurve() const;

private:

	boost::shared_ptr<ForwardSchwartzSmithProcess> process_; /**< ForwardSchwartzSmithProcess */
	Handle<YieldTermStructure> discountCurve_; /**< Discount Curve.*/
};

#endif // !FORWARD_SCHWARTZ_SMITH_BARONE_ADESI_WHALEY_ENGINE_H
