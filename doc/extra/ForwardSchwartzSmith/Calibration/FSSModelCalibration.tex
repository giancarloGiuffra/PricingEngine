\documentclass{basepricingengine}

\settitle{Forward Schwartz Smith Calibration}
\begin{document}

% INTRODUCTION
\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}
This document focuses on the Forward Schwartz Smith model for commodities implemented in the PricingEngine library. The purpose is to provide an initial empirical analysis of the flexibility of the model to accomodate different structures of market prices. In the first section the model is briefly described. Then an analysis is performed by plotting how the structure of prices changes as one of the parameters varies while the other ones are kept fixed. This is repeated for all the parameters of the model. Conclusions are exposed in the last section.

% FSS MODEL
\section{Forward Schwartz Smith Model}
The model describes the dynamics of the whole term structure of future prices.

\begin{equation}% EQUATION model
\dfrac{dF(t,T)}{F(t,T)} = \sigma_Se^{-\beta(T-t)}dW_{t}^{1} + \sigma_L\left(1-e^{-\beta(T-t)}\right)dW_{t}^{2}
\end{equation}

where $W^1$ and $W^2$ are two brownian motions with instantaneous correlation equal to $\rho$. The remaining 3 parameters $\sigma_S, \sigma_L$ and $\beta$ are positive constants. 

% ANALYSIS
\section{Analysis}
The initial set of parameters is shown in Table~\ref{table:initial-set}. The spot and initial term structure of futures is not included for brevity. The goal of the analysis is to study the flexibility of the model to replicate different structures of market prices. The approach implemented is quite simple, it consists in plotting the variations of the price structure as one parameter changes while the others remain fixed. The american call options considered in this analysis are shown in Table~\ref{table:american-options} and the valuation date is \DTMsetdatestyle{d-m-yy}\DTMDisplaydate{2015}{12}{10}{-1}\DTMsetdatestyle{default}.

\begin{table}[ht]% TABLE initial-set
\centering
\newcolumntype{.}{D{.}{.}{-1}}
\begin{tabular}{l | . }
Parameter & \text{Value} \\
\hline
$\sigma_S$ & 0.20 \\
$\sigma_L$ & 0.20 \\
$\beta$ & 0.80 \\
$\rho$ & -0.50
\end{tabular}
\caption{Initial set of parameters.}\label{table:initial-set}
\end{table}

\DTMsetdatestyle{d-m-yy}
\begin{table}[ht]% TABLE american-options
\centering
\begin{tabular}{l | c | c | c | c }
 & Maturity & Strike & Future Maturity &  \text{Market Price} \\
\hline
Call 1 & \DTMDisplaydate{2016}{1}{10}{-1} & 46.21 & \DTMDisplaydate{2016}{2}{10}{-1} & 1.88 \\
Call 2 & \DTMDisplaydate{2016}{1}{10}{-1} & 46.71 & \DTMDisplaydate{2016}{2}{10}{-1} & 1.63 \\
Call 3 & \DTMDisplaydate{2016}{1}{10}{-1} & 47.21 & \DTMDisplaydate{2016}{2}{10}{-1} & 1.27 \\
Call 4 & \DTMDisplaydate{2016}{2}{10}{-1} & 46.72 & \DTMDisplaydate{2016}{3}{10}{-1} & 3.10 \\
Call 5 & \DTMDisplaydate{2016}{2}{10}{-1} & 47.22 & \DTMDisplaydate{2016}{3}{10}{-1} & 2.45 \\
Call 6 & \DTMDisplaydate{2016}{2}{10}{-1} & 47.72 & \DTMDisplaydate{2016}{3}{10}{-1} & 2.25 \\
Call 7 & \DTMDisplaydate{2016}{3}{10}{-1} & 47.44 & \DTMDisplaydate{2016}{4}{10}{-1} & 3.30 \\
Call 8 & \DTMDisplaydate{2016}{3}{10}{-1} & 47.94 & \DTMDisplaydate{2016}{4}{10}{-1} & 3.29 \\
Call 9 & \DTMDisplaydate{2016}{3}{10}{-1} & 48.44 & \DTMDisplaydate{2016}{4}{10}{-1} & 3.03 \\
Call 10 & \DTMDisplaydate{2016}{4}{10}{-1} & 48.11 & \DTMDisplaydate{2016}{5}{10}{-1} & 4.05 \\
Call 11 & \DTMDisplaydate{2016}{4}{10}{-1} & 48.61 & \DTMDisplaydate{2016}{5}{10}{-1} & 3.80 \\
Call 12 & \DTMDisplaydate{2016}{4}{10}{-1} & 49.11 & \DTMDisplaydate{2016}{5}{10}{-1} & 3.56 \\
\end{tabular}
\caption{ American call options considered in the analysis.}
\label{table:american-options}
\end{table}
\DTMsetdatestyle{default}

%description of plots
Figure~\ref{figure:sigma-s-variation-initial-set} shows the variation of the structure of prices as the parameter $\sigma_S$ changes. The values considered are shown as legend in the different plots. The first plot shows the ATM term structure of prices while the other four show the smiles for the corresponding maturities. Figures~\ref{figure:sigma-l-variation-initial-set}, \ref{figure:beta-variation-initial-set}, \ref{figure:rho-variation-initial-set} contain analogous plots for the other three parameters.

\begin{figure}[ht] % FIGURE sigma_S initial
\centering
\includegraphics[width=0.75\textwidth]{Sigma_S_Variation.eps}
\caption{Variation of price structure due to $\sigma_S$. Remaining parameters are as in Table~\ref{table:initial-set}.}
\label{figure:sigma-s-variation-initial-set}
\end{figure}

\begin{figure}[ht] % FIGURE sigma_L initial
\centering
\includegraphics[width=0.75\textwidth]{Sigma_L_Variation.eps}
\caption{Variation of price structure due to $\sigma_L$. Remaining parameters are as in Table~\ref{table:initial-set}.}
\label{figure:sigma-l-variation-initial-set}
\end{figure}

\begin{figure}[ht] % FIGURE beta initial
\centering
\includegraphics[width=0.75\textwidth]{Beta_Variation.eps}
\caption{Variation of price structure due to $\beta$. Remaining parameters are as in Table~\ref{table:initial-set}.}
\label{figure:beta-variation-initial-set}
\end{figure}

\begin{figure}[ht] % FIGURE rho initial
\centering
\includegraphics[width=0.75\textwidth]{Rho_Variation.eps}
\caption{Variation of price structure due to $\rho$. Remaining parameters are as in Table~\ref{table:initial-set}.}
\label{figure:rho-variation-initial-set}
\end{figure}

%comment on first set of plots
The parameter that has the most important impact on prices is $\sigma_S$, the variations due to the other parameters are contained when compared to the ones due to $\sigma_S$, at least in the configuration given by the initial set of parameters of Table~\ref{table:initial-set}. It is worth noticing that in this configuration the impact due to $\sigma_L$ is almost negligible and quite counterintuitive, prices decrease as the parameter increases. The same behavior happens due to $\beta$, probably because an increasing $\beta$ diminishes the impact of $\sigma_S$ and increases that of $\sigma_L$. The impact due to $\rho$ is expected, more correlation increases the volatility of the future price driving prices higher. Another aspect that requires attention is the fact that the smiles given by the model are predominantly linear which is a strong limitation.\\

%motivation for second set of plots - positive correlation
In order to further inspect the interplay between the parameters and their impact on prices the analysis is repeated but this time with another set of initial paramaters, shown in Table~\ref{table:initial-set-positive-correlation}. The only modification is the correlation parameter, from negative to positive. Figures~\ref{figure:sigma-s-variation-positive-correlation}, \ref{figure:sigma-l-variation-positive-correlation}, \ref{figure:beta-variation-positive-correlation} show the price structure variations in this case. The plots regarding the variations due to $\rho$ are omitted since they don't add any insight. In this second case the impact due to $\sigma_S$ is still the largest. $\sigma_L$ has a more intuitive effect: prices increase as it increases, furthermore its impact is larger with respect to the initial configuration but still small compared to that of $\sigma_S$. The impact due to $\beta$ follows the same behavior as in the first case but it's considerably smaller.\\

\begin{table}[!ht]% TABLE initial-set-positive correlation
\centering
\newcolumntype{.}{D{.}{.}{-1}}
\begin{tabular}{l | . }
Parameter & \text{Value} \\
\hline
$\sigma_S$ & 0.20 \\
$\sigma_L$ & 0.20 \\
$\beta$ & 0.80 \\
$\rho$ & 0.50
\end{tabular}
\caption{Initial set of parameters (positive correlation).}\label{table:initial-set-positive-correlation}
\end{table}

\begin{figure}[ht] % FIGURE sigma_S positive correlation
\centering
\includegraphics[width=0.75\textwidth]{Sigma_S_Variation_PosCorrel.eps}
\caption{Variation of price structure due to $\sigma_S$ (positive correlation). Remaining parameters are as in Table~\ref{table:initial-set-positive-correlation}.}
\label{figure:sigma-s-variation-positive-correlation}
\end{figure}

\begin{figure}[ht] % FIGURE sigma_L positive correlation
\centering
\includegraphics[width=0.75\textwidth]{Sigma_L_Variation_PosCorrel.eps}
\caption{Variation of price structure due to $\sigma_L$ (positive correlation). Remaining parameters are as in Table~\ref{table:initial-set-positive-correlation}.}
\label{figure:sigma-l-variation-positive-correlation}
\end{figure}

\begin{figure}[ht] % FIGURE beta positive correlation
\centering
\includegraphics[width=0.75\textwidth]{Beta_Variation_PosCorrel.eps}
\caption{Variation of price structure due to $\beta$ (positive correlation). Remaining parameters are as in Table~\ref{table:initial-set-positive-correlation}.}
\label{figure:beta-variation-positive-correlation}
\end{figure}

%third set of plots - different volatility values
To close the analysis another set of initial parameters is considered. This time the correlation is still kept positive but the two volatility parameters assume different values. In particular $\sigma_S$ is greater than $\sigma_L$. The set is shown in Table~\ref{table:initial-set-different-vols}. Figures~\ref{figure:beta-variation-different-vols}, \ref{figure:rho-variation-different-vols} show the results regarding $\beta$, $\rho$ respectively. It is worth noticing that in this case both parameters $\beta$ and $\rho$ have a more significant role, in the sense that as they change the price structure covers a wider range of values.

\begin{table}[!ht]% TABLE initial-set different vols
\centering
\newcolumntype{.}{D{.}{.}{-1}}
\begin{tabular}{l | . }
Parameter & \text{Value} \\
\hline
$\sigma_S$ & 0.30 \\
$\sigma_L$ & 0.20 \\
$\beta$ & 0.80 \\
$\rho$ & 0.50
\end{tabular}
\caption{Initial set of parameters (different volatilities).}\label{table:initial-set-different-vols}
\end{table}

\begin{figure}[ht] % FIGURE beta different vols
\centering
\includegraphics[width=0.75\textwidth]{Beta_Variation_DiffVols.eps}
\caption{Variation of price structure due to $\beta$  (different volatilities). Remaining parameters are as in Table~\ref{table:initial-set-different-vols}.}
\label{figure:beta-variation-different-vols}
\end{figure}

\begin{figure}[ht] % FIGURE rho different vols
\centering
\includegraphics[width=0.75\textwidth]{Rho_Variation_DiffVols.eps}
\caption{Variation of price structure due to $\rho$  (different volatilities). Remaining parameters are as in Table~\ref{table:initial-set-different-vols}.}
\label{figure:rho-variation-different-vols}
\end{figure}

% CONCLUSION
\section{Conclusion}
In this section we collect as conclusions all the comments made in the Analysis section.

\begin{enumerate}
\item The parameter that has the greatest impact on prices is $\sigma_S$.
\item $\sigma_S$ and $\rho$ have a positive impact on prices.
\item $\beta$ has a negative impact on prices.
\item $\sigma_L$ has a positive impact on prices when $\rho$ is positive, and a negative impact when $\rho$ is negative.
\item $\beta$'s and $\rho$'s impact increases when the volatility parameters have different values.
\item A strong limitation of the model is the fact that it produces predominantly linear smiles.
\end{enumerate}

These conclusions can help with the choice of initial parameters for a calibration procedure. As a guideline one can start with a positive correlation and sensibly different values for the volatility parameters in order to maximize the flexibility of the model.

%APPENDIX
\begin{appendices}
\section{Code}
In this appendix we include the \texttt{.cpp} file used for the creation of the plots. The code is by no means optimized. Listing~\ref{listing:plots.cpp} shows the file.

\lstset{%
language=[Visual]C++,
basicstyle=\footnotesize\color[RGB]{88,110,117},
numbers=left,
numberstyle=\footnotesize\color[RGB]{147,161,161},
keywordstyle=\color[RGB]{38,139,210},
commentstyle=\color[RGB]{133,153,0},
stringstyle=\color[RGB]{203,75,22},
directivestyle=\color[RGB]{181,137,0},
identifierstyle=\color[RGB]{238,232,213},
backgroundcolor=\color[RGB]{0,43,54},
breaklines=true}

\lstinputlisting[caption={cpp file used to make the plots in this document.},label=listing:plots.cpp]{plots.cpp}

\end{appendices}

\end{document}