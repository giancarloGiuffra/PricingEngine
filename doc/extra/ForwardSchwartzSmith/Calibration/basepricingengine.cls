% basepricingengine.cls
% Base class for latex documents for PricingEngine library
% 10-Dic-2015 Giancarlo Miguel Giuffra Moncayo

% --- Class structure: identification
% ---
\ProvidesClass{basepricingengine}[2015/12/09 version 1.0.0 Base class for PricingEngine]
\NeedsTeXFormat{LaTeX2e}

% --- Class structure: declaration of options
% ---
% This class extends the article class
% Reads all the documentclass options and passes them to article
\DeclareOption*{\PassOptionToClass{\CurrentOption}{article}}

% --- Class structure: execution of options
% ---
\ProcessOptions \relax

% --- Class structure: declaration of options
% ---
\LoadClass[a4paper]{article}

% --- Author
% ---
\def\@author{Giancarlo Giuffra}
\let\show@author\@author

% --- Project Name
% ---
\def\@project{PricingEngine}
\let\show@project\@project

% --- Title
% ---
\def\@title{None}
\newcommand{\settitle}[1]{%
\def\@title{#1}
\let\show@title\@title}

% --- Date
% ---
\def\@date{\today}
\newcommand{\setdate}[1]{\def\@date{#1}}
\let\show@date\@date

% --- Page Layout
% ---
\RequirePackage[left=1.5in,right=1.5in,top=1in,bottom=1in]{geometry}

% --- Header and Footer
% ---
\RequirePackage{fancyhdr}
\fancypagestyle{pricingengine}{%
\fancyhf{} %clear all six fields
\fancyhead[L]{\show@project}
\fancyhead[R]{\show@author}
\fancyfoot[L]{\show@title}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{1pt}
}
\pagestyle{pricingengine}

% --- Title Page
% ---
\newcommand*{\@titlepage}{%
\begin{titlepage}
\begingroup%
\centering
\vspace*{\fill}
{\Huge \show@title}\\[\baselineskip]
{\large \show@author}\\[0.1\textheight]
{\Large \show@project}\\[\baselineskip]
{\large\scshape \show@date}\par
\vspace*{\fill}
\endgroup
\end{titlepage}}

% --- Table Of Contents
% ---
\RequirePackage[colorlinks=true]{hyperref}
\newcommand*{\@toc}{%
\vspace*{\fill}
\tableofcontents
\vspace*{\fill}
\newpage
}

% --- Packages
% ---
\RequirePackage{mathtools} %math formulas, it loads amsmath
\RequirePackage{graphicx} %import svg images
\RequirePackage{listings} %to include source code
\RequirePackage{dcolumn} %inside tabular to align on decimal point
\RequirePackage[en-GB]{datetime2} % to format date
\RequirePackage[toc,page]{appendix} %more flexible manipulation of appendices
\RequirePackage{xcolor} %to define colors

% --- Date Formats
% ---
\DTMnewdatestyle{d-m-yy}{%
\renewcommand*{\DTMdisplaydate}[4]{%
##3-\DTMenglishshortmonthname{##2}-\DTMtwodigits{##1}}%
\renewcommand*{\DTMDisplaydate}{\DTMdisplaydate}%
}

% --- Begin Document
% ---
\AtBeginDocument{%
\@titlepage
\@toc}
