
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithProcess.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithModel.h>
#include <Commodity\ForwardSchwartzSmith\include\ForwardSchwartzSmithBaroneAdesiWhaleyEngine.h>
#include <Commodity\ForwardSchwartzSmith\include\VanillaOptionFuture.h>

#include <ql\errors.hpp>
#include <ql\time\date.hpp>
#include <ql\time\period.hpp>
#include <ql\settings.hpp>
#include <ql\cashflows\simplecashflow.hpp>
#include <ql\time\daycounters\actual360.hpp>
#include <ql\time\daycounters\thirty360.hpp>
#include <ql\cashflows\coupon.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\termstructures\yield\zeroyieldstructure.hpp>
#include <ql\termstructures\yield\piecewiseyieldcurve.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\option.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\instruments\payoffs.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>

#include <boost\foreach.hpp>
#include <boost\range\irange.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\pointer_cast.hpp>

#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <iomanip>

#include <gnuplot-iostream.h>


using namespace QuantLib;

//enum to identify parameter that is varying
enum Variable {
	None,
	Sigma_S,
	Sigma_L,
	Beta,
	Rho
};

//to simplify notation
template<class T> using VecOfVec = std::vector < std::vector<T> >;

//function that does the plotting
void calculateAndPlotPrices(
	const std::vector<double>& prices_,
	const std::vector<double>& strikes_,
	const std::vector<boost::shared_ptr<VanillaOptionFuture>>& options_,
	boost::shared_ptr<PricingEngine>& engine,
	boost::shared_ptr<ForwardSchwartzSmithProcess>& process,
	const std::vector<Real>& variations = std::vector<Real>(),
	const Variable param = None)
{
	//title for window
	std::string title;
	
	if (!variations.empty() && param != None)
	{
		//set option's pricing engine and calculate model prices
		VecOfVec<double> modelPrices(variations.size());
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			//reset process
			switch (param)
			{
			case Sigma_S:
				title = "Sigma_S_Variation";
				process.reset(new ForwardSchwartzSmithProcess(
					process->spot(),
					process->dividendTS(),
					variations[i],
					process->sigma_L(),
					process->beta(),
					process->rho()
					));
				break;
			case Sigma_L:
				title = "Sigma_L_Variation";
				process.reset(new ForwardSchwartzSmithProcess(
					process->spot(),
					process->dividendTS(),
					process->sigma_S(),
					variations[i],
					process->beta(),
					process->rho()
					));
				break;
			case Beta:
				title = "Beta_Variation";
				process.reset(new ForwardSchwartzSmithProcess(
					process->spot(),
					process->dividendTS(),
					process->sigma_S(),
					process->sigma_L(),
					variations[i],
					process->rho()
					));
				break;
			case Rho:
				title = "Rho_Variation";
				process.reset(new ForwardSchwartzSmithProcess(
					process->spot(),
					process->dividendTS(),
					process->sigma_S(),
					process->sigma_L(),
					process->beta(),
					variations[i]
					));
				break;
			default:
				QL_FAIL("Variable not recognized! : Sigma_S, Sigma_L, Beta, Rho available.");
				break;
			}
			//reset engine
			engine.reset(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process,
				boost::dynamic_pointer_cast<ForwardSchwartzSmithBaroneAdesiWhaleyEngine>(engine)->discountCurve()));
			for (auto j : boost::irange(0, (int)options_.size()))
			{
				options_[j]->setPricingEngine(engine);
				modelPrices[i].push_back(options_[j]->NPV());
			}
		}
		//plot market prices vs initial model prices - ATM
		std::vector<int> atmIndices = { 1, 4, 7, 10 };
		std::vector<std::pair<std::string, double>> atmMarket;
		VecOfVec<std::pair<std::string, double>> atmModel(variations.size());
		std::vector<std::string> atmMaturities;
		std::vector<std::pair<double, double>> smile1Market, smile2Market, smile3Market, smile4Market;
		VecOfVec<std::pair<double, double>>  smile1Model(variations.size()),
			smile2Model(variations.size()),
			smile3Model(variations.size()),
			smile4Model(variations.size());
		Date maturity;
		for (auto i : atmIndices)
		{
			maturity = options_[i]->exercise()->lastDate();
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << maturity.dayOfMonth() << "/"
				<< maturity.month() << "/"
				<< maturity.year();
			atmMaturities.push_back(ss.str());
			atmMarket.push_back(std::pair<std::string, double>(atmMaturities.back(), prices_[i]));
		}
		for (auto i : boost::irange(0, (int)variations.size()))
			for (auto j : boost::irange(0,(int)atmIndices.size()))
			{
				atmModel[i].push_back(std::pair<std::string, double>(atmMaturities[j], modelPrices[i][atmIndices[j]]));
			}
		//smiles
		int nro_strikes = 3;
		for (auto j : boost::irange(0, (int)options_.size()))
		{
			int tmp = j / nro_strikes;
			switch (tmp)
			{
			case 0:
				smile1Market.push_back(std::pair<double, double>(strikes_[j], prices_[j]));
				break;
			case 1:
				smile2Market.push_back(std::pair<double, double>(strikes_[j], prices_[j]));
				break;
			case 2:
				smile3Market.push_back(std::pair<double, double>(strikes_[j], prices_[j]));
				break;
			case 3:
				smile4Market.push_back(std::pair<double, double>(strikes_[j], prices_[j]));
				break;
			default:
				break;
			}
		}
		for (auto i : boost::irange(0, (int)variations.size()))
			for (auto j : boost::irange(0, (int)options_.size()))
			{
				int tmp = j / nro_strikes;
				switch (tmp)
				{
				case 0:
					smile1Model[i].push_back(std::pair<double, double>(strikes_[j], modelPrices[i][j]));
					break;
				case 1:
					smile2Model[i].push_back(std::pair<double, double>(strikes_[j], modelPrices[i][j]));
					break;
				case 2:
					smile3Model[i].push_back(std::pair<double, double>(strikes_[j], modelPrices[i][j]));
					break;
				case 3:
					smile4Model[i].push_back(std::pair<double, double>(strikes_[j], modelPrices[i][j]));
					break;
				default:
					break;
				}
			}
		//actual plotting
		Gnuplot gp;
		gp << "set terminal postscript eps enhanced color font 'Verdana,10'\n";
		gp << "set output '" << title << ".eps'\n";
		gp << "set multiplot layout 3,2\n";
		gp << "set origin 0.25,0.66\n";
		gp << "set title 'ATM Prices'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['01/January/2016':'30/April/2016']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		gp << "plot '-' using 1:2 with linespoints pt 4 title 'market', ";
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp << "'-' using 1:2 with linespoints pt 6 title '" << std::fixed << std::setprecision(2) << variations[i] << "', ";
		}
		gp << std::endl;
		gp.send1d(atmMarket);
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp.send1d(atmModel[i]);
		}
		//smiles - using temporary files for gnuplot
		gp << "set multiplot next\n";
		//smile 1M
		gp << "set title 'Smile 1M'\n";
		gp << "set xdata\n";
		gp << "set format\n";
		gp << "set xrange [*:*]\n";
		gp << "set xtics norotate\n";
		gp << "plot" << gp.file1d(smile1Market) << "with linespoints pt 4 title 'market',";
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp << gp.file1d(smile1Model[i]) << " with linespoints pt 6 title '" <<
				std::fixed << std::setprecision(2) << variations[i] << "', ";
		}
		gp << std::endl;
		//smile 2M
		gp << "set title 'Smile 2M'\n";
		gp << "plot" << gp.file1d(smile2Market) << "with linespoints pt 4 title 'market',";
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp << gp.file1d(smile2Model[i]) << " with linespoints pt 6 title '" <<
				std::fixed << std::setprecision(2) << variations[i] << "', ";
		}
		gp << std::endl;
		//smile 3M
		gp << "set title 'Smile 3M'\n";
		gp << "plot" << gp.file1d(smile3Market) << "with linespoints pt 4 title 'market',";
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp << gp.file1d(smile3Model[i]) << " with linespoints pt 6 title '" <<
				std::fixed << std::setprecision(2) << variations[i] << "', ";
		}
		gp << std::endl;
		//smile 4M
		gp << "set title 'Smile 4M'\n";
		gp << "plot" << gp.file1d(smile4Market) << "with linespoints pt 4 title 'market',";
		for (auto i : boost::irange(0, (int)variations.size()))
		{
			gp << gp.file1d(smile4Model[i]) << " with linespoints pt 6 title '" <<
				std::fixed << std::setprecision(2) << variations[i] << "', ";
		}
		gp << std::endl;
		gp << "unset multiplot\n";

	}
	else
	{
		//set option's pricing engine and calculate model prices
		std::vector<double> modelPrices;
		for (auto i : boost::irange(0, (int)options_.size()))
		{
			options_[i]->setPricingEngine(engine);
			modelPrices.push_back(options_[i]->NPV());
		}
		//plot market prices vs initial model prices - ATM
		std::vector<int> atmIndices = { 1, 4, 7, 10 };
		std::vector<std::pair<std::string, double>> atmMarket, atmModel;
		std::vector<std::string> atmMaturities;
		std::vector<std::pair<double, double>> smile1Market, smile1Model, smile2Market, smile2Model, smile3Market,
			smile3Model, smile4Market, smile4Model;
		Date maturity;
		for (auto i : atmIndices)
		{
			maturity = options_[i]->exercise()->lastDate();
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << maturity.dayOfMonth() << "/"
				<< maturity.month() << "/"
				<< maturity.year();
			atmMaturities.push_back(ss.str());
			atmMarket.push_back(std::pair<std::string, double>(atmMaturities.back(), prices_[i]));
			atmModel.push_back(std::pair<std::string, double>(atmMaturities.back(), modelPrices[i]));
		}
		int nro_strikes = 3;
		for (auto i : boost::irange(0, (int)options_.size()))
		{
			int tmp = i / nro_strikes;
			switch (tmp)
			{
			case 0:
				smile1Market.push_back(std::pair<double, double>(strikes_[i], prices_[i]));
				smile1Model.push_back(std::pair<double, double>(strikes_[i], modelPrices[i]));
				break;
			case 1:
				smile2Market.push_back(std::pair<double, double>(strikes_[i], prices_[i]));
				smile2Model.push_back(std::pair<double, double>(strikes_[i], modelPrices[i]));
				break;
			case 2:
				smile3Market.push_back(std::pair<double, double>(strikes_[i], prices_[i]));
				smile3Model.push_back(std::pair<double, double>(strikes_[i], modelPrices[i]));
				break;
			case 3:
				smile4Market.push_back(std::pair<double, double>(strikes_[i], prices_[i]));
				smile4Model.push_back(std::pair<double, double>(strikes_[i], modelPrices[i]));
				break;
			default:
				break;
			}
		}
		//actual plotting
		Gnuplot gp;
		gp << "set terminal wxt title 'Initial Parameters' size 1000,800\n";
		gp << "set multiplot layout 3,2\n";
		gp << "set origin 0.25,0.66\n";
		gp << "set title 'ATM Prices'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['01/January/2016':'30/April/2016']\n";
		gp << "set format x '%b-%y'\n";
		gp << "set xtics rotate by -45\n";
		gp << "plot '-' using 1:2 with linespoints pt 4 title 'market', '-' using 1:2 with linespoints pt 6 title 'model'\n";
		gp.send1d(atmMarket);
		gp.send1d(atmModel);
		//smiles
		gp << "set multiplot next\n";
		gp << "set title 'Smile 1M'\n";
		gp << "set xdata\n";
		gp << "set format\n";
		gp << "set xrange [*:*]\n";
		gp << "set xtics norotate\n";
		gp << "plot '-' with linespoints pt 4 title 'market', '-' with linespoints pt 6 title 'model'\n";
		gp.send1d(smile1Market);
		gp.send1d(smile1Model);
		gp << "set title 'Smile 2M'\n";
		gp << "plot '-' with linespoints pt 4 title 'market', '-' with linespoints pt 6 title 'model'\n";
		gp.send1d(smile2Market);
		gp.send1d(smile2Model);
		gp << "set title 'Smile 3M'\n";
		gp << "plot '-' with linespoints pt 4 title 'market', '-' with linespoints pt 6 title 'model'\n";
		gp.send1d(smile3Market);
		gp.send1d(smile3Model);
		gp << "set title 'Smile 4M'\n";
		gp << "plot '-' with linespoints pt 4 title 'market', '-' with linespoints pt 6 title 'model'\n";
		gp.send1d(smile4Market);
		gp.send1d(smile4Model);
		gp << "unset multiplot\n";
	}
}

int main(void)
{
	try {
		//spot
		RelinkableHandle<Quote> spot;
		boost::shared_ptr<Quote> quote(new SimpleQuote(46.66));
		spot.linkTo(quote);
		//futures
		std::vector<Date> dates;
		std::vector<Rate> yields;
		std::vector<double> futures = { quote->value(), 46.71, 47.22, 47.94, 48.61, 49.14, 49.57, 50.02, 50.33, 50.75,
			50.94, 50.83, 52.47 };
		Date today = Date::todaysDate();
		DayCounter dc = Actual365Fixed();
		Rate q;
		for (auto i : boost::irange(0, (int)futures.size()))
		{
			dates.push_back(today + Period(i, Months));
			q = (i == 0) ? 0 : log(futures[i] / spot->value()) / dc.yearFraction(today, dates.back());
			yields.push_back(q);
		}
		boost::shared_ptr<YieldTermStructure> dividends(new InterpolatedZeroCurve<BackwardFlat>(dates, yields, dc, TARGET()));
		RelinkableHandle<YieldTermStructure> dividendTS;
		dividendTS.linkTo(dividends);
		Real sigma_S = 0.30;
		Real sigma_L = 0.20;
		Real beta = 0.8;
		Real rho = 0.5;
		boost::shared_ptr<ForwardSchwartzSmithProcess> process( 
			new ForwardSchwartzSmithProcess(spot, dividendTS, sigma_S, sigma_L, beta, rho));
		//options - prices given, 3 strikes per maturity around future price, maturities up to 4 months, future expiry + 1 month
		std::vector<double> prices_ = { 1.88, 1.63, 1.27, 3.10, 2.45, 2.25, 3.30, 3.29, 3.03, 4.05, 3.80, 3.56 };
		int nro_strikes = 3;
		double delta_strike = 0.5;
		std::vector<double> strikes_ = { 46.71 - delta_strike, 46.71, 46.71 + delta_strike,
			47.22 - delta_strike, 47.22, 47.22 + delta_strike,
			47.94 - delta_strike, 47.94, 47.94 + delta_strike,
			48.61 - delta_strike, 48.61, 48.61 + delta_strike };
		std::vector<boost::shared_ptr<VanillaOptionFuture>> options_;
		for (auto i : boost::irange(0, (int)prices_.size()))
		{
			boost::shared_ptr<StrikedTypePayoff> payoff = boost::shared_ptr<StrikedTypePayoff>(
				new PlainVanillaPayoff(Option::Type::Call, strikes_[i]));
			boost::shared_ptr<Exercise> exercise = boost::shared_ptr<Exercise>(
				new AmericanExercise(today, today + Period(1 + i / nro_strikes, Months)));
			options_.push_back(boost::shared_ptr<VanillaOptionFuture>(
				new VanillaOptionFuture(
				payoff,
				exercise,
				exercise->lastDate() + Period(1, Months)
				)));
		}
		//discounts
		Rate riskFreeRate = 0.01;
		Handle<YieldTermStructure> flatTermStructure(
			boost::shared_ptr<YieldTermStructure>(
			new FlatForward(dividendTS->referenceDate(), riskFreeRate, dividendTS->dayCounter())));
		//pricing engine
		boost::shared_ptr<PricingEngine> bawEngine(new ForwardSchwartzSmithBaroneAdesiWhaleyEngine(process, flatTermStructure));
		
		/*PLOTTING*/
		//initial parameters
		std::cout << "sigma_s: " << sigma_S << std::endl;
		std::cout << "sigma_l: " << sigma_L << std::endl;
		std::cout << "beta: " << beta << std::endl;
		std::cout << "rho: " << rho << std::endl;
		std::cout << std::endl;

		//plot - sigma_S variation
		std::vector<Real> sigma_s_variation = {0.10, 0.20, 0.30, 0.40};
		calculateAndPlotPrices(prices_, strikes_, options_, bawEngine, process, sigma_s_variation, Sigma_S);
		//plot - sigma_L variation
		process.reset(new ForwardSchwartzSmithProcess(spot, dividendTS, sigma_S, sigma_L, beta, rho));
		std::vector<Real> sigma_l_variation = { 0.10, 0.20, 0.30, 0.40 };
		calculateAndPlotPrices(prices_, strikes_, options_, bawEngine, process, sigma_l_variation, Sigma_L);
		//plot - beta variation
		process.reset(new ForwardSchwartzSmithProcess(spot, dividendTS, sigma_S, sigma_L, beta, rho));
		std::vector<Real> beta_variation = { 0.01, 0.10, 0.5, 1 };
		calculateAndPlotPrices(prices_, strikes_, options_, bawEngine, process, beta_variation, Beta);
		//plot - rho variation
		process.reset(new ForwardSchwartzSmithProcess(spot, dividendTS, sigma_S, sigma_L, beta, rho));
		std::vector<Real> rho_variation = { -0.9, -0.5, 0, 0.5, 0.9 };
		calculateAndPlotPrices(prices_, strikes_, options_, bawEngine, process, rho_variation, Rho);
		
	}
	catch (Error e) {
		std::cerr << e.what() << std::endl;
	}

	// wait for enter key to exit application
	std::cout << "Press ENTER to quit" << std::endl;
	char dummy[2];
	std::cin.getline(dummy, 2);
	return 0;
}