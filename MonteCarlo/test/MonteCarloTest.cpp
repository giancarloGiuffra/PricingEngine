#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
#include <gtest\gtest.h>
  using ::testing::Test;

namespace MonteCarlo
{
namespace testing
{
    class MonteCarloTest : public Test
    {
    protected:
        MonteCarloTest(){}
        ~MonteCarloTest(){}
		
		virtual void SetUp(){}
        virtual void TearDown(){}

    };

} //namespace testing
} //namespace MarketData