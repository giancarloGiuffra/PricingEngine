#ifndef MONTE_CARLO_TRAITS_H
#define MONTE_CARLO_TRAITS_H

#include <ql\methods\montecarlo\mctraits.hpp>

using namespace QuantLib;

/*! \brief Monte Carlo traits for Path Pricer with Array results.
*/
template <class RNG = PseudoRandom>
struct MultiVariateWithArrayResults {
	typedef RNG rng_traits;
	typedef MultiPath path_type;
	typedef PathPricer<path_type, Array> path_pricer_type; //only difference with QuantLib::MultiVariate
	typedef typename RNG::rsg_type rsg_type;
	typedef MultiPathGenerator<rsg_type> path_generator_type;
	enum { allowsErrorEstimate = RNG::allowsErrorEstimate };
};

#endif // !MONTE_CARLO_TRAITS_H
