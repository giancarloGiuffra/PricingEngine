#ifndef SEQUENCE_STATISTICS_ARRAY_INSPECTORS_H
#define SEQUENCE_STATISTICS_ARRAY_INSPECTORS_H

#include <ql\math\statistics\sequencestatistics.hpp>

using namespace QuantLib;

/*! \brief SequenceStatistics with N-D Array Inspectors.

*/
class SequenceStatisticsArrayInspectors : public SequenceStatistics
{
public:

	/** Constructor.
	@param dimension
	*/
	SequenceStatisticsArrayInspectors(Size dimension = 0);

	//! \name N-D inspectors lifted from underlying statistics class
	//@{
	
	/** Mean.
	@return Array
	*/
	Array mean() const;

	/** Variance.
	@return Array
	*/
	Array variance() const;

	/** Standard Deviation.
	@return Array
	*/
	Array standardDeviation() const;

	/** Error Estimate.
	@return Array
	*/
	Array errorEstimate() const;

	//@}
};

#endif