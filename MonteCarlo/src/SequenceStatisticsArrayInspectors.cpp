#include "SequenceStatisticsArrayInspectors.h"

/*constructor*/
SequenceStatisticsArrayInspectors::SequenceStatisticsArrayInspectors(Size dimension)
	: SequenceStatistics(dimension)
{}

/*mean*/
Array SequenceStatisticsArrayInspectors::mean() const
{
	std::vector<Real> a = SequenceStatistics::mean();
	return Array(a.begin(), a.end());
}

/*variance*/
Array SequenceStatisticsArrayInspectors::variance() const
{
	std::vector<Real> a = SequenceStatistics::variance();
	return Array(a.begin(), a.end());
}

/*Standard Deviation*/
Array SequenceStatisticsArrayInspectors::standardDeviation() const
{
	std::vector<Real> a = SequenceStatistics::standardDeviation();
	return Array(a.begin(), a.end());
}

/*Error Estimate*/
Array SequenceStatisticsArrayInspectors::errorEstimate() const
{
	std::vector<Real> a = SequenceStatistics::errorEstimate();
	return Array(a.begin(), a.end());
}
