#include <MarketData\include\OISCurveMktDataRequest.h>
#include <MarketData\include\EONIAQuery.h>
#include <MarketData\include\OISCurveQuery.h>
#include <MarketData\include\OISCurveEUR.h>
#include <MarketData\include\BaseMarketDataRequest.h>
#include <MarketData\include\EONIAMktDataRequest.h>
#include <MarKetQuote\include\OISQuote.h>
#include <MarketData\include\FutureStripBrent.h>
#include <MarketData\include\FilterByVolume.h>
#include <MarketData\include\FilterByOpenInterest.h>
#include <MarketData\include\FutureStripMktDataRequest.h>
#include <MarketData\include\OptionsMktDataRequest.h>
#include <MarketData\include\CommodityOptionsMktDataRequest.h>
#include <MarketData\include\UtilityFunctions.h>
#include <MarketData\include\FilterIf.h>
#include <MarketData\include\FilterIfITM.h>
#include <MarketData\include\FilterIfBidAskSpreadGreaterThan.h>
#include <MarketData\include\BaseFileMktDataRequest.h>
#include <MarketData\include\FileMktDataRequest.h>
#include <MarketData\include\OISCurveFileMktDataRequest.h>
#include <MarketData\include\OptionsFileMktDataRequest.h>

#include <algorithm>
#include <iterator>
#include <regex>
#include <fstream>
#include <cstdio>

#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::ContainerEq;
  using ::testing::Contains;
  using ::testing::UnorderedElementsAreArray;
  using ::testing::IsEmpty;
  using ::testing::Ne;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <exception>
#include <boost\foreach.hpp>

namespace MarketData
{
namespace testing
{
    class MarketDataTest : public Test
    {
    protected:
        MarketDataTest(){}
        ~MarketDataTest(){}
		
		virtual void SetUp(){
			_securities = { "SX5E Index", "SBE Index" };
			_fields = { "BID", "ASK" };
			_request = BaseMarketDataRequest(_securities, _fields);
			option_tickers = {
				"SX5E 01/15/16 C2300 Index",
				"SX5E 01/15/16 C1150 Index",
				"SX5E 02/19/16 P1400 Index",
				"SX5E 02/19/16 P3675 Index",
				"SX5E 02/19/16 P3225 Index",
				"SX5E 02/19/16 P5200 Index",
				"SX5E 01/15/16 P4200 Index",
				"SX5E 01/15/16 C2475 Index",
				"SX5E 02/19/16 P3375 Index",
				"SX5E 01/15/16 P3625 Index",
				"SX5E 02/19/16 C3675 Index",
				"SX5E 02/19/16 P2800 Index",
				"SX5E 01/15/16 C3475 Index",
				"SX5E 02/19/16 P2750 Index",
				"SX5E 01/15/16 P3125 Index",
				"SX5E 01/15/16 P3450 Index",
				"SX5E 02/19/16 C3525 Index",
				"SX5E 02/19/16 P4950 Index",
				"SX5E 02/19/16 C1950 Index",
				"SX5E 01/15/16 P4100 Index",
				"SX5E 02/19/16 C3575 Index",
				"SX5E 02/19/16 C1150 Index",
				"SX5E 02/19/16 C3275 Index",
				"SX5E 02/19/16 P2975 Index",
				"SX5E 02/19/16 C4800 Index",
				"SX5E 02/19/16 P3275 Index",
				"SX5E 02/19/16 C4300 Index",
				"SX5E 01/15/16 C3750 Index",
				"SX5E 01/15/16 C2100 Index",
				"SX5E 02/19/16 C4450 Index",
				"SX5E 02/19/16 P2175 Index",
				"SX5E 02/19/16 C1050 Index",
				"SX5E 01/15/16 P2050 Index",
				"SX5E 01/15/16 P3075 Index",
				"SX5E 02/19/16 C5800 Index",
				"SX5E 02/19/16 C5600 Index",
				"SX5E 01/15/16 P3750 Index",
				"SX5E 02/19/16 C4100 Index",
				"SX5E 02/19/16 P2025 Index",
				"SX5E 01/15/16 P1250 Index",
				"SX5E 02/19/16 P3125 Index",
				"SX5E 02/19/16 C3875 Index",
				"SX5E 02/19/16 P3775 Index",
				"SX5E 02/19/16 C4650 Index",
				"SX5E 01/15/16 C1950 Index",
				"SX5E 01/15/16 P2525 Index",
				"SX5E 02/19/16 P2375 Index",
				"SX5E 01/15/16 P2575 Index",
				"SX5E 01/15/16 C1350 Index",
				"SX5E 01/15/16 C4000 Index",
				"SX5E 02/19/16 P1850 Index",
				"SX5E 02/19/16 P5400 Index",
				"SX5E 01/15/16 P4125 Index",
				"SX5E 02/19/16 P3600 Index",
				"SX5E 02/19/16 P1050 Index",
				"SX5E 01/15/16 C2700 Index",
				"SX5E 02/19/16 P2875 Index",
				"SX5E 02/19/16 C2700 Index",
				"SX5E 01/15/16 C1000 Index",
				"SX5E 02/19/16 P2300 Index",
				"SX5E 01/15/16 P1000 Index",
				"SX5E 02/19/16 C2250 Index",
				"SX5E 02/19/16 P3050 Index",
				"SX5E 02/19/16 C3000 Index",
				"SX5E 02/19/16 P3425 Index",
				"SX5E 02/19/16 P3400 Index",
				"SX5E 02/19/16 P1700 Index",
				"SX5E 01/15/16 C3900 Index",
				"SX5E 02/19/16 P3650 Index",
				"SX5E 01/15/16 P3350 Index",
				"SX5E 02/19/16 P2925 Index",
				"SX5E 01/15/16 C3100 Index",
				"SX5E 01/15/16 C4200 Index",
				"SX5E 02/19/16 P3875 Index",
				"SX5E 02/19/16 P1650 Index",
				"SX5E 02/19/16 C2150 Index",
				"SX5E 01/15/16 C2750 Index",
				"SX5E 01/15/16 P3175 Index",
				"SX5E 01/15/16 C4025 Index",
				"SX5E 02/19/16 C4900 Index",
				"SX5E 02/19/16 C3175 Index",
				"SX5E 02/19/16 C2525 Index",
				"SX5E 02/19/16 P2700 Index",
				"SX5E 01/15/16 P3800 Index",
				"SX5E 01/15/16 P5400 Index",
				"SX5E 02/19/16 C3950 Index",
				"SX5E 02/19/16 C3400 Index",
				"SX5E 01/15/16 P5200 Index",
				"SX5E 01/15/16 C3875 Index",
				"SX5E 01/15/16 P1150 Index",
				"SX5E 01/15/16 P2475 Index",
				"SX5E 02/19/16 P3075 Index",
				"SX5E 01/15/16 C5800 Index",
				"SX5E 01/15/16 C2675 Index",
				"SX5E 02/19/16 C2400 Index",
				"SX5E 01/15/16 P4600 Index",
				"SX5E 02/19/16 P1000 Index",
				"SX5E 01/15/16 P3400 Index",
				"SX5E 01/15/16 C2450 Index",
				"SX5E 01/15/16 P3650 Index",
				"SX5E 01/15/16 C3525 Index",
				"SX5E 02/19/16 C3925 Index",
				"SX5E 01/15/16 C2425 Index",
				"SX5E 01/15/16 P4025 Index",
				"SX5E 01/15/16 P4050 Index",
				"SX5E 01/15/16 C2500 Index",
				"SX5E 01/15/16 P2900 Index",
				"SX5E 01/15/16 P2925 Index",
				"SX5E 02/19/16 C1600 Index",
				"SX5E 02/19/16 C3600 Index",
				"SX5E 02/19/16 C3025 Index",
				"SX5E 02/19/16 P4850 Index",
				"SX5E 01/15/16 C1700 Index",
				"SX5E 02/19/16 P1550 Index",
				"SX5E 01/15/16 C2850 Index",
				"SX5E 01/15/16 P4075 Index",
				"SX5E 02/19/16 C5000 Index",
				"SX5E 01/15/16 C2050 Index",
				"SX5E 02/19/16 P4800 Index",
				"SX5E 02/19/16 P1800 Index",
				"SX5E 02/19/16 C1650 Index",
				"SX5E 01/15/16 P2550 Index",
				"SX5E 02/19/16 C3350 Index",
				"SX5E 01/15/16 C4050 Index",
				"SX5E 01/15/16 C3825 Index",
				"SX5E 02/19/16 C2000 Index",
				"SX5E 01/15/16 P3100 Index",
				"SX5E 01/15/16 C2575 Index",
				"SX5E 01/15/16 C5600 Index",
				"SX5E 02/19/16 P3000 Index",
				"SX5E 01/15/16 P2350 Index",
				"SX5E 01/15/16 P1950 Index",
				"SX5E 01/15/16 P3200 Index",
				"SX5E 02/19/16 P1750 Index",
				"SX5E 02/19/16 P2550 Index",
				"SX5E 02/19/16 C3450 Index",
				"SX5E 02/19/16 C4850 Index",
				"SX5E 02/19/16 C1350 Index",
				"SX5E 02/19/16 C3150 Index",
				"SX5E 02/19/16 C3425 Index",
				"SX5E 01/15/16 C3250 Index",
				"SX5E 02/19/16 C4700 Index",
				"SX5E 02/19/16 C2275 Index",
				"SX5E 02/19/16 C2350 Index",
				"SX5E 01/15/16 C3800 Index",
				"SX5E 02/19/16 P2450 Index",
				"SX5E 02/19/16 C2550 Index",
				"SX5E 02/19/16 C2850 Index",
				"SX5E 01/15/16 P3300 Index",
				"SX5E 01/15/16 C1250 Index",
				"SX5E 02/19/16 P3975 Index",
				"SX5E 02/19/16 C1900 Index",
				"SX5E 02/19/16 C2125 Index",
				"SX5E 01/15/16 C2625 Index",
				"SX5E 02/19/16 C2200 Index",
				"SX5E 01/15/16 C2725 Index",
				"SX5E 02/19/16 P2425 Index",
				"SX5E 01/15/16 C3200 Index",
				"SX5E 01/15/16 C3850 Index",
				"SX5E 02/19/16 P4900 Index",
				"SX5E 02/19/16 C4150 Index",
				"SX5E 01/15/16 C1850 Index",
				"SX5E 01/15/16 C2900 Index",
				"SX5E 01/15/16 P1400 Index",
				"SX5E 02/19/16 C3075 Index",
				"SX5E 01/15/16 P6000 Index",
				"SX5E 01/15/16 P1300 Index",
				"SX5E 01/15/16 P2975 Index",
				"SX5E 02/19/16 C2975 Index",
				"SX5E 01/15/16 C3375 Index",
				"SX5E 01/15/16 C1900 Index",
				"SX5E 01/15/16 C3425 Index",
				"SX5E 01/15/16 C3175 Index",
				"SX5E 02/19/16 P3175 Index",
				"SX5E 02/19/16 C2475 Index",
				"SX5E 02/19/16 C2600 Index",
				"SX5E 02/19/16 C4400 Index",
				"SX5E 01/15/16 C3050 Index",
				"SX5E 02/19/16 C1450 Index",
				"SX5E 01/15/16 P3875 Index",
				"SX5E 01/15/16 P1850 Index",
				"SX5E 02/19/16 P1350 Index",
				"SX5E 01/15/16 P3900 Index",
				"SX5E 02/19/16 C3250 Index",
				"SX5E 02/19/16 P3250 Index",
				"SX5E 02/19/16 C2050 Index",
				"SX5E 02/19/16 P3900 Index",
				"SX5E 02/19/16 P4650 Index",
				"SX5E 02/19/16 P3625 Index",
				"SX5E 01/15/16 P1350 Index",
				"SX5E 02/19/16 C3200 Index",
				"SX5E 02/19/16 C6000 Index",
				"SX5E 02/19/16 P2100 Index",
				"SX5E 01/15/16 P2850 Index",
				"SX5E 01/15/16 P3575 Index",
				"SX5E 02/19/16 P5000 Index",
				"SX5E 02/19/16 C4200 Index",
				"SX5E 02/19/16 C3475 Index",
				"SX5E 02/19/16 C3300 Index",
				"SX5E 02/19/16 C2225 Index",
				"SX5E 02/19/16 P1950 Index",
				"SX5E 01/15/16 C1800 Index",
				"SX5E 01/15/16 P2150 Index",
				"SX5E 02/19/16 C1550 Index",
				"SX5E 01/15/16 C2925 Index",
				"SX5E 01/15/16 C2000 Index",
				"SX5E 01/15/16 C3675 Index",
				"SX5E 01/15/16 C3550 Index",
				"SX5E 02/19/16 P3925 Index",
				"SX5E 01/15/16 C1100 Index",
				"SX5E 01/15/16 P4175 Index",
				"SX5E 02/19/16 C3050 Index",
				"SX5E 02/19/16 P2600 Index",
				"SX5E 01/15/16 P3925 Index",
				"SX5E 02/19/16 P2525 Index",
				"SX5E 01/15/16 P3275 Index",
				"SX5E 02/19/16 P1150 Index",
				"SX5E 02/19/16 C2900 Index",
				"SX5E 02/19/16 P3550 Index",
				"SX5E 01/15/16 C3275 Index",
				"SX5E 02/19/16 P4000 Index",
				"SX5E 02/19/16 C3625 Index",
				"SX5E 01/15/16 P2725 Index",
				"SX5E 01/15/16 C2550 Index",
				"SX5E 02/19/16 C5400 Index",
				"SX5E 01/15/16 C5000 Index",
				"SX5E 01/15/16 P1050 Index",
				"SX5E 01/15/16 P2650 Index",
				"SX5E 02/19/16 P3525 Index",
				"SX5E 01/15/16 C5200 Index",
				"SX5E 02/19/16 P4050 Index",
				"SX5E 02/19/16 C1250 Index",
				"SX5E 02/19/16 P4750 Index",
				"SX5E 01/15/16 C3575 Index",
				"SX5E 01/15/16 C2400 Index",
				"SX5E 02/19/16 P2900 Index",
				"SX5E 02/19/16 C2625 Index",
				"SX5E 02/19/16 C4500 Index",
				"SX5E 01/15/16 P3000 Index",
				"SX5E 02/19/16 P3350 Index",
				"SX5E 01/15/16 C4125 Index",
				"SX5E 02/19/16 P6000 Index",
				"SX5E 02/19/16 C3500 Index",
				"SX5E 01/15/16 P5800 Index",
				"SX5E 01/15/16 P2775 Index",
				"SX5E 02/19/16 P1200 Index",
				"SX5E 01/15/16 P2750 Index",
				"SX5E 02/19/16 P3725 Index",
				"SX5E 02/19/16 P2475 Index",
				"SX5E 01/15/16 C1400 Index",
				"SX5E 02/19/16 P3100 Index",
				"SX5E 02/19/16 P5600 Index",
				"SX5E 01/15/16 C4075 Index",
				"SX5E 02/19/16 P2325 Index",
				"SX5E 02/19/16 C2100 Index",
				"SX5E 02/19/16 C1200 Index",
				"SX5E 02/19/16 C2775 Index",
				"SX5E 02/19/16 C2950 Index",
				"SX5E 02/19/16 P2575 Index",
				"SX5E 02/19/16 P3475 Index",
				"SX5E 01/15/16 C1300 Index",
				"SX5E 01/15/16 P1700 Index",
				"SX5E 01/15/16 P1900 Index",
				"SX5E 02/19/16 C4950 Index",
				"SX5E 01/15/16 P2625 Index",
				"SX5E 02/19/16 C2175 Index",
				"SX5E 02/19/16 C2325 Index",
				"SX5E 02/19/16 P1250 Index",
				"SX5E 01/15/16 C3075 Index",
				"SX5E 02/19/16 P2075 Index",
				"SX5E 02/19/16 P3150 Index",
				"SX5E 02/19/16 P2625 Index",
				"SX5E 02/19/16 C3550 Index",
				"SX5E 02/19/16 C3900 Index",
				"SX5E 01/15/16 C3000 Index",
				"SX5E 02/19/16 P2825 Index",
				"SX5E 02/19/16 P3750 Index",
				"SX5E 02/19/16 P2050 Index",
				"SX5E 02/19/16 C1500 Index",
				"SX5E 02/19/16 C1000 Index",
				"SX5E 02/19/16 P1900 Index",
				"SX5E 02/19/16 P4150 Index",
				"SX5E 02/19/16 P3575 Index",
				"SX5E 02/19/16 P3800 Index",
				"SX5E 01/15/16 P3050 Index",
				"SX5E 01/15/16 P2450 Index",
				"SX5E 02/19/16 P2350 Index",
				"SX5E 01/15/16 C3400 Index",
				"SX5E 01/15/16 C1750 Index",
				"SX5E 02/19/16 C4250 Index",
				"SX5E 01/15/16 P2950 Index",
				"SX5E 02/19/16 C2450 Index",
				"SX5E 01/15/16 P4000 Index",
				"SX5E 01/15/16 P1100 Index",
				"SX5E 01/15/16 P3225 Index",
				"SX5E 01/15/16 P3825 Index",
				"SX5E 01/15/16 C3600 Index",
				"SX5E 02/19/16 P1600 Index",
				"SX5E 02/19/16 P2650 Index",
				"SX5E 01/15/16 P2800 Index",
				"SX5E 01/15/16 P3250 Index",
				"SX5E 02/19/16 P2775 Index",
				"SX5E 02/19/16 C2500 Index",
				"SX5E 01/15/16 C2650 Index",
				"SX5E 02/19/16 C4050 Index",
				"SX5E 01/15/16 C2250 Index",
				"SX5E 02/19/16 P2250 Index",
				"SX5E 01/15/16 C3725 Index",
				"SX5E 02/19/16 P4700 Index",
				"SX5E 02/19/16 P2225 Index",
				"SX5E 01/15/16 C3950 Index",
				"SX5E 01/15/16 P3425 Index",
				"SX5E 02/19/16 C2825 Index",
				"SX5E 02/19/16 C2650 Index",
				"SX5E 01/15/16 P2200 Index",
				"SX5E 02/19/16 C3325 Index",
				"SX5E 01/15/16 P2000 Index",
				"SX5E 02/19/16 C2925 Index",
				"SX5E 01/15/16 P5000 Index",
				"SX5E 02/19/16 P2000 Index",
				"SX5E 01/15/16 C3775 Index",
				"SX5E 02/19/16 P5800 Index",
				"SX5E 01/15/16 C3300 Index",
				"SX5E 02/19/16 C2300 Index",
				"SX5E 02/19/16 C3225 Index",
				"SX5E 01/15/16 P3950 Index",
				"SX5E 02/19/16 C4600 Index",
				"SX5E 01/15/16 C2825 Index",
				"SX5E 02/19/16 P2150 Index",
				"SX5E 02/19/16 P1300 Index",
				"SX5E 02/19/16 C4350 Index",
				"SX5E 01/15/16 C6000 Index",
				"SX5E 01/15/16 P2675 Index",
				"SX5E 02/19/16 P1100 Index",
				"SX5E 01/15/16 P1750 Index",
				"SX5E 01/15/16 P3025 Index",
				"SX5E 02/19/16 C3125 Index",
				"SX5E 01/15/16 C1200 Index",
				"SX5E 01/15/16 P3675 Index",
				"SX5E 01/15/16 P2600 Index",
				"SX5E 01/15/16 C3350 Index",
				"SX5E 01/15/16 C3325 Index",
				"SX5E 02/19/16 P2725 Index",
				"SX5E 02/19/16 C3375 Index",
				"SX5E 02/19/16 P4400 Index",
				"SX5E 02/19/16 C1800 Index",
				"SX5E 01/15/16 C4175 Index",
				"SX5E 01/15/16 P3550 Index",
				"SX5E 01/15/16 P3850 Index",
				"SX5E 02/19/16 C2575 Index",
				"SX5E 01/15/16 P3600 Index",
				"SX5E 02/19/16 C3725 Index",
				"SX5E 01/15/16 P3475 Index",
				"SX5E 01/15/16 P3500 Index",
				"SX5E 01/15/16 P2500 Index",
				"SX5E 01/15/16 C4600 Index",
				"SX5E 02/19/16 P2400 Index",
				"SX5E 02/19/16 C3825 Index",
				"SX5E 02/19/16 C3700 Index",
				"SX5E 02/19/16 C1850 Index",
				"SX5E 01/15/16 C3225 Index",
				"SX5E 02/19/16 P4450 Index",
				"SX5E 01/15/16 P2875 Index",
				"SX5E 01/15/16 P3150 Index",
				"SX5E 01/15/16 C1050 Index",
				"SX5E 01/15/16 P1600 Index",
				"SX5E 02/19/16 P3325 Index",
				"SX5E 02/19/16 C3975 Index",
				"SX5E 01/15/16 C2975 Index",
				"SX5E 02/19/16 P4500 Index",
				"SX5E 02/19/16 C1300 Index",
				"SX5E 01/15/16 P2250 Index",
				"SX5E 01/15/16 C3125 Index",
				"SX5E 01/15/16 P2425 Index",
				"SX5E 02/19/16 C1700 Index",
				"SX5E 02/19/16 P1450 Index",
				"SX5E 01/15/16 P3375 Index",
				"SX5E 01/15/16 P1800 Index",
				"SX5E 02/19/16 P4100 Index",
				"SX5E 01/15/16 P2825 Index",
				"SX5E 02/19/16 C2075 Index",
				"SX5E 02/19/16 P1500 Index",
				"SX5E 02/19/16 P3950 Index",
				"SX5E 02/19/16 C2875 Index",
				"SX5E 01/15/16 C2350 Index",
				"SX5E 01/15/16 P3325 Index",
				"SX5E 01/15/16 C1600 Index",
				"SX5E 02/19/16 P3450 Index",
				"SX5E 01/15/16 C4100 Index",
				"SX5E 01/15/16 C3150 Index",
				"SX5E 01/15/16 P3775 Index",
				"SX5E 02/19/16 C3650 Index",
				"SX5E 02/19/16 P4550 Index",
				"SX5E 01/15/16 P3975 Index",
				"SX5E 02/19/16 P3700 Index",
				"SX5E 01/15/16 C3650 Index",
				"SX5E 02/19/16 P2200 Index",
				"SX5E 01/15/16 P2100 Index",
				"SX5E 01/15/16 C3500 Index",
				"SX5E 01/15/16 P1200 Index",
				"SX5E 02/19/16 P2275 Index",
				"SX5E 01/15/16 P3700 Index",
				"SX5E 02/19/16 P4300 Index",
				"SX5E 02/19/16 P3850 Index",
				"SX5E 02/19/16 C2675 Index",
				"SX5E 01/15/16 C3975 Index",
				"SX5E 02/19/16 C3750 Index",
				"SX5E 02/19/16 P4600 Index",
				"SX5E 01/15/16 C3450 Index",
				"SX5E 02/19/16 P2950 Index",
				"SX5E 02/19/16 P4350 Index",
				"SX5E 01/15/16 P2400 Index",
				"SX5E 02/19/16 P3200 Index",
				"SX5E 01/15/16 C3925 Index",
				"SX5E 02/19/16 C4000 Index",
				"SX5E 01/15/16 C2775 Index",
				"SX5E 01/15/16 C3700 Index",
				"SX5E 02/19/16 C2725 Index",
				"SX5E 01/15/16 C2600 Index",
				"SX5E 02/19/16 C2750 Index",
				"SX5E 02/19/16 C4750 Index",
				"SX5E 02/19/16 C1100 Index",
				"SX5E 01/15/16 C2525 Index",
				"SX5E 02/19/16 P2500 Index",
				"SX5E 01/15/16 C2200 Index",
				"SX5E 02/19/16 P2125 Index",
				"SX5E 02/19/16 C3800 Index",
				"SX5E 02/19/16 C4550 Index",
				"SX5E 02/19/16 P3500 Index",
				"SX5E 02/19/16 C1750 Index",
				"SX5E 02/19/16 C2425 Index",
				"SX5E 01/15/16 C2800 Index",
				"SX5E 01/15/16 C5400 Index",
				"SX5E 02/19/16 C2800 Index",
				"SX5E 02/19/16 P4200 Index",
				"SX5E 02/19/16 P3025 Index",
				"SX5E 01/15/16 P3525 Index",
				"SX5E 01/15/16 P2300 Index",
				"SX5E 01/15/16 P5600 Index",
				"SX5E 01/15/16 P3725 Index",
				"SX5E 02/19/16 P3825 Index",
				"SX5E 02/19/16 C2375 Index",
				"SX5E 02/19/16 C3850 Index",
				"SX5E 01/15/16 C3025 Index",
				"SX5E 02/19/16 P2850 Index",
				"SX5E 02/19/16 C5200 Index",
				"SX5E 01/15/16 P2700 Index",
				"SX5E 02/19/16 P4250 Index",
				"SX5E 02/19/16 P3300 Index",
				"SX5E 01/15/16 C3625 Index",
				"SX5E 02/19/16 C3775 Index",
				"SX5E 01/15/16 C2875 Index",
				"SX5E 02/19/16 C2025 Index",
				"SX5E 02/19/16 C1400 Index",
				"SX5E 01/15/16 C2150 Index",
				"SX5E 01/15/16 C2950 Index",
				"SX5E 02/19/16 C3100 Index",
				"SX5E 02/19/16 P2675 Index"
			};
		}
        virtual void TearDown(){}

		std::vector<std::string> _securities;
		std::vector<std::string> _fields;
		BaseMarketDataRequest _request;
		std::vector<std::string> option_tickers;

    };

    TEST_F(MarketDataTest, GeneralQuery_Constructor)
    {
		GeneralQuery generalQuery = GeneralQuery(_securities, _fields);

		ASSERT_THAT( _securities.size(), Eq(generalQuery.securities().size()) );
		ASSERT_THAT(_fields.size(), Eq(generalQuery.fields().size()));

		EXPECT_THAT(_securities[0], Eq(generalQuery.securities().at(0)) );
		EXPECT_THAT(_securities[1], Eq(generalQuery.securities().at(1)));

		EXPECT_THAT(_fields[0], Eq(generalQuery.fields().at(0)));
		EXPECT_THAT(_fields[1], Eq(generalQuery.fields().at(1)));
    }

	TEST_F(MarketDataTest, OISCurveMktDataRequest_Constructor)
	{
		OISCurveQuery query = OISCurveEUR();
		OISCurveMktDataRequest request = OISCurveMktDataRequest();
		EXPECT_THAT(request.securities(), ContainerEq(query.securities()));
		EXPECT_THAT(request.fields(), ContainerEq(query.fields()));
	}

	TEST_F(MarketDataTest, OISCurveMktDataRequest_ThrowsExceptionWhenDifferentCurrencies)
	{
		std::vector<std::string> oisRatesDifferentCurrencies = { "EUSWE5 Curncy", "USSO5 Curncy" };
		OISCurveMktDataRequest oisMktDataRequest = OISCurveMktDataRequest(OISCurveQuery(oisRatesDifferentCurrencies));
		ASSERT_THROW(oisMktDataRequest.run(), std::exception);
	}

	TEST_F(MarketDataTest, OISCurveMktDataRequest_GetOISQuotes)
	{
		OISCurveMktDataRequest request = OISCurveMktDataRequest(OISCurveQuery({ "EUSWE5 Curncy" })); //5Y Euro OIS
		std::vector<OISQuote> quotes = request.oisQuotes();
		OISQuote quote = quotes.back();
		OISQuote handMadeQuote("EUR", "5Y");
		EXPECT_THAT(quote.currency(), Eq(handMadeQuote.currency()));
		EXPECT_THAT(quote.tenor(), Eq(handMadeQuote.tenor()));
	}

	TEST_F(MarketDataTest, EoniaQuery_Constructor)
	{
		std::vector<std::string> index = {"EONIA Index"}; //hard coded in ctor
		std::vector<std::string> fields = { "LAST_PRICE" };
		EONIAQuery query = EONIAQuery(fields);
		EXPECT_THAT(query.securities(), ContainerEq(index));
		EXPECT_THAT(query.fields(), ContainerEq(fields));
	}

	TEST_F(MarketDataTest, EoniaQuery_DefaultConstructor)
	{
		std::vector<std::string> index = { "EONIA Index" }; //hard coded in ctor
		std::vector<std::string> fields = { "LAST_PRICE", "LAST_UPDATE_DT", "NAME", "CRNCY", "CALENDAR_CODE" }; //hard coded in ctor
		EONIAQuery query = EONIAQuery();
		EXPECT_THAT(query.securities(), ContainerEq(index));
		EXPECT_THAT(query.fields(), UnorderedElementsAreArray(fields));
	}

	TEST_F(MarketDataTest, OISCurveQuery_Constructor)
	{
		std::vector<std::string> index = { "EUSWE5 Curncy" };
		std::vector<std::string> fields = { "SETTLE_DT", "CRNCY", "SECURITY_TENOR_TWO", "MID" }; //hard coded in ctor
		OISCurveQuery query = OISCurveQuery(index);
		EXPECT_THAT(query.securities(), ContainerEq(index));
		EXPECT_THAT(query.fields(), UnorderedElementsAreArray(fields));
	}

	TEST_F(MarketDataTest, OISCurveEUR_Constructor)
	{
		EXPECT_NO_THROW(OISCurveEUR());
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_Constructor)
	{
		EXPECT_THAT(_request.fields(), ContainerEq(_fields));
		EXPECT_THAT(_request.securities(), ContainerEq(_securities));
		EXPECT_THAT(_request.result(), IsEmpty());
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_DefaultConstructor)
	{
		BaseMarketDataRequest request = BaseMarketDataRequest();
		EXPECT_THAT(request.fields(), IsEmpty());
		EXPECT_THAT(request.securities(), IsEmpty());
		EXPECT_THAT(request.result(), IsEmpty());
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_CopyConstructor)
	{
		ASSERT_NO_THROW(_request.run());
		BaseMarketDataRequest copy = BaseMarketDataRequest(_request);
		EXPECT_THAT(_request.fields(), ContainerEq(copy.fields()));
		EXPECT_THAT(_request.securities(), ContainerEq(copy.securities()));
		EXPECT_THAT(_request.result(), ContainerEq(copy.result()));
		_request.setSecurities({"FTSEMIB Index"});
		EXPECT_THAT(_request.securities(), Ne(copy.securities()));
		ASSERT_NO_THROW(_request.run()); //run new request
		EXPECT_THAT(_request.result(), Ne(copy.result()));
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_CopyAssignment)
	{
		ASSERT_NO_THROW(_request.run());
		BaseMarketDataRequest copy;
		copy = _request; //copy assignment
		EXPECT_THAT(_request.fields(), ContainerEq(copy.fields()));
		EXPECT_THAT(_request.securities(), ContainerEq(copy.securities()));
		EXPECT_THAT(_request.result(), ContainerEq(copy.result()));
		_request.setSecurities({ "FTSEMIB Index" });
		EXPECT_THAT(_request.securities(), Ne(copy.securities()));
		ASSERT_NO_THROW(_request.run()); //run new request
		EXPECT_THAT(_request.result(), Ne(copy.result()));
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_SetAddSecuritiesFields) //order of execution is important
	{
		_request.addSecurities({"FTSEMIB Index"});
		_securities.push_back("FTSEMIB Index");
		EXPECT_THAT(_request.securities(), ContainerEq(_securities)); //test for addSecurities
		_request.addFields({ "MID" });
		_fields.push_back("MID");
		EXPECT_THAT(_request.fields(), ContainerEq(_fields)); //test for addFields
		std::vector<std::string> securities = {"SMI Index"};
		std::vector<std::string> fields = { "MID" };
		_request.setSecurities(securities);
		_request.setFields(fields);
		EXPECT_THAT(_request.securities(), ContainerEq(securities)); //test for setSecurities
		EXPECT_THAT(_request.fields(), ContainerEq(fields)); //test for setFields
	}

	TEST_F(MarketDataTest, EONIAMktDataRequest_Constructor)
	{
		EONIAQuery query = EONIAQuery();
		EONIAMktDataRequest request = EONIAMktDataRequest();
		EXPECT_THAT(request.fields(), ContainerEq(query.fields()));
		EXPECT_THAT(request.securities(), ContainerEq(query.securities()));
	}

	TEST_F(MarketDataTest, FutureStripBrent_Constructor)
	{
		FutureStripBrent futures = FutureStripBrent(4);
		std::vector<std::string> securities = { "CO1 Comdty", "CO2 Comdty", "CO3 Comdty", "CO4 Comdty" };
		std::vector<std::string> fields = { "CRNCY", "PX_MID", "FUTURES_VALUATION_DATE", "PX_VOLUME", "OPEN_INT" };
		ASSERT_THAT(futures.securities(), ContainerEq(securities));
		ASSERT_THAT(futures.fields(), ContainerEq(fields));
	}

	TEST_F(MarketDataTest, FilterByVolume_Usage)
	{
		//setup
		Natural volume = 100;
		FilterByVolume filter(volume);
		std::vector<Natural> volumes = {50, 100, 120};
		QueryResult result;
		QueryResult manuallyFilteredResult;
		for (int i = 0; i < volumes.size(); i++)
		{
			QueryResultElement el("FakeSecurity");
			el.fields["PX_VOLUME"] = std::to_string(volumes[i]);
			result.push_back(el);
			if (volumes[i] >= volume)
				manuallyFilteredResult.push_back(el);
		}

		//add result with no field PX_VOLUME
		QueryResultElement el("FakeSecurity");
		el.fields["PX_MID"] = "100.0";
		result.push_back(el);

		//filter
		QueryResult filteredResult = filter.filter(result);

		//test
		ASSERT_THAT(filteredResult, ContainerEq(manuallyFilteredResult));
	}

	TEST_F(MarketDataTest, FilterIf_Usage)
	{
		//setup
		Natural volume = 100;
		std::vector<Natural> volumes = { 50, 100, 120 };
		QueryResult result;
		QueryResult manuallyFilteredResult;
		for (int i = 0; i < volumes.size(); i++)
		{
			QueryResultElement el("FakeSecurity");
			el.fields["PX_VOLUME"] = std::to_string(volumes[i]);
			result.push_back(el);
			if (volumes[i] >= volume)
				manuallyFilteredResult.push_back(el);
		}

		//filter if definition
		FilterIf filter = FilterIf(
			[](QueryResultElement el){ return std::stod(el.fields["PX_VOLUME"]) < 100; }, "Filter if PX_VOLUME < 100");

		//filter
		QueryResult filteredResult = filter.filter(result);

		//test
		ASSERT_THAT(filteredResult, ContainerEq(manuallyFilteredResult));
	}

	TEST_F(MarketDataTest, FilterIfITM_Usage)
	{
		//setup
		Real price = 100;
		std::vector<Real> strikes = { 50, 50, 100, 100, 120, 120 };
		std::vector<std::string> types = { "C", "P", "C", "P", "C", "P" };
		QueryResult result;
		QueryResult manuallyFilteredResult;
		for (int i = 0; i < strikes.size(); i++)
		{
			QueryResultElement el("FakeSecurity");
			el.fields["OPT_PUT_CALL"] = types[i];
			el.fields["OPT_STRIKE_PX"] = std::to_string(strikes[i]);
			el.fields["OPT_UNDL_PX"] = std::to_string(price);
			result.push_back(el);
			if (el.fields["OPT_PUT_CALL"] == "C" && strikes[i] >= price)
				manuallyFilteredResult.push_back(el);
			else if (el.fields["OPT_PUT_CALL"] == "P" && strikes[i] < price)
				manuallyFilteredResult.push_back(el);
		}

		//filter
		FilterIfITM filter = FilterIfITM();
		QueryResult filteredResult = filter.filter(result);

		//test
		ASSERT_THAT(filteredResult, ContainerEq(manuallyFilteredResult));
	}

	TEST_F(MarketDataTest, Filters_DecoratorPattern)
	{
		//setup
		Natural volume = 100, open = 1100;
		std::shared_ptr<FilterMktData> filterVolume(new FilterByVolume(volume));
		std::shared_ptr<FilterMktData> filterOpenAndVolume(new FilterByOpenInterest(open,filterVolume));
		std::vector<Natural> volumes = { 50, 100, 120 };
		std::vector<Natural> opens = {500, 1000, 1200};
		QueryResult result;
		QueryResult manuallyFilteredResult;
		for (int i = 0; i < volumes.size(); i++)
		{
			QueryResultElement el("FakeSecurity");
			el.fields["PX_VOLUME"] = std::to_string(volumes[i]);
			el.fields["OPEN_INT"] = std::to_string(opens[i]);
			result.push_back(el);
			if (volumes[i] >= volume && opens[i] >= open)
				manuallyFilteredResult.push_back(el);
		}

		//filter
		QueryResult filteredResult = filterOpenAndVolume->filter(result);

		//test
		ASSERT_THAT(filteredResult, ContainerEq(manuallyFilteredResult));
	}

	TEST_F(MarketDataTest, FilterBy_ImplicictConversion)
	{
		EXPECT_NO_THROW(FilterByVolume filter = FilterByVolume(100, FilterByOpenInterest(1000)));
	}

	TEST_F(MarketDataTest, FutureStripMktDataRequest_Constructor)
	{
		FutureStripQuery query({"CO1 Comdty","CL2 Comdty"}, {"PX_MID"});
		ASSERT_THROW(FutureStripMktDataRequest dataRequest(query),std::exception);
	}

	TEST_F(MarketDataTest, FutureStripMktDataRequest_FilterSetterGetter)
	{
		FutureStripMktDataRequest request = FutureStripMktDataRequest(FutureStripBrent(4));
		std::shared_ptr<FilterMktData> filter(new FilterByVolume(100));
		request.setFilter(filter);
		EXPECT_THAT(filter->description(), Eq(request.filter()->description()));
	}

	TEST_F(MarketDataTest, FutureStripMktDataRequest_RunAddsDefaultFields)
	{
		FutureStripQuery query({ "CO1 Comdty", "CO2 Comdty" }, { "EXCH_CODE" });
		FutureStripMktDataRequest request(query);
		request.run();
		QueryResult result = request.result();
		for (auto el : result)
		{
			std::vector<std::string> keys;
			std::transform(el.fields.begin(), el.fields.end(), std::back_inserter(keys),
				[](std::pair<std::string, std::string> value){return value.first; });
			EXPECT_THAT(keys, Contains("CRNCY"));
			EXPECT_THAT(keys, Contains("PX_MID"));
			EXPECT_THAT(keys, Contains("FUTURES_VALUATION_DATE"));
		}
	}

	TEST_F(MarketDataTest, FutureStripMktDataRequest_RunAddsSpecifiedFields)
	{
		FutureStripQuery query({ "CL1 Comdty", "CL2 Comdty" }, { "EXCH_CODE" });
		std::string priceField = "PX_BID";
		std::string expiryField = "LAST_TRADEABLE_DT";
		FutureStripMktDataRequest request(query, priceField, expiryField);
		request.run();
		QueryResult result = request.result();
		for (auto el : result)
		{
			std::vector<std::string> keys;
			std::transform(el.fields.begin(), el.fields.end(), std::back_inserter(keys),
				[](std::pair<std::string, std::string> value){return value.first; });
			EXPECT_THAT(keys, Contains("CRNCY"));
			EXPECT_THAT(keys, Contains(priceField));
			EXPECT_THAT(keys, Contains(expiryField));
		}
	}

	TEST_F(MarketDataTest, FutureStripMktDataRequest_GetFutureQuotes)
	{
		FutureStripMktDataRequest request = FutureStripMktDataRequest(
			FutureStripQuery({ "COA Comdty" }, {"EXCH_CODE"})); //Active Future Contract Brent
		std::vector<FutureQuote> quotes = request.futureQuotes();
		QueryResultElement result = request.result().back();
		FutureQuote quote = quotes.back();
		FutureQuote handMadeQuote("CO", result.fields["FUTURES_VALUATION_DATE"], "USD");
		EXPECT_THAT(quote.currency(), Eq(handMadeQuote.currency()));
		EXPECT_THAT(quote.underlying(), Eq(handMadeQuote.underlying()));
		EXPECT_THAT(quote.expiry(), Eq(handMadeQuote.expiry()));
	}

	TEST_F(MarketDataTest, BulkField_Functionality)
	{
		BulkField bulkField = BulkField("BULK_FIELD_NAME");
		std::unordered_map<std::string, std::string> map1 = { {"FIELD_NAME", "FIELD_VALUE_1"} };
		std::unordered_map<std::string, std::string> map2 = { { "FIELD_NAME", "FIELD_VALUE_2" } };
		bulkField.bulkPoints.push_back(map1);
		bulkField.bulkPoints.push_back(map2);

		//test
		EXPECT_THROW(bulkField.field("ANOTHER_FIELD_NAME"), std::exception);
		std::vector<std::string> values;
		values.push_back(map1["FIELD_NAME"]);
		values.push_back(map2["FIELD_NAME"]);
		std::vector<std::string> extracted;
		ASSERT_NO_THROW(extracted = bulkField.field("FIELD_NAME"));
		EXPECT_THAT(extracted, ContainerEq(values));
	}

	TEST_F(MarketDataTest, SingleSecurityQueryResult_BulkFieldGetter)
	{
		SingleSecurityQueryResult result = SingleSecurityQueryResult("SECURITY");
		//add one reference field
		result.fields["REF_FIELD"] = "REF_FIELD_VALUE";
		EXPECT_THROW(result.bulkField("SOME_BULK_NAME"), std::exception);
		//declare bulk field 1
		BulkField bulkField1 = BulkField("BULK_FIELD_NAME_1");
		std::unordered_map<std::string, std::string> map1 = { { "FIELD_NAME_1", "FIELD_VALUE_1" } };
		bulkField1.bulkPoints.push_back(map1);
		//decalre bulk field 2
		BulkField bulkField2 = BulkField("BULK_FIELD_NAME_2");
		std::unordered_map<std::string, std::string> map2 = { { "FIELD_NAME_2", "FIELD_VALUE_2" } };
		bulkField2.bulkPoints.push_back(map2);
		//add bulk fields to result
		result.bulkFields.push_back(bulkField1);
		result.bulkFields.push_back(bulkField2);

		//test
		EXPECT_THROW(result.bulkField("ANOTHER_BULK_NAME"), std::exception);
		EXPECT_THAT(result.bulkField("BULK_FIELD_NAME_1"), Eq(bulkField1));
		EXPECT_THAT(result.bulkField("BULK_FIELD_NAME_2"), Eq(bulkField2));
		std::vector<std::string> values = { "FIELD_VALUE_1" };
		EXPECT_THAT(result.bulkField("BULK_FIELD_NAME_1").field("FIELD_NAME_1"), Eq(values));
	}

	TEST_F(MarketDataTest, QueryResult_extractResultForSecurity)
	{
		//single results
		SingleSecurityQueryResult singleResult1 = SingleSecurityQueryResult("SECURITY_1");
		singleResult1.fields["REF_FIELD_1"] = "REF_FIELD_VALUE_1";
		SingleSecurityQueryResult singleResult2 = SingleSecurityQueryResult("SECURITY_2");
		singleResult2.fields["REF_FIELD_2"] = "REF_FIELD_VALUE_2";
		//declare query result
		QueryResult result;
		result.push_back(singleResult1);
		result.push_back(singleResult2);

		//test
		SingleSecurityQueryResult extracted = extractResultForSecurity("SECURITY_1", result);
		EXPECT_THAT(extracted, Eq(singleResult1));
		extracted.fields["ADDED_FIELD"] = "ADDED_VALUE";
		EXPECT_THAT(extracted, Ne(result[0]));
	}

	TEST_F(MarketDataTest, BaseMarketDataRequest_resultForSecurity)
	{
		BaseMarketDataRequest request = BaseMarketDataRequest({ "SX5E Index", "HSI Index" }, { "PX_LAST", "CRNCY" });
		request.run();
		QueryResult result = request.result();
		EXPECT_THAT(request.resultForSecurity("SX5E Index"), Eq(extractResultForSecurity("SX5E Index", result)));
	}

	TEST_F(MarketDataTest, PriceType_Functionality)
	{
		EXPECT_THAT(PriceType::BID.to_string(), Eq("BID"));
		EXPECT_THAT(PriceType::ASK.to_string(), Eq("ASK"));
		EXPECT_THAT(PriceType::MID.to_string(), Eq("MID"));
		EXPECT_THAT(PriceType::SETTLEMENT.to_string(), Eq("PX_SETTLE_ACTUAL_RT"));
	}

	TEST_F(MarketDataTest, UtilityFunctions_truncateVectorBy)
	{
		//even number of elements
		std::vector<int> vec = { 1, 2, 3, 4, 5, 6 };
		std::vector<int> result = { 3, 4 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(2, vec), ContainerEq(result));
		result = { 2, 3, 4 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(3, vec), ContainerEq(result));
		//odd number of elements
		vec = { 1, 2, 3, 4, 5 };
		result = { 3, 4 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(2, vec), ContainerEq(result));
		result = { 2, 3, 4 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(3, vec), ContainerEq(result));
		//first , last and some other
		result = { 1, 2 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(2, vec, 0), ContainerEq(result));
		result = { 4, 5 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(2, vec, vec.size() - 1), ContainerEq(result));
		result = { 3, 4, 5 };
		EXPECT_THAT(marketDataUtilities::truncateVectorBy(3, vec, 3), ContainerEq(result));
	}

	TEST_F(MarketDataTest, UtilityFunctions_isOptionTicker)
	{
		std::vector<std::string> tickers = { "SX5E 01/15/16 C1000 Index", "SX5E 01/15/16 P1000 Index",
			"HSI 01/15/16 C1000 Index", "ENI2 GR 01/07/16 C.01 Equity", "ENI2 GR 01/07/16 C12.5 Equity", 
			"asdwqedq", "12312312"};
		std::vector<bool> matches = { true, true, true, true, true, false, false };
		for (auto i = 0; i < tickers.size(); i++)
		{
			EXPECT_THAT(marketDataUtilities::isOptionTicker(tickers[i]), Eq(matches[i]));
		}
	}

	TEST_F(MarketDataTest, UtilityFunctions_extractExpiries)
	{
		std::vector<std::string> tickers = { "SX5E 01/15/16 C1000 Index", "SX5E 01/15/16 P1000 Index",
			"HSI 01/15/16 C1000 Index", "ENI2 GR 01/07/16 C.01 Equity", "ENI2 GR 01/07/16 C12.5 Equity",
			"asdwqedq", "12312312" };
		EXPECT_THROW(marketDataUtilities::extractExpiries(tickers), std::exception);
		tickers = { "SX5E 01/15/16 C1000 Index", "SX5E 01/15/16 P1000 Index",
			"SX5E 01/21/16 C1000 Index", "SX5E 01/21/16 C1500 Index" };
		std::vector<std::string> expiries = {"01/15/16", "01/21/16"};
		EXPECT_THAT(marketDataUtilities::extractExpiries(tickers), ContainerEq(expiries));
	}

	TEST_F(MarketDataTest, UtilityFunctions_extractOptionsForExpiry)
	{
		std::vector<std::string> tickers = { "SX5E 01/15/16 C1000 Index", "SX5E 01/15/16 P1000 Index",
			"SX5E 01/21/16 C1000 Index", "SX5E 01/21/16 C1500 Index" };
		EXPECT_THROW(marketDataUtilities::extractOptionsForExpiry("wrong_format", Option::Type::Call, tickers), 
			std::exception);
		std::vector<std::string> results = { "SX5E 01/15/16 C1000 Index" };
		EXPECT_THAT(marketDataUtilities::extractOptionsForExpiry("01/15/16", Option::Type::Call, tickers),
			ContainerEq(results));
		results = { "SX5E 01/21/16 C1000 Index", "SX5E 01/21/16 C1500 Index" };
		EXPECT_THAT(marketDataUtilities::extractOptionsForExpiry("01/21/16", Option::Type::Call, tickers),
			ContainerEq(results));
		results = { "SX5E 01/15/16 P1000 Index" };
		EXPECT_THAT(marketDataUtilities::extractOptionsForExpiry("01/15/16", Option::Type::Put, tickers),
			ContainerEq(results));
		EXPECT_THAT(marketDataUtilities::extractOptionsForExpiry("01/21/16", Option::Type::Put, tickers),
			IsEmpty());
	}

	TEST_F(MarketDataTest, UtilityFunctions_truncateOptionsByNumberOfStrikes)
	{
		std::vector<std::string> result = {
			"SX5E 01/15/16 C3050 Index",
			"SX5E 01/15/16 C3075 Index",
			"SX5E 01/15/16 C3100 Index"
		};
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("C(\\d*\\.?\\d+)\\s"), 3, 
			marketDataUtilities::extractCallOptionsForExpiry("01/15/16", option_tickers),
			3076
			), ContainerEq(result));
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("C(\\d*\\.?\\d+)\\s"), 3,
			marketDataUtilities::extractCallOptionsForExpiry("01/15/16", option_tickers),
			3074
			), ContainerEq(result));

		result = {
			"SX5E 01/15/16 P1000 Index",
			"SX5E 01/15/16 P1050 Index",
			"SX5E 01/15/16 P1100 Index"
		};
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("P(\\d*\\.?\\d+)\\s"), 3,
			marketDataUtilities::extractPutOptionsForExpiry("01/15/16", option_tickers),
			1000
			), ContainerEq(result));
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("P(\\d*\\.?\\d+)\\s"), 3,
			marketDataUtilities::extractPutOptionsForExpiry("01/15/16", option_tickers),
			900
			), ContainerEq(result));

		result = {
			"SX5E 01/15/16 C5800 Index",
			"SX5E 01/15/16 C6000 Index"
		};
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("C(\\d*\\.?\\d+)\\s"), 2,
			marketDataUtilities::extractCallOptionsForExpiry("01/15/16", option_tickers),
			6000
			), ContainerEq(result));
		EXPECT_THAT(marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			std::regex("C(\\d*\\.?\\d+)\\s"), 2,
			marketDataUtilities::extractCallOptionsForExpiry("01/15/16", option_tickers),
			6500
			), ContainerEq(result));
	}

	TEST_F(MarketDataTest, UtilityFunctions_sortExpiries)
	{
		std::vector<std::string> expiries = {"01/15/17", "02/19/16"};
		marketDataUtilities::sortExpiries(expiries);
		std::vector<std::string> sorted = { "02/19/16", "01/15/17" };
		EXPECT_THAT(expiries, ContainerEq(sorted));
	}

	TEST_F(MarketDataTest, UtilityFunctions_truncateExpiries)
	{
		std::vector<std::string> expiries = { "01/15/16", "02/19/16", "03/20/17", "04/15/19" };
		std::vector<std::string> truncated = { "01/15/16", "02/19/16" };
		marketDataUtilities::truncateExpiries(expiries, Date(19, Feb, 2016));
		EXPECT_THAT(expiries, ContainerEq(truncated));
	}

	TEST_F(MarketDataTest, OptionsMktDataRequest_Functionality)
	{
		OptionsMktDataRequest request = OptionsMktDataRequest({ "SX5E Index", "HSI Index" }, { "PX_LAST", "PX_VOLUME" });
		request.setNumberStrikesPerExpiry(3);
		request.setLastExpiry(Date::todaysDate() + Period(1, Months));
		EXPECT_THROW(request.optionDataForSecurity("SX5E Index"), std::exception);
		request.run();
		EXPECT_THROW(request.optionDataForSecurity("CAC Index"), std::out_of_range);
		QueryResult resultSX5E;
		EXPECT_NO_THROW( resultSX5E = request.optionDataForSecurity("SX5E Index"));
		EXPECT_NO_THROW(QueryResult resultHSI = request.optionDataForSecurity("HSI Index"));
		EXPECT_NO_THROW(request.optionQuotes("SX5E Index", PriceType::BID));
	}

	TEST_F(MarketDataTest, CommodityOptionsMktDataRequest_Functionality)
	{
		CommodityOptionsMktDataRequest request =
			CommodityOptionsMktDataRequest({ "COA Comdty", "CLA Comdty" }, { "PX_LAST" });
		request.setNumberOfFutures(1);
		request.setNumberStrikesPerExpiry(3);
		request.run();
		EXPECT_NO_THROW(request.optionFutureQuotes("COA Comdty", PriceType::LAST));
		EXPECT_NO_THROW(request.optionFutureQuotes("CLA Comdty", PriceType::MID));
	}

	TEST_F(MarketDataTest, FilterIfBidAskSpreadGreaterThan_Functionality)
	{
		//mock results
		QueryResult result;

		QueryResultElement onlyBID("fake");
		onlyBID.fields["BID"] = "10";
		result.push_back(onlyBID);

		QueryResultElement onlyASK("fake");
		onlyASK.fields["ASK"] = "10";
		result.push_back(onlyASK);

		QueryResultElement bothButSpreadGreaterThan50Percent("fake");
		bothButSpreadGreaterThan50Percent.fields["BID"] = "10";
		bothButSpreadGreaterThan50Percent.fields["ASK"] = "20";
		result.push_back(bothButSpreadGreaterThan50Percent);

		QueryResultElement ok("fake");
		ok.fields["BID"] = "10";
		ok.fields["ASK"] = "12";
		result.push_back(ok);

		//manually filtered
		QueryResult manuallyFiltered;
		manuallyFiltered.push_back(ok);

		//filter
		FilterIfBidAskSpreadGreaterThan filter = FilterIfBidAskSpreadGreaterThan(0.5);
		QueryResult filteredResult = filter.filter(result);

		//test
		EXPECT_THAT(filteredResult, ContainerEq(manuallyFiltered));
	}

	TEST_F(MarketDataTest, BaseFileMktDataRequest_Functionality)
	{
		std::string file = "C:\\xywz_0348__-2-2.txt"; //it should not exist fro the test to make sense
		EXPECT_THROW(auto request = BaseFileMktDataRequest(file), std::exception);
	}

	TEST_F(MarketDataTest, FileMktDataRequest_CorrectMktDataFile)
	{
		std::string text =
			"security 1\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n"
			"security 2\n"
			"field1 = value1\n"
			"field2 = value2\n";

		std::ofstream fs("\\CorrectMktDataFile.txt");
		fs << text;
		fs.close();

		//read from file
		FileMktDataRequest request("\\CorrectMktDataFile.txt");
		request.run();
		std::remove("\\CorrectMktDataFile.txt"); //deletes file

		//tests
		std::vector<std::string> securities = { "security 1", "security 2" };
		EXPECT_THAT(request.securities(), ContainerEq(securities));
		std::vector<std::string> fields = {"field1", "field2"};
		EXPECT_THAT(request.fields("security 1"), ContainerEq(fields));
		EXPECT_THAT(request.fields("security 2"), ContainerEq(fields));
		QueryResult result = request.result();
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
	}

	TEST_F(MarketDataTest, FileMktDataRequest_CorrectMktDataFileWithLeadingAndTrailingBlankLines)
	{
		std::string text =
			"\n\n\n"
			"security 1\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n"
			"security 2\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n\n\n";

		std::ofstream fs("\\CorrectMktDataFileWithLeadingAndTrailingBlankLines.txt");
		fs << text;
		fs.close();

		//read from file
		FileMktDataRequest request("\\CorrectMktDataFileWithLeadingAndTrailingBlankLines.txt");
		request.run();
		std::remove("\\CorrectMktDataFileWithLeadingAndTrailingBlankLines.txt"); //deletes file

		//tests
		std::vector<std::string> securities = { "security 1", "security 2" };
		EXPECT_THAT(request.securities(), ContainerEq(securities));
		std::vector<std::string> fields = { "field1", "field2" };
		EXPECT_THAT(request.fields("security 1"), ContainerEq(fields));
		EXPECT_THAT(request.fields("security 2"), ContainerEq(fields));
		QueryResult result = request.result();
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
	}

	TEST_F(MarketDataTest, FileMktDataRequest_IncorrectFileTooManyEqualSigns)
	{
		std::string text =
			"security 1\n"
			"field1 = value1 = random\n"
			"field2 = value2\n";

		std::ofstream fs("\\IncorrectFileTooManyEqualSigns.txt");
		fs << text;
		fs.close();

		//test
		FileMktDataRequest request("\\IncorrectFileTooManyEqualSigns.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileTooManyEqualSigns.txt"); //deletes file
	}

	TEST_F(MarketDataTest, FileMktDataRequest_IncorrectFileSecurityNameNotPrecededByBlankLine)
	{
		std::string text =
			"security 1\n"
			"field1 = value1\n"
			"security 2\n"
			"field2 = value2\n";

		std::ofstream fs("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		FileMktDataRequest request("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, FileMktDataRequest_IncorrectFilePairPrecededByBlankLine)
	{
		std::string text =
			"security 1\n"
			"field1 = value1\n"
			"\n"
			"security 2\n"
			"\n"
			"field2 = value2\n";

		std::ofstream fs("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		FileMktDataRequest request("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileSecurityNameNotPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OISCurveFileMktDataRequest_CorrectOISCurveMktDataFile)
	{
		std::string text =
			"currency 1\n"
			"tenor1 = quote1\n"
			"tenor2 = quote2\n"
			"\n"
			"currency 2\n"
			"tenor1 = quote1\n";

		std::ofstream fs("\\CorrectOISCurveMktDataFile.txt");
		fs << text;
		fs.close();

		//test
		OISCurveFileMktDataRequest request("\\CorrectOISCurveMktDataFile.txt");
		request.run();
		std::remove("\\CorrectOISCurveMktDataFile.txt"); //deletes file

		//tests
		QueryResult result = request.result();
		EXPECT_THAT(result.size(), Eq(3));
		EXPECT_THAT(result[0].fields["CRNCY"], Eq("currency 1"));
		EXPECT_THAT(result[0].fields["SECURITY_TENOR_TWO"], Eq("tenor1"));
		EXPECT_THAT(result[0].fields["MID"], Eq("quote1"));
		EXPECT_THAT(result[1].fields["CRNCY"], Eq("currency 1"));
		EXPECT_THAT(result[1].fields["SECURITY_TENOR_TWO"], Eq("tenor2"));
		EXPECT_THAT(result[1].fields["MID"], Eq("quote2"));
		EXPECT_THAT(result[2].fields["CRNCY"], Eq("currency 2"));
		EXPECT_THAT(result[2].fields["SECURITY_TENOR_TWO"], Eq("tenor1"));
		EXPECT_THAT(result[2].fields["MID"], Eq("quote1"));
	}

	TEST_F(MarketDataTest, OISCurveFileMktDataRequest_CorrectOISCurveMktDataFileWithExtraBlankLines)
	{
		std::string text =
			"\n\n\n"
			"currency 1\n"
			"tenor1 = quote1\n"
			"tenor2 = quote2\n"
			"\n\n"
			"currency 2\n"
			"tenor1 = quote1\n"
			"\n\n";

		std::ofstream fs("\\CorrectOISCurveMktDataFileWithExtraBlankLines.txt");
		fs << text;
		fs.close();

		//test
		OISCurveFileMktDataRequest request("\\CorrectOISCurveMktDataFileWithExtraBlankLines.txt");
		request.run();
		std::remove("\\CorrectOISCurveMktDataFileWithExtraBlankLines.txt"); //deletes file

		//tests
		QueryResult result = request.result();
		EXPECT_THAT(result.size(), Eq(3));
		EXPECT_THAT(result[0].fields["CRNCY"], Eq("currency 1"));
		EXPECT_THAT(result[0].fields["SECURITY_TENOR_TWO"], Eq("tenor1"));
		EXPECT_THAT(result[0].fields["MID"], Eq("quote1"));
		EXPECT_THAT(result[1].fields["CRNCY"], Eq("currency 1"));
		EXPECT_THAT(result[1].fields["SECURITY_TENOR_TWO"], Eq("tenor2"));
		EXPECT_THAT(result[1].fields["MID"], Eq("quote2"));
		EXPECT_THAT(result[2].fields["CRNCY"], Eq("currency 2"));
		EXPECT_THAT(result[2].fields["SECURITY_TENOR_TWO"], Eq("tenor1"));
		EXPECT_THAT(result[2].fields["MID"], Eq("quote1"));
	}

	TEST_F(MarketDataTest, OISCurveFileMktDataRequest_IncorrectFileCurrencyNotPrecededByBlankLine)
	{
		std::string text =
			"currency 1\n"
			"tenor1 = quote1\n"
			"tenor2 = quote2\n"
			"currency 2\n"
			"tenor1 = quote1\n";

		std::ofstream fs("\\IncorrectFileCurrencyNotPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		OISCurveFileMktDataRequest request("\\IncorrectFileCurrencyNotPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileCurrencyNotPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OISCurveFileMktDataRequest_IncorrectFileTooManyEqualSigns)
	{
		std::string text =
			"currency 1\n"
			"tenor1 = quote1 = random\n"
			"tenor2 = quote2\n"
			"currency 2\n"
			"tenor1 = quote1\n";

		std::ofstream fs("\\IncorrectFileTooManyEqualSigns.txt");
		fs << text;
		fs.close();

		//test
		OISCurveFileMktDataRequest request("\\IncorrectFileTooManyEqualSigns.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileTooManyEqualSigns.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OISCurveFileMktDataRequest_IncorrectFilePairPrecededByBlankLine)
	{
		std::string text =
			"currency 1\n"
			"tenor1 = quote1\n"
			"\n"
			"tenor2 = quote2\n"
			"currency 2\n"
			"tenor1 = quote1\n";

		std::ofstream fs("\\IncorrectFilePairPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		OISCurveFileMktDataRequest request("\\IncorrectFilePairPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFilePairPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OptionsFileMktDataRequest_CorrectOptionsMktDataFile)
	{
		std::string text =
			"underlying 1\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n"
			"underlying 2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n";

		std::ofstream fs("\\CorrectOptionsMktDataFile.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\CorrectOptionsMktDataFile.txt");
		request.run();
		std::remove("\\CorrectOptionsMktDataFile.txt"); //deletes file

		//tests
		QueryResult result = request.optionDataForSecurity("underlying 1");
		ASSERT_THAT(result.size(), Eq(2));
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
		result = request.optionDataForSecurity("underlying 2");
		ASSERT_THAT(result.size(), Eq(2));
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
	}

	TEST_F(MarketDataTest, OptionsFileMktDataRequest_CorrectOptionsMktDataFileWithExtraBlankLines)
	{
		std::string text =
			"\n\n"
			"underlying 1\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n"
			"underlying 2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"\n";

		std::ofstream fs("\\CorrectOptionsMktDataFileWithExtraBlankLines.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\CorrectOptionsMktDataFileWithExtraBlankLines.txt");
		request.run();
		std::remove("\\CorrectOptionsMktDataFileWithExtraBlankLines.txt"); //deletes file

		//tests
		QueryResult result = request.optionDataForSecurity("underlying 1");
		ASSERT_THAT(result.size(), Eq(2));
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
		result = request.optionDataForSecurity("underlying 2");
		ASSERT_THAT(result.size(), Eq(2));
		EXPECT_THAT(result[0].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[0].fields["field2"], Eq("value2"));
		EXPECT_THAT(result[1].fields["field1"], Eq("value1"));
		EXPECT_THAT(result[1].fields["field2"], Eq("value2"));
	}

	TEST_F(MarketDataTest, OptionsFileMktDataRequest_IncorrectFileOptionKeywordPrecededByBlankLine)
	{
		std::string text =
			"underlying 1\n"
			"\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n";

		std::ofstream fs("\\IncorrectFileOptionKeywordPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\IncorrectFileOptionKeywordPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileOptionKeywordPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OptionsFileMktDataRequest_IncorrectFileSecurityNotPrecededByBlankLine)
	{
		std::string text =
			"underlying 1\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"underlying 2";

		std::ofstream fs("\\IncorrectFileSecurityNotPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\IncorrectFileSecurityNotPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFileSecurityNotPrecededByBlankLine.txt"); //deletes file
	}

	TEST_F(MarketDataTest, OptionsFileMktDataRequest_IncorrectFilePairPrecededByBlankLine)
	{
		std::string text =
			"underlying 1\n"
			"option\n"
			"field1 = value1\n"
			"field2 = value2\n"
			"option\n"
			"field1 = value1\n"
			"\n"
			"field2 = value2\n";

		std::ofstream fs("\\IncorrectFilePairPrecededByBlankLine.txt");
		fs << text;
		fs.close();

		//test
		OptionsFileMktDataRequest request("\\IncorrectFilePairPrecededByBlankLine.txt");
		EXPECT_THROW(request.run(), std::exception);
		std::remove("\\IncorrectFilePairPrecededByBlankLine.txt"); //deletes file
	}

} //namespace testing
} //namespace MarketData