#include "GeneralQuery.h"

/* constructor */
GeneralQuery::GeneralQuery(std::vector<std::string> securities, std::vector<std::string> fields)
{
	this->securities_ = securities;
	this->fields_ = fields;
}

/* destructor */
GeneralQuery::~GeneralQuery()
{
}

/* returns a copy of the securities that are part of the query */
std::vector<std::string> GeneralQuery::securities()
{
	return this->securities_;
}

/* returns a copy of the fields that are part of the query */
std::vector<std::string> GeneralQuery::fields()
{
	return this->fields_;
}