#include "FilterByOpenInterest.h"

/*constructor*/
FilterByOpenInterest::FilterByOpenInterest(Real openInterest, std::string fieldName, std::shared_ptr<FilterMktData> filter)
	: FilterBy<Real>(
	fieldName,
	[openInterest](Real value){return value < openInterest; },
	"Filter if " + fieldName + " < " + std::to_string(openInterest),
	filter
	), openInterest_(openInterest)
{}

/*constructor*/
FilterByOpenInterest::FilterByOpenInterest(Real openInterest, std::shared_ptr<FilterMktData> filter)
	: FilterByOpenInterest(openInterest, "OPEN_INT", filter)
{}

/*open ineterest*/
Real FilterByOpenInterest::openInterest() const
{
	return openInterest_;
}