#include "FilterIfMaturityBeforeThan.h"

#include <MarKetQuote\include\UtilityFunctions.h>

/*constructor*/
FilterIfMaturityBeforeThan::FilterIfMaturityBeforeThan(
	Date date,
	std::string maturity,
	std::shared_ptr<FilterMktData> filter)
	: FilterIf(
	[date, maturity](QueryResultElement el){
		return marketQuoteUtilities::fromStringToDate(el.fields[maturity], "%Y-%m-%d") < date;
	},
	"Filter out if maturity is later than " 
	+ std::to_string(date.dayOfMonth()) + "/" + std::to_string(date.month()) + "/" + std::to_string(date.year()),
	filter
	)
{}