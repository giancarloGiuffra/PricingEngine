#include "FileMktDataRequest.h"
#include <Utilities\include\StringUtilities.h>

#include <ql\errors.hpp>
#include <boost\algorithm\string\trim.hpp>

#include <fstream>

/*constructor*/
FileMktDataRequest::FileMktDataRequest(std::string file)
	: BaseFileMktDataRequest(file)
{}

/*run*/
void FileMktDataRequest::run()
{
	//clear result_
	this->result_.clear();

	//open file
	std::ifstream fs(this->file_);
	if (!fs.is_open()) QL_FAIL("unable to open file " << file_);

	//read line by line
	std::string line;
	std::string previous = "";
	SingleSecurityQueryResult singleResult("");
	while (std::getline(fs, line))
	{
		if (!line.empty())
		{
			std::vector<std::string> tokens = utilities::split(line, '=');
			switch (tokens.size())
			{
				case 1: //security
				{
					if (!previous.empty())
					{
						result_.clear();
						QL_FAIL("incorrect market data format in file " << file_
							<< " (new security header must be preceded by blank line)");
					}
					std::string security = tokens[0];
					boost::trim(security);
					singleResult.security = security;
					break;
				}
				case 2: //field-value pair
				{
					if (previous.empty())
					{
						result_.clear();
						QL_FAIL("incorrect market data format in file " << file_
							<< " (field-value pair preceded by blank line)");
					}
					std::string field = tokens[0], value = tokens[1];
					boost::trim(field);
					boost::trim(value);
					singleResult.fields[field] = value;
					break;
				}
				default:
					result_.clear();
					QL_FAIL("incorrect market data format in file " << file_ << " (two many =)");
			}
		}
		else {
			if (!singleResult.security.empty())
			{
				this->result_.push_back(singleResult);
				//clear
				singleResult.security = "";
				singleResult.fields.clear();
				singleResult.bulkFields.clear();
			}
		}
		previous = line;
		boost::trim(previous);
	}

	//include last security when file does not end on blank line
	if (fs.eof() && !previous.empty()) this->result_.push_back(singleResult);
}

/*securities*/
std::vector<std::string> FileMktDataRequest::securities() const
{
	std::vector<std::string> securities;
	for (auto const & singleResult : this->result_)
	{
		securities.push_back(singleResult.security);
	}
	return securities;
}

/*fields*/
std::vector<std::string> FileMktDataRequest::fields(std::string security) const
{
	SingleSecurityQueryResult result = this->resultForSecurity(security);
	std::vector<std::string> fields;
	for (auto const & element : result.fields)
		fields.push_back(element.first);
	return fields;
}

/*set file to read*/
void FileMktDataRequest::setFileToRead(std::string file)
{
	BaseFileMktDataRequest::setFileToRead(file);
	this->result_.clear();
}