#include "FilterIfITM.h"
#include <MarKetQuote\include\UtilityFunctions.h>

FilterIfITM::FilterIfITM(std::string strike, std::string underlyingPrice, std::shared_ptr<FilterMktData> filter)
	: FilterIf(
	[strike, underlyingPrice](QueryResultElement el){
	return marketQuoteUtilities::fromStringToOptionType(el.fields["OPT_PUT_CALL"]) == Option::Type::Call ?
		std::stod(el.fields[strike]) < std::stod(el.fields[underlyingPrice]) :
		std::stod(el.fields[strike]) >= std::stod(el.fields[underlyingPrice]);
	},
	"Filter out if option is ITM ( strike = " + strike + ", underlying price = " + underlyingPrice + " )",
	filter
	)
{}