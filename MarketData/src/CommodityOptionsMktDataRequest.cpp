#include "CommodityOptionsMktDataRequest.h"
#include "UtilityFunctions.h"
#include "FilterBy.h"
#include <ql\errors.hpp>
#include <algorithm>
#include <regex>

/*constructor*/
CommodityOptionsMktDataRequest::CommodityOptionsMktDataRequest(
	std::vector<std::string> securities,
	std::vector<std::string> optionFields,
	std::string optionsBulkFieldName,
	std::string fieldTickerIdentifier,
	std::shared_ptr<FilterMktData> filter)
	: OptionsMktDataRequest(securities, optionFields, optionsBulkFieldName, fieldTickerIdentifier, filter)
{
	//simple control - could be improved by using regex
	QL_REQUIRE(std::all_of(securities.begin(), securities.end(), [](std::string s){return s.find("A Comdty") != std::string::npos; }),
		"There are securities that are not commodities (active contract)!");
	//add required field
	this->addRequiredFields({"OPT_UNDL_EXP_DT"});
	this->numberOfFutures_ = 48; //default
	//remove MID because it gives BAD_FLD for commodities
	requiredFields_.erase(
	std::remove_if(requiredFields_.begin(), requiredFields_.end(), [](std::string field){ return field == "MID"; }),
	requiredFields_.end());
}

/*constructor*/
CommodityOptionsMktDataRequest::CommodityOptionsMktDataRequest(
	std::vector<std::string> securities,
	std::vector<std::string> optionFields,
	std::shared_ptr<FilterMktData> filter)
	: CommodityOptionsMktDataRequest(securities, optionFields, "OPT_CHAIN", "Security Description", filter)
{}

/*run*/
void CommodityOptionsMktDataRequest::run()
{
	//save securities, optionFields and requiredFields
	std::vector<std::string> securities = this->d_securities;
	std::vector<std::string> optionFields = this->d_fields;
	std::vector<std::string> requiredFields = this->requiredFields_;
	//extract chain of futures
	this->setFields(std::vector<std::string>(1, "FUT_CHAIN"));
	this->clearRequiredFields();
	BaseFilteredMktDataRequest::run();
	//extract result
	QueryResult futureChains = this->result_;
	for (auto security : securities)
	{
		//extract futures for single security and truncate
		std::vector<std::string> futures = extractResultForSecurity(security, futureChains)
			.bulkField("FUT_CHAIN")
			.field("Security Description");
		if (futures.size() > numberOfFutures_) futures.resize(numberOfFutures_); //not sorted, bbg gives them in chronological order
		
		//extract option tickers
		this->setSecurities(futures);
		this->setFields({ "PX_LAST", optionsBulkFieldName_ });
		this->clearRequiredFields();
		BaseFilteredMktDataRequest::run();

		//filter futures that don't have a PX_LAST
		std::shared_ptr<FilterMktData> hasPxLast(new FilterBy<double>("PX_LAST", [](double last){return last <= 0; }));
		QueryResult filteredResult = hasPxLast->filter(this->result_);
		futures.clear();
		std::transform(filteredResult.begin(), filteredResult.end(), std::back_inserter(futures),
			[](QueryResultElement q){ return q.security; });

		//add option tickers to securities - cycle through futures that have a PX_LAST
		this->clearSecurities();
		for (auto future : futures)
		{
			this->addSecuritiesUsingNumberOfStrikesTruncation(future, filteredResult);
		}

		//run the request
		this->setFields(optionFields);
		this->requiredFields_ = requiredFields;
		BaseFilteredMktDataRequest::run();

		//save it in results_
		this->results_.insert(std::pair<std::string, QueryResult>(security, this->result_));
	}

	//repristinate securities and fields
	this->setSecurities(securities);
	this->setFields(optionFields);
}

/*add securities but truncate first*/
void CommodityOptionsMktDataRequest::addSecuritiesUsingNumberOfStrikesTruncation(
	std::string future,  const QueryResult& result)
{
	//extract option tickers
	std::vector<std::string> optionTickers;
	try {
		optionTickers = extractResultForSecurity(future, result)
			.bulkField(optionsBulkFieldName_)
			.field(fieldTickerIdentifier_);
	}
	catch (const std::exception& e){
		std::cout << "Exception caught: " << e.what() << std::endl;
		//we are assuming that the future has an empty optionsBulkFieldName_
		std::cout << "Future " << future << " (most probably) does not have options" << std::endl;
		return;
	}

	//extract calls/puts
	std::regex call_format("C\\s+(\\d*\\.?\\d+)\\s");
	std::regex put_format("P\\s+(\\d*\\.?\\d+)\\s");
	std::vector<std::string> calls;
	std::vector<std::string> puts;
	std::copy_if(optionTickers.begin(), optionTickers.end(), std::back_inserter(calls),
		[call_format](std::string ticker){ return std::regex_search(ticker, call_format); });
	std::copy_if(optionTickers.begin(), optionTickers.end(), std::back_inserter(puts),
		[put_format](std::string ticker){ return std::regex_search(ticker, put_format); });
	
	//truncate 
	Real value = std::stod(extractResultForSecurity(future, result).fields["PX_LAST"]);
	calls = marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
		call_format, this->numberStrikesPerExpiry_, calls, value);
	puts = marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
		put_format, this->numberStrikesPerExpiry_, puts, value);

	//add securities
	this->addSecurities(calls);
	this->addSecurities(puts);
}

/*option future quotes*/
std::vector<VanillaOptionFutureQuote> CommodityOptionsMktDataRequest::optionFutureQuotes(std::string security, PriceType type)
{
	if (this->results_.empty()){
		std::cout << "Running CommodityOptionsMktDataRequest because result was empty ..." << std::endl;
		this->run();
	}

	/*filter - MID is treated differently because quote has to be constructed from BID and ASK, bbg gives BAD_FLD*/
	QueryResult filteredResult;
	if (type == PriceType::MID){
		FilterBy<double> combinedFilter = FilterBy<double>("BID", [](double bid){ return bid <= 0; },
			FilterBy<double>("ASK", [](double ask){ return ask <= 0; },
			filter_));
		filteredResult = combinedFilter.filter(this->optionDataForSecurity(security));
		for (auto &singleResult : filteredResult)
			singleResult.fields["MID"] = std::to_string(
			0.5*(std::stod(singleResult.fields["BID"]) + std::stod(singleResult.fields["ASK"])));
	} 
	else{
		FilterBy<double> combinedFilter = FilterBy<double>(type.to_string(), [](double price){return price <= 0; },
			filter_);
		filteredResult = combinedFilter.filter(this->optionDataForSecurity(security));
	}

	//build quotes
	std::vector<VanillaOptionFutureQuote> quotes;
	for (auto el : filteredResult)
	{
		quotes.push_back(VanillaOptionFutureQuote(
			el.fields["OPT_UNDL_TICKER"],
			el.fields["OPT_PUT_CALL"],
			el.fields["OPT_EXER_TYP"],
			std::stod(el.fields["OPT_STRIKE_PX"]),
			el.fields["OPT_EXPIRE_DT"],
			el.fields["OPT_UNDL_EXP_DT"],
			el.fields["CRNCY"],
			std::stod(el.fields[type.to_string()])
			));
	}

	return quotes;
}

/*set number of futures*/
void CommodityOptionsMktDataRequest::setNumberOfFutures(int n)
{
	QL_REQUIRE(n > 0, "n (" << n << ") must be greater than zero.");
	this->numberOfFutures_ = n;
}

/*number of futures for request*/
int CommodityOptionsMktDataRequest::numberOfFutures() const
{
	return this->numberOfFutures_;
}