#include "BaseFileMktDataRequest.h"

#include <ql\errors.hpp>

#include <fstream>

/*constructor*/
BaseFileMktDataRequest::BaseFileMktDataRequest(std::string file)
{
	setFileToRead(file);
}

/*setter*/
void BaseFileMktDataRequest::setFileToRead(std::string file)
{
	std::ifstream fs(file);
	if (!fs.good())
	{
		fs.close();
		QL_FAIL("unable to process file " << file << ". please check if it exists.");
	}
	fs.close();
	this->file_ = file;
}

/*getter*/
std::string BaseFileMktDataRequest::file() const
{
	return this->file_;
}