#include "OISCurveMktDataRequest.h"
#include <ql\errors.hpp>
#include <algorithm>
#include <boost\foreach.hpp>

/* constructor */
OISCurveMktDataRequest::OISCurveMktDataRequest(OISCurveQuery query)
	: BaseMarketDataRequest(query.securities(), query.fields())
{}

/* destructor */
OISCurveMktDataRequest::~OISCurveMktDataRequest()
{}

/*hides: BaseMarketDataRequest::run()
besides running the mkt data request checks that all securities have the same currency*/
void OISCurveMktDataRequest::run()
{
	this->BaseMarketDataRequest::run(); //calls the base class method
	std::string currency = this->result_[0].fields["CRNCY"];
	QL_ASSERT(std::all_of(result_.begin(), result_.end(), [&currency](QueryResultElement el){return el.fields["CRNCY"].compare(currency) == 0;}),
		"Not all securities have the same currency in OISCurveMktDataRequest!");
}

/*genera un vector di OISQuotes.
se la market data request non � stata ancora lanciata questa viene lanciata*/
std::vector<OISQuote> OISCurveMktDataRequest::oisQuotes()
{
	if (this->result_.empty()){
		std::cout << "Running OIS Curve request because result was empty ..." << std::endl;
		this->run();
	}

	std::vector<OISQuote> quotes;
	BOOST_FOREACH(QueryResultElement el, this->result_)
	{
		quotes.push_back(OISQuote(el.fields["CRNCY"], el.fields["SECURITY_TENOR_TWO"], std::stod(el.fields["MID"])/100));
	}

	return quotes;
}