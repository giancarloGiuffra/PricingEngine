#include "OISCurveEUR.h"

#include <boost\assign\list_of.hpp>

/* constructor */
OISCurveEUR::OISCurveEUR()
	: OISCurveQuery( std::vector<std::string>(
	boost::assign::list_of
	("EUSWE1Z Curncy")	//1W
	("EUSWE2Z Curncy")	//2W
	("EUSWE3Z Curncy")	//3W
	("EUSWEA Curncy")	//1M
	("EUSWEB Curncy")	//2M
	("EUSWEC Curncy")	//3M
	("EUSWED Curncy")	//4M
	("EUSWEE Curncy")	//5M
	("EUSWEF Curncy")	//6M
	("EUSWEG Curncy")	//7M
	("EUSWEH Curncy")	//8M
	("EUSWEI Curncy")	//9M
	("EUSWEJ Curncy")	//10M
	("EUSWEK Curncy")	//11M
	("EUSWE1 Curncy")	//1Y
	("EUSWE1C Curncy")	//15M
	("EUSWE1F Curncy")	//18M
	("EUSWE1I Curncy")	//21M
	("EUSWE2 Curncy")	//2Y
	("EUSWE3 Curncy")	//3Y
	("EUSWE4 Curncy")	//4Y
	("EUSWE5 Curncy")	//5Y
	("EUSWE6 Curncy")	//6Y
	("EUSWE7 Curncy")	//7Y
	("EUSWE8 Curncy")	//8Y
	("EUSWE9 Curncy")	//9Y
	("EUSWE10 Curncy")	//10Y
	("EUSWE11 Curncy")	//11Y
	("EUSWE12 Curncy")	//12Y
	("EUSWE15 Curncy")	//15Y
	("EUSWE20 Curncy")	//20Y
	("EUSWE25 Curncy")	//25Y
	("EUSWE30 Curncy")	//30Y
	) //ctor std::vector - it was necessary to wrap list_of around the desired container otherwise the correct OISCurveQuery constructor wasn't being called
	) //ctor OISCurveQuery
{
}

/* destructor */
OISCurveEUR::~OISCurveEUR()
{
}