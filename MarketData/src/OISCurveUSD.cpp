#include "OISCurveUSD.h"

/*constructor*/
OISCurveUSD::OISCurveUSD()
	: OISCurveQuery(
	{
		"USSO1Z Curncy",
		"USSO2Z Curncy",
		"USSO3Z Curncy",
		"USSOA Curncy",
		"USSOB Curncy",
		"USSOC Curncy",
		"USSOD Curncy",
		"USSOE Curncy",
		"USSOF Curncy",
		"USSOG Curncy",
		"USSOH Curncy",
		"USSOI Curncy",
		"USSOJ Curncy",
		"USSOK Curncy",
		"USSO1 Curncy",
		"USSO1C Curncy",
		"USSO1F Curncy",
		"USSO1I Curncy",
		"USSO2 Curncy",
		"USSO3 Curncy",
		"USSO4 Curncy",
		"USSO5 Curncy",
		"USSO7 Curncy",
		"USSO10 Curncy",
		"USSO12 Curncy",
		"USSO15 Curncy",
		"USSO20 Curncy",
		"USSO25 Curncy",
		"USSO30 Curncy"
	})
{}