#include "EONIAQuery.h"

#include <boost\assign\list_of.hpp>

/*default constructor*/
EONIAQuery::EONIAQuery()
	: GeneralQuery(
	boost::assign::list_of("EONIA Index"),
	boost::assign::list_of
	("NAME")
	("CRNCY")
	("CALENDAR_CODE")
	("LAST_PRICE")
	("LAST_UPDATE_DT")
	)
{}

/*constructor*/
EONIAQuery::EONIAQuery(std::vector<std::string> fields)
	: GeneralQuery(boost::assign::list_of("EONIA Index"), fields)
{}

/*destructor*/
EONIAQuery::~EONIAQuery()
{}