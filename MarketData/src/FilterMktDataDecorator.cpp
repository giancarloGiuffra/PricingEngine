#include "FilterMktDataDecorator.h"

/*constructor*/
FilterMktDataDecorator::FilterMktDataDecorator(std::shared_ptr<FilterMktData> filter)
	: filter_(filter)
{}

/*filter*/
QueryResult FilterMktDataDecorator::filter(QueryResult result)
{
	return filter_->filter(result);
}

/*description*/
std::string FilterMktDataDecorator::description()
{
	return filter_->description();
}

/*insert description*/
std::string FilterMktDataDecorator::insertDescription()
{
	return filter_->description().empty() ? "" : filter_->description() + " + ";
}