#include "OptionsMktDataRequest.h"
#include "FilterBy.h"
#include "UtilityFunctions.h"

#include <ql\errors.hpp>

/*PriceType--------------------------------------------------------------------------------------------------------------*/

/*constructor*/
PriceType::PriceType(std::string type)
	: type_(type)
{}

/*"enum"*/
const PriceType PriceType::BID = PriceType("BID");
const PriceType PriceType::ASK = PriceType("ASK");
const PriceType PriceType::MID = PriceType("MID");
const PriceType PriceType::LAST = PriceType("LAST_PRICE");
const PriceType PriceType::SETTLEMENT = PriceType("PX_SETTLE_ACTUAL_RT");

/*to string*/
std::string PriceType::to_string() const
{
	return type_;
}

/*operator==*/
bool operator==(const PriceType& lhs, const PriceType& rhs)
{
	return lhs.type_ == rhs.type_;
}

/*operator!=*/
bool operator!=(const PriceType& lhs, const PriceType& rhs)
{
	return !(lhs == rhs);
}

/*OptionsMktDataRequest ------------------------------------------------------------------------------------------------*/

/*constructor*/
OptionsMktDataRequest::OptionsMktDataRequest(
	std::vector<std::string> securities,
	std::vector<std::string> optionFields,
	std::string optionsBulkFieldName,
	std::string fieldTickerIdentifier,
	std::shared_ptr<FilterMktData> filter)
	: BaseFilteredMktDataRequest(securities, optionFields, filter),
	optionsBulkFieldName_(optionsBulkFieldName), fieldTickerIdentifier_(fieldTickerIdentifier)
{
	requiredFields_ = {
		"OPT_UNDL_TICKER",
		"OPT_STRIKE_PX",
		"OPT_PUT_CALL",
		"OPT_EXER_TYP",
		"OPT_EXPIRE_DT",
		"CRNCY",
		"BID",
		"ASK",
		"MID",
		"PX_SETTLE_ACTUAL_RT"
	};

	numberStrikesPerExpiry_ = 20;
	lastExpiry_ = Date::todaysDate() + Period(4, Years);
}

/*constructor*/
OptionsMktDataRequest::OptionsMktDataRequest(
	std::vector<std::string> securities,
	std::vector<std::string> optionFields,
	std::shared_ptr<FilterMktData> filter)
	: OptionsMktDataRequest(securities, optionFields, "OPT_CHAIN", "Security Description", filter)
{}

/*run*/
void OptionsMktDataRequest::run()
{
	//save securities and optionFields
	std::vector<std::string> securities = this->d_securities;
	std::vector<std::string> optionFields = this->d_fields;
	std::vector<std::string> requiredFields = this->requiredFields_;
	//set fields to bulk field
	this->setFields({ "PX_LAST", optionsBulkFieldName_ });
	//run for all securities
	this->clearRequiredFields(); //so they are not requested
	BaseFilteredMktDataRequest::run();
	//extract result
	QueryResult result = this->result_;
	//cycle through result to extract option tickers, run the request and save it in results_
	for (auto security : securities)
	{
		//set securities to option tickers
		this->setSecuritiesUsingNumberOfStrikesTruncation(security, result);
		//run the request
		this->setFields(optionFields);
		this->requiredFields_ = requiredFields;
		BaseFilteredMktDataRequest::run();
		//save it in results_
		this->results_.insert(std::pair<std::string, QueryResult>(security, this->result_));
	}
	//repristinate securities and fields
	this->setSecurities(securities);
	this->setFields(optionFields);
}

/*set securities - truncate by number of strikes*/
void OptionsMktDataRequest::setSecuritiesUsingNumberOfStrikesTruncation(std::string security,
	const QueryResult& result)
{
	//extract all options
	std::vector<std::string> options = extractResultForSecurity(security, result)
		.bulkField(optionsBulkFieldName_).field(fieldTickerIdentifier_);

	//define regex to extract strikes
	std::regex strike_format("[C|P](\\d*\\.?\\d+)\\s");
	//define value for truncation
	Real value = std::stod(extractResultForSecurity(security, result).fields["PX_LAST"]);
	//cycle through expiries - first truncate
	std::vector<std::string> expiries = marketDataUtilities::extractExpiries(options);
	marketDataUtilities::truncateExpiries(expiries, lastExpiry_);
	std::vector<std::string> truncated;
	for (auto expiry : expiries)
	{
		//extract calls
		std::vector<std::string> calls = marketDataUtilities::extractCallOptionsForExpiry(expiry, options);
		std::vector<std::string> callsTruncated =
			marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			strike_format, numberStrikesPerExpiry_, calls, value);
		truncated.insert(truncated.end(), callsTruncated.begin(), callsTruncated.end());
		//extract puts
		std::vector<std::string> puts = marketDataUtilities::extractPutOptionsForExpiry(expiry, options);
		std::vector<std::string> putsTruncated =
			marketDataUtilities::truncateOptionsByNumberOfStrikesWithRegex(
			strike_format, numberStrikesPerExpiry_, puts, value);
		truncated.insert(truncated.end(), putsTruncated.begin(), putsTruncated.end());
	}

	//set securities
	this->setSecurities(truncated);
}

/*vanilla option quotes*/
std::vector<VanillaOptionQuote> OptionsMktDataRequest::optionQuotes(std::string security, PriceType type)
{
	if (this->results_.empty()){
		std::cout << "Running OptionsMktDataRequest because result was empty ..." << std::endl;
		this->run();
	}

	//filter (implied in PriceType) + the one set by the user
	FilterBy<double> combinedFilter = FilterBy<double>(type.to_string(), [](double price){return price <= 0; },
		filter_);
	QueryResult filteredResult = combinedFilter.filter(this->optionDataForSecurity(security));

	//build quotes
	std::vector<VanillaOptionQuote> quotes;
	for (auto el : filteredResult)
	{
		quotes.push_back(VanillaOptionQuote(
			el.fields["OPT_UNDL_TICKER"],
			el.fields["OPT_PUT_CALL"],
			el.fields["OPT_EXER_TYP"],
			std::stod(el.fields["OPT_STRIKE_PX"]),
			el.fields["OPT_EXPIRE_DT"],
			el.fields["CRNCY"],
			std::stod(el.fields[type.to_string()])
			));
	}

	return quotes;
}

/*option data for specific security*/
QueryResult OptionsMktDataRequest::optionDataForSecurity(std::string security) const
{
	QL_ASSERT(!results_.empty(), "option data is empty!");
	return this->results_.at(security);
}

/*inspector optionsBulkFieldName*/
std::string OptionsMktDataRequest::optionsBulkFieldName() const
{
	return optionsBulkFieldName_;
}

/*inspector fieldTickerIdentifier*/
std::string OptionsMktDataRequest::fieldTickerIdentifier() const
{
	return fieldTickerIdentifier_;
}

/*set number of strikes per expiry*/
void OptionsMktDataRequest::setNumberStrikesPerExpiry(int n)
{
	QL_REQUIRE(n > 0, "n must be greater than zero (given value is " << n << ")");
	this->numberStrikesPerExpiry_ = n;
}

/*set last expiry*/
void OptionsMktDataRequest::setLastExpiry(Date date)
{
	QL_REQUIRE(date > Date::todaysDate(), "date " << date << " must be later than today");
	this->lastExpiry_ = date;
}

/*number of strikes per expiry*/
int OptionsMktDataRequest::numberStrikesPerExpiry() const
{
	return this->numberStrikesPerExpiry_;
}

/*last expiry for request*/
Date OptionsMktDataRequest::lastExpiry() const
{
	return this->lastExpiry_;
}