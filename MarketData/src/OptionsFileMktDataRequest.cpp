#include "OptionsFileMktDataRequest.h"
#include <Utilities\include\StringUtilities.h>

#include <ql\errors.hpp>
#include <boost\algorithm\string\trim.hpp>

#include <fstream>

/*constructor*/
OptionsFileMktDataRequest::OptionsFileMktDataRequest(std::string file)
	: BaseFileMktDataRequest(file)
{}

/*run*/
void OptionsFileMktDataRequest::run()
{
	//clear result_
	this->results_.clear();

	//open file
	std::ifstream fs(this->file_);
	if (!fs.is_open()) QL_FAIL("unable to open file " << file_);

	//read line by line
	std::string line;
	std::string previous = "";
	std::string security;
	SingleSecurityQueryResult singleResult("");
	QueryResult result;
	while (std::getline(fs, line))
	{
		if (!line.empty())
		{
			std::vector<std::string> tokens = utilities::split(line, '=');
			switch (tokens.size())
			{
			case 1: //security or option
			{
				boost::trim(tokens[0]);
				//option
				if (tokens[0] == "option")
				{
					if (previous.empty())
					{
						result_.clear();
						QL_FAIL("incorrect market data format in file " << file_
							<< " (option keyword preceded by blank line)");
					}
					else if (utilities::split(previous, '=').size() == 2)
					{
						singleResult.security = "option";
						result.push_back(singleResult);
						//clear
						singleResult.security = "";
						singleResult.fields.clear();
						singleResult.bulkFields.clear();
					}
					
				}
				else //security
				{
					if (!previous.empty())
					{
						result_.clear();
						QL_FAIL("incorrect market data format in file " << file_
							<< " (new security header must be preceded by blank line)");
					}
					security = tokens[0];
					boost::trim(security);
				}
				
				break;
			}
			case 2: //field-value pair
			{
				if (previous.empty())
				{
					result_.clear();
					QL_FAIL("incorrect market data format in file " << file_
						<< " (field-value pair preceded by blank line)");
				}
				std::string field = tokens[0], value = tokens[1];
				boost::trim(field);
				boost::trim(value);
				singleResult.fields[field] = value;
				break;
			}
			default:
				result_.clear();
				QL_FAIL("incorrect market data format in file " << file_ << " (two many =)");
			}
		}
		else {
			if (!security.empty())
			{
				singleResult.security = "option";
				result.push_back(singleResult);
				this->results_[security] = result;
				//clear
				security = "";
				result.clear();
				singleResult.security = "";
				singleResult.fields.clear();
				singleResult.bulkFields.clear();
			}
		}
		previous = line;
		boost::trim(previous);
	}

	//include last security when file does not end in blank line
	if (fs.eof() && !previous.empty())
	{
		singleResult.security = "option";
		result.push_back(singleResult);
		this->results_[security] = result;
	}
}

/*set file to read*/
void OptionsFileMktDataRequest::setFileToRead(std::string file)
{
	BaseFileMktDataRequest::setFileToRead(file);
	this->results_.clear();
}
