#include "FilterIf.h"
#include <algorithm>
#include <iterator>

/*constructor*/
FilterIf::FilterIf(std::function<bool(QueryResultElement)> predicate,
	std::string description,
	std::shared_ptr<FilterMktData> filter)
	: FilterMktDataDecorator(filter), predicate_(predicate), description_(description)
{}

/*impliocit conversion to std::shared_ptr<FilterMktData>*/
FilterIf::operator std::shared_ptr<FilterMktData>() const
{
	return std::shared_ptr<FilterMktData>(new FilterIf(*this));
}

/*filter - FilterMktData interface*/
QueryResult FilterIf::filter(QueryResult result)
{
	return thisFilter(FilterMktDataDecorator::filter(result));
}

/*description - FilterMktData interface*/
std::string FilterIf::description()
{
	return FilterMktDataDecorator::insertDescription() + description_;
}

/*this filter*/
QueryResult FilterIf::thisFilter(QueryResult result)
{
	//filter - .begin() of empty vector not valid must use std::back_inserter
	std::function<bool(QueryResultElement)> p = predicate_;
	QueryResult filteredResult;
	std::remove_copy_if(result.begin(), result.end(), std::back_inserter(filteredResult),
		[&p](QueryResultElement el){return p(el); });

	return filteredResult;
}