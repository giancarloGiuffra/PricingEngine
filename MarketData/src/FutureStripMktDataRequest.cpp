#include "FutureStripMktDataRequest.h"
#include "FilterBy.h"

#include <algorithm>
#include <ql\errors.hpp>

/*constructor*/
FutureStripMktDataRequest::FutureStripMktDataRequest(FutureStripQuery query,
	std::string priceField,
	std::string expiryField,
	std::shared_ptr<FilterMktData> filter)
	: BaseFilteredMktDataRequest(query.securities(), query.fields(), filter), 
	priceField_(priceField), expiryField_(expiryField)
{
	//assert all futures have the same ticker prefix (first 2 characters)
	std::string tickerPrefix = d_securities[0].substr(0, 2); //extract first two characters
	QL_ASSERT(std::all_of(d_securities.begin(), d_securities.end(),
		[&tickerPrefix](std::string sec){return sec.substr(0, 2) == tickerPrefix; }),
		"Not all future tickers have the same ticker prefix (first 2 characters)!");
	//initialize underlying_
	this->underlying_ = tickerPrefix;
}

/*constructor*/
FutureStripMktDataRequest::FutureStripMktDataRequest(FutureStripQuery query,
	std::shared_ptr<FilterMktData> filter)
	: FutureStripMktDataRequest(query, "PX_MID", "FUTURES_VALUATION_DATE", filter)
{}

/*run*/
void FutureStripMktDataRequest::run()
{
	//defines required fields
	requiredFields_ = { "CRNCY", priceField_, expiryField_ };
	this->BaseFilteredMktDataRequest::run(); //calls the base class method
}

/*future quotes*/
std::vector<FutureQuote> FutureStripMktDataRequest::futureQuotes()
{
	if (this->result_.empty()){
		std::cout << "Running FutureStripMktDataRequest because result was empty ..." << std::endl;
		this->run();
	}

	//filter by price + the one set by the user
	std::shared_ptr<FilterMktData> combinedFilter = FilterBy<double>(priceField_, [](double price){ return price <= 0; },
		filter_);
	QueryResult filteredResult = combinedFilter->filter(this->result_);

	//build quotes
	std::vector<FutureQuote> quotes;
	for (auto el : filteredResult)
	{
		quotes.push_back(FutureQuote(underlying_,
			el.fields[expiryField_],
			el.fields["CRNCY"],
			std::stod(el.fields[priceField_])
			));
	}

	return quotes;
}