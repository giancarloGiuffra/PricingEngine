#include "OISCurveFileMktDataRequest.h"
#include <Utilities\include\StringUtilities.h>

#include <boost\algorithm\string\trim.hpp>

#include <fstream>

/*constructor*/
OISCurveFileMktDataRequest::OISCurveFileMktDataRequest(std::string file)
	: BaseFileMktDataRequest(file)
{}

/*run*/
void OISCurveFileMktDataRequest::run()
{
	//clear result_
	this->result_.clear();

	//open file
	std::ifstream fs(this->file_);
	if (!fs.is_open()) QL_FAIL("unable to open file " << file_);

	//read line by line
	std::string line;
	std::string previous = "";
	SingleSecurityQueryResult singleResult("");
	std::string currency;
	while (std::getline(fs, line))
	{
		if (!line.empty())
		{
			std::vector<std::string> tokens = utilities::split(line, '=');
			switch (tokens.size())
			{
			case 1: //currency
			{
				if (!previous.empty())
				{
					result_.clear();
					QL_FAIL("incorrect market data format in file " << file_
						<< " (new currency header must be preceded by blank line)");
				}
				currency = tokens[0];
				boost::trim(currency);
				break;
			}
			case 2: //tenor-quote pair
			{
				if (previous.empty())
				{
					result_.clear();
					QL_FAIL("incorrect market data format in file " << file_
						<< " (tenor-quote pair preceded by blank line)");
				}
				std::string tenor = tokens[0], quote = tokens[1];
				boost::trim(tenor);
				boost::trim(quote);
				//emulate the fields used to build the OIS quotes
				singleResult.security = currency + " " + tenor;
				singleResult.fields["CRNCY"] = currency;
				singleResult.fields["SECURITY_TENOR_TWO"] = tenor;
				singleResult.fields["MID"] = quote;
				this->result_.push_back(singleResult);
				break;
			}
			default:
				result_.clear();
				QL_FAIL("incorrect market data format in file " << file_ << " (two many =)");
			}
		}
		
		previous = line;
		boost::trim(previous);
	}
}

/*set file to read*/
void OISCurveFileMktDataRequest::setFileToRead(std::string file)
{
	BaseFileMktDataRequest::setFileToRead(file);
	this->result_.clear();
}