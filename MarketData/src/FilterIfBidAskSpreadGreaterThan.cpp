#include "FilterIfBidAskSpreadGreaterThan.h"
#include "FilterBy.h"

/*constructor*/
FilterIfBidAskSpreadGreaterThan::FilterIfBidAskSpreadGreaterThan(
	Real percentage,
	std::shared_ptr<FilterMktData> filter
	) : FilterIf(
	[percentage](QueryResultElement el){ 
	return std::abs(std::stod(el.fields["ASK"]) - std::stod(el.fields["BID"])) >
		percentage*std::stod(el.fields["BID"]);
	},
	"Filters option if bid-ask spread is greater than " + std::to_string(percentage * 100) + "% of bid",
	FilterBy<double>("BID", [](double price){ return price <= 0; },
	FilterBy<double>("ASK", [](double price){ return price <= 0; },
	filter))
	)
{}