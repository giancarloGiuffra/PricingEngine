#include "FilterByVolume.h"

#include <algorithm>
#include <iterator>
#include <ql\errors.hpp>

/*constructor*/
FilterByVolume::FilterByVolume(Real volume, std::string volumeFieldName, std::shared_ptr<FilterMktData> filter)
	: FilterBy<Real>(
	volumeFieldName,
	[volume](Real value){return value < volume; },
	"Filter if " + volumeFieldName + " < " + std::to_string(volume),
	filter), volume_(volume)
{}

/*constructor*/
FilterByVolume::FilterByVolume(Real volume, std::shared_ptr<FilterMktData> filter)
	: FilterByVolume(volume, "PX_VOLUME", filter)
{}

/*volume*/
Real FilterByVolume::volume() const
{
	return this->volume_;
}