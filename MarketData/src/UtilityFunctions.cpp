#include "UtilityFunctions.h"
#include <MarKetQuote\include\UtilityFunctions.h>
#include <Utilities\include\VectorUtilities.h>

#include <ql\errors.hpp>
#include <regex>
#include <algorithm>
#include <iterator>

namespace marketDataUtilities
{
	/*is option ticker ?*/
	bool isOptionTicker(std::string ticker)
	{
		std::regex option_ticker_format(".+\\s\\d{2}/\\d{2}/\\d{2}\\s[C|P]\\d*\\.?\\d+\\s.+");
		return std::regex_match(ticker, option_ticker_format);
	}

	/*extract expiries*/
	std::vector<std::string> extractExpiries(std::vector<std::string> tickers)
	{
		QL_REQUIRE(std::all_of(tickers.begin(), tickers.end(), isOptionTicker)
			, "not all of the strings are option tickers!");
		//extract expiries
		std::regex expiry_format("\\d{2}/\\d{2}/\\d{2}");
		std::vector<std::string> expiries;
		for (auto ticker : tickers)
		{
			std::smatch match;
			std::regex_search(ticker, match, expiry_format);
			expiries.push_back(match[0]);
		}
		//sort
		sortExpiries(expiries);
		//remove duplicates - it is necessary to sort in order to have duplicates in consecutive positions
		auto last = std::unique(expiries.begin(), expiries.end());
		expiries.erase(last, expiries.end());
		//result
		return expiries;
	}

	/*sort expiries*/
	void sortExpiries(std::vector<std::string>& expiries, std::string format)
	{
		std::sort(expiries.begin(), expiries.end(), [format](std::string e1, std::string e2){
			return marketQuoteUtilities::fromStringToDate(e1, format) < marketQuoteUtilities::fromStringToDate(e2, format);
		});
	}

	/*truncate expiries*/
	void truncateExpiries(std::vector<std::string>& expiries, Date date, std::string format)
	{
		std::vector<Date> dates;
		std::transform(expiries.begin(), expiries.end(), std::back_inserter(dates),
			[format](std::string expiry){ return marketQuoteUtilities::fromStringToDate(expiry,format); });
		auto upper = std::upper_bound(dates.begin(), dates.end(), date);
		expiries.resize(std::distance(dates.begin(),upper));
	}

	/*extract options for expiry*/
	std::vector<std::string> extractOptionsForExpiry(std::string expiry, ::Option::Type type, 
		const std::vector<std::string>& tickers)
	{
		std::regex expiry_format("\\d{2}/\\d{2}/\\d{2}");
		QL_REQUIRE(std::regex_match(expiry, expiry_format), 
			"expiry " << expiry << " is not in correct format mm/dd/yy");
		//define string to search
		std::string token = expiry + " " + (type == Option::Type::Call ? "C" : "P");
		//search
		std::vector<std::string> options;
		std::copy_if(tickers.begin(), tickers.end(), std::back_inserter(options),
			[token](std::string ticker){ return isOptionTicker(ticker) && ticker.find(token) != std::string::npos; });
		return options;
	}

	/*extract calls for expiry*/
	std::vector<std::string> extractCallOptionsForExpiry(
		std::string expiry,
		const std::vector<std::string>& tickers)
	{
		return extractOptionsForExpiry(expiry, Option::Type::Call, tickers);
	}

	/*extract puts for expiry*/
	std::vector<std::string> extractPutOptionsForExpiry(
		std::string expiry,
		const std::vector<std::string>& tickers)
	{
		return extractOptionsForExpiry(expiry, Option::Type::Put, tickers);
	}

	/*truncate options by number of strikes - REGEX*/
	std::vector<std::string> truncateOptionsByNumberOfStrikesWithRegex(
		std::regex strike_format,
		int n, const std::vector<std::string>& tickers, Real value)
	{
		//copy
		std::vector<std::string> options;
		std::copy(tickers.begin(), tickers.end(), std::back_inserter(options));
		
		//sort by strike
		std::vector<double> strikes;
		std::transform(options.begin(), options.end(), std::back_inserter(strikes),
			[strike_format](std::string t){
			std::smatch m;
			std::regex_search(t, m, strike_format);
			return std::stod(m[1]);
		});
		auto permutation = utilities::sort_permutation(strikes, [](double s1, double s2){ return s1 < s2; });
		utilities::apply_permutation(options, permutation);
		
		//truncate
		int center = Null<int>();
		if (value != Null<Real>()){
			//this way the center is the closest in absolute value
			auto upper = std::upper_bound(strikes.begin(), strikes.end(), value);
			if (upper == strikes.begin())
				center = 0;
			else if (upper == strikes.end())
				center = strikes.size() - 1;
			else
				center = (int)std::distance(strikes.begin(), upper) - (value - *(upper - 1) < *upper - value ? 1 : 0);

			center = std::max(0, std::min((int)strikes.size() - 1, center));
		}

		//result
		options = options.empty() ? options : truncateVectorBy(n, options, center);
		return options;
	}

}