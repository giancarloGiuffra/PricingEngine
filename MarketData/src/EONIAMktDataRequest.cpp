#include "EONIAMktDataRequest.h"

EONIAMktDataRequest::EONIAMktDataRequest(EONIAQuery query)
	: BaseMarketDataRequest(query.securities(), query.fields())
{}

EONIAMktDataRequest::~EONIAMktDataRequest()
{}