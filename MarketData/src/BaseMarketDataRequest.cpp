#include "BaseMarketDataRequest.h"

#include <boost\foreach.hpp>
#include <iomanip>
#include <algorithm>
#include <ql\errors.hpp>
#include <functional>
#include <iterator>

/*BulkField---------------------------------------------------------------------------------------------------------------*/

/*constructor*/
BulkField::BulkField(std::string name)
	:name(name)
{}

/*constructor*/
BulkField::BulkField(Element bulkField)
	: name(bulkField.name().string())
{
	// Get the total number of Bulk data points
	size_t numofBulkValues = bulkField.numValues();
	for (size_t bvCtr = 0; bvCtr < numofBulkValues; bvCtr++) {
		const Element  bulkElement = bulkField.getValueAsElement(bvCtr);
		this->addBulkPoint(bulkElement);
	}
}

/*add bulk point*/
void BulkField::addBulkPoint(Element point)
{
	std::unordered_map<std::string, std::string> bulkPoint;
	size_t numofBulkElements = point.numElements();
	// Read each field in Bulk data
	for (size_t beCtr = 0; beCtr < numofBulkElements; beCtr++){
		const Element  elem = point.getElement(beCtr);
		bulkPoint.insert(std::pair<std::string, std::string>(elem.name().string(), elem.getValueAsString()));
	}
	this->bulkPoints.push_back(bulkPoint);
}

/*get field values for all bulk points*/
std::vector<std::string> BulkField::field(std::string name) const
{
	//define handy lambda expression
	std::function<bool(std::unordered_map<std::string, std::string>)> fieldPresent =
		[name](std::unordered_map<std::string, std::string> map){ return map.count(name) > 0; };

	//filter
	std::vector<std::unordered_map<std::string, std::string>> mapsWithField;
	std::copy_if(this->bulkPoints.begin(), this->bulkPoints.end(), std::back_inserter(mapsWithField), fieldPresent);

	//map
	QL_ASSERT(!mapsWithField.empty(), "BulkField::field: There are no bulk points with such field!");
	std::vector<std::string> result;
	std::transform(mapsWithField.begin(), mapsWithField.end(), std::back_inserter(result),
		[name](std::unordered_map<std::string, std::string> map){return map[name]; });

	return result;
}

/*overload operator<< BulkField*/
std::ostream& operator<<(std::ostream& os, const BulkField& bulkField)
{
	int width = 20;
	os << bulkField.name << std::endl;
	for (auto bulkPoint : bulkField.bulkPoints)
	{
		for (auto field : bulkPoint)
		{
			os << std::setw(width) << std::left << field.first << ":\t\t"
				<< std::setw(width) << std::left << field.second << std::endl;
		}
		os << "--" << std::endl;
	}
	return os;
}

/*SingleSecurityQueryResult----------------------------------------------------------------------------------------------*/

/* constructor of struct */
SingleSecurityQueryResult::SingleSecurityQueryResult(std::string security)
	: security(security)
{
}

/* aggiunge field alla single query (si veda BloombergLP::blpapi::Element per i metodi di accesso) */
void SingleSecurityQueryResult::addField(Element e)
{
	e.datatype() == DataType::SEQUENCE ? this->addBulkField(e) : this->addRefField(e);
}

void SingleSecurityQueryResult::addBulkField(Element e)
{
	this->bulkFields.push_back(BulkField(e));
}

void SingleSecurityQueryResult::addRefField(Element e)
{
	this->fields.insert( std::pair<std::string,std::string>( e.name().string(), e.getValueAsString() ) );
}

/*extract bulk field by name*/
BulkField SingleSecurityQueryResult::bulkField(std::string name) const
{
	QL_ASSERT(!this->bulkFields.empty(), "SingleSecurityQueryResult::bulkField: There are no bulk fields!");
	for (auto bulk : this->bulkFields)
	{
		if (bulk.name == name)
			return bulk;
	}
	QL_FAIL("SingleSecurityQueryResult::bulkField: There is no bulk with such name!");
}

/*operator<< for SingleSecurityQueryResult*/
std::ostream& operator<<(std::ostream& os, const SingleSecurityQueryResult& singleResult)
{
	int width = 20;
	os << "TICKER:\t" << singleResult.security << std::endl;
	for (auto const& field : singleResult.fields)
	{
		os << std::setw(width) << std::left << field.first << ":\t\t" 
			<< std::setw(width) << std::left << field.second << std::endl;
	}
	for (auto bulkField : singleResult.bulkFields)
	{
		os << bulkField;
	}
	return os;
}

/*QueryResult -------------------------------------------------------------------------------------------------------------*/

/*extracts result for single security*/
SingleSecurityQueryResult extractResultForSecurity(std::string security, const QueryResult& queryResult)
{
	QL_ASSERT(!queryResult.empty(), "extractResultForSecurity: Query result is empty!");
	for (auto const& singleResult : queryResult)
	{
		if (singleResult.security == security)
			return SingleSecurityQueryResult(singleResult);
	}
	QL_FAIL("extractResultForSecurity: No result for security " << security);
}

/*operator<< for QueryResult*/
std::ostream& operator<<(std::ostream& os, const QueryResult& result)
{
	for (auto const& singleResult : result)
	{
		os << singleResult << std::endl;
	}
	return os;
}

/*----------------------------------------------------------------------------------------------------------------------------*/

/* costruttore di default */
BaseMarketDataRequest::BaseMarketDataRequest()
	: RefDataExample()
{
	d_securities.clear();
	d_fields.clear();
}

/* costruttore */
BaseMarketDataRequest::BaseMarketDataRequest(std::vector<std::string> securities, std::vector<std::string> fields)
	: BaseMarketDataRequest()
{
	QL_REQUIRE(!securities.empty(), "vector of securities is empty!");
	QL_REQUIRE(!fields.empty(), "vector of fields is empty!");
	setSecurities(securities);
	setFields(fields);
}

/* copy constructor */
BaseMarketDataRequest::BaseMarketDataRequest(const BaseMarketDataRequest& other)
	: RefDataExample(other), result_(other.result_)
{
}

/* copy assignment */
BaseMarketDataRequest& BaseMarketDataRequest::operator= (BaseMarketDataRequest other)
{
	swap(*this, other); //other is passed by value so it's ok
	return *this;
}

/* destructor */
BaseMarketDataRequest::~BaseMarketDataRequest()
{
}

/* set the securities to be queried */
void BaseMarketDataRequest::setSecurities(std::vector<std::string> securities)
{
	d_securities = securities;
}

/*clear securities*/
void BaseMarketDataRequest::clearSecurities()
{
	d_securities.clear();
}

/* set the fields to be queried */
void BaseMarketDataRequest::setFields(std::vector<std::string> fields)
{
	d_fields = fields;
}

/*clear fields*/
void BaseMarketDataRequest::clearFields()
{
	d_fields.clear();
}

/* add the list of securities to the current ones */
void BaseMarketDataRequest::addSecurities(std::vector<std::string> securities)
{
	d_securities.insert(d_securities.end(), securities.begin(), securities.end());
}

/* add the list of fields to the current ones */
void BaseMarketDataRequest::addFields(std::vector<std::string> fields)
{
	d_fields.insert(d_fields.end(), fields.begin(), fields.end());
}

/* launches the query and saves the result */
void BaseMarketDataRequest::run()
{
	//clears result
	result_.clear();

	//check if there are securities
	if (d_securities.empty())
	{
		std::cout << "There are no securities loaded. Aborting Run." << std::endl;
		return;
	}

	SessionOptions sessionOptions;
	sessionOptions.setServerHost(d_host.c_str());
	sessionOptions.setServerPort((unsigned short)d_port);

	std::cout << "Connecting to " + d_host + ":" << d_port << std::endl;
	Session session(sessionOptions);
	if (!session.start()) {
		std::cout << "Failed to start session." << std::endl;
		return;
	}
	if (!session.openService("//blp/refdata")) {
		std::cout << "Failed to open //blp/refdata" << std::endl;
		return;
	}
	sendRefDataRequest(session);

	// wait for events from session.

	try {
		eventLoop(session);
	}
	catch (Exception &e) {
		std::cerr << "Library Exception !!!"
			<< e.description()
			<< std::endl;
	}
	catch (...) {
		std::cerr << "Unknown Exception !!!" << std::endl;
	}


	session.stop();
}

/* prints the securities that have been loaded in this request */
void BaseMarketDataRequest::printSecurities()
{
	std::cout << "SECURITIES:" << std::endl;
	BOOST_FOREACH(std::string security, this->d_securities)
		std::cout << security << std::endl;
}

/* prints the fields that have been loaded in this request */
void BaseMarketDataRequest::printFields()
{
	std::cout << "FIELDS:" << std::endl;
	BOOST_FOREACH(std::string field, this->d_fields)
		std::cout << field << std::endl;
}

/* prints the securities and fields that have been loaded in this request */
void BaseMarketDataRequest::printSecuritiesAndFields()
{
	printSecurities();
	std::cout << std::endl;
	printFields();
}

/* prints result */
void BaseMarketDataRequest::printResult()
{
	//check
	if (this->result_.empty())
	{
		std::cout << "Result is empty, run the request first. Aborting printResult." << std::endl;
		return;
	}
	
	std::cout << result_ << std::endl;
}

/* launches query and prints result */
void BaseMarketDataRequest::runAndPrintResult()
{
	run();
	printResult();
}

/* friend swap function */ //reference http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
void swap(BaseMarketDataRequest& first, BaseMarketDataRequest& second)
{
	using std::swap; //good practice according to reference
	swap(first.d_host, second.d_host);
	swap(first.d_port, second.d_port);
	swap(first.d_securities, second.d_securities);
	swap(first.d_fields, second.d_fields);
	swap(first.result_, second.result_);
}

/* loop che gestisce l'arrivo degli eventi da parte del server di bloomberg */
void BaseMarketDataRequest::eventLoop(Session &session)
{
	bool done = false;
	while (!done) {
		Event event = session.nextEvent();
		if (event.eventType() == Event::PARTIAL_RESPONSE) {
			//std::cout << "Processing Partial Response" << std::endl;
			processResponseEvent(event);
		}
		else if (event.eventType() == Event::RESPONSE) {
			//std::cout << "Processing Response" << std::endl;
			processResponseEvent(event);
			done = true;
		}
		else {
			MessageIterator msgIter(event);
			while (msgIter.next()) {
				Message msg = msgIter.message();
				if (event.eventType() == Event::REQUEST_STATUS) {
					std::cout << "REQUEST FAILED: " << msg.getElement(REASON) << std::endl;
					done = true;
				}
				else if (event.eventType() == Event::SESSION_STATUS) {
					if (msg.messageType() == SESSION_TERMINATED ||
						msg.messageType() == SESSION_STARTUP_FAILURE) {
						done = true;
					}
				}
			}
		}
	}
}

/* gestice la singola risposta (valida) dal server */
void BaseMarketDataRequest::processResponseEvent(Event event)
{
	MessageIterator msgIter(event);
	while (msgIter.next()) {
		Message msg = msgIter.message();
		if (msg.asElement().hasElement(RESPONSE_ERROR)) {
			printErrorInfo("REQUEST FAILED: ",
				msg.getElement(RESPONSE_ERROR));
			continue;
		}

		Element securities = msg.getElement(SECURITY_DATA);
		size_t numSecurities = securities.numValues();
		/*std::cout << "Processing " << (unsigned int)numSecurities
			<< " securities:" << std::endl; */
		for (size_t i = 0; i < numSecurities; ++i) {
			Element security = securities.getValueAsElement(i);
			std::string ticker = security.getElementAsString(SECURITY);
			//std::cout << "\nTicker: " + ticker << std::endl;
			SingleSecurityQueryResult singleSecurityQuery(ticker);
			if (security.hasElement(SECURITY_ERROR)) {
				std::cout << "\nTicker: " + ticker << std::endl;
				printErrorInfo("\tSECURITY FAILED: ",
					security.getElement(SECURITY_ERROR));
				continue;
			}

			if (security.hasElement(FIELD_DATA)) {
				const Element fields = security.getElement(FIELD_DATA);
				if (fields.numElements() > 0) {
					//std::cout << "FIELD\t\tVALUE" << std::endl;
					//std::cout << "-----\t\t-----" << std::endl;
					size_t numElements = fields.numElements();
					for (size_t j = 0; j < numElements; ++j) {
						Element field = fields.getElement(j);
						singleSecurityQuery.addField(field);
						/*std::cout << field.name() << "\t\t" <<
							field.getValueAsString() << std::endl;*/
					}
				}
			}
			result_.push_back(singleSecurityQuery);

			Element fieldExceptions = security.getElement(
				FIELD_EXCEPTIONS);
			if (fieldExceptions.numValues() > 0) {
				std::cout << "\nTicker: " + ticker << std::endl;
				std::cout << "FIELD\t\tEXCEPTION" << std::endl;
				std::cout << "-----\t\t---------" << std::endl;
				for (size_t k = 0; k < fieldExceptions.numValues(); ++k) {
					Element fieldException =
						fieldExceptions.getValueAsElement(k);
					Element errInfo = fieldException.getElement(
						ERROR_INFO);
					std::cout
						<< fieldException.getElementAsString(FIELD_ID)
						<< "\t\t"
						<< errInfo.getElementAsString(CATEGORY)
						<< " ( "
						<< errInfo.getElementAsString(MESSAGE)
						<< ")"
						<< std::endl;
				}
			}
		}
	}
}

/*restituisce il risultato della market data request*/
QueryResult BaseMarketDataRequest::result() const
{
	if (result_.empty())
		std::cout << "Result is empty, run the request first!" << std::endl;

	return this->result_;
}

/*result for single security*/
SingleSecurityQueryResult BaseMarketDataRequest::resultForSecurity(std::string security) const
{
	return extractResultForSecurity(security, this->result_);
}

/*restituisce la lista delle securities*/
std::vector<std::string> BaseMarketDataRequest::securities() const
{
	return this->d_securities;
}

/*restituisce la lista dei fields*/
std::vector<std::string> BaseMarketDataRequest::fields() const
{
	return this->d_fields;
}