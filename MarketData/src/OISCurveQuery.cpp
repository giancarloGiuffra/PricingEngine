#include "OISCurveQuery.h"
#include <boost\assign\list_of.hpp>

/* constructor, it has set of predefined fields */
OISCurveQuery::OISCurveQuery(std::vector<std::string> securities)
	: GeneralQuery(securities, boost::assign::list_of
	("SECURITY_TENOR_TWO")
	("MID")
	("CRNCY")
	("SETTLE_DT")
	)
{
}

/* destructor */
OISCurveQuery::~OISCurveQuery()
{
}