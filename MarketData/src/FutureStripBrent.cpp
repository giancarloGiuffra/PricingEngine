#include "FutureStripBrent.h"

#include <ql\time\date.hpp>
#include <ql\time\calendars\unitedkingdom.hpp>

using namespace QuantLib;

/*constructor*/
FutureStripBrent::FutureStripBrent(int numberOfContracts)
	: FutureStripQuery(buildFutureTickers(numberOfContracts),
	{
		"CRNCY",
		"PX_MID",
		"FUTURES_VALUATION_DATE",
		"PX_VOLUME",
		"OPEN_INT"
	})
{}

/*build future tickers*/
std::vector<std::string> FutureStripBrent::buildFutureTickers(int numberOfContracts)
{
	//setup
	const std::string tickerPrefix = "CO";
	const std::string tickerSuffix = " Comdty"; //the initial blank space is important
	const std::vector<std::string> monthLetters = {
		"F", //Jan
		"G", //Feb
		"H", //Mar
		"J", //Apr
		"K", //May
		"M", //Jun
		"N", //Jul
		"Q", //Aug
		"U", //Sep
		"V", //Oct
		"X", //Nov
		"Z" //Dec
	};

	//build strings
	std::vector<std::string> futures;
	for (int i = 1; i <= numberOfContracts; i++)
	{
		futures.push_back(tickerPrefix + std::to_string(i) + tickerSuffix);
	}

	return futures;
}