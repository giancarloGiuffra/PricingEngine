#include "BaseFilteredMktDataRequest.h"

/*constructor*/
BaseFilteredMktDataRequest::BaseFilteredMktDataRequest(std::vector<std::string> securities,
	std::vector<std::string> fields,
	std::shared_ptr<FilterMktData> filter)
	: BaseMarketDataRequest(securities, fields),
	filter_(filter)
{}

/*add required fields*/
void BaseFilteredMktDataRequest::addRequiredFields(std::vector<std::string> fields)
{
	requiredFields_.insert(requiredFields_.end(), fields.begin(), fields.end());
}

/*clear required fields*/
void BaseFilteredMktDataRequest::clearRequiredFields()
{
	requiredFields_.clear();
}

/*run*/
void BaseFilteredMktDataRequest::run()
{
	//adds required fields to the request if not already present
	std::vector<std::string> fields = d_fields;
	for (auto field : requiredFields_)
	{
		if (std::find(d_fields.begin(), d_fields.end(), field) == d_fields.end())
			d_fields.push_back(field);
	}
	this->BaseMarketDataRequest::run(); //calls the base class method
	this->setFields(fields); //repristinate fields
}

/*setter for filter*/
void BaseFilteredMktDataRequest::setFilter(std::shared_ptr<FilterMktData> filter)
{
	this->filter_ = filter;
}

/*inspector for filter*/
const std::shared_ptr<FilterMktData> BaseFilteredMktDataRequest::filter() const
{
	return filter_;
}