#ifndef OIS_CURVE_MKT_DATA_REQUEST_H
#define OIS_CURVE_MKT_DATA_REQUEST_H

#include "BaseMarketDataRequest.h"
#include "OISCurveQuery.h"
#include "OISCurveEUR.h"
#include <vector>
#include <MarKetQuote\include\OISQuote.h>

/*! \brief Class to launch a request to the Bloomberg Data Center for OIS market quotes.

This class is intended to be used to get OIS market quotes for the bootstrapping of OIS Curves, thus
it checks that all securities have the same currency when launching the request.
\sa OISCurveQuery.
*/
class OISCurveMktDataRequest : public BaseMarketDataRequest
{
public:

	/** Constructor.
	@param query it contains the securities and fields to be queried.
	@see OISCurveQuery.
	@see OISCurveEUR.
	*/
	OISCurveMktDataRequest(OISCurveQuery query = OISCurveEUR());

	/** Destructor.
	It does nothing.
	*/
	~OISCurveMktDataRequest();

	/** Method to launch the request and store the result.
	It verifies that the securities have the same currency.
	@throws std::exception if the securities don't have the same currency.
	*/
	void run(); //hides BaseMarketDataRequest::run()

	/** Method to get an std::vector<OISQuote> built from the result of the request to the Bloomberg Data Center.
	It launches the request if it hasn't been launched before.
	@return std::vector<OISQuote> with the OIS Market Quotes.
	@see OISQuote.
	*/
	std::vector<OISQuote> oisQuotes();

};

#endif