#ifndef FILTER_IF_ITM_H
#define FILTER_IF_ITM_H

#include "FilterIf.h"

/*! \brief Filters if the option is In The Money.

It predilects calls.
Call ITM if strike < underlying price.
Put ITM if strike >= underlying price.
*/
class FilterIfITM : public FilterIf
{
public:

	/** Constructor.
	@param strike
	@param underlyingPrice
	*/
	FilterIfITM(std::string strike = "OPT_STRIKE_PX", std::string underlyingPrice = "OPT_UNDL_PX",
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));
};

#endif // !FILTER_IF_ITM_H
