#ifndef FILTER_IF_BID_ASK_SPREAD_GREATER_THAN_H
#define FILTER_IF_BID_ASK_SPREAD_GREATER_THAN_H

#include "FilterIf.h"

#include <ql\types.hpp>

using namespace QuantLib;

/*! \brief Filters option if the bid-ask spread is greater than a percentage of the bid.

*/
class FilterIfBidAskSpreadGreaterThan : public FilterIf
{
public:

	/** Constructor.
	Filters out options if the bid-ask spread in absolute value is greater than a percentage of the bid price.
	@param percentage of BID price, passed as pure number (e.g. 0.60 for 60%)
	@param filter to decorate
	*/
	FilterIfBidAskSpreadGreaterThan(
		Real percentage,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial())
		);
};

#endif // !FILTER_IF_BID_ASK_SPREAD_GREATER_THAN_H
