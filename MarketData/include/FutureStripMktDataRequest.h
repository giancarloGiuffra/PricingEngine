#ifndef FUTURE_STRIP_MKT_DATA_REQUEST_H
#define FUTURE_STRIP_MKT_DATA_REQUEST_H

#include "BaseFilteredMktDataRequest.h"
#include "FutureStripQuery.h"
#include "FilterMktData.h"
#include <MarKetQuote\include\FutureQuote.h>

#include <memory>

/*! \brief Class to launch a request to the Bloomberg Data Center for Future Quotes.

*/
class FutureStripMktDataRequest : public BaseFilteredMktDataRequest
{
public:

	/** Constructor.
	Notice that the filter is used only for creating the future quotes, i.e. after receiving results from the mkt data request.
	@param query
	@param priceField mnemonic of the field to be used as the quote's value.
	@param expiryField mnemonic of the field to be used as the quote's expiry.
	@param filter to use when creating the future quotes.
	@throws std::exception if futures don't have the same ticker prefix, i.e. don't have the same underlying.
	*/
	FutureStripMktDataRequest(FutureStripQuery query,
		std::string priceField,
		std::string expiryField,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	Notice that the filter is used only for creating the future quotes, i.e. after receiving results from the mkt data request.
	The default priceField and expiryField used are PX_MID and FUTURES_VALUATION_DATE respectively.
	@param query
	@param filter to use when creating the future quotes.
	@throws std::exception if futures don't have the same ticker prefix, i.e. don't have the same underlying.
	*/
	FutureStripMktDataRequest(FutureStripQuery query,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Method to launch the request and store the result.
	Fields CRNCY, priceField, expiryField (the latter two indicated upon construction) are included in the request
	by default.
	*/
	void run();

	/** It returns the result of the market data query as a vector of FutureQuotes.
	Method run() is called if the result is empty.
	@return std::vector<FutureQuote>
	*/
	std::vector<FutureQuote> futureQuotes();

private:

	std::string underlying_; /**< Identifier for the underlying of the Future Contracts */
	std::string priceField_; /**< Mnemonic of the field to be used as the quote's value. */
	std::string expiryField_; /**< Mnemonic of the field to be used as the quote's expiry. */
};

#endif // !FUTURE_STRIP_MKT_DATA_REQUEST_H
