#ifndef OIS_CURVE_QUERY_H
#define OIS_CURVE_QUERY_H

#include "GeneralQuery.h"

/*! \brief Class to define queries for OIS market quotes.
*/
class OISCurveQuery : public GeneralQuery
{
public:

	/** Constructor.
	It defines as fields to be queried: SECURITY_TENOR_TWO, MID, CRNCY, SETTLE_DT.
	@param securities the securities to be queried.
	@see GeneralQuery(std::vector<std::string>, std::vector<std::string>)
	*/
	OISCurveQuery(std::vector<std::string> securities);

	/** Destructor.
	It does nothing.
	*/
	virtual ~OISCurveQuery();
};

#endif