#ifndef OPTIONS_MKT_DATA_REQUEST_H
#define OPTIONS_MKT_DATA_REQUEST_H

#include "BaseFilteredMktDataRequest.h"
#include <MarKetQuote\include\VanillaOptionQuote.h>

#include <unordered_map>

/*! \brief Class to emulate enum with methods.

*/
class PriceType
{
public:

	static const PriceType BID;
	static const PriceType ASK;
	static const PriceType MID;
	static const PriceType LAST;
	static const PriceType SETTLEMENT;

	/** returns the associated TICKER
	@return std::string
	*/
	std::string to_string() const;

private:

	/** Private Constructor.
	@param type
	*/
	PriceType(std::string type);

	std::string type_; /**< type */

	/** operator==.
	@param lhs
	@param rhs
	*/
	friend bool operator==(const PriceType& lhs, const PriceType& rhs);

	/** operator!=.
	@param lhs
	@param rhs
	*/
	friend bool operator!=(const PriceType& lhs, const PriceType& rhs);
};

/*! \brief Class to request option data for Equity and Indices.

*/
class OptionsMktDataRequest : public BaseFilteredMktDataRequest
{
public:

	/** Constructor.
	@param securities for which to request option data
	@param optionFields 
	@param optionsBulkFieldName MNEMONIC of the bulk field that contains the option tickers.
	@param fieldTickerIdentifier field name in the bulk points that has as value the option ticker.
	@param filter
	*/
	OptionsMktDataRequest(
		std::vector<std::string> securities,
		std::vector<std::string> optionFields,
		std::string optionsBulkFieldName,
		std::string fieldTickerIdentifier,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	@param securities for which to request option data
	@param optionFields
	@param filter
	*/
	OptionsMktDataRequest(
		std::vector<std::string> securities,
		std::vector<std::string> optionFields,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Method to launch the request and store the result.
	*/
	void run();

	/** Method to extract result for specific security.
	@param security
	@return QueryResult containing option data for security.
	*/
	QueryResult optionDataForSecurity(std::string security) const;

	/** Method to generate vector of VanillaOptionQuotes from the results of the request.
	The results are filtered according to the following inequality: keep if price > 0.
	@param security
	@param type of price to use to build the quotes.
	@return std::vector<VanillaOptionQuote>
	*/
	std::vector<VanillaOptionQuote> optionQuotes(std::string security, PriceType type = PriceType::MID);

	/** Inspector for the name of the bulk field used to extract the option tickers.
	@return std::string
	*/
	std::string optionsBulkFieldName() const;

	/** Inspector for the name of the field that contains the option tickers.
	@return std::string
	*/
	std::string fieldTickerIdentifier() const;

	/** Inspector for the number of strikes per expiry.
	@return int
	*/
	int numberStrikesPerExpiry() const;

	/** Inspector for the last expiry for request.
	@return Date
	*/
	Date lastExpiry() const;

	/* Setter.
	The default is 20. The value is applied to calls and puts separately, i.e. by default 20 strikes for calls and
	20 for puts.
	@param n number of strikes per expiry to use when running the request. 
	*/
	void setNumberStrikesPerExpiry(int n);

	/** Setter.
	The default is 4Y from the current date.
	It sets the last expiry for which option data will be requested, i.e.
	options with a expiry greater than this value will not be requested.
	@param date
	*/
	void setLastExpiry(Date date);

protected:

	int numberStrikesPerExpiry_; /**< Strikes to be requested per expiry */
	std::string optionsBulkFieldName_; /**<  MNEMONIC of the bulk field that contains the option tickers*/
	std::string fieldTickerIdentifier_; /**< field name in the bulk points that has as value the option ticker */
	std::unordered_map<std::string, QueryResult> results_; /**< Collective results for all securities */

	OptionsMktDataRequest(){};
	
private:

	/** Helper method to truncate the result (precondition: result has bulk option data) based on the number of strikes.
	Post-condition: truncated vector of options is set as securities for the request.
	@param security for which to set the option tickers.
	@param result precondition: it has bulk option data, e.g. OPT_CHAIN.
	*/
	void setSecuritiesUsingNumberOfStrikesTruncation(std::string security, const QueryResult& result);

	Date lastExpiry_; /**< Last expiry for which option data is requested */

	/* --HIDDEN--
	method derived from base class, hidden because it does not make much sense for this class.
	better to use optionDataForSecurity*/
	SingleSecurityQueryResult resultForSecurity(std::string security) const;

};

#endif // !OPTIONS_MKT_DATA_REQUEST_H
