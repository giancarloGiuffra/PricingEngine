#ifndef OIS_CURVE_FILE_MKT_DATA_REQUEST_H
#define OIS_CURVE_FILE_MKT_DATA_REQUEST_H

#include "OISCurveMktDataRequest.h"
#include "BaseFileMktDataRequest.h"

/*! \brief Class to read OIS market data from file.

The format is the following:
\image html ContentFormatOISCurveMktDataFile.jpg "Content format for OIS Curve Market Data File" width=12cm
*/
class OISCurveFileMktDataRequest : public BaseFileMktDataRequest, private OISCurveMktDataRequest
{
public:

	/** Constructor.
	@param file
	@throws if file does not exist
	*/
	OISCurveFileMktDataRequest(std::string file);

	/** Method to read market data from file.
	If there is an error results are cleared.
	@throws if market data format is not correct
	*/
	void run();

	/** Setter for file.
	It cleans the result after setting the file.
	@param file
	@throws if file does not exist
	*/
	void setFileToRead(std::string file);

	//! \name Exposed Interface of OISCurveMktDataRequest
	//{@

	/** Method that returns a copy of the result.
	@return QueryResult.
	@see QueryResult.
	*/
	using OISCurveMktDataRequest::result;

	/** Method to get an std::vector<OISQuote> built from the result of the request.
	It launches the request if it hasn't been launched before.
	@return std::vector<OISQuote> with the OIS Market Quotes.
	@see OISQuote.
	*/
	using  OISCurveMktDataRequest::oisQuotes;

	//@}
};

#endif // !OIS_CURVE_FILE_MKT_DATA_REQUEST_H
