#ifndef FILTER_MKT_DATA_DECORATOR_H
#define FILTER_MKT_DATA_DECORATOR_H

#include "FilterMktData.h"

#include <memory>

/*! \brief Base decorator pattern class for Filters.

Derived classes must have an optional argument in their constructors : 
std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial())
for the whole machinery to work.
*/
class FilterMktDataDecorator : public FilterMktData
{
public:

	//! \name FilterMktData Interface
	//@{
	/** Filters the result.
	@param result
	@return QueryResult the filtered result.
	*/
	virtual QueryResult filter(QueryResult result);

	/** Description of the filter.
	@return std::string brief description.
	*/
	virtual std::string description();
	//@}

protected:

	/** Constructor.
	@param std::shared_ptr<FilterMktData> pointer to the delegate.
	*/
	FilterMktDataDecorator(std::shared_ptr<FilterMktData> filter);

	/** Method to insert description in derived classes.
	@return std::string description taking into consideration that it may be empty.
	*/
	std::string insertDescription();

	std::shared_ptr<FilterMktData> filter_; /**< Delegate*/
};

#endif // !FILTER_MKT_DATA_DECORATOR_H
