#ifndef EONIA_MKT_DATA_REQUEST_H
#define EONIA_MKT_DATA_REQUEST_H

#include "BaseMarketDataRequest.h"
#include "EONIAQuery.h"

/*! \brief Class to launch a request to the Bloomberg Data Center for the EONIA Index.

\sa EONIAQuery.
*/
class EONIAMktDataRequest : public BaseMarketDataRequest
{
public:
	/** Constructor.
	@param query it contains the fields to be requested for the EONIA Index.
	@see EONIAQuery().
	*/
	EONIAMktDataRequest(EONIAQuery query = EONIAQuery());

	/** Destructor.
	It does nothing.
	*/
	~EONIAMktDataRequest();

};

#endif // !EONIA_MKT_DATA_REQUEST_H
