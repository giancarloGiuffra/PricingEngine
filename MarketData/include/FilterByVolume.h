#ifndef FILTER_BY_VOLUME_H
#define FILTER_BY_VOLUME_H

#include "FilterBy.h"

#include <ql\types.hpp>

using namespace QuantLib;

/*! \brief Filters if volume is less than the one indicated.

*/
class FilterByVolume : public FilterBy<Real>
{
public:

	/** Constructor.
	@param volume used in filter
	@param volumeFieldName name of the field to use for the filtering
	@param filter to decorate
	*/
	FilterByVolume(Real volume, 
		std::string volumeFieldName,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	It uses PX_VOLUME as field.
	@param volume
	@param filter to decorate
	*/
	FilterByVolume(Real volume,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Inspector for the volume used in the filtering.
	@return Natural
	*/
	Real volume() const;

private:

	Real volume_; /**< Volume used in filtering.*/

};

#endif // !FILTER_BY_VOLUME_H
