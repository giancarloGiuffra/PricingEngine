#ifndef FILTER_MKT_DATA_H
#define FILTER_MKT_DATA_H

#include "BaseMarketDataRequest.h"

#include <string>

/*! \brief Interface for Filters of Market Data.

*/
class FilterMktData
{
public:

	/** Method that filters the result of a market data query.
	@param result of a market data query.
	@return QueryResult the filtered result.
	*/
	virtual QueryResult filter(QueryResult result) = 0;

	/** Method to describe the filter.
	@return std::string a brief description
	*/
	virtual std::string description() = 0;
};

/*! \brief Trivial filter, it simply does not filter.

*/
class FilterTrivial : public FilterMktData
{
public:

	/** Constructor.
	*/
	FilterTrivial();

	//! \name FilterMktData Interface
	//@{
	/** Filters the result.
	@param result
	@return QueryResult since trivial it simply returns the same result.
	*/
	QueryResult filter(QueryResult result);

	/** Describes the filter.
	@return std::string empty since trivial.
	*/
	std::string description();
	//@}
};

#endif // !FILTER_MKT_DATA_H
