#ifndef COMMODITY_OPTIONS_MKT_DATA_REQUEST_H
#define COMMODITY_OPTIONS_MKT_DATA_REQUEST_H

#include "OptionsMktDataRequest.h"
#include <MarKetQuote\include\VanillaOptionFutureQuote.h>

/*! \brief Class to request option data for commodities.

It takes as securities the active future contracts associated to the commodities.
The request takes into account that the underlyings is not only the active contract but all the different future contracts 
in the chain.
To extract option data for a specific future use OptionsMktDataRequest.
\sa OptionsMktDataRequest
*/
class CommodityOptionsMktDataRequest : public OptionsMktDataRequest
{
public:

	/** Constructor.
	@param securities for which to request option data, must be commodities (active contract).
	@param optionFields
	@param optionsBulkFieldName MNEMONIC of the bulk field that contains the option tickers.
	@param fieldTickerIdentifier field name in the bulk points that has as value the option ticker.
	@param filter
	@throws if securities are not commodities.
	*/
	CommodityOptionsMktDataRequest(
		std::vector<std::string> securities,
		std::vector<std::string> optionFields,
		std::string optionsBulkFieldName,
		std::string fieldTickerIdentifier,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	@param securities for which to request option data, must be commodities (active contract).
	@param optionFields
	@param filter
	@throws if securities are not commodities.
	*/
	CommodityOptionsMktDataRequest(
		std::vector<std::string> securities,
		std::vector<std::string> optionFields,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Method to launch the request and store the result.
	If a future does not have a PX_LAST it's options are not requested.
	*/
	void run();

	/** Method to generate vector of VanillaOptionFutureQuotes from the results of the request.
	@param security
	@param type of price to use to build the quotes.
	@return std::vector<VanillaOptionFutureQuote>
	*/
	std::vector<VanillaOptionFutureQuote> optionFutureQuotes(std::string security, PriceType type = PriceType::MID);

	/** Setter for the number of futures for which to request option data.
	The default is 48, i.e. for the futures with expiries up to 4Y from now.
	@param n number of futures (coincides with number of months).
	*/
	void setNumberOfFutures(int n);

	/** Inspector for number of futures for request.
	return int
	@see setNumberOfFutures(int)
	*/
	int numberOfFutures() const;

private:

	/** Helper method to truncate options based on number of strikes.
	It assumes that result contains bulk option data for the specified future.
	Post-condition: truncated vector of options is added to the securities for the request.
	@param future for which options are added to securities.
	@param result must contain bulk option data.
	*/
	void addSecuritiesUsingNumberOfStrikesTruncation(std::string future, const QueryResult& result);

	int numberOfFutures_; /**< number of futures for which to request option data.*/

	/* --HIDDEN--
	Method derived from OptionsMktDataRequest. hidden because it does not make sense for this class.
	Use optionFutureQuotes to generate the correct quotes.
	*/
	std::vector<VanillaOptionQuote> optionQuotes(std::string security, PriceType type = PriceType::MID);

	/* --HIDDEN--
	Getter and Setter for last expiry. hidden because it does not make sense for this derived class.
	Use the appropriate method void setNumberOfFutures(int n).
	*/
	Date lastExpiry() const;
	void setLastExpiry(Date date);
};

#endif // !COMMODITY_OPTIONS_MKT_DATA_REQUEST_H
