#ifndef BASE_MARKET_DATA_QUERY_H
#define BASE_MARKET_DATA_QUERY_H

#include "RefDataExample.h"
#include <unordered_map>

/*! \brief Struct to handle Bulk Field Data

*/
struct BulkField {

	/** Constructor.
	@param name
	*/
	BulkField(std::string name);

	/** Constructor.
	@param bulkField
	*/
	BulkField(Element bulkField);

	/** Method to add a bulk point.
	@param point BloombergLP::blpapi::Element
	*/
	void addBulkPoint(Element point);

	/** Method to extract the value corresponding to the field name for each bulk point.
	@param name of the field to extract
	@return std::vector<std::string>
	*/
	std::vector<std::string> field(std::string name) const;

	std::string name; /**< name of the bulk field*/
	std::vector<std::unordered_map<std::string, std::string>> bulkPoints; /*bulk points*/

	/** Overload of operator<<
	@param os an std::ostream&
	@param bulkField to be printed.
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const BulkField& bulkField);
};

/** \relates BulkField
*/
inline bool operator==(const BulkField& lhs, const BulkField& rhs)
{
	return lhs.name == rhs.name && lhs.bulkPoints == rhs.bulkPoints;
}

/** \relates BulkField
*/
inline bool operator!=(const BulkField& lhs, const BulkField& rhs)
{
	return !(lhs == rhs);
}

/*! \brief Struct to store the query results pertaining a single security.
*/
struct SingleSecurityQueryResult {
	/** Constructor.
	@param security name of the security.
	*/
	SingleSecurityQueryResult(std::string security);

	/** Method to add the result relative the queried field.
	@param e BloombergLP::blpapi::Element object that contains the information regarding the queried field.
	*/
	void addField(Element e);

	void addRefField(Element e);

	void addBulkField(Element e);

	/** Method to extract bulk field by name.
	@param name
	@return BulkField
	*/
	BulkField bulkField(std::string name) const;

	std::string security; /**< Name of the security.*/
	std::unordered_map<std::string, std::string > fields; /**< Maps the name of the field to its value.*/
	std::vector<BulkField> bulkFields; /**< eventually bulk fields */

	/** Overload of operator<<.
	@param os an std::ostream&
	@param singleResult to be printed.
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const SingleSecurityQueryResult& singleResult);
};

/** \relates SingleSecurityQueryResult
*/
inline bool operator==(const SingleSecurityQueryResult& lhs, const SingleSecurityQueryResult& rhs)
{
	return lhs.security == rhs.security && lhs.fields == rhs.fields && lhs.bulkFields == rhs.bulkFields;
}

/** \relates SingleSecurityQueryResult
*/
inline bool operator!=(const SingleSecurityQueryResult& lhs, const SingleSecurityQueryResult& rhs)
{
	return !(lhs == rhs);
}

/** \defgroup TypedefsQueryResult TypeDefs For Query Results.
Typedefs to facilitate the scanning of a query result.
*/

/*! \brief It represents the result pertaining a single security.

Nickname for SingleSecurityQueryResult.
\ingroup TypedefsQueryResult
*/
typedef SingleSecurityQueryResult QueryResultElement;

/*! \brief It represents the result of a query.

Nickname for a vector of QueryResultElement.
\ingroup TypedefsQueryResult
*/
typedef std::vector < SingleSecurityQueryResult > QueryResult;

/** \relates QueryResult
*/
std::ostream& operator<<(std::ostream& os, const QueryResult& result);

/** \relates QueryResult
*/
SingleSecurityQueryResult extractResultForSecurity(std::string security, const QueryResult& queryResult);

/*----------------------------------------------------------------------------------------------------------------------------*/

/*! \brief Class to make queries to the Bloomberg Data Center and store the results.

This is the class to derive from to handle query results more specifically. It basically adds functionality to RefDataExample
allowing it to store the results and making it more user friendly.
*/
class BaseMarketDataRequest : public RefDataExample
{
public:
	/** Default Constructor.
	It simply calls the base class default constructor and clears the securities and the fields containers.
	*/
	BaseMarketDataRequest();

	/** Constructor.
	It initializes the securities and fields containers.
	@param securities the securities to query. (Use Bloomberg Tickers)
	@param fields the fields to query. (Use Field Mnemonic)
	*/
	BaseMarketDataRequest(std::vector<std::string> securities, std::vector<std::string> fields);

	/** Copy Constructor.
	It calls the base class copy constructor and copies #result_ as well.
	*/
	BaseMarketDataRequest(const BaseMarketDataRequest& other);

	/** Copy Assignment Operator.
	It uses the copy and swap idiom.
	@see swap(BaseMarketDataRequest&, BaseMarketDataRequest&)
	*/
	BaseMarketDataRequest& operator=(BaseMarketDataRequest other);

	/** Destructor.
	It does nothing.
	*/
	virtual ~BaseMarketDataRequest();

	/** Method to set the securities to be queried.
	It overwrites any security previously stored.
	@param securities the securities to be queried.
	*/
	void setSecurities(std::vector<std::string> securities);

	/** It clears the securities.
	*/
	void clearSecurities();

	/** Method to set the fields to be queried.
	It overwrites any field previously stored.
	@param fields the fields to be queried.
	*/
	void setFields(std::vector<std::string> fields);

	/** It clears the fields.
	*/
	void clearFields();

	/** Method to add to the securities to be queried.
	It appends the securities to any security previously stored.
	@param securities the securities to be queried.
	*/
	void addSecurities(std::vector<std::string> securities);

	/** Method to add to the fields to be queried.
	It appends the fields to any field previously stored.
	@param fields the fields to be queried.
	*/
	void addFields(std::vector<std::string> fields);

	/** Method to print the list of securities.
	*/
	void printSecurities();

	/** Method to print the list of fields.
	*/
	void printFields();

	/** Method to print the list of securities and fields.
	*/
	void printSecuritiesAndFields();

	/** Method to send the query, process the response and store the result.
	It stores the result in #result_.
	*/
	virtual void run();

	/** Method to print the result.
	*/
	void printResult();

	/** Method that runs the request and prints the result.
	It simply calls run() and printResult() in that order.
	*/
	void runAndPrintResult();

	/** Method that returns a copy of the securities.
	@return std::vector<std::string> of securities.
	*/
	std::vector<std::string> securities() const;

	/** Method that returns a copy of the fields.
	@return std::vector<std::string> of fields.
	*/
	std::vector<std::string> fields() const;

	/** Method that returns a copy of the result.
	@return QueryResult.
	@see QueryResult.
	*/
	QueryResult result() const;

	/** Method to extract result for specific security.
	@param security
	@return SingleSecurityQueryResult
	*/
	SingleSecurityQueryResult resultForSecurity(std::string security) const;

	/** Friend function to implement the copy and swap idiom.
	@param first left hand side of the assignment.
	@param second right hand side.
	*/
	friend void swap(BaseMarketDataRequest& first, BaseMarketDataRequest& second);

private:
	/** Method that hides the base class method.
	It introduces the necessary changes to store the result.
	*/
	void eventLoop(Session &session);

	/** Method that hides the base class method.
	It introduces the necessary changes to store the result.
	*/
	void processResponseEvent(Event event);

protected:
	QueryResult result_; /**< It contains the result.*/

};

#endif