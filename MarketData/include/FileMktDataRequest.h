#ifndef FILE_MKT_DATA_REQUEST_H
#define FILE_MKT_DATA_REQUEST_H

#include "BaseFileMktDataRequest.h"
#include "BaseMarketDataRequest.h"

#include <vector>
#include <string>

/*! \brief Class that reads Market Data from file.

The format is the following:
\image html ContentFormatMktDataFile.jpg "Content format for Market Data File" width=12cm
*/
class FileMktDataRequest : public BaseFileMktDataRequest, private BaseMarketDataRequest
{
public:

	/** Constructor.
	@param file
	@throws if file does not exist
	*/
	FileMktDataRequest(std::string file);

	/** Method to read market data from file.
	If there is an error results are cleared.
	@throws if market data format is not correct
	*/
	void run();

	/** It returns the securities read from the file.
	It return an empty vector if the file has not yet been read.
	@return std::vector<std::string>>
	*/
	std::vector<std::string> securities() const;

	/** It returns the fields read for the specified security.
	@param security
	@return std::vector<std::string>
	@throws if security is not present
	*/
	std::vector<std::string> fields(std::string security) const;

	/** Setter for file.
	It cleans the result after setting the file.
	@param file
	@throws if file does not exist
	*/
	void setFileToRead(std::string file);

	//! \name Exposed Interface of BaseMarketDataRequest
	//{@

	/** Method that returns a copy of the result.
	@return QueryResult.
	@see QueryResult.
	*/
	using BaseMarketDataRequest::result; //doxygen_exposed_interface

	/** Method to extract result for specific security.
	@param security
	@return SingleSecurityQueryResult
	*/
	using  BaseMarketDataRequest::resultForSecurity; //doxygen_exposed_interface

	//@}

};

#endif // !FILE_MKT_DATA_REQUEST_H
