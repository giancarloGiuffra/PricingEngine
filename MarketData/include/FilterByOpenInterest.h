#ifndef FILTER_BY_OPEN_INTEREST_H
#define FILTER_BY_OPEN_INTEREST_H

#include "FilterBy.h"

#include <ql\types.hpp>

using namespace QuantLib;

/*! \brief Filters if open interest is less than the one indicated.

*/
class FilterByOpenInterest : public FilterBy < Real >
{
public:

	/** Constructor.
	@param openInterest
	@param fieldName
	@param filter to decorate
	*/
	FilterByOpenInterest(
		Real openInterest,
		std::string fieldName,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	It uses OPEN_INT as field.
	@param openInterest
	@param filter to decorate
	*/
	FilterByOpenInterest(
		Real openInterest,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Inspector for the open interest.
	@return Natural
	*/
	Real openInterest() const;

private:

	Real openInterest_; /**< Open Interest used in the filtering. */
};

#endif // !FILTER_BY_OPEN_INTEREST_H
