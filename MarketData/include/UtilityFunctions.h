#ifndef MARKET_DATA_UTILITY_FUNCTIONS_H
#define MARKET_DATA_UTILITY_FUNCTIONS_H

#include <ql\errors.hpp>
#include <ql\option.hpp>
#include <cmath>
#include <vector>
#include <iterator>
#include <algorithm>
#include <regex>
#include <numeric>

using namespace QuantLib;

/*! \brief Namespace that contains utility functions to facilitate manipulation of Market Data.
*/
namespace marketDataUtilities
{
	/** It returns a truncated copy of the initial vector.
	The truncation is symmetrical (when possible).
	@param n number of elements to retain.
	@param vec vector to be truncated.
	@param c center around which truncate (index), if not given truncation is around the middle index.
	@return std::vector<T> of n elements.
	@throws if n <= 0 or vector is empty or c is not a valid index (when given).
	*/
	template<class T>
	std::vector<T> truncateVectorBy(int n, const std::vector<T>& vec, int c = Null<int>())
	{
		QL_REQUIRE(n > 0, "n must be greater than zero (given value is " << n << " )");
		QL_REQUIRE(!vec.empty(), "vector must not be empty!");
		if (c != Null<int>())
			QL_REQUIRE(c >= 0 && c < vec.size(), 
			"center c must be a valid index (given value is " << c << " while size is " << vec.size() << ")");
		if (n >= vec.size())
			return std::vector<T>(vec);
		else {
			int center = c != Null<int>() ? c : (int)std::ceil( vec.size() / 2.0) - 1;
			int begin = std::max( 0, n % 2 == 0 ? center - (n / 2 - 1) : center - (n - 1) / 2 );
			begin = begin + n > vec.size() ? vec.size() - n : begin;
			int end = std::min(begin + n, (int)vec.size());
			std::vector<T> truncated;
			std::copy(vec.begin() + begin, vec.begin() + end, std::back_inserter(truncated));
			return truncated;
		}
	}

	/** It extracts the expiries from the tickers.
	The expiries are sorted in chronological order.
	@param tickers
	@return std::vector<std::string>
	@throws if any of the elements is not a well formed ticker.
	*/
	std::vector<std::string> extractExpiries(std::vector<std::string> tickers);

	/** It sorts the expiries in chronological order.
	Pay attention to the passage by reference.
	@param expiries
	@param format
	@throws if not all expiries have the specified format
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	void sortExpiries(std::vector<std::string>& expiries, std::string format = "%m/%d/%y");

	/** It truncates the expiries later than the given date.
	Pay attention to the passage by reference.
	@param expiries
	@param date
	@param format
	@throws if not all expiries have the specified format
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	void truncateExpiries(std::vector<std::string>& expiries, Date date, std::string format = "%m/%d/%y");

	/** It controls if string is a well formed option ticker.
	@param ticker
	@return true if it is a well formed option ticker.
	*/
	bool isOptionTicker(std::string ticker);

	/** It extracts the option tickers for a given expiry and type.
	@param expiry in format mm/dd/yy.
	@param type QuantLib::Option::Type
	@param tickers from which to extract the options
	@return std::vector<std::string>
	@throws if expiry is not in correct format
	*/
	std::vector<std::string> extractOptionsForExpiry(
		std::string expiry, 
		::Option::Type type,
		const std::vector<std::string>& tickers);

	/** It extracts the call option tickers for a given expiry.
	@param expiry in format mm/dd/yy.
	@param tickers from which to extract the options
	@return std::vector<std::string>
	@throws if expiry is not in correct format
	*/
	std::vector<std::string> extractCallOptionsForExpiry(
		std::string expiry,
		const std::vector<std::string>& tickers);

	/** It extracts the put option tickers for a given expiry.
	@param expiry in format mm/dd/yy.
	@param tickers from which to extract the options
	@return std::vector<std::string>
	@throws if expiry is not in correct format
	*/
	std::vector<std::string> extractPutOptionsForExpiry(
		std::string expiry,
		const std::vector<std::string>& tickers);

	/** It truncates symmetrically the option tickers by number of strikes.
	It assumes that the tickers correspond to the same underlying and expiry.
	@param strike_format regex to extract the strike, the regex must have only one group
	@param n number of strikes
	@param tickers option tickers to truncate
	@param value strike around which truncate, if not given truncation is performed around middle index.
	@return std::vector<std::string>
	*/
	std::vector<std::string> truncateOptionsByNumberOfStrikesWithRegex(
		std::regex strike_format,
		int n, const std::vector<std::string>& tickers, Real value = Null<Real>());

}

#endif // !MARKET_DATA_UTILITY_FUNCTIONS_H
