#ifndef BASE_FILE_MKT_DATA_REQUEST_H
#define BASE_FILE_MKT_DATA_REQUEST_H

#include <string>

/*! \brief Base class for Market Data Requests that read from file.

*/
class BaseFileMktDataRequest
{
public:

	/** Constructor.
	@param file
	@throws if file does not exist
	*/
	BaseFileMktDataRequest(std::string file);

	/** Setter for file.
	@param file
	@throws if file does not exist
	*/
	void setFileToRead(std::string file);

	/** Getter for file.
	@return std::string
	*/
	std::string file() const;

protected:

	std::string file_; /**< file to process */
};

#endif // !BASE_FILE_MKT_DATA_REQUEST_H
