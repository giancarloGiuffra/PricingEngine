#ifndef OIS_CURVE_EUR_H
#define OIS_CURVE_EUR_H

#include "OISCurveQuery.h"

/*! \brief Class that contains the OIS market quotes to be queried for EUR currency.

Currently the market quotes are the ones used for the bootstrapping of the EUR OIS Curve.
\sa OISCurveMktDataRequest(OISCurveQuery)
*/
class OISCurveEUR : public OISCurveQuery
{
public:

	/** Constructor.
	*/
	OISCurveEUR();

	/** Destructor.
	It does nothing.
	*/
	~OISCurveEUR();
};

#endif