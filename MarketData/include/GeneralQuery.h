#ifndef GENERAL_QUERY
#define GENERAL_QUERY

#include <vector>

/*! \brief Base Class to represent a query to the Bloomberg Data Center.

This class is used to derive classes that can be used to couple queries to requests. e.g. class OISCurveQuery and class
OISCurveMktDataRequest. OISCurveMktDataRequest constructor accepts only instances of class OISCurveQuery.
*/
class GeneralQuery
{
public:
	/** Constructor.
	@param securities the securities to be queried. (Use Bloomberg Tickers)
	@param fields the fields to be queried. (Use Field Mnemonic)
	*/
	GeneralQuery(std::vector<std::string> securities, std::vector<std::string> fields);

	/** Destructor.
	It does nothing.
	*/
	virtual ~GeneralQuery();

	/** Method to return a copy of the securities.
	@return std::vector<std::string> of securities.
	*/
	std::vector<std::string> securities();

	/** Method to return a copy of the fields.
	@return std::vector<std::string> of fields.
	*/
	std::vector<std::string> fields();

private:
	std::vector<std::string> securities_; /**< Contains the securities. */
	std::vector<std::string> fields_; /**< Contains the fields.*/

};

#endif