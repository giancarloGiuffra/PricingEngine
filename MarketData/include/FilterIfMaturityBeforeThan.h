#ifndef FILTER_IF_MATURITY_LATER_THAN_H
#define FILTER_IF_MATURITY_LATER_THAN_H

#include "FilterIf.h"

#include <ql\time\date.hpp>

using namespace QuantLib;

/*! \brief Filters if maturity is before than specified date.

*/
class FilterIfMaturityBeforeThan : public FilterIf
{
public:

	/** Constructor.
	@param date
	@param maturity
	*/
	FilterIfMaturityBeforeThan(Date date, std::string maturity = "OPT_EXPIRE_DT",
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));
};

#endif // !FILTER_IF_MATURITY_LATER_THAN_H
