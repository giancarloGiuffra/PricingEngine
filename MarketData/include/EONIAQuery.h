#ifndef EONIA_QUERY_H
#define EONIA_QUERY_H

#include "GeneralQuery.h"

/*! \brief Class to define queries for the EONIA Index.
*/
class EONIAQuery : public GeneralQuery
{
public:
	/** Constructor.
	@param fields the fields to be queried.
	@see GeneralQuery(std::vector<std::string>, std::vector<std::string>)
	*/
	EONIAQuery(std::vector<std::string> fields);

	/** Default Constructor.
	It defines the following fields to be queried: NAME, CRNCY, CALENDAR_CODE, LAST_PRICE, LAST_UPDATE_DT.
	*/
	EONIAQuery();

	/** Destructor.
	It does nothing.
	*/
	~EONIAQuery();
};

#endif