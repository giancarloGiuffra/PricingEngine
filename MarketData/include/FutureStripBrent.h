#ifndef FUTURE_STRIP_BRENT_H
#define FUTURE_STRIP_BRENT_H

#include "FutureStripQuery.h"

/*! \brief Class to facilitate building queries for Brent Future Contracts.

*/
class FutureStripBrent : public FutureStripQuery
{
public:

	/** Constructor.
	@param numberOfContracts , consider that there are brent futures for each month, i.e. a value of 48 means
	48 contracts one for each month up to 4 years from now. 
	*/
	FutureStripBrent(int numberOfContracts = 48);

private:

	/** Helper method to build the Future Tickers.
	@param numberOfContracts
	@return std::vector<std::string>
	*/
	static std::vector<std::string> buildFutureTickers(int numberOfContracts);

};

#endif // !FUTURE_STRIP_BRENT_H
