#ifndef OIS_CURVE_USD_H
#define OIS_CURVE_USD_H

#include "OISCurveQuery.h"

/*! \brief Class that contains the OIS market quotes to be queried for EUR currency.

*/
class OISCurveUSD : public OISCurveQuery
{
public:

	/** Constructor.
	*/
	OISCurveUSD();
};

#endif // !OIS_CURVE_USD_H
