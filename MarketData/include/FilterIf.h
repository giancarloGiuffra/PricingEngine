#ifndef FILTER_IF_H
#define FILTER_IF_H

#include "FilterMktDataDecorator.h"

#include <functional>

/*! \brief Class for generic Filter on QueryResultElement.

*/
class FilterIf : public FilterMktDataDecorator
{
public:

	/** Constructor.
	Filters out query result if predicate is true.
	@param predicate
	@param description
	@param filter to decorate
	*/
	FilterIf(std::function<bool(QueryResultElement)> predicate, std::string description,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Implicit conversion to std::shared_ptr<FilterMktData>.
	*/
	operator std::shared_ptr<FilterMktData>() const;

	//! \name FilterMktData Interface
	//@{

	/** Filters the result.
	@param result
	@return QueryResult the filtered result.
	*/
	QueryResult filter(QueryResult result);

	/** Description of the filter.
	@return std::string
	*/
	std::string description();
	//@}

protected:

	/** Filters the result according to the passed predicate.
	@param result
	@return QueryResult the filtered result.
	*/
	QueryResult thisFilter(QueryResult result);

	std::string description_; /**< Description of the filter. */
	std::function<bool(QueryResultElement)> predicate_; /**< Predicate that defines the filter. */
};

#endif // !FILTER_IF_H
