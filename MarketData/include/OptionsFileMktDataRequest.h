#ifndef OPTIONS_FILE_MKT_DATA_REQUEST_H
#define OPTIONS_FILE_MKT_DATA_REQUEST_H

#include "BaseFileMktDataRequest.h"
#include "OptionsMktDataRequest.h"

/*! \brief Class to read Options Market Data from file.

The format is the following:
\image html ContentFormatOptionsMktDataFile.jpg "Content format for Options Market Data File" width=12cm

The option keyword must be matched exactly (it's trim before comparison) to signal the start of
data for a new option. Furthermore to be able to construct option quotes from the data the following fields 
must be present (this is not checked when running the request):
- OPT_UNDL_TICKER
- OPT_PUT_CALL
- OPT_EXER_TYP
- OPT_STRIKE_PX
- OPT_EXPIRE_DT
- CRNCY
- one among BID, ASK, MID, LAST_PRICE, PX_SETTLE_ACTUAL_RT (depending on the type of quote to build)

To apply filters to the data one has to make sure that the relevant fields are present. For example this
is a list of built-in filters and the required field:
- FilterByVolume: default field is PX_VOLUME
- FilterByOpenInterest: default field is OPEN_INT
- FilterIfITM: OPT_UNDL_PX

*/
class OptionsFileMktDataRequest : public BaseFileMktDataRequest, private OptionsMktDataRequest
{
public:

	/** Constructor.
	@param file
	@throws if file does not exist
	*/
	OptionsFileMktDataRequest(std::string file);

	/** Method to read market data from file.
	If there is an error results are cleared.
	@throws if market data format is not correct
	*/
	void run();

	/** Setter for file.
	It cleans the result after setting the file.
	@param file
	@throws if file does not exist
	*/
	void setFileToRead(std::string file);

	//! \cond EXPOSED_INTERFACE_OPTIONS_FILE_MKT_DATA

	//! \name Exposed Interface of OptionsMktDataRequest
	//{@

	/** Method to extract result for specific security.
	@param security
	@return QueryResult containing option data for security.
	*/
	using OptionsMktDataRequest::optionDataForSecurity;

	/** Method to generate vector of VanillaOptionQuotes from the results of the request.
	The results are filtered according to the following inequality: keep if price > 0.
	@param security
	@param type of price to use to build the quotes.
	@return std::vector<VanillaOptionQuote>
	*/
	using OptionsMktDataRequest::optionQuotes;

	/** Setter for the filter.
	@param filter
	*/
	using OptionsMktDataRequest::setFilter;

	/** Inspector for the filter.
	@return std::shared_ptr<FilterMktData>
	*/
	using OptionsMktDataRequest::filter;

	//@}

	//! \endcond
};

#endif // !OPTIONS_FILE_MKT_DATA_REQUEST_H
