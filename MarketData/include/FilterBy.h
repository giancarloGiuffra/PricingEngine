#ifndef FILTER_BY_H
#define FILTER_BY_H

#include "FilterMktDataDecorator.h"

#include <functional>
#include <algorithm>
#include <iterator>
#include <type_traits>
#include <ql\errors.hpp>
#include <boost\lexical_cast.hpp>

/*! \brief Class for generic Filter by Field.

The template parameter is used to indicate the type of the variable that is to be filtered.
*/
template<typename T>
class FilterBy : public FilterMktDataDecorator
{
public:

	/** Constructor.
	@param field name of the field to be used in the filtering, e.g. Bloomberg Ticker.
	@param predicate std::function<bool(T)> used to filter.
	@param description
	@param filter to decorate
	*/
	FilterBy(std::string field, 
		std::function<bool(T)> predicate,
		std::string description,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Constructor.
	It sets a default description.
	@param field name of the field to be used in the filtering, e.g. Bloomberg Ticker.
	@param predicate std::function<bool(T)> used to filter.
	@param filter to decorate
	*/
	FilterBy(std::string field,
		std::function<bool(T)> predicate,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	/** Implicit conversion to std::shared_ptr<FilterMktData>.
	*/
	operator std::shared_ptr<FilterMktData>() const;

	//! \name FilterMktData Interface
	//@{

	/** Filters the result.
	If the field is not present it is filtered out as well.
	@param result
	@return QueryResult the filtered result.
	*/
	QueryResult filter(QueryResult result);

	/** Description of the filter.
	@return std::string
	*/
	std::string description();
	//@}

	/** Inspector for the name of the field used in the filtering.
	@return std::string
	*/
	std::string fieldName();

protected:

	/** Filters the result according to the passed predicate.
	@param result
	@return QueryResult the filtered result.
	*/
	QueryResult thisFilter(QueryResult result);

	std::string field_; /**< Name of the field to use in the filtering. */
	std::string description_; /**< Description of the filter. */
	std::function<bool(T)> predicate_; /**< Predicate that defines the filter. */
	std::function<T(std::string)> converter_; /**< Support function to convert from std::string to T*/
};

/*---------------------IMPLEMENTATION--------------------------------------------------------------------------*/

/*constructor*/
template<typename T>
FilterBy<T>::FilterBy(std::string field,
	std::function<bool(T)> predicate,
	std::string description,
	std::shared_ptr<FilterMktData> filter)
	: FilterMktDataDecorator(filter), field_(field), predicate_(predicate), description_(description)
{
	converter_ = [](std::string s){return boost::lexical_cast<T>(s); };
}

/*constructor*/
template<typename T>
FilterBy<T>::FilterBy(std::string field,
	std::function<bool(T)> predicate,
	std::shared_ptr<FilterMktData> filter)
	:FilterBy(field, predicate, "Filter's description not provided",filter)
{}

/*implicit conversion to std::shared_ptr<FilterMktData>*/
template<typename T>
FilterBy<T>::operator std::shared_ptr<FilterMktData>() const
{
	return std::shared_ptr<FilterMktData>(new FilterBy<T>(*this));
}

/*filter*/
template<typename T>
QueryResult FilterBy<T>::filter(QueryResult result)
{
	return thisFilter(FilterMktDataDecorator::filter(result));
}

/*description*/
template<typename T>
std::string FilterBy<T>::description()
{
	return FilterMktDataDecorator::insertDescription() + description_;
}

/*field name*/
template<typename T>
std::string FilterBy<T>::fieldName()
{
	return this->field_;
}


/*last filter*/
template<typename T>
QueryResult FilterBy<T>::thisFilter(QueryResult result)
{
	//lambda to check if field is present
	std::string field = field_;
	std::function<bool(QueryResultElement)> fieldPresent = 
		[field](QueryResultElement el){return el.fields.find(field) != el.fields.end(); };

	//filter - .begin() of empty vector not valid must use std::back_inserter
	std::function<bool(T)> p = predicate_;
	std::function<T(std::string)> convert = converter_;
	QueryResult filteredResult;
	std::remove_copy_if(result.begin(), result.end(), std::back_inserter(filteredResult),
		[&field, &p, &convert, &fieldPresent](QueryResultElement el){return !fieldPresent(el) || p(convert(el.fields[field])); });

	return filteredResult;
}

#endif // !FILTER_BY_H
