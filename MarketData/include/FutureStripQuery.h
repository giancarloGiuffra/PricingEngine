#ifndef FUTURE_STRIP_QUERY_H
#define FUTURE_STRIP_QUERY_H

#include "GeneralQuery.h"

/*! \brief Class to define queries for Future market quotes.

*/
class FutureStripQuery : public GeneralQuery
{
public:

	/** Constructor.
	@param securities futures to be queried.
	@param fields to be queried.
	*/
	FutureStripQuery(std::vector<std::string> securities, std::vector<std::string> fields);
};

#endif // !FUTURE_STRIP_QUERY_H
