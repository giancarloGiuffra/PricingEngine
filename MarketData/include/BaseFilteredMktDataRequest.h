#ifndef BASE_FILTERED_MKT_DATA_REQUEST_H
#define BASE_FILTERED_MKT_DATA_REQUEST_H

#include "BaseMarketDataRequest.h"
#include "FilterMktData.h"
#include <memory>

/*! \brief Adds a Filter to BaseMarketDataRequest.

Constructor protected to be unable to directly instantiate.
\sa BaseMarketDataRequest, FilterMktData
*/
class BaseFilteredMktDataRequest : public BaseMarketDataRequest
{
public:

	/** Setter for the filter.
	@param filter
	*/
	void setFilter(std::shared_ptr<FilterMktData> filter);

	/** Inspector for the filter.
	@return std::shared_ptr<FilterMktData>
	*/
	const std::shared_ptr<FilterMktData> filter() const;

	/** Method to add required fields.
	It's meant to be used to indicate the fields necessary for the filter to work.
	These fields will be added to the query before running it.
	It's handy when the MktDataRequest class is built to produce some specific type of quotes that needs
	to be filtered and needs as well some required fields to be constructed.
	@param fields
	*/
	void addRequiredFields(std::vector<std::string> fields);

	/** Clears the content of the required fields.
	*/
	void clearRequiredFields();

	/** Method to run the request and store the result.
	*/
	void run();

protected:

	/** Constructor.
	@param securities
	@param fields
	@param filter
	*/
	BaseFilteredMktDataRequest(std::vector<std::string> securities,
		std::vector<std::string> fields,
		std::shared_ptr<FilterMktData> filter = std::shared_ptr<FilterMktData>(new FilterTrivial()));

	BaseFilteredMktDataRequest(){};

	std::shared_ptr<FilterMktData> filter_; /**< Filter to use when manipulating result.*/

	std::vector<std::string> requiredFields_; /**< Filters use specific fields*/
};

#endif // !BASE_FILTERED_MKT_DATA_REQUEST_H
