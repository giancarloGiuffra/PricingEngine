#import blpapi library (bloomberg)
add_library(blpapi SHARED IMPORTED)
set_property(TARGET blpapi PROPERTY IMPORTED_LOCATION ${BLPAPI_DIR}/lib/blpapi3_64.dll)
set_property(TARGET blpapi PROPERTY IMPORTED_IMPLIB ${BLPAPI_DIR}/lib/blpapi3_64.lib)

# create variable marketData_SRC with all cpp and h files in the /src /include directories respectively
file(GLOB marketData_SRC RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" 
	"${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")

#include directory /include so cmake knows where to find the headers
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

#add subdirecty test to the build
add_subdirectory(test)

# define library marketData with dependencies given by the list above and link to the imported blpapi library and the bboost library
add_library(MarketData ${marketData_SRC})
target_link_libraries(MarketData blpapi Utilities MarketQuote ${Boost_LIBRARIES} optimized quantlib debug quantlibDebug)

install(TARGETS MarketData DESTINATION lib EXPORT pricingengine)
