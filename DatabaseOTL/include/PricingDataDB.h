#ifndef PRICING_DATA_DB_H
#define PRICING_DATA_DB_H

#include <MarKetQuote\include\FutureQuote.h>
#include <MarKetQuote\include\OISQuote.h>
#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarKetQuote\include\VanillaOptionFutureQuote.h>

#include <vector>
#include <utility>
#include <tuple>

#define OTL_ODBC
#define OTL_STL
#include <otlv4.h>

/*! \brief Class to interact with the pricing_data database.

*/
class PricingDataDB
{
public:

	/** Method to access the singleton class.
	It takes care of establishing the connection with the database.
	@return PricingDataDB&
	*/
	static PricingDataDB& instance();

	/** It returns the last future quotes of the date for the specified underlying.
	@param date, it must be in the format %Y-%m-%d
	@param underlying
	@return std::vector<std::pair<std::string,FutureQuote>>, the string corresponds to the timestamp in the db.
	*/
	std::vector<std::pair<std::string,FutureQuote>> selectFutureQuotes(std::string date, std::string underlying);

	/** It inserts the future quotes into the database.
	The timestamp is calculated automatically and not required as input parameter.
	@param quotes
	*/
	void insertFutureQuotes(const std::vector<FutureQuote>& quotes);

	/** It returns the last ois quotes of the date for the specified currency.
	@param date, it must be in the format %Y-%m-%d
	@param currency, it must be an ISO 4217 three-letter code, e.g, "EUR"
	@return std::vector<std::pair<std::string, OISQuote>>, the string corresponds to the timestamp in the db.
	*/
	std::vector<std::pair<std::string, OISQuote>> selectOISQuotes(std::string date, std::string currency);

	/** It inserts the ois quotes into the database.
	The timestamp is calculated automatically and not required as input parameter.
	@param quotes
	*/
	void insertOISQuotes(const std::vector<OISQuote>& quotes);

	/** It returns a tuple containing the timestamp, currency, and value of the last quote of the date.
	@param date, it must be in the format %Y-%m-%d
	@param underlying
	@return std::tuple<std::string, std::string, double>
	*/
	std::tuple<std::string, std::string, double> selectSpotQuote(std::string date, std::string underlying);

	/** It inserts the spot quote into the database.
	The timestamp is calculated automatically and not required as input parameter.
	@param underlying
	@param currency
	@param value
	*/
	void insertSpotQuote(std::string underlying, std::string currency, double value);

	/** It returns the last option quotes of the date for the specified underlying.
	@param date, it must be in the format %Y-%m-%d
	@param underlying
	@return std::vector<std::pair<std::string, VanillaOptionQuote>>
	*/
	std::vector<std::pair<std::string, VanillaOptionQuote>> 
		selectOptionQuotes(std::string date, std::string underlying);

	/** It inserts the option quotes into the database.
	The timestamp is calculated automatically and not required as input parameter.
	@param quotes
	*/
	void insertOptionQuotes(std::vector<VanillaOptionQuote> quotes);

	/** It returns the last option on future quotes of the date for the specified underlying.
	@param date, it must be in the format %Y-%m-%d
	@param underlying, use the first two letters of the future's ticker name (e.g. CO for the Brent Crude Oil)
	@return std::vector<std::pair<std::string, VanillaOptionQuote>>
	*/
	std::vector<std::pair<std::string, VanillaOptionFutureQuote>>
		selectOptionFutureQuotes(std::string date, std::string underlying);

	/** It inserts the option future quotes into the database.
	The timestamp is calculated automatically and not required as input parameter.
	@param quotes
	*/
	void insertOptionFutureQuotes(std::vector<VanillaOptionFutureQuote> quotes);

	/** Destructor.
	It terminates the connection with the database.
	*/
	~PricingDataDB();

private:

	/** Constructor.
	Private since it's a singleton class.
	*/
	PricingDataDB();

	/** Copy Constructor.
	delete disallows copying.
	*/
	PricingDataDB(PricingDataDB const&) = delete;

	/** Copy Assignment.
	delete disallows copying.
	*/
	void operator=(PricingDataDB const&) = delete;

	otl_connect db; /**< connection object */

};

#endif // !PRICING_DATA_DB_H
