#ifndef DATABASEOTL_UTILITY_FUNCTIONS_H
#define DATABASEOTL_UTILITY_FUNCTIONS_H

#define OTL_ODBC
#include <otlv4.h>
#include <ctime>

/*! \brief Namespace that contains utility functions to facilitate calls to OTL library.
*/
namespace dbOTLUtilities
{
	/** It returns the corresponding otl_datetime from std::tm.
	The mapped fields are the ones corresponding to year, month, day, hour, minute and second only.
	@param date
	@return otl_datetime
	*/
	otl_datetime dateSTLToOTL(std::tm date);

	/** It returns the corresponding std::tm from otl_datetime.
	The mapped fields are the ones corresponding to year, month, day, hour, minute and second only.
	@param date
	@return std::tm
	*/
	std::tm dateOTLToSTL(otl_datetime date);

	/** It returns the OTL date corresponding to the string.
	The mapped fields are the ones corresponding to year, month, day, hour, minute and second only.
	@param date
	@param format
	@return otl_datetime
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	otl_datetime dateStringToOTL(std::string date, std::string format);

	/** It returns the string corresponding to the OTL date.
	The mapped fields are the ones corresponding to year, month, day, hour, minute and second only.
	@param date
	@param format
	@return std::string
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	std::string dateOTLToString(otl_datetime date, std::string format);
}

#endif // !DATABASEOTL_UTILITY_FUNCTIONS_H

