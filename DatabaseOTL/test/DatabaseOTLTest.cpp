#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <DatabaseOTL\include\UtilityFunctions.h>
#include <Utilities\include\DateConversion.h>
#include <DatabaseOTL\include\PricingDataDB.h>

namespace DatabaseOTL
{
namespace testing
{
	/*the insert procedures have been tested once and then erased*/
	class DatabaseOTLTest : public Test
    {
    protected:
        DatabaseOTLTest(){}
        ~DatabaseOTLTest(){}
		
		virtual void SetUp(){}
        virtual void TearDown(){}

    };

    TEST_F(DatabaseOTLTest, dbOTLUtilities_dateSTLToOTLAndViceversa)
    {
		std::tm date = utilities::dateStringToTM("2016-04-15", "%Y-%m-%d");
		otl_datetime otlDate = dbOTLUtilities::dateSTLToOTL(date);
		std::tm dateTransformed = dbOTLUtilities::dateOTLToSTL(otlDate);
		EXPECT_THAT(date.tm_year, Eq(dateTransformed.tm_year));
		EXPECT_THAT(date.tm_mon, Eq(dateTransformed.tm_mon));
		EXPECT_THAT(date.tm_mday, Eq(dateTransformed.tm_mday));
		EXPECT_THAT(date.tm_hour, Eq(dateTransformed.tm_hour));
		EXPECT_THAT(date.tm_min, Eq(dateTransformed.tm_min));
		EXPECT_THAT(date.tm_sec, Eq(dateTransformed.tm_sec));
    }

	TEST_F(DatabaseOTLTest, PricingDataDB_connection)
	{
		PricingDataDB::instance();
	}

	TEST_F(DatabaseOTLTest, PricingDataDB_selectFutureQuotes)
	{
		PricingDataDB::instance().selectFutureQuotes("2016-02-18", "CO");
	}

	TEST_F(DatabaseOTLTest, PricingDataDB_selectOISQuotes)
	{
		auto rates = PricingDataDB::instance().selectOISQuotes("2016-02-18", "EUR");
		for (auto& rate : rates)
		{
			std::cout << "timestamp : " << rate.first << ", quote: " << rate.second << std::endl;
		}
	}

	TEST_F(DatabaseOTLTest, PricingDataDB_selectSpotQuote)
	{
		auto spot = PricingDataDB::instance().selectSpotQuote("2016-02-18", "SX5E");
		std::cout << "timestamp: " << std::get<0>(spot) << std::endl;
		std::cout << "currency: " << std::get<1>(spot) << std::endl;
		std::cout << "value: " << std::get<2>(spot) << std::endl;
	}

	TEST_F(DatabaseOTLTest, PricingDataDB_selectOptionQuotes)
	{
		auto quotes = PricingDataDB::instance().selectOptionQuotes("2016-02-17", "SX5E");
		for (auto& q : quotes)
		{
			std::cout << "timestamp : " << q.first << ", quote: " << q.second << std::endl;
		}
	}

	TEST_F(DatabaseOTLTest, PricingDataDB_selectOptionFutureQuotes)
	{
		auto quotes = PricingDataDB::instance().selectOptionFutureQuotes("2016-02-18", "CO");
		for (auto& q : quotes)
		{
			std::cout << "timestamp : " << q.first << ", quote: " << q.second << std::endl;
		}
	}

} //namespace testing
} //namespace MarketData