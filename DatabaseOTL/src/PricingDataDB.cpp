#include "PricingDataDB.h"

#include "UtilityFunctions.h"
#include <Utilities\include\DateConversion.h>
#include <Utilities\include\StringUtilities.h>
#include <Instruments\include\UtilityFunctions.h>

#include <sstream>

/*private constructor*/
PricingDataDB::PricingDataDB()
{
	otl_connect::otl_initialize();
	db.rlogon("SERVER=localhost;PORT=3306;DATABASE=pricing_data;USER=pricing.engine;PASSWORD=pwd");
};

/*destructor*/
PricingDataDB::~PricingDataDB()
{
	db.logoff();
};

/*instance*/
PricingDataDB& PricingDataDB::instance()
{
	static PricingDataDB instance_;

	return instance_;
}

/*select future quotes*/
std::vector<std::pair<std::string, FutureQuote>> PricingDataDB::selectFutureQuotes(std::string date, std::string underlying)
{
	//call to stored procedure
	otl_stream otl(
		100,
		"{call select_last_future_quotes(:date<timestamp,in>, :underlying<char(31),in>)}",
		db,
		otl_implicit_select
		);

	//input parameters of stored procedure - order is important
	otl << dbOTLUtilities::dateStringToOTL(date, "%Y-%m-%d");
	otl << underlying;

	//loop through results
	std::vector<std::pair<std::string, FutureQuote>> quotes;
	while (!otl.eof())
	{
		otl_datetime timestamp;
		std::string underlying;
		otl_datetime expiry;
		std::string currency;
		double value;

		//order is important
		otl >> timestamp >> underlying >> expiry >> currency >> value;

		//quotes
		quotes.push_back(std::make_pair(
			dbOTLUtilities::dateOTLToString(timestamp, "%Y-%m-%d %H:%M:%S"),
			FutureQuote(
			underlying,
			dbOTLUtilities::dateOTLToString(expiry, "%Y-%m-%d"),
			currency,
			value)
			));
	}

	return quotes;
}

/*insert future quotes*/
void PricingDataDB::insertFutureQuotes(const std::vector<FutureQuote>& quotes)
{
	//call to stored procedure
	otl_stream otl(
		100,
		"{call insert_future_quote(:underlying<char(31),in>, :expiry<timestamp,in>, :currency<char(4),in>, :value<double,in>)}",
		db
		);

	//loop through quotes
	for (auto& q : quotes)
	{
		std::stringstream ss;
		ss << io::iso_date(q.expiry());
		otl_datetime expiry = dbOTLUtilities::dateStringToOTL(ss.str(), "%Y-%m-%d");
		otl << q.underlying() << expiry << q.currency().code() << q.value();
	}
}

/*selec ois quotes*/
std::vector<std::pair<std::string, OISQuote>> PricingDataDB::selectOISQuotes(std::string date, std::string currency)
{
	//call to stored procedure
	otl_stream otl(
		100,
		"{call select_last_ois_rates(:date<timestamp,in>, :currency<char(4),in>)}",
		db,
		otl_implicit_select
		);

	//input parameters of stored procedure - order is important
	otl << dbOTLUtilities::dateStringToOTL(date, "%Y-%m-%d");
	otl << currency;

	//loop through results
	std::vector<std::pair<std::string, OISQuote>> quotes;
	while (!otl.eof())
	{
		otl_datetime timestamp;
		std::string currency;
		std::string tenor;
		double value;

		//order is important
		otl >> timestamp >> currency >> tenor >> value;

		//quotes
		quotes.push_back(std::make_pair(
			dbOTLUtilities::dateOTLToString(timestamp, "%Y-%m-%d %H:%M:%S"),
			OISQuote(
			currency,
			tenor,
			value)
			));
	}

	return quotes;
}

void PricingDataDB::insertOISQuotes(const std::vector<OISQuote>& quotes)
{
	//call to stored procedure
	otl_stream otl(
		100,
		"{call insert_ois_rate(:currency<char(4),in>, :tenor<char(4),in>, :value<double,in>)}",
		db
		);

	//loop through quotes
	for (auto& q : quotes)
	{
		std::stringstream tenor;
		tenor << q.tenor();
		otl << q.currency().code() << tenor.str() << q.value();
	}
}

/*select spot quote*/
std::tuple<std::string, std::string, double> PricingDataDB::selectSpotQuote(std::string date, std::string underlying)
{
	//call to stored procedure
	otl_stream otl(
		1,
		"{call select_last_spot_quotes(:date<timestamp,in>, :underlying<char(31),in>)}",
		db,
		otl_implicit_select
		);

	//input parameters of stored procedure - order is important
	otl << dbOTLUtilities::dateStringToOTL(date, "%Y-%m-%d");
	otl << underlying;

	//loop through results
	
	otl_datetime timestamp;
	std::string undl;
	std::string currency;
	double value;

	//order is important
	otl >> timestamp >> undl >> currency >> value;

	//return tuple
	return std::make_tuple(
		dbOTLUtilities::dateOTLToString(timestamp, "%Y-%m-%d %H:%M:%S"),
		currency,
		value
		);
}

/*insert spot quote*/
void PricingDataDB::insertSpotQuote(std::string underlying, std::string currency, double value)
{
	//call to stored procedure
	otl_stream otl(
		1,
		"{call insert_spot_quote(:underlying<char(31),in>, :currency<char(4),in>, :value<double,in>)}",
		db
		);

	otl << underlying << currency << value;

}

/*select option quotes*/
std::vector<std::pair<std::string, VanillaOptionQuote>> 
PricingDataDB::selectOptionQuotes(std::string date, std::string underlying)
{
	//call to stored procedure
	otl_stream otl(
		200,
		"{call select_last_option_quotes(:date<timestamp,in>, :underlying<char(31),in>)}",
		db,
		otl_implicit_select
		);

	//input parameters of stored procedure - order is important
	otl << dbOTLUtilities::dateStringToOTL(date, "%Y-%m-%d");
	otl << underlying;

	//loop through results
	std::vector<std::pair<std::string, VanillaOptionQuote>> quotes;
	while (!otl.eof())
	{
		otl_datetime timestamp;
		std::string underlying;
		std::string type;
		std::string exercise;
		double strike;
		otl_datetime expiry;
		std::string currency;
		double value;

		//order is important
		otl >> timestamp >> underlying >> type >> exercise >> strike >> expiry >> currency >> value;

		//quotes
		quotes.push_back(std::make_pair(
			dbOTLUtilities::dateOTLToString(timestamp, "%Y-%m-%d %H:%M:%S"),
			VanillaOptionQuote(
			underlying,
			type,
			exercise,
			strike,
			dbOTLUtilities::dateOTLToString(expiry, "%Y-%m-%d"),
			currency,
			value)
			));
	}

	return quotes;
}

/*insert option quotes*/
void PricingDataDB::insertOptionQuotes(std::vector<VanillaOptionQuote> quotes)
{
	//call to stored procedure
	otl_stream otl(
		200,
		"{call insert_option_quote(:underlying<char(31),in>, :type<char(5),in>, :exercise<char(9),in>, :strike<double,in>, :expiry<timestamp,in>, :currency<char(4),in>, :value<double,in>)}",
		db
		);

	//loop through quotes
	for (auto& q : quotes)
	{
		std::stringstream ss;
		ss << io::iso_date(q.expiry());
		otl_datetime expiry = dbOTLUtilities::dateStringToOTL(ss.str(), "%Y-%m-%d");

		std::stringstream exercise;
		exercise << q.exerciseType();

		std::stringstream type;
		type << q.optionType();

		otl << q.underlying() 
			<< type.str()
			<< utilities::split(exercise.str(),':').back()
			<< q.strike() 
			<< expiry 
			<< q.currency().code() 
			<< q.value();
	}
}

/*select option future quotes*/
std::vector<std::pair<std::string, VanillaOptionFutureQuote>>
PricingDataDB::selectOptionFutureQuotes(std::string date, std::string underlying)
{
	//call to stored procedure
	otl_stream otl(
		200,
		"{call select_last_option_future_quotes(:date<timestamp,in>, :underlying<char(31),in>)}",
		db,
		otl_implicit_select
		);

	//input parameters of stored procedure - order is important
	otl << dbOTLUtilities::dateStringToOTL(date, "%Y-%m-%d");
	otl << underlying;

	//loop through results
	std::vector<std::pair<std::string, VanillaOptionFutureQuote>> quotes;
	while (!otl.eof())
	{
		otl_datetime timestamp;
		std::string underlying;
		std::string type;
		std::string exercise;
		double strike;
		otl_datetime expiry;
		otl_datetime futureExpiry;
		std::string currency;
		double value;

		//order is important
		otl >> timestamp >> underlying >> type >> exercise >> strike >> expiry >> futureExpiry >> currency >> value;

		//quotes
		quotes.push_back(std::make_pair(
			dbOTLUtilities::dateOTLToString(timestamp, "%Y-%m-%d %H:%M:%S"),
			VanillaOptionFutureQuote(
			underlying,
			type,
			exercise,
			strike,
			dbOTLUtilities::dateOTLToString(expiry, "%Y-%m-%d"),
			dbOTLUtilities::dateOTLToString(futureExpiry, "%Y-%m-%d"),
			currency,
			value)
			));
	}

	return quotes;
}

/*insert option future quotes*/
void PricingDataDB::insertOptionFutureQuotes(std::vector<VanillaOptionFutureQuote> quotes)
{
	//call to stored procedure
	otl_stream otl(
		200,
		"{call insert_option_future_quote(:underlying<char(31),in>, :type<char(5),in>, :exercise<char(9),in>, :strike<double,in>, :expiry<timestamp,in>, :future_expiry<timestamp,in>, :currency<char(4),in>, :value<double,in>)}",
		db
		);

	//loop through quotes
	for (auto& q : quotes)
	{
		std::stringstream sse;
		sse << io::iso_date(q.expiry());
		otl_datetime expiry = dbOTLUtilities::dateStringToOTL(sse.str(), "%Y-%m-%d");

		std::stringstream ssfe;
		ssfe << io::iso_date(q.futureExpiry());
		otl_datetime future_expiry = dbOTLUtilities::dateStringToOTL(ssfe.str(), "%Y-%m-%d");

		std::stringstream exercise;
		exercise << q.exerciseType();

		std::stringstream type;
		type << q.optionType();

		otl << q.underlying()
			<< type.str()
			<< utilities::split(exercise.str(), ':').back()
			<< q.strike()
			<< expiry
			<< future_expiry
			<< q.currency().code()
			<< q.value();
	}
}