#include "UtilityFunctions.h"

#include <Utilities\include\DateConversion.h>

namespace dbOTLUtilities
{
	/*stl to otl*/
	otl_datetime dateSTLToOTL(std::tm date)
	{
		return otl_datetime(
			date.tm_year + 1900,
			date.tm_mon + 1,
			date.tm_mday,
			date.tm_hour,
			date.tm_min,
			date.tm_sec);
	}

	/*otl to stl*/
	std::tm dateOTLToSTL(otl_datetime date)
	{
		std::tm tm = {};
		tm.tm_year = date.year - 1900;
		tm.tm_mon = date.month - 1;
		tm.tm_mday = date.day;
		tm.tm_hour = date.hour;
		tm.tm_min = date.minute;
		tm.tm_sec = date.second;
		return tm;
	}

	/*string to otl*/
	otl_datetime dateStringToOTL(std::string date, std::string format)
	{
		return dateSTLToOTL(utilities::dateStringToTM(date, format));
	}

	/*otl to string*/
	std::string dateOTLToString(otl_datetime date, std::string format)
	{
		return utilities::dateTMToString(dateOTLToSTL(date), format);
	}
}