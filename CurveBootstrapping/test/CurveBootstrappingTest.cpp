#include <CurveBootstrapping\include\OISCurve.h>
#include <MarKetQuote\include\OISQuote.h>
#include <MarKetQuote\include\UtilityFunctions.h>
#include <CurveBootstrapping\include\OvernightIndexFactory.h>
#include <CurveBootstrapping\include\OISBootstrap.h>

#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::Le;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <vector>
#include <boost\shared_ptr.hpp>
#include <ql\errors.hpp>
#include <boost\foreach.hpp>
#include <boost\range\irange.hpp>
#include <ql\time\date.hpp>
#include <ql\time\daycounters\actual360.hpp>
#include <ql\instruments\overnightindexedswap.hpp>
#include <ql\instruments\makeois.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\currencies\america.hpp>
#include <ql\currencies\europe.hpp>
#include <ql\indexes\ibor\sonia.hpp>
#include <ql\cashflow.hpp>

namespace CurveBootstrapping
{
namespace testing
{
    class CurveBootstrappingTest : public Test
    {
    protected:
        CurveBootstrappingTest(){}
        ~CurveBootstrappingTest(){}
		
		virtual void SetUp(){
			std::string currency = "EUR";
			std::vector<std::string> tenors = { "1W", "2W", "3W", "1M", "2M", "3M", "4M", "5M", "6M", "7M", "8M", "9M", "10M",
				"11M", "1Y", "15M", "18M", "21M", "2Y", "3Y", "4Y", "5Y", "6Y", "7Y", "8Y", "9Y", "10Y", "11Y", "12Y",
				"15Y", "20Y", "25Y", "30Y" };
			std::vector<Real> values = { -0.00131, -0.00133, -0.001318, -0.00133, -0.00133, -0.00135, -0.00135, -0.00144,
				-0.0014, -0.00142, -0.00144, -0.00144, -0.00146, -0.00147, -0.00148, -0.001497, -0.00147, -0.001405, -0.0014,
				-0.00070, 0.00014, 0.00127, 0.00263, 0.00399, 0.00533, 0.00658, 0.00774, 0.00875, 0.00965, 0.01167, 0.01344,
			 0.01403, 0.01431};
			QL_REQUIRE(tenors.size() == values.size(), "tenors is not the same length as values.");
			for (auto i : boost::irange(0, (int)tenors.size()))
			{
				_quotes.push_back(boost::shared_ptr<Quote>(new OISQuote(currency, tenors[i], values[i])));
				_oiss.push_back(boost::shared_ptr<OvernightIndexedSwap>(
					MakeOIS(
					marketQuoteUtilities::fromStringToPeriod(tenors[i]),
					boost::shared_ptr<OvernightIndex>(new Eonia(discountingTermStructure)),
					values[i])
					.withDiscountingTermStructure(discountingTermStructure)));
			}
		}
        virtual void TearDown(){}

		std::vector<boost::shared_ptr<Quote>> _quotes;
		std::vector<boost::shared_ptr<OvernightIndexedSwap>> _oiss;
		RelinkableHandle<YieldTermStructure> discountingTermStructure;
    };

	TEST_F(CurveBootstrappingTest, OvernightIndexFactory_Functionality)
	{
		boost::shared_ptr<OvernightIndex> eonia(OvernightIndexFactory::instance().create(EURCurrency()));
		boost::shared_ptr<OvernightIndex> sonia(OvernightIndexFactory::instance().create(USDCurrency()));
		EXPECT_THAT(eonia->name(), Eq("EoniaON Actual/360"));
		EXPECT_THAT(sonia->name(), Eq("SoniaON Actual/365 (Fixed)"));
		ASSERT_THROW(OvernightIndexFactory::instance().create(PENCurrency()), std::exception);
	}

	TEST_F(CurveBootstrappingTest, OISBootstrap_Constructor)
	{
		ASSERT_THROW((OISCurve<ZeroYield, LogLinear, OISBootstrap>(2, TARGET(), _quotes, Actual360(), 1.0e-12)),
			std::exception);
	}

	TEST_F(CurveBootstrappingTest, OISCurve_BootstrapQuality)
	{
		boost::shared_ptr<YieldTermStructure> oisCurve( new
			OISCurve<Discount, LogLinear, OISBootstrap>(2, TARGET(), _quotes, Actual360(), 1.0e-12));
		discountingTermStructure.linkTo(oisCurve);
		Real tolerance = 1.0e-12;
		for (auto i : boost::irange(0, (int)_oiss.size()))
		{
			EXPECT_THAT(std::abs(_quotes[i]->value() - _oiss[i]->fairRate()), Le(tolerance));
		}
		//test updateQuotes
		std::vector<OISQuote> newQuote;
		newQuote.push_back(OISQuote("EUR","30Y",0.02)); //previous 0.01431
		boost::dynamic_pointer_cast<OISCurve<Discount, LogLinear, OISBootstrap>>(oisCurve)->updateQuotes(newQuote);
		EXPECT_THAT(std::abs(_quotes.back()->value() - _oiss.back()->fairRate()), Le(tolerance));
    }

} //namespace testing
} //namespace MarketData
