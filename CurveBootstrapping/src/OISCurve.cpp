#include "OISCurve.h"
#include "OvernightIndexFactory.h"
#include <ql\errors.hpp>
#include <algorithm>
#include <boost\foreach.hpp>
#include <ql\termstructures\yield\oisratehelper.hpp>
#include <MarKetQuote\include\OISQuote.h>
#include <ql\handle.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <boost\pointer_cast.hpp>

#ifndef OIS_CURVE_IMPL
#define OIS_CURVE_IMPL

/*constructor*/
template <class T, class I, template <class> class B>
OISCurve<T, I, B>::OISCurve(
	Natural settlementDays,
	const Calendar& calendar,
	const std::vector<boost::shared_ptr<Quote>>& quotes,
	const DayCounter& dayCounter,
	Real accuracy,
	const I& i,
	const B<PiecewiseYieldCurve<T, I, B>>& bootstrap)
	: PiecewiseYieldCurve < T, I, B >(
	settlementDays,
	calendar,
	constructRateHelpers(quotes),
	dayCounter, 
	accuracy,
	i,
	bootstrap), quotes_(quotes)
{
	for (auto quote : quotes_)
	{
		this->mappedQuotes_.insert(
			std::make_pair(boost::dynamic_pointer_cast<OISQuote>(quote)->tenor(),quote)
			);
	}
}

/*updates the quotes*/
template <class T, class I, template <class> class B>
void OISCurve<T, I, B>::updateQuotes(const std::vector<OISQuote>& newQuotes)
{
	Currency currency = newQuotes[0].currency();
	QL_REQUIRE(std::all_of(newQuotes.begin(), newQuotes.end(), [&currency](OISQuote quote){return quote.currency() == currency; }),
		"Not all OISQuotes have the same currency!");

	for (auto oisquote : newQuotes)
	{
		QL_REQUIRE( mappedQuotes_.find(oisquote.tenor()) != mappedQuotes_.end(), 
			"Maturity " << oisquote.tenor() << " not present in original bootstrap!");
		boost::dynamic_pointer_cast<OISQuote>(mappedQuotes_[oisquote.tenor()])->setValue(oisquote.value());
	}
}

/*friend function to be able to chain base class constructor.*/
std::vector<boost::shared_ptr<RateHelper>> constructRateHelpers(const std::vector<boost::shared_ptr<Quote>>& quotes)
{
	
	QL_REQUIRE(std::all_of(quotes.begin(), quotes.end(), [](boost::shared_ptr<Quote> quote){ return boost::dynamic_pointer_cast<OISQuote>(quote) != 0; }),
		"All quotes must be OISQuotes!");
	Currency currency = boost::dynamic_pointer_cast<OISQuote>(quotes[0])->currency();
	QL_REQUIRE(std::all_of(quotes.begin(), quotes.end(), [&currency](boost::shared_ptr<Quote> quote){return boost::dynamic_pointer_cast<OISQuote>(quote)->currency() == currency; }),
		"Not all OISQuotes have the same currency!");
	
	std::vector<boost::shared_ptr<RateHelper>> rateHelpers;
	BOOST_FOREACH(boost::shared_ptr<Quote> quote, quotes)
	{
		rateHelpers.push_back(boost::shared_ptr<RateHelper>(new OISRateHelper(
			2, //settlement days TODO: extract it from the settlement day of the quotes - maybe not worth it
			boost::dynamic_pointer_cast<OISQuote>(quote)->tenor(),
			Handle<Quote>(quote),
			boost::shared_ptr<OvernightIndex>(
			OvernightIndexFactory::instance().create(boost::dynamic_pointer_cast<OISQuote>(quote)->currency())
			))));
	}

	return rateHelpers;
}

#endif