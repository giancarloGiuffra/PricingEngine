#include "OvernightIndexFactory.h"
#include <ql\errors.hpp>
#include <ql\currencies\europe.hpp>
#include <ql\currencies\america.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <ql\indexes\ibor\sonia.hpp>

/*private constructor*/
OvernightIndexFactory::OvernightIndexFactory()
{
	this->registerOI(EURCurrency(), &OvernightIndexTemplate<Eonia>::create);
	this->registerOI(USDCurrency(), &OvernightIndexTemplate<Sonia>::create);
}

/*only method to access the instance*/
OvernightIndexFactory& OvernightIndexFactory::instance()
{
	static OvernightIndexFactory instance_; //static variable declared in a function's scope is intialized only once and
											//retains its value between function calls.
	return instance_;
}

/*returns the OvernightIndex constructor*/
OvernightIndex* OvernightIndexFactory::create(Currency ccy)
{
	QL_REQUIRE(ctorList_.find(ccy) != ctorList_.end(), "Currency " << ccy << "is not registered in this factory!");
	return ctorList_[ccy]();
}

/*registers the OvernightIndex Constructor*/
void OvernightIndexFactory::registerOI(Currency ccy, Constructor ctor)
{
	ctorList_.insert(std::pair<Currency,Constructor>(ccy,ctor));
}

/*destructor*/
OvernightIndexFactory::~OvernightIndexFactory()
{
	this->ctorList_.clear();
}