#include "OISBootstrap.h"
#include <ql\math\interpolations\linearinterpolation.hpp>
#include <ql\termstructures\yield\oisratehelper.hpp>
#include <ql\cashflow.hpp>
#include <algorithm>
#include <numeric>
#include <ql/utilities/dataformatters.hpp>
#include <type_traits>
#include <ql\termstructures\yield\bootstraptraits.hpp>

#ifndef OIS_BOOTSTRAP_IMPL
#define OIS_BOOTSTRAP_IMPL

/*constructor*/
template <class Curve>
OISBootstrap<Curve>::OISBootstrap()
	: ts_(0), initialized_(false), validCurve_(false)
{
	QL_REQUIRE( (std::is_same<Discount,Traits>()), "Only Trait Discount allowed!");
}

/*initializes ts, gets the number of helpers(instruments) to be used and registers ts with each one of them*/
template <class Curve>
void OISBootstrap<Curve>::setup(Curve* ts) {

	ts_ = ts;
	n_ = ts_->instruments_.size();
	QL_REQUIRE(n_ > 0, "no bootstrap helpers given")
		for (Size j = 0; j<n_; ++j)
			ts_->registerWith(ts_->instruments_[j]);

	// do not initialize yet: instruments could be invalid here
	// but valid later when bootstrapping is actually required
}

/*sorts the helpers and controls there are enough for the interpolation, initializes dates and times (from the helpers),
and if the curve is not valid it sets the bootstrapped values (data_ from InterpolatedCurve) to default values.*/
template <class Curve>
void OISBootstrap<Curve>::initialize() const {
	// ensure helpers are sorted
	std::sort(ts_->instruments_.begin(), ts_->instruments_.end(),
		detail::BootstrapHelperSorter());

	// skip expired helpers
	Date firstDate = Traits::initialDate(ts_);
	QL_REQUIRE(ts_->instruments_[n_ - 1]->latestDate()>firstDate,
		"all instruments expired");
	firstAliveHelper_ = 0;
	while (ts_->instruments_[firstAliveHelper_]->latestDate() <= firstDate)
		++firstAliveHelper_;
	alive_ = n_ - firstAliveHelper_;
	QL_REQUIRE(alive_ >= Interpolator::requiredPoints - 1,
		"not enough alive instruments: " << alive_ <<
		" provided, " << Interpolator::requiredPoints - 1 <<
		" required");

	// calculate dates and times
	std::vector<Date>& dates = ts_->dates_;
	std::vector<Time>& times = ts_->times_;
	dates.resize(alive_ + 1);
	times.resize(alive_ + 1);
	errors_.resize(alive_ + 1);
	dates[0] = firstDate;
	times[0] = ts_->timeFromReference(dates[0]);
	// pillar counter: i
	// helper counter: j
	for (Size i = 1, j = firstAliveHelper_; j<n_; ++i, ++j) {
		const boost::shared_ptr<typename Traits::helper>& helper =
			ts_->instruments_[j];
		dates[i] = helper->latestDate();
		times[i] = ts_->timeFromReference(dates[i]);
		// check for duplicated maturity
		QL_REQUIRE(dates[i - 1] != dates[i],
			"more than one instrument with maturity " << dates[i]);
		errors_[i] = boost::shared_ptr<BootstrapError<Curve> >(new
			BootstrapError<Curve>(ts_, helper, i));
	}

	// set initial guess only if the current curve cannot be used as guess
	if (!validCurve_ || ts_->data_.size() != alive_ + 1) {
		// ts_->data_[0] is the only relevant item,
		// but reasonable numbers might be needed for the whole data vector
		// because, e.g., of interpolation's early checks
		ts_->data_ = std::vector<Real>(alive_ + 1, Traits::initialValue(ts_));
		previousData_.resize(alive_ + 1);
	}
	initialized_ = true;
}

/*calls initialize if required, controls helpers have valid quotes, and sets term structure of helper to the one being
bootstrapped*/
template <class Curve>
void OISBootstrap<Curve>::setupHelpers() const
{
	// we might have to call initialize even if the curve is initialized
	// and not moving, just because helpers might be date relative and change
	// with evaluation date change.
	// anyway it makes little sense to use date relative helpers with a
	// non-moving curve if the evaluation date changes
	if (!initialized_ || ts_->moving_)
		initialize();

	// setup helpers
	for (Size j = firstAliveHelper_; j<n_; ++j) {
		const boost::shared_ptr<typename Traits::helper>& helper =
			ts_->instruments_[j];
		// check for valid quote
		QL_REQUIRE(helper->quote()->isValid(),
			io::ordinal(j + 1) << " instrument (maturity: " <<
			helper->latestDate() << ") has an invalid quote");
		// don't try this at home!
		// This call creates helpers, and removes "const".
		// There is a significant interaction with observability.
		helper->setTermStructure(const_cast<Curve*>(ts_));
	}
}

/*destructor*/
template <class Curve>
OISBootstrap<Curve>::~OISBootstrap()
{}

/*bootsrapping algorithm*/
template <class Curve>
void OISBootstrap<Curve>::calculate() const
{
	// setup helpers: some initialization + control of valid quotes 
	setupHelpers();

	const std::vector<Time>& times = ts_->times_;
	std::vector<Real>& data = ts_->data_;
	Real accuracy = ts_->accuracy_;

	Size maxIterations = Traits::maxIterations() - 1;

	// there might be a valid curve state to use as guess
	bool validData = validCurve_;

	for (Size iteration = 0;; ++iteration) {
		previousData_ = ts_->data_;

		//i for data
		//j for helpers
		for (Size i = 1, j = firstAliveHelper_; i <= alive_; ++i, ++j) { // pillar loop
			const boost::shared_ptr<typename Traits::helper>& helper =
				ts_->instruments_[j];
			if (times[i] <= 1.1) //there is no counting convention that gives 1Y > 1.1
				data[i] = 1.0 / (1.0 + times[i] * helper->quote()->value()); //formula
			else{

				//extend interpolation for maturity > 1Y
				if (!validData) {
					try { // extend interpolation a point at a time
						// excluding the pillar to be boostrapped
						ts_->interpolation_ = ts_->interpolator_.interpolate(
							times.begin(), times.begin() + i, data.begin());
					}
					catch (...) {
						if (!Interpolator::global)
							throw; // no chance to fix it in a later iteration

						// otherwise use Linear while the target
						// interpolation is not usable yet
						ts_->interpolation_ = Linear().interpolate(
							times.begin(), times.begin() + i, data.begin());
					}
					ts_->interpolation_.update();
				} // if !validData

				//get schedule of fixed leg (times)
				std::vector<Time> timesToInterpolate;
				for (auto cashflow : boost::dynamic_pointer_cast<OISRateHelper>(helper)->swap()->fixedLeg())
					timesToInterpolate.push_back(ts_->timeFromReference(cashflow->date()));
				//get year fraction of accrual period
				std::vector<Time> deltas;
				deltas.push_back(timesToInterpolate[0]);
				for (int k = 1; k < timesToInterpolate.size(); ++k)
					deltas.push_back(timesToInterpolate[k] - timesToInterpolate[k - 1]);
				//calculate discount
				if (timesToInterpolate.rbegin()[1] <= times[i - 1])//points to interpolate don't depend on current pillar
				{
					//eliminate last point in fixed leg schedule
					Time deltasLast = deltas.back();
					Size requiredNumber = timesToInterpolate.size() - 1;
					timesToInterpolate.resize(requiredNumber);
					deltas.resize(requiredNumber);
					//interpolate discounts 
					std::vector<Real> discounts(requiredNumber);
					std::transform(timesToInterpolate.begin(), timesToInterpolate.end(), discounts.begin(),
						[this](Time time){return this->ts_->interpolation_(time); });
					//formula
					data[i] = (1.0 - helper->quote()->value()*std::inner_product(deltas.begin(), deltas.end(), discounts.begin(), 0.0))
						/ (1.0 + deltasLast*helper->quote()->value());
				}
				else //points to interpolate depend on current pillar --> solver
				{
					// bracket root and calculate guess
					Real min = Traits::minValueAfter(i, ts_, validData,
						firstAliveHelper_);
					Real max = Traits::maxValueAfter(i, ts_, validData,
						firstAliveHelper_);
					Real guess = Traits::guess(i, ts_, validData,
						firstAliveHelper_);
					// adjust guess if needed
					if (guess >= max)
						guess = max - (max - min) / 5.0;
					else if (guess <= min)
						guess = min + (max - min) / 5.0;
					
					//extend interpolation to include current pillar
					if (!validData) {
						try { // extend interpolation a point at a time
							// including the pillar to be boostrapped
							ts_->interpolation_ = ts_->interpolator_.interpolate(
								times.begin(), times.begin() + i + 1, data.begin());
						}
						catch (...) {
							if (!Interpolator::global)
								throw; // no chance to fix it in a later iteration

							// otherwise use Linear while the target
							// interpolation is not usable yet
							ts_->interpolation_ = Linear().interpolate(
								times.begin(), times.begin() + i + 1, data.begin());
						}
						ts_->interpolation_.update();
					} // if !validData
					
					//calculation
					try {
						if (validData)
							solver_.solve(*errors_[i], accuracy, guess, min, max);
						else
							firstSolver_.solve(*errors_[i], accuracy, guess, min, max);
					}
					catch (std::exception &e) {
						// the previous curve state could have been a bad guess
						// let's restart without using it
						if (validCurve_) {
							validCurve_ = validData = false;
							continue;
						}
						QL_FAIL(io::ordinal(iteration + 1) << " iteration: failed "
							"at " << io::ordinal(i) << " alive instrument, "
							"maturity " << errors_[i]->helper()->latestDate() <<
							", reference date " << ts_->dates_[0] <<
							": " << e.what());
					}
				}
			}
		}

		//include last pillar in interpolation - necessary if last gap between maturities is not greater than 1Y
		ts_->interpolation_ = ts_->interpolator_.interpolate(
			times.begin(), times.end(), data.begin());
		
		if (!Interpolator::global)
			break;     // no need for convergence loop

		// exit condition
		Real change = std::fabs(data[1] - previousData_[1]);
		for (Size i = 2; i <= alive_; ++i)
			change = std::max(change, std::fabs(data[i] - previousData_[i]));
		if (change <= accuracy)  // convergence reached
			break;

		QL_REQUIRE(iteration<maxIterations,
			"convergence not reached after " << iteration <<
			" iterations; last improvement " << change <<
			", required accuracy " << accuracy);

		validData = true;
	}
	validCurve_ = true;
}

#endif // !OIS_BOOTSTRAP_IMPL