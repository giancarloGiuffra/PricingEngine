#include "BaseBootstrap.h"

#ifndef BASE_BOOTSTRAP_IMPL
#define BASE_BOOTSTRAP_IMPL

/*constructor*/
template <class Curve>
BaseBootstrap<Curve>::BaseBootstrap()
	: ts_(0), initialized_(false), validCurve_(false)
{}

/*initializes ts, gets the number of helpers(instruments) to be used and registers ts with each one of them*/
template <class Curve>
void BaseBootstrap<Curve>::setup(Curve* ts) {

	ts_ = ts;
	n_ = ts_->instruments_.size();
	QL_REQUIRE(n_ > 0, "no bootstrap helpers given")
		for (Size j = 0; j<n_; ++j)
			ts_->registerWith(ts_->instruments_[j]);

	// do not initialize yet: instruments could be invalid here
	// but valid later when bootstrapping is actually required
}

/*sorts the helpers and controls there are enough for the interpolation, initializes dates and times (from the helpers),
and if the curve is not valid it sets the bootstrapped values (data_ from InterpolatedCurve) to default values.*/
template <class Curve>
void BaseBootstrap<Curve>::initialize() const {
	// ensure helpers are sorted
	std::sort(ts_->instruments_.begin(), ts_->instruments_.end(),
		detail::BootstrapHelperSorter());

	// skip expired helpers
	Date firstDate = Traits::initialDate(ts_);
	QL_REQUIRE(ts_->instruments_[n_ - 1]->latestDate()>firstDate,
		"all instruments expired");
	firstAliveHelper_ = 0;
	while (ts_->instruments_[firstAliveHelper_]->latestDate() <= firstDate)
		++firstAliveHelper_;
	alive_ = n_ - firstAliveHelper_;
	QL_REQUIRE(alive_ >= Interpolator::requiredPoints - 1,
		"not enough alive instruments: " << alive_ <<
		" provided, " << Interpolator::requiredPoints - 1 <<
		" required");

	// calculate dates and times
	std::vector<Date>& dates = ts_->dates_;
	std::vector<Time>& times = ts_->times_;
	dates.resize(alive_ + 1);
	times.resize(alive_ + 1);
	dates[0] = firstDate;
	times[0] = ts_->timeFromReference(dates[0]);
	// pillar counter: i
	// helper counter: j
	for (Size i = 1, j = firstAliveHelper_; j<n_; ++i, ++j) {
		const boost::shared_ptr<typename Traits::helper>& helper =
			ts_->instruments_[j];
		dates[i] = helper->latestDate();
		times[i] = ts_->timeFromReference(dates[i]);
		// check for duplicated maturity
		QL_REQUIRE(dates[i - 1] != dates[i],
			"more than one instrument with maturity " << dates[i]);
	}

	// set initial guess only if the current curve cannot be used as guess
	if (!validCurve_ || ts_->data_.size() != alive_ + 1) {
		// ts_->data_[0] is the only relevant item,
		// but reasonable numbers might be needed for the whole data vector
		// because, e.g., of interpolation's early checks
		ts_->data_ = std::vector<Real>(alive_ + 1, Traits::initialValue(ts_));
		previousData_.resize(alive_ + 1);
	}
	initialized_ = true;
}

/*calls initialize if required, controls helpers have valid quotes, and sets term structure of helper to the one being
bootstrapped*/
template <class Curve>
void BaseBootstrap<Curve>::setupHelpers() const
{
	// we might have to call initialize even if the curve is initialized
	// and not moving, just because helpers might be date relative and change
	// with evaluation date change.
	// anyway it makes little sense to use date relative helpers with a
	// non-moving curve if the evaluation date changes
	if (!initialized_ || ts_->moving_)
		initialize();

	// setup helpers
	for (Size j = firstAliveHelper_; j<n_; ++j) {
		const boost::shared_ptr<typename Traits::helper>& helper =
			ts_->instruments_[j];
		// check for valid quote
		QL_REQUIRE(helper->quote()->isValid(),
			io::ordinal(j + 1) << " instrument (maturity: " <<
			helper->latestDate() << ") has an invalid quote");
		// don't try this at home!
		// This call creates helpers, and removes "const".
		// There is a significant interaction with observability.
		helper->setTermStructure(const_cast<Curve*>(ts_));
	}
}

/*destructor*/
template <class Curve>
BaseBootstrap<Curve>::~BaseBootstrap()
{}

#endif // !BASE_BOOTSTRAP_IMPL