#ifndef BASE_BOOTSTRAP_H
#define BASE_BOOTSTRAP_H

#include <ql/termstructures/bootstraperror.hpp>
#include <ql/math/solvers1d/finitedifferencenewtonsafe.hpp>
#include <ql/math/solvers1d/brent.hpp>
#include <ql/termstructures/bootstraphelper.hpp>
#include <vector>

using namespace QuantLib;

/*! \brief Abstract base class for a piecewise-term-structure bootsrapper.

This class is basically a copy of IterativeBootstrap. It leaves the actual implementation of the bootstrap algorithm
abstract.

## Currently (Sept 2015) it's not possible to use this as base class for bootstrapping algorithms since QuantLib::PiecewiseYieldCurve uses friendship to make members available to the bootstrap class.
*/
template<class Curve>
class BaseBootstrap
{
protected:
	typedef typename Curve::traits_type Traits;
	typedef typename Curve::interpolator_type Interpolator;

public:
	BaseBootstrap();
	void setup(Curve* ts);
	virtual void calculate() const = 0;
	virtual ~BaseBootstrap();

protected:
	void initialize() const;
	void setupHelpers() const;
	Curve* ts_;
	Size n_;
	Brent firstSolver_;
	FiniteDifferenceNewtonSafe solver_;
	mutable bool initialized_, validCurve_;
	mutable Size firstAliveHelper_, alive_;
	mutable std::vector<Real> previousData_;
	mutable std::vector<boost::shared_ptr<BootstrapError<Curve> > > errors_;
};

#include <CurveBootstrapping\src\BaseBootstrap.cpp>

#endif // !BASE_BOOTSTRAP_H
