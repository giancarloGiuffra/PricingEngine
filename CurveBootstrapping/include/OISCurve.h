#ifndef OIS_CURVE_H
#define OIS_CURVE_H

#include <MarketQuote\include\OISQuote.h>
#include <ql\termstructures\yield\piecewiseyieldcurve.hpp>
#include <ql\termstructures\yield\ratehelpers.hpp>
#include <ql\quote.hpp>
#include <vector>
#include <boost\shared_ptr.hpp>
#include <map>

using namespace QuantLib;

/*! \brief Class to bootstrap an OIS Curve.

\sa QuantLib::PiecewiseYieldCurve.
*/
template <class Traits, class Interpolator,
	template <class> class Bootstrap = IterativeBootstrap>
class OISCurve : public PiecewiseYieldCurve < Traits, Interpolator, Bootstrap >
{
public:
	
	/** Constructor.
	It replicates one of the constructors of QuantLib::PiecewiseYieldCurve.
	@param settlementDays days to settlement from referenceDate which is by default today's date (see QuantLib::Settings).
	@param calendar QuantLib::Calendar. 
	@param quotes are supposed to be from class OISQuote and have the same currency.
	@param dayCounter QuantLib::DayCounter.
	@param accuracy QuantLib::Real.
	@param i interpolator.
	@param bootstrap bootstrap algorithm
	@throws std::exception if quotes don't come from class OISQuote or don't have the same currency.
	@see OISQuote.
	@see QuantLib::PiecewiseYieldCurve.
	*/
	OISCurve(
		Natural settlementDays,
		const Calendar& calendar,
		const std::vector<boost::shared_ptr<Quote>>& quotes,
		const DayCounter& dayCounter,
		Real accuracy,
		const Interpolator& i = Interpolator(),
		const Bootstrap<PiecewiseYieldCurve<Traits, Interpolator, Bootstrap>>& bootstrap = Bootstrap<PiecewiseYieldCurve<Traits, Interpolator, Bootstrap>>());

	/** Method to update the OISQuotes used in the bootstrap.
	The input has to be a subset of the quotes used in construction (one cannot include new maturities for example).
	@param newQuotes std::vector<OISQuote>& the quotes to be updated.
	*/
	void updateQuotes(const std::vector<OISQuote>& newQuotes);

	/** Friend function to facilitate the construction.
	It is used to be able to chain the base class constructor.
	@param quotes are supposed to be from class OISQuote and have the same currency.
	@return std::vector<boost::shared_ptr<RateHelper>>
	@throws std::exception if quotes don't come from class OISQuote or don't have the same currency.
	@see OISQuote.
	@see QuantLib::RateHelper.
	*/
	friend std::vector<boost::shared_ptr<RateHelper>> constructRateHelpers(const std::vector<boost::shared_ptr<Quote>>& quotes);

private:
	std::vector<boost::shared_ptr<Quote>> quotes_; /**< It's used to be able to update the quotes. */
	std::map<Period, boost::shared_ptr<Quote>> mappedQuotes_; /**< It's used to easily find the quotes. (handy when updating)*/

};

#include <CurveBootstrapping\src\OISCurve.cpp>

#endif