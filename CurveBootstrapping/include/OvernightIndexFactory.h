#ifndef OVERNIGHT_INDEX_FACTORY_H
#define OVERNIGHT_INDEX_FACTORY_H

#include <ql\indexes\iborindex.hpp>
#include <ql\currency.hpp>
#include <map>

using namespace QuantLib;

namespace std
{
	/** Struct to add the less operator to QuantLib::Currency.
	It was necessary to be able to use Currency as key in std::map. This way the compiler finds the operator by default.
	It uses lexicographical order.
	*/
	template<> struct less<Currency>
	{
		bool operator()(const Currency& lhs, const Currency& rhs)
		{
			return lhs.name() < rhs.name(); //lexicographical order
		}
	};
}

/*! \brief Class template to be able to store constructors of derived classes from OvernightIndex.

Since it's not possible to access the address of a constructor it was necessary to introduce this class to be used to map
a QuantLib::Currency to an OvernightIndex Constructor.
\sa OvernightIndexFactory.
*/
template<typename Ctor>
class OvernightIndexTemplate
{
public:
	/** Method to make available the OvernightIndex Constructors.
	@return QuantLib::OvernightIndex*.
	*/
	static OvernightIndex* create()
	{
		return new Ctor();
	}
};


/*! \brief Class to instantiate Overnight Indices based on currency.

It uses the Singleton Pattern and the Factory Pattern.
\sa QuantLib::OvernightIndex.
*/
class OvernightIndexFactory
{
public:
	/** Function pointer to OvernightIndex "Constructor".
	@see OvernightIndexTemplate::create.
	*/
	typedef OvernightIndex* (*Constructor)(void);

	/** Method to access the factory instance (only access method).
	To access other public methods the user should first call OvernightIndexFactory::instance().
	@return OvernightIndexFactory& 
	*/
	static OvernightIndexFactory& instance();

	/** Method to register the OvernightIndex in the Factory.
	@param ccy QuantLib::Currency.
	@param ctor Constructor.
	@see Constructor.
	*/
	void registerOI(Currency ccy, Constructor ctor);

	/** Method to create the OvernightIndex associated to the Currency ccy.
	@param ccy QuantLib::Currency.
	@return OvernightIndex*.
	*/
	OvernightIndex* create(Currency ccy);

	/** Destructor.
	It clears the map of constructors.
	*/
	~OvernightIndexFactory();

private:
	/** Constructor.
	Private since it's a factory class.
	*/
	OvernightIndexFactory();

	/** Copy Constructor.
	delete disallows copying.
	*/
	OvernightIndexFactory(OvernightIndexFactory const&) = delete;

	/** Copy Assignment.
	delete disallows copying.
	*/
	void operator=(OvernightIndexFactory const&) = delete;

	std::map<Currency, Constructor> ctorList_; /**< stores the constructors */

};

#endif // OVERNIGHT_INDEX_FACTORY_H
