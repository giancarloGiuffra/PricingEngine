#ifndef OIS_BOOTSTRAP_H
#define OIS_BOOTSTRAP_H

#include <ql/termstructures/bootstraperror.hpp>
#include <ql/math/solvers1d/finitedifferencenewtonsafe.hpp>
#include <ql/math/solvers1d/brent.hpp>
#include <ql/termstructures/bootstraphelper.hpp>
#include <vector>
#include <boost\shared_ptr.hpp>

using namespace QuantLib;

/*! \brief Class that implements the bootstrap algorithm for the OIS Curve.

This algorithm assumes the curve is a discount curve (i.e. OISCurve<Discount,-,OISBootstrap>). It 
doesn't support the other traits.
*/
template <class Curve>
class OISBootstrap
{
public:
	/** Constructor.
	*/
	OISBootstrap();

	/** Method to assign the curve to this bootstrap class and register the curve with each provided RateHelper.
	@param ts curve to be bootstrapped.
	*/
	void setup(Curve* ts);

	/** Nethod that implements the actual bootstrapping algorithm.
	*/
	virtual void calculate() const;

	/** Destructor.
	*/
	virtual ~OISBootstrap();
	
protected:
	/** Method to initialize some variables.
	It sorts the RateHelpers and controls there are enough as required by the interpolation method. It initializes dates_, 
	times_ and errors_. It sets data_ to a default value.
	@see QuantLib::InterpolatedCurve for times_ and data_.
	@see QuantLib::InterpolatedDiscountCurve for dates_.
	*/
	void initialize() const;

	/** Method to call initialize() when required and to control all RateHelpers have valid quotes.
	*/
	void setupHelpers() const;

	Curve* ts_; /**< Curve to be bootstrapped. */
	Size n_; /**< Total number of RateHelpers. */
	Brent firstSolver_; /**< Solver used when curve hasn't been bootstrapped entirely.*/
	FiniteDifferenceNewtonSafe solver_; /**< Solver used when curve has been bootstrapped once.*/
	mutable bool initialized_; /**< Flag to register when curve has been initialized. */
	mutable bool validCurve_; /**< Flag to register when curve has been bootstrapped once. */
	mutable Size firstAliveHelper_; /**< Index of first alive RateHelper.*/
	mutable Size alive_; /**< Number of alive RateHelpers.*/
	mutable std::vector<Real> previousData_; /**<  Data at previous iteration.*/
	mutable std::vector<boost::shared_ptr<BootstrapError<Curve> > > errors_; /**< BootstrapError defines the function to solve for. */

	typedef typename Curve::traits_type Traits; /**< Discount, ZeroYield or ForwardRate.*/
	typedef typename Curve::interpolator_type Interpolator; /**< Interpolator class.*/
};

#include <CurveBootstrapping\src\OISBootstrap.cpp>

#endif // !OIS_BOOTSTRAP_H
