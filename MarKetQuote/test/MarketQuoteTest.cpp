#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::ContainerEq;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <MarKetQuote\include\OISQuote.h>
#include <MarKetQuote\include\UtilityFunctions.h>
#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarKetQuote\include\VanillaOptionFutureQuote.h>
#include <MarKetQuote\include\SyntheticFwd.h>
#include <MarKetQuote\include\FilterFwdData.h>
#include <MarKetQuote\include\FilterIfFwdsPerExpiryLessThan.h>
#include <MarKetQuote\include\FilterFwdOutliersByPercentile.h>

#include <ql\currencies\europe.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>

#include <boost\shared_ptr.hpp>

#include <sstream>

namespace MarketQuote
{
namespace testing
{
    class MarketQuoteTest : public Test
    {
    protected:
        MarketQuoteTest(){}
        ~MarketQuoteTest(){}
		
		virtual void SetUp(){}
        virtual void TearDown(){}

    };

    TEST_F(MarketQuoteTest, OISQuote_Constructor)
    {
		OISQuote quote = OISQuote("EUR", "1Y", 0.001);
		EXPECT_THAT(EURCurrency(), Eq(quote.currency()));
		EXPECT_THAT(1, Eq(quote.tenor().length()));
		EXPECT_THAT(Years, Eq(quote.tenor().units()));
		EXPECT_THAT(0.001, Eq(quote.value()));
    }

	TEST_F(MarketQuoteTest, OISQuote_CopyConstructor)
	{
		OISQuote first = OISQuote("EUR", "1Y", 0.001);
		OISQuote second = OISQuote(first);
		EXPECT_THAT(first.currency(), Eq(second.currency()));
		EXPECT_THAT(first.value(), Eq(second.value()));
		EXPECT_THAT(first.tenor(), Eq(second.tenor()));
		first.setValue(0.002);
		EXPECT_THAT(0.001, Eq(second.value()));
	}

	TEST_F(MarketQuoteTest, OISQuote_CopyAssignment)
	{
		OISQuote first = OISQuote("EUR", "1Y", 0.001);
		OISQuote second = first;
		EXPECT_THAT(first.currency(), Eq(second.currency()));
		EXPECT_THAT(first.value(), Eq(second.value()));
		EXPECT_THAT(first.tenor(), Eq(second.tenor()));
		first.setValue(0.002);
		EXPECT_THAT(0.001, Eq(second.value()));
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToTimeUnit)
	{
		EXPECT_THAT(Days, Eq(marketQuoteUtilities::fromStringToTimeUnit("D")));
		EXPECT_THAT(Weeks , Eq(marketQuoteUtilities::fromStringToTimeUnit("W")));
		EXPECT_THAT(Months, Eq(marketQuoteUtilities::fromStringToTimeUnit("M")));
		EXPECT_THAT(Years, Eq(marketQuoteUtilities::fromStringToTimeUnit("Y")));
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToTimeUnitThrowsWhenStringLongerThanOne)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToTimeUnit("DD"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToTimeUnitThrowsWhenStringNotAlphabetic)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToTimeUnit("1"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToTimeUnitThrowsWhenStringNotValid)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToTimeUnit("X"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_isNumeric)
	{
		EXPECT_THAT(true, Eq(marketQuoteUtilities::isNumeric("123456")));
		EXPECT_THAT(false, Eq(marketQuoteUtilities::isNumeric("123U56")));
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToPeriod)
	{
		Period p = Period(1, Years);
		EXPECT_THAT(p, Eq(marketQuoteUtilities::fromStringToPeriod("1Y")));
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToPeriodThrowsWhenStringShorterThanTwo)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToPeriod("Y"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToPeriodThrowsWhenPrecedingStringNotNumeric)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToPeriod("1U2Y"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToPeriodThrowsWhenLastCharacterNotValid)
	{
		ASSERT_THROW(marketQuoteUtilities::fromStringToPeriod("11"), std::exception);
		ASSERT_THROW(marketQuoteUtilities::fromStringToPeriod("1X"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToDate)
	{
		std::string date = "2016-04-15";
		std::string format = "%Y-%m-%d";
		Date dt(15, Month::Apr, 2016);
		EXPECT_THAT(dt, Eq(marketQuoteUtilities::fromStringToDate(date, format)));
		ASSERT_THROW(marketQuoteUtilities::fromStringToDate("qwerty", "%Y"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToOptionType)
	{
		std::string type = "Call";
		EXPECT_THAT(::Option::Type::Call, Eq(marketQuoteUtilities::fromStringToOptionType(type)));
		type = "Put";
		EXPECT_THAT(::Option::Type::Put, Eq(marketQuoteUtilities::fromStringToOptionType(type)));
		ASSERT_THROW(marketQuoteUtilities::fromStringToOptionType("randomstring"), std::exception);
	}

	TEST_F(MarketQuoteTest, marketQuoteUtilities_fromStringToExerciseType)
	{
		std::string type = "European";
		EXPECT_THAT(::Exercise::Type::European, Eq(marketQuoteUtilities::fromStringToExerciseType(type)));
		type = "American";
		EXPECT_THAT(::Exercise::Type::American, Eq(marketQuoteUtilities::fromStringToExerciseType(type)));
		type = "Bermudan";
		EXPECT_THAT(::Exercise::Type::Bermudan, Eq(marketQuoteUtilities::fromStringToExerciseType(type)));
		ASSERT_THROW(marketQuoteUtilities::fromStringToOptionType("randomstring"), std::exception);
	}

	TEST_F(MarketQuoteTest, VanillaOptionQuote_Functionality)
	{
		//european
		VanillaOptionQuote quote = VanillaOptionQuote("underlying", "Call", "European", 100, "2017-04-15", "EUR", 10);
		boost::shared_ptr<VanillaOption> option = quote.option();
		boost::shared_ptr<StrikedTypePayoff> payoff = 
			boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff());
		boost::shared_ptr<EuropeanExercise> exercise =
			boost::dynamic_pointer_cast<EuropeanExercise>(option->exercise());
		EXPECT_THAT(payoff->optionType(), Eq(Option::Type::Call));
		EXPECT_THAT(payoff->strike(), Eq(100));
		EXPECT_THAT(exercise->type(), Eq(Exercise::Type::European));
		EXPECT_THAT(exercise->lastDate(), Eq(Date(15, Apr, 2017)));
		//american
		quote = VanillaOptionQuote("underlying", "Call", "American", 100, "2017-04-15", "EUR", 10);
		option = quote.option();
		payoff = boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff());
		boost::shared_ptr<AmericanExercise> americanExercise =
			boost::dynamic_pointer_cast<AmericanExercise>(option->exercise());
		EXPECT_THAT(payoff->optionType(), Eq(Option::Type::Call));
		EXPECT_THAT(payoff->strike(), Eq(100));
		EXPECT_THAT(americanExercise->type(), Eq(Exercise::Type::American));
		EXPECT_THAT(americanExercise->lastDate(), Eq(Date(15, Apr, 2017)));

		EXPECT_NO_THROW(VanillaOptionQuote("underlying", payoff, exercise, EURCurrency(), 10));
	}

	TEST_F(MarketQuoteTest, VanillaOptionFutureQuote_Functionality)
	{
		//european
		VanillaOptionFutureQuote quote = 
			VanillaOptionFutureQuote("underlying", "Call", "European", 100, "2017-04-15", "2017-11-10", "EUR", 10);
		boost::shared_ptr<VanillaOptionFuture> option = quote.optionFuture();
		boost::shared_ptr<StrikedTypePayoff> payoff =
			boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff());
		boost::shared_ptr<EuropeanExercise> exercise =
			boost::dynamic_pointer_cast<EuropeanExercise>(option->exercise());
		EXPECT_THAT(payoff->optionType(), Eq(Option::Type::Call));
		EXPECT_THAT(payoff->strike(), Eq(100));
		EXPECT_THAT(exercise->type(), Eq(Exercise::Type::European));
		EXPECT_THAT(exercise->lastDate(), Eq(Date(15, Apr, 2017)));
		EXPECT_THAT(option->futureExpiry(), Eq(Date(10, Nov, 2017)));
		//american
		quote = VanillaOptionFutureQuote("underlying", "Call", "American", 100, "2017-04-15", "2017-11-10", "EUR", 10);
		option = quote.optionFuture();
		payoff = boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff());
		boost::shared_ptr<AmericanExercise> americanExercise =
			boost::dynamic_pointer_cast<AmericanExercise>(option->exercise());
		EXPECT_THAT(payoff->optionType(), Eq(Option::Type::Call));
		EXPECT_THAT(payoff->strike(), Eq(100));
		EXPECT_THAT(americanExercise->type(), Eq(Exercise::Type::American));
		EXPECT_THAT(americanExercise->lastDate(), Eq(Date(15, Apr, 2017)));
		//throws
		ASSERT_THROW(
			quote = VanillaOptionFutureQuote("underlying", "Call", "American", 100, "2017-04-15", "2016-04-15", "EUR", 10),
			std::exception);
	}

	TEST_F(MarketQuoteTest, SyntheticFwd_Functionality)
	{
		std::stringstream ss;
		ss << io::iso_date(Date::todaysDate() + Period(1, Years));
		Rate riskFreeRate = 0.01;
		Handle<YieldTermStructure> riskFreeTS(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(Date::todaysDate(),riskFreeRate,Actual365Fixed())));
		auto call = VanillaOptionQuote("fake", "call", "european", 100, ss.str(), "EUR", 10);
		auto put = VanillaOptionQuote("fake", "put", "european", 100, ss.str(), "EUR", 10);

		//tests
		ASSERT_NO_THROW(SyntheticFwd sf(call, put, riskFreeTS));
		auto sf = SyntheticFwd(call, put, riskFreeTS);
		EXPECT_THAT(sf.expiry, Eq(Date::todaysDate() + Period(1, Years)));
		EXPECT_THAT(sf.strike, Eq(100));
		EXPECT_THAT(sf.value, Eq(100));
		EXPECT_THROW(SyntheticFwd sf(put, put, riskFreeTS), std::exception);
		EXPECT_THROW(SyntheticFwd sf(call, call, riskFreeTS), std::exception);
		auto putDifferentStrike = VanillaOptionQuote("fake", "put", "european", 90, ss.str(), "EUR", 10);
		auto putDifferentExpiry = VanillaOptionQuote("fake", "put", "european", 100, "2016-01-01", "EUR", 10);
		EXPECT_THROW(SyntheticFwd sf(call, putDifferentStrike, riskFreeTS), std::exception);
		EXPECT_THROW(SyntheticFwd sf(call, putDifferentExpiry, riskFreeTS), std::exception);
	}
	
	TEST_F(MarketQuoteTest, FilterTrivialFwd_Functionality)
	{
		auto sf1 = SyntheticFwd(Date::todaysDate(), 100, 90);
		auto sf2 = SyntheticFwd(Date::todaysDate(), 110, 91);
		auto sfDifferentExpiry = SyntheticFwd(Date::todaysDate()+Period(1,Months), 100, 90);

		FilterTrivialFwd f;

		//tests
		GroupedSyntheticFwds emptyFwds(2);
		EXPECT_THROW(f.filter(emptyFwds), std::exception);
		GroupedSyntheticFwds diffExpiriesFwds(1);
		diffExpiriesFwds[0].push_back(sf1);
		diffExpiriesFwds[0].push_back(sfDifferentExpiry);
		EXPECT_THROW(f.filter(diffExpiriesFwds), std::exception);
		GroupedSyntheticFwds okFwds(1);
		okFwds[0].push_back(sf1);
		okFwds[0].push_back(sf2);
		EXPECT_NO_THROW(f.filter(okFwds));
	}

	TEST_F(MarketQuoteTest, FilterIfFwdsPerExpiryLessThan_Functionality)
	{
		auto sfToday100 = SyntheticFwd(Date::todaysDate(), 100, 90);
		auto sfToday110 = SyntheticFwd(Date::todaysDate(), 110, 91);
		auto sf1Year100 = SyntheticFwd(Date::todaysDate() + Period(1,Years), 100, 85);

		GroupedSyntheticFwds fwds(2);
		fwds[0].push_back(sfToday100);
		fwds[0].push_back(sfToday110);
		fwds[1].push_back(sf1Year100);

		GroupedSyntheticFwds manuallyFiltered(1);
		manuallyFiltered[0].push_back(sfToday100);
		manuallyFiltered[0].push_back(sfToday110);

		FilterIfFwdsPerExpiryLessThan f(2);

		//test
		EXPECT_THAT(f.filter(fwds), ContainerEq(manuallyFiltered));
	}

	TEST_F(MarketQuoteTest, FilterFwdOutliersByPercentile_Functionality)
	{
		auto sfToday100 = SyntheticFwd(Date::todaysDate(), 100, 90);
		auto sfToday110 = SyntheticFwd(Date::todaysDate(), 110, 91);
		auto sfToday90 = SyntheticFwd(Date::todaysDate(), 90, 90);
		auto sf1Year100 = SyntheticFwd(Date::todaysDate() + Period(1, Years), 100, 85);

		GroupedSyntheticFwds fwds(2);
		fwds[0].push_back(sfToday100);
		fwds[0].push_back(sfToday110);
		fwds[0].push_back(sfToday90);
		fwds[1].push_back(sf1Year100);

		GroupedSyntheticFwds manuallyFiltered(1);
		manuallyFiltered[0].push_back(sfToday100);

		FilterFwdOutliersByPercentile f(0.40);

		//test
		EXPECT_THAT(f.filter(fwds), ContainerEq(manuallyFiltered));
		std::cout << f.description() << std::endl;
	}

} //namespace testing
} //namespace MarketData