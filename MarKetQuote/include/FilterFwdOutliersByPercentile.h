#ifndef FILTER_FWD_OUTLIERS_BY_PERCENTILE_H
#define FILTER_FWD_OUTLIERS_BY_PERCENTILE_H

#include "FilterFwd.h"

/*! \brief Filters out the highest and lowest percentiles.

The given percentage is capped at 40%. Strips with less than 3 forwards are filtered out as well.
*/
class FilterFwdOutliersByPercentile : public FilterFwd
{
public:

	/** Constructor.
	The highest and lowest percentiles are filtered out. Strips with less than 3 forwards are filtered out as well.
	@param percentage pure number (e.g. 0.30 for 30%), it is capped at 40%.
	@param filter to decorate
	*/
	FilterFwdOutliersByPercentile(
		Real percentage,
		std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd()));
};

#endif // !FILTER_FWD_OUTLIERS_BY_PERCENTILE_H
