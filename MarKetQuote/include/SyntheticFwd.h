#ifndef SYNTHETIC_FWD_H
#define SYNTHETIC_FWD_H

#include <MarKetQuote\include\VanillaOptionQuote.h>

#include <ql\time\date.hpp>
#include <ql\types.hpp>
#include <ql\handle.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>

using namespace QuantLib;

/** \brief Struct to represent a Synthetic Forward.

*/
struct SyntheticFwd
{
	/** Constructor.
	@param expiry
	@param strike
	@param value
	*/
	SyntheticFwd(Date expiry, Real strike, Real value);

	/** Constructor.
	It uses the formula considering the options european.
	@param call
	@param put
	@param riskFreeRate
	@throws std::exception if options don't have the same strike and expiry, and if first is not a call or second
	is not a put
	*/
	SyntheticFwd(
		VanillaOptionQuote call,
		VanillaOptionQuote put, 
		const Handle<YieldTermStructure> riskFreeRate);

	Date expiry; /**< expiry of the forward */
	Real strike; /**< strike from put-call parity */
	Real value; /**< value of synthetic forward */

	/** operator<<
	@param os
	@param sfwd
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const SyntheticFwd& sfwd);
};

/** \relates SyntheticFwd
*/
inline bool operator==(const SyntheticFwd& lhs, const SyntheticFwd& rhs)
{
	return lhs.expiry == rhs.expiry && lhs.strike == rhs.strike && lhs.value == rhs.value;
}

/** \relates SyntheticFwd
*/
inline bool operator!=(const SyntheticFwd& lhs, const SyntheticFwd& rhs)
{
	return !(lhs==rhs);
}

#endif // !SYNTHETIC_FWD_H
