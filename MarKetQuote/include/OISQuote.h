#ifndef OIS_QUOTE_H
#define OIS_QUOTE_H

#include <ql\quotes\simplequote.hpp>
#include <ql\time\period.hpp>
#include <ql\currency.hpp>
#include <string>
#include <vector>

using namespace QuantLib;

/*! \brief Class to represent an OIS market quote.
*/
class OISQuote : public QuantLib::SimpleQuote
{
public:

	/** Constructor.
	@param currency the three letter ISO 4217 Currency Code.
	@param tenor e.g. 1Y.
	@param value the value of the OIS market quote.
	*/
	OISQuote(std::string currency, std::string tenor, Real value = Null<Real>());

	/** Copy Constructor.
	*/
	OISQuote(const OISQuote& other);

	/** Copy Assignment operator.
	It uses the copy and swap idiom.
	@see swap(OISQuote& , OISQuote& ).
	*/
	OISQuote& operator=(OISQuote other);

	/** Destructor.
	It does nothing.
	*/
	~OISQuote();

	/** Method to return a copy of the tenor.
	@return QuantLib::Period.
	*/
	Period tenor() const;

	/** Method to return a copy of the currency.
	@return QuantLib::Currency.
	*/
	Currency currency() const;

	/** Friend function to implement the copy and swap idiom.
	@param first left hand side of the assignment.
	@param second right hand side.
	*/
	friend void swap(OISQuote& first, OISQuote& second);

	/** overload operator<<
	@param os
	@param quote
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const OISQuote& quote);

private:
	Period tenor_; /**< Tenor. */
	Currency currency_; /**< Currency. */
};

#endif