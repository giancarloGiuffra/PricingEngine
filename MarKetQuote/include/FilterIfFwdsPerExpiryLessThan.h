#ifndef FILTER_IF_FWDS_PER_EXPIRY_LESS_THAN_H
#define FILTER_IF_FWDS_PER_EXPIRY_LESS_THAN_H

#include "FilterFwd.h"

using namespace QuantLib;

/*! \brief Filters out the whole strip if the number of Forwards is less than the given minimum.

*/
class FilterIfFwdsPerExpiryLessThan : public FilterFwd
{
public:

	/** Constructor.
	@param minimum number of fwds per expiry
	@param filter to decorate
	*/
	FilterIfFwdsPerExpiryLessThan(
		Natural minimum,
		std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd()));
};

#endif // !FILTER_IF_FWDS_PER_EXPIRY_LESS_THAN_H
