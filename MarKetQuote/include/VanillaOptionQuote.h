#ifndef VANILLA_OPTION_QUOTE_H
#define VANILLA_OPTION_QUOTE_H

#include <ql\quotes\simplequote.hpp>
#include <ql\exercise.hpp>
#include <ql\option.hpp>
#include <ql\time\date.hpp>
#include <ql\currency.hpp>
#include <string>
#include <memory>
#include <ql\instruments\vanillaoption.hpp>
#include <boost\shared_ptr.hpp>

using namespace QuantLib;

/*! \brief Quote for a Vanilla Option.

*/
class VanillaOptionQuote : public SimpleQuote
{
public:

	/** Constructor.
	@param underlying
	@param optionType
	@param exerciseType
	@param strike
	@param expiry
	@param value
	@param expiryFormat default is e.g. 2016-04-15
	@see std::get_time for convention on date formats http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	VanillaOptionQuote(
		std::string underlying,
		std::string optionType,
		std::string exerciseType,
		Real strike,
		std::string expiry,
		std::string currency,
		Real value = Null<Real>(),
		std::string expiryFormat = "%Y-%m-%d");

	/** Constructor.
	It is used only as a container for a price.
	@param underlying
	@param payoff
	@param exercise
	@param currency
	@param value
	*/
	VanillaOptionQuote(
		std::string underlying,
		boost::shared_ptr<StrikedTypePayoff> payoff,
		boost::shared_ptr<Exercise> exercise,
		Currency currency,
		Real value);

	/** Method to build a VanillaOption.
	@return boost::shared_ptr<VanillaOption>
	*/
	boost::shared_ptr<VanillaOption> option() const;

	/** Inspector for underlying.
	@return std::string
	*/
	std::string underlying() const;

	/** Inspector for optionType.
	@return QuantLib::Option::Type
	*/
	::Option::Type optionType() const;

	/** Inspector for exerciseType.
	@return QuantLib::Exercise::Type
	*/
	::Exercise::Type exerciseType() const;

	/** Inspector for strike.
	@return Real
	*/
	Real strike() const;

	/** Inspector for expiry.
	@return Date
	*/
	Date expiry() const;

	/** Inspector for currency.
	@return Currency
	*/
	Currency currency() const;

	/** overload operator<<
	@param os
	@param quote
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const VanillaOptionQuote& quote);

protected:

	std::string underlying_; /**< underlying security*/
	::Option::Type optionType_; /**< call or put */
	::Exercise::Type exerciseType_; /**< american, bermudan or european */
	Real strike_; /**< option strike*/
	Date expiry_; /**< option expiry*/
	Currency currency_; /**< currency of the quote*/
};

/** \relates VanillaOptionQuote
*/
inline bool operator==(const VanillaOptionQuote& lhs, const VanillaOptionQuote& rhs)
{
	return lhs.underlying() == rhs.underlying() &&
		lhs.optionType() == rhs.optionType() &&
		lhs.exerciseType() == rhs.exerciseType() &&
		lhs.strike() == rhs.strike() &&
		lhs.expiry() == rhs.expiry() &&
		lhs.currency() == rhs.currency();
}

/** \relates VanillaOptionQuote
*/
inline bool operator!=(const VanillaOptionQuote& lhs, const VanillaOptionQuote& rhs)
{
	return !(lhs == rhs);
}

#endif // !VANILLA_OPTION_QUOTE_H
