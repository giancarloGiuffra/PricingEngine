#ifndef FILTER_FWD_DATA_DECORATOR_H
#define FILTER_FWD_DATA_DECORATOR_H

#include "FilterFwdData.h"

#include <memory>

/*! \brief Base decorator pattern class for FilterFwdData.

Derived classes must have an optional argument in their constructors :
std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd())
for the whole machinery to work.
*/
class FilterFwdDataDecorator : public FilterFwdData
{
public:

	//! \name FilterFwdData Interface
	//@{

	/** Filters the grouped fwds.
	@param fwds
	@return GroupedSyntheticFwds
	*/
	virtual GroupedSyntheticFwds filter(GroupedSyntheticFwds fwds);

	/** Description of the filter.
	@return std::string brief description.
	*/
	virtual std::string description();
	//@}

protected:

	/** Constructor.
	@param filter that takes care of the FilterFwdData interface implementation.
	*/
	FilterFwdDataDecorator(std::shared_ptr<FilterFwdData> filter);

	/** Method to insert description in derived classes.
	@return std::string description taking into consideration that it may be empty.
	*/
	std::string insertDescription();

	std::shared_ptr<FilterFwdData> filter_; /**< delegate */
};

#endif // !FILTER_FWD_DATA_DECORATOR_H
