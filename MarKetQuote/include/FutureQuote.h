#ifndef FUTURE_QUOTE_H
#define FUTURE_QUOTE_H

#include <ql\quotes\simplequote.hpp>
#include <ql\time\date.hpp>
#include <ql\currency.hpp>
#include <string>

using namespace QuantLib;

/*! \brief Class to represent a Future Quote.

*/
class FutureQuote : public SimpleQuote
{
public:

	/** Constructor.
	@param underlying
	@param expiry
	@param currency
	@param value
	@param expiryFormat default is e.g. 2016-04-15
	@see std::get_time for convention on date formats http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	FutureQuote(std::string underlying,
		std::string expiry,
		std::string currency,
		Real value = Null<Real>(),
		std::string expiryFormat = "%Y-%m-%d");

	/** Inspector for underlying.
	@return std::string
	*/
	std::string underlying() const;

	/** Inspector for expiry.
	@return QuantLib::Date
	*/
	Date expiry() const;

	/** Inspector for currency.
	@return QuantLib::Currency
	*/
	Currency currency() const;

	/** overload operator<<
	@param os
	@param quote
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const FutureQuote& quote);

private:

	std::string underlying_; /**< Underlying of the future contract*/
	Date expiry_; /**< Expiry of the future contract. */
	Currency currency_; /**< Currency of the quote*/
};

#endif // !FUTURE_QUOTE_H
