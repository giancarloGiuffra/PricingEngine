#ifndef UTILITY_FUNCTIONS_H
#define UTILITY_FUNCTIONS_H

#include <ql\time\timeunit.hpp>
#include <ql\time\period.hpp>
#include <ql\time\date.hpp>
#include <ql\currency.hpp>
#include <ql\exercise.hpp>
#include <ql\option.hpp>

#include <string>

#include "FutureQuote.h"

using namespace QuantLib;

/*! \brief Namespace that contains utility functions to facilitate the construction of Quotes.
*/
namespace marketQuoteUtilities
{
	/** It converts a string of one character into a QuantLib::TimeUnit.
	@param s	std::string to be converted. s.length == 1 && s must be alphabetic.
	@return QuantLib::TimeUnit.  */
	QuantLib::TimeUnit fromStringToTimeUnit(std::string s);

	/** It converts a string representing a tenor into a QuantLib::Period.
	@param tenor	std::string to be converted. tenor.length >=2 && last character must be alphabetic && remaining characters must be numeric.
	@return QuantLib::Period. */
	QuantLib::Period fromStringToPeriod(std::string tenor);

	/** It converts to an option type.
	@param type type.length >=1
	@return QuantLib::Option::Type
	*/
	::Option::Type fromStringToOptionType(std::string type);

	/** It converts to an exercise type.
	@param type type.length >=1
	@return QuantLib::Exercise::Type
	*/
	::Exercise::Type fromStringToExerciseType(std::string type);

	/** It checks if input is numeric.
	@param input	to be checked for the condition.
	@return true if all characters are numeric.
	*/
	bool isNumeric(const std::string& input);

	/** It converts a string representing a currency into a QuantLib::Currency.
	@param currency		std::string to be converted. currency.length == 3.
	@return QuantLib::Currency. */
	QuantLib::Currency fromStringToCurrency(std::string currency);

	/** It converts a string representing a date in the specified format into a QuantLib::Date.
	@param date
	@param format
	@return Date
	@throws if parsing fails
	@see std::get_time for format conventions http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	QuantLib::Date fromStringToDate(std::string date, std::string format);

}

#endif // !UTILITY_FUNCTIONS_H
