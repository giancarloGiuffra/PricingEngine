#ifndef FILTER_FWD_DATA_H
#define FILTER_FWD_DATA_H

#include "SyntheticFwd.h"

#include <vector>
#include <string>

/** \defgroup TypedefsFilterFwds Typedefs for FilterFwd interface.
Typedefs for the arguments manipulated by the FilterFwd interface.
*/

/*! \brief It represents a std::vector of Synthetic Forwards.

\ingroup TypedefsFilterFwds
*/
typedef std::vector<SyntheticFwd> SyntheticFwds;

/*! \brief It represents Synthetic Forwards grouped by expiry.

\ingroup TypedefsFilterFwds
*/
typedef std::vector<SyntheticFwds> GroupedSyntheticFwds;

/*************************************************************************************************************/

/*! \brief Interface for filters of Synthetic Forward Data.

*/
class FilterFwdData
{
public:

	/** Method to filter GroupedSyntheticFwds.
	@param fwds
	@return GroupedSyntheticFwds
	*/
	virtual GroupedSyntheticFwds filter(GroupedSyntheticFwds fwds) = 0;

	/** Method to describe the filter.
	@return std::string a brief description
	*/
	virtual std::string description() = 0;
};

/*! \brief Trivial Filter that returns the same grouped fwds.

Its filter method can be used to check that the fwds are grouped correctly by expiry.
*/
class FilterTrivialFwd : public FilterFwdData
{
public:

	/** Constructor.
	*/
	FilterTrivialFwd();

	//! \name FilterFwdData Interface
	//@{

	/** Filters the grouped fwds.
	@param fwds
	@return GroupedSyntheticFwds it simply returns the same fwds.
	@throws std::exception if fwds are not correctly grouped by expiry
	*/
	GroupedSyntheticFwds filter(GroupedSyntheticFwds fwds);

	/** Describes the filter.
	@return std::string
	*/
	std::string description();
	//@}

private:

	/** It checks fwds are correctly grouped by expiry.
	@param fwds
	@throws std::exception if they are not
	*/
	void checkCorrectGrouping(GroupedSyntheticFwds fwds);
};

#endif // !FILTER_FWD_DATA_H
