#ifndef FILTER_FWD_H
#define FILTER_FWD_H

#include "FilterFwdDataDecorator.h"

#include <functional>
#include <memory>

/*! \brief Class to facilitate creation of FilterFwd by specifying the filter by expiry.

*/
class FilterFwd : public FilterFwdDataDecorator
{
public:

	/** Constructor.
	@param filterByExpiry filter applied to each group of fwds.
	@param description
	@param filter to be decorated
	*/
	FilterFwd(
		std::function<SyntheticFwds(SyntheticFwds)> filterByExpiry,
		std::string description,
		std::shared_ptr<FilterFwdData> filter = std::shared_ptr<FilterFwdData>(new FilterTrivialFwd()));

	/** Implicit conversion to std::shared_ptr<FilterFwdData>.
	It simplifies the syntax when chaining filters.
	*/
	operator std::shared_ptr<FilterFwdData>() const;

	//! \name FilterFwdData Interface
	//@{

	/** Filters the grouped fwds.
	@param fwds
	@return GroupedSyntheticFwds
	*/
	GroupedSyntheticFwds filter(GroupedSyntheticFwds fwds);

	/** Description of the filter.
	@return std::string brief description.
	*/
	std::string description();
	//@}

protected:

	/** Implements the filter according to the passed filterByExpiry.
	If for a expiry the result is empty it is not included in the main result.
	@param fwds
	@return GroupedSyntheticFwds
	*/
	GroupedSyntheticFwds thisFilter(GroupedSyntheticFwds fwds);

	std::function<SyntheticFwds(SyntheticFwds)> filterByExpiry_; /**< filters each group of fwds */
	std::string description_; /**< description */
};

#endif // !FILTER_FWD_H
