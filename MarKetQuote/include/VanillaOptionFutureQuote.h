#ifndef VANILLA_OPTION_FUTURE_QUOTE_H
#define VANILLA_OPTION_FUTURE_QUOTE_H

#include "VanillaOptionQuote.h"
#include <Instruments\include\VanillaOptionFuture.h>

/*! \brief Quote for Vanilla Option on Future.

*/
class VanillaOptionFutureQuote : public VanillaOptionQuote
{
public:

	/** Constructor.
	@param underlying
	@param optionType
	@param exerciseType
	@param strike
	@param expiry
	@param futureExpiry
	@param value
	@param expiryFormat default is e.g. 2016-04-15, used by expiry and futureExpiry.
	@see std::get_time for convention on date formats http://en.cppreference.com/w/cpp/io/manip/get_time
	*/
	VanillaOptionFutureQuote(
		std::string underlying,
		std::string optionType,
		std::string exerciseType,
		Real strike,
		std::string expiry,
		std::string futureExpiry,
		std::string currency,
		Real value = Null<Real>(),
		std::string expiryFormat = "%Y-%m-%d");

	/** Method to build a VanillaOptionFuture.
	@return boost::shared_ptr<VanillaOptionFuture>
	*/
	boost::shared_ptr<VanillaOptionFuture> optionFuture() const;

	/** Inspector for futureExpiry.
	@return Date
	*/
	Date futureExpiry() const;

	/** overload operator<<
	@param os
	@param quote
	@return std::ostream&
	*/
	friend std::ostream& operator<<(std::ostream& os, const VanillaOptionFutureQuote& quote);

private:

	/*method from derived class, hidden because the option associated is a VanillaOptionFuture (see optionFuture()).
	Overload could have been used since VanillaOptionFuture derives from VanillaOption so the VanillaOption pointer was ok.
	I preferred this way to avoid dynamic casting*/
	boost::shared_ptr<VanillaOption> option() const;

	Date futureExpiry_; /**< expiry of the underlying future*/
};

#endif // !VANILLA_OPTION_FUTURE_QUOTE_H
