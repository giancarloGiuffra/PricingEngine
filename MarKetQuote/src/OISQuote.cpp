#include "OISQuote.h"
#include "UtilityFunctions.h"

using namespace marketQuoteUtilities;

/*constructor*/
OISQuote::OISQuote(std::string currency, std::string tenor, Real value)
	: SimpleQuote(value)
{
	this->tenor_ = fromStringToPeriod(tenor);
	this->currency_ = fromStringToCurrency(currency);
};

/*destructor*/
OISQuote::~OISQuote()
{};

/*copy constructor*/
OISQuote::OISQuote(const OISQuote& other)
	: SimpleQuote(other.value()), tenor_(other.tenor().length(), other.tenor().units()), currency_(other.currency())
{}

/* copy assignment */
OISQuote& OISQuote::operator = (OISQuote other)
{
	swap(*this, other); //other is passed by value so it's ok
	return *this;
}

/* friend swap function */ //reference http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
void swap(OISQuote& first, OISQuote& second)
{
	using std::swap; //good practice according to reference
	swap(first.tenor_, second.tenor_);
	first.setValue(second.value()); //this function doesn't have access to the private members of SimpleQuote - this works fine
	swap(first.currency_, second.currency_);
}

/*getter for tenor_*/
Period OISQuote::tenor() const
{
	return this->tenor_;
}

/*getter for currency*/
Currency OISQuote::currency() const
{
	return this->currency_;
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const OISQuote& quote)
{
	os << quote.tenor_ << " " << quote.currency_.code() << " " << quote.value();
	return os;
}