#include "FilterFwd.h"

/*constructor*/
FilterFwd::FilterFwd(
	std::function<SyntheticFwds(SyntheticFwds)> filterByExpiry,
	std::string description,
	std::shared_ptr<FilterFwdData> filter)
	: FilterFwdDataDecorator(filter), filterByExpiry_(filterByExpiry), description_(description)
{}

/*implicit conversion std::shared_ptr<FilterFwdData>*/
FilterFwd::operator std::shared_ptr<FilterFwdData>() const
{
	return std::shared_ptr<FilterFwd>(new FilterFwd(*this));
}

/*filter*/
GroupedSyntheticFwds FilterFwd::filter(GroupedSyntheticFwds fwds)
{
	return thisFilter(FilterFwdDataDecorator::filter(fwds));
}

/*description*/
std::string FilterFwd::description()
{
	return FilterFwdDataDecorator::insertDescription() + description_;
}

/*this filter*/
GroupedSyntheticFwds FilterFwd::thisFilter(GroupedSyntheticFwds fwds)
{
	GroupedSyntheticFwds filteredFwds;
	for (auto const & stripFwds : fwds)
	{
		SyntheticFwds filteredStripFwds = filterByExpiry_(stripFwds);
		if (!filteredStripFwds.empty())
			filteredFwds.push_back(filteredStripFwds);
	}

	return filteredFwds;
}