#include "SyntheticFwd.h"

#include <ql\errors.hpp>

/*constructor*/
SyntheticFwd::SyntheticFwd(Date expiry, Real strike, Real value)
	: expiry(expiry), strike(strike), value(value)
{}

/*constructor from options*/
SyntheticFwd::SyntheticFwd(
	VanillaOptionQuote call,
	VanillaOptionQuote put,
	const Handle<YieldTermStructure> riskFreeRate)
{
	QL_REQUIRE(call.optionType() == Option::Type::Call, "first option is not a call");
	QL_REQUIRE(put.optionType() == Option::Type::Put, "second option is not a put");
	QL_REQUIRE(call.expiry() == put.expiry(),
		"call expiry (" << call.expiry() << ") is not equal to put expiry (" << put.expiry() << ").");
	QL_REQUIRE(call.strike() == put.strike(),
		"call strike (" << call.strike() << ") is not equal to put strike (" << put.strike() << ").");
	
	//expiry and strike
	this->expiry = call.expiry();
	this->strike = call.strike();

	//formula
	this->value = this->strike + (call.value() - put.value()) / riskFreeRate->discount(call.expiry(), true);
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const SyntheticFwd& sfwd)
{
	os << "Synthetic Forward(" << io::iso_date(sfwd.expiry) << ", " << sfwd.strike << ", "
		<< sfwd.value << ")";
	return os;
}