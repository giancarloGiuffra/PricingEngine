#include "VanillaOptionFutureQuote.h"
#include "UtilityFunctions.h"

#include <boost\pointer_cast.hpp>
#include <ql\instruments\payoffs.hpp>
#include <ql\errors.hpp>

/*constructor*/
VanillaOptionFutureQuote::VanillaOptionFutureQuote(
	std::string underlying,
	std::string optionType,
	std::string exerciseType,
	Real strike,
	std::string expiry,
	std::string futureExpiry,
	std::string currency,
	Real value,
	std::string expiryFormat)
	: VanillaOptionQuote(underlying, optionType, exerciseType, strike, expiry, currency, value, expiryFormat)
{
	this->futureExpiry_ = marketQuoteUtilities::fromStringToDate(futureExpiry, expiryFormat);
	QL_REQUIRE(futureExpiry_ >= expiry_, 
		"future expiry " << futureExpiry_ << " must be equal or later than option expiry " << expiry_);
}

/*option future*/
boost::shared_ptr<VanillaOptionFuture> VanillaOptionFutureQuote::optionFuture() const
{
	boost::shared_ptr<VanillaOption> option = VanillaOptionQuote::option();
	return boost::shared_ptr<VanillaOptionFuture>(
		new VanillaOptionFuture(
		boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff()),
		option->exercise(),
		this->futureExpiry_
		));
}

/*future expiry*/
Date VanillaOptionFutureQuote::futureExpiry() const
{
	return this->futureExpiry_;
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const VanillaOptionFutureQuote& quote)
{
	os << quote.optionType_ << " " << quote.strike_ << " strike" << " - "
		<< quote.exerciseType_ << " ( " << quote.expiry_ << " )" << " - "
		<< quote.futureExpiry_ << " - "
		<< quote.currency_.code() << " " << quote.value();
	return os;
}