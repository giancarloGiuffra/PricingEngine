#include "VanillaOptionQuote.h"
#include "UtilityFunctions.h"

#include <Instruments\include\UtilityFunctions.h>

#include <ql\settings.hpp>

/*constructor*/
VanillaOptionQuote::VanillaOptionQuote(
	std::string underlying,
	std::string optionType,
	std::string exerciseType,
	Real strike,
	std::string expiry,
	std::string currency,
	Real value,
	std::string expiryFormat)
	: SimpleQuote(value), underlying_(underlying), strike_(strike)
{
	this->optionType_ = marketQuoteUtilities::fromStringToOptionType(optionType);
	this->exerciseType_ = marketQuoteUtilities::fromStringToExerciseType(exerciseType);
	this->currency_ = marketQuoteUtilities::fromStringToCurrency(currency);
	this->expiry_ = marketQuoteUtilities::fromStringToDate(expiry, expiryFormat);
}

/*constructor*/
VanillaOptionQuote::VanillaOptionQuote(
	std::string underlying,
	boost::shared_ptr<StrikedTypePayoff> payoff,
	boost::shared_ptr<Exercise> exercise,
	Currency currency,
	Real value)
	: SimpleQuote(value), underlying_(underlying), currency_(currency),
	optionType_(payoff->optionType()), strike_(payoff->strike()),
	exerciseType_(exercise->type()), expiry_(exercise->lastDate())
{}

/*vanilla option*/
boost::shared_ptr<VanillaOption> VanillaOptionQuote::option() const
{
	boost::shared_ptr<Exercise> exercise;
	switch (this->exerciseType_)
	{
	case Exercise::European:
		exercise = boost::shared_ptr<Exercise>(new EuropeanExercise(this->expiry_));
		break;
	case Exercise::American:
		//uses evaluation date as earliest date
		exercise = boost::shared_ptr<Exercise>(new AmericanExercise(Settings::instance().evaluationDate(), this->expiry_));
		break;
	default:
		QL_FAIL(this->exerciseType_ << " is not handled by this type of quote.");
		break;
	}
	
	return boost::shared_ptr<VanillaOption>(
		new VanillaOption(
		boost::shared_ptr<StrikedTypePayoff>( new PlainVanillaPayoff(this->optionType_, this->strike_)),
		exercise)
		);
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const VanillaOptionQuote& quote)
{
	os << quote.optionType_ << " " << quote.strike_ << " strike" << " - "
		<< quote.exerciseType_ << " ( " << quote.expiry_ << " )" << " - "
		<< quote.currency_.code() << " " << quote.value();

	return os;
}

/*underlying*/
std::string VanillaOptionQuote::underlying() const
{
	return underlying_;
}

/*option type*/
::Option::Type VanillaOptionQuote::optionType() const
{
	return optionType_;
}

/*exercise type*/
::Exercise::Type VanillaOptionQuote::exerciseType() const
{
	return exerciseType_;
}

/*strike*/
Real VanillaOptionQuote::strike() const
{
	return strike_;
}

/*expiry*/
Date VanillaOptionQuote::expiry() const
{
	return expiry_;
}

/*currency*/
Currency VanillaOptionQuote::currency() const
{
	return currency_;
}