#include "FilterIfFwdsPerExpiryLessThan.h"

/*constructor*/
FilterIfFwdsPerExpiryLessThan::FilterIfFwdsPerExpiryLessThan(
	Natural minimum,
	std::shared_ptr<FilterFwdData> filter)
	: FilterFwd(
	[minimum](SyntheticFwds fwds){
	return fwds.size() < minimum ? SyntheticFwds() : fwds;
	},
	"Filters the whole strip if the number of Forwards is less than " + std::to_string(minimum),
	filter
	)
{}