#include "FutureQuote.h"
#include "UtilityFunctions.h"

/*constructor*/
FutureQuote::FutureQuote(std::string underlying, std::string expiry, std::string currency, Real value, std::string format)
	: SimpleQuote(value), underlying_(underlying)
{
	this->currency_ = marketQuoteUtilities::fromStringToCurrency(currency);
	this->expiry_ = marketQuoteUtilities::fromStringToDate(expiry, format);
}

/*inspector underlying*/
std::string FutureQuote::underlying() const
{
	return underlying_;
}

/*inspector expiry*/
Date FutureQuote::expiry() const
{
	return expiry_;
}

/*inspector currency*/
Currency FutureQuote::currency() const
{
	return currency_;
}

/*operator<<*/
std::ostream& operator<<(std::ostream& os, const FutureQuote& quote)
{
	os << quote.underlying_ << " " << quote.expiry_ << " - " << quote.currency_.code() << " " << quote.value();
	return os;
}