#include "FilterFwdOutliersByPercentile.h"
#include "FilterIfFwdsPerExpiryLessThan.h"

#include <algorithm>

/*constructor*/
FilterFwdOutliersByPercentile::FilterFwdOutliersByPercentile(
	Real percentage,
	std::shared_ptr<FilterFwdData> filter)
	: FilterFwd(
	[percentage](SyntheticFwds fwds){
		//cap percentage
		Real p = std::min(percentage, 0.40);
		//sort
		std::sort(fwds.begin(), fwds.end(),
			[](SyntheticFwd sf1, SyntheticFwd sf2){ return sf1.strike < sf2.strike; });
		//filter out percentiles
		int cardinality = (int)std::floor(p*fwds.size());
		auto start = fwds.begin() + cardinality;
		auto end = fwds.end() - cardinality;
		return SyntheticFwds(start, end);
	},
	"Filters out highest and lowest " + std::to_string(std::min(percentage, 0.40) * 100) + "[st,nd,th] percentiles",
	FilterIfFwdsPerExpiryLessThan(3, filter)
	)
{}