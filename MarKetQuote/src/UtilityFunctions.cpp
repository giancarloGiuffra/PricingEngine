#include "UtilityFunctions.h"
#include <algorithm>
#include <locale>
#include <ql\errors.hpp>
#include <ql\currencies\europe.hpp>
#include <ql\currencies\america.hpp>
#include <ql\currencies\asia.hpp>

#include <ctime>
#include <sstream>
#include <iomanip>

#include <ql\termstructures\yield\zerocurve.hpp>
#include <ql\math\interpolations\backwardflatinterpolation.hpp>

using namespace QuantLib;

namespace marketQuoteUtilities
{

	/*to time unit*/
	TimeUnit fromStringToTimeUnit(std::string character)
	{
		std::locale default; //default locale
		QL_REQUIRE(character.length() == 1, "string " << character << " must be of length one.");
		QL_REQUIRE(std::isalpha(character.back(), default), character << " must be alphabetic.")
		std::transform(character.begin(), character.end(), character.begin(), ::tolower); //to lowercase
		if (character.compare("d") == 0)
			return Days;
		else if (character.compare("w") == 0)
			return Weeks;
		else if (character.compare("m") == 0)
			return Months;
		else if (character.compare("y") == 0)
			return Years;
		else
			QL_FAIL("expected d, w, m or y (case insensitive)");
	}

	/*to period*/
	Period fromStringToPeriod(std::string tenor)
	{
		//control last character of tenor alphabetic is done inside fromStringToTimeUnit
		QL_REQUIRE(tenor.length() >= 2, tenor << " must be of length at least two.");
		QL_REQUIRE(isNumeric(tenor.substr(0, tenor.length() - 1)), tenor.substr(0, tenor.length() - 1) << " must be numeric.");
		TimeUnit timeUnit = fromStringToTimeUnit(std::string(1, tenor.back()));
		Integer n = std::stoi(tenor.substr(0, tenor.length() - 1));
		return Period(n, timeUnit);
	}

	/*to option type*/
	::Option::Type fromStringToOptionType(std::string type)
	{
		QL_REQUIRE(type.length() >= 1, type << " must be of length at least one.");
		std::transform(type.begin(), type.end(), type.begin(), ::tolower); //to lowercase
		if (type == "call" || type == "c")
			return ::Option::Type::Call;
		else if (type == "put" || type == "p")
			return ::Option::Type::Put;
		else
			QL_FAIL(type << " must be call|c or put|p. (case insensitive)");
	}

	/*to exercise type*/
	::Exercise::Type fromStringToExerciseType(std::string type)
	{
		QL_REQUIRE(type.length() >= 1, type << " must be of length at least one.");
		std::transform(type.begin(), type.end(), type.begin(), ::tolower); //to lowercase
		if (type == "european")
			return ::Exercise::Type::European;
		else if (type == "american")
			return ::Exercise::Type::American;
		else if (type == "bermudan")
			return ::Exercise::Type::Bermudan;
		else
			QL_FAIL(type << " must be european, american, bermudan. (case insensitive)");
	}

	bool isNumeric(const std::string& input)
	{
		return std::all_of(input.begin(), input.end(), ::isdigit);
	}

	/*to currency*/
	Currency fromStringToCurrency(std::string currency)
	{
		QL_REQUIRE(currency.length() == 3, currency << "must be of length three (ISO 4217 Currency Code).");
		if (currency.compare("EUR") == 0)
			return EURCurrency();
		else if (currency.compare("USD") == 0)
			return USDCurrency();
		else if (currency.compare("JPY") == 0)
			return JPYCurrency();
		else if (currency.compare("CHF") == 0)
			return CHFCurrency();
		else if (currency.compare("GBP") == 0)
			return GBPCurrency();
		else if (currency.compare("HKD") == 0)
			return HKDCurrency();
		else
			QL_FAIL("Currency " << currency << " not available.");
	}

	/*to date*/
	Date fromStringToDate(std::string date, std::string format)
	{
		std::tm tm = {};
		std::stringstream ss(date);
		ss >> std::get_time(&tm, format.c_str());
		QL_ASSERT(!ss.fail(), "Parsing of the string " << date << " into format " << format << " failed!");
		//months: quantlib starts at 1 while std::tm.tm_mon starts at 0
		//year: tm_year is based on 1900
		return Date(tm.tm_mday, Month(tm.tm_mon + 1), 1900 + tm.tm_year);
	}

}