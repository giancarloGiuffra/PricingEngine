#include "FilterFwdData.h"

#include <algorithm>

/*TRIVIAL FILTER FWD*/

/*constructor*/
FilterTrivialFwd::FilterTrivialFwd()
{}

/*filter*/
GroupedSyntheticFwds FilterTrivialFwd::filter(GroupedSyntheticFwds fwds)
{
	checkCorrectGrouping(fwds);
	return fwds;
}

/*description*/
std::string FilterTrivialFwd::description()
{
	return "No filter if correctly grouped";
}

/*check grouping*/
void FilterTrivialFwd::checkCorrectGrouping(GroupedSyntheticFwds fwds)
{
	for (auto& stripFwds : fwds)
	{
		if (stripFwds.empty())
			QL_FAIL("there is an empty group!");
		Date expiry = stripFwds.front().expiry;
		if (!std::all_of(stripFwds.begin(), stripFwds.end(),
			[expiry](SyntheticFwd fwd){ return fwd.expiry == expiry; }))
			QL_FAIL("at least one group has mixed expiries!");
	}
}