#include "FilterFwdDataDecorator.h"

/*constructor*/
FilterFwdDataDecorator::FilterFwdDataDecorator(std::shared_ptr<FilterFwdData> filter)
	: filter_(filter)
{}

/*filter*/
GroupedSyntheticFwds FilterFwdDataDecorator::filter(GroupedSyntheticFwds fwds)
{
	return filter_->filter(fwds);
}

/*description*/
std::string FilterFwdDataDecorator::description()
{
	return filter_->description();
}

/*insert description*/
std::string FilterFwdDataDecorator::insertDescription()
{
	return filter_->description().empty() ? "" : filter_->description() + " + ";
}