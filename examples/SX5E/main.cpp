#include <MarketData\include\OISCurveMktDataRequest.h>
#include <MarKetQuote\include\OISQuote.h>
#include <MarketData\include\EONIAMktDataRequest.h>
#include <MarketData\include\BaseMarketDataRequest.h>
#include <MarKetQuote\include\UtilityFunctions.h>
#include <CurveBootstrapping\include\OISBootstrap.h>
#include <MarketData\include\OptionsMktDataRequest.h>
#include <MarKetQuote\include\VanillaOptionQuote.h>
#include <MarketData\include\UtilityFunctions.h>
#include <Utilities\include\ImpliedVolCalculations.h>
#include <MarKetQuote\include\OISQuote.h>
#include <CurveBootstrapping\include\OISCurve.h>
#include <MarketData\include\FilterByVolume.h>
#include <MarketData\include\FilterByOpenInterest.h>
#include <MarketData\include\FutureStripMktDataRequest.h>
#include <MarketData\include\FutureStripBrent.h>
#include <MarketData\include\FilterIfITM.h>
#include <MarketData\include\OISCurveUSD.h>
#include <Utilities\include\VolatilityStream.h>
#include <Utilities\include\DividendStream.h>
#include <Utilities\include\ConfigurationStream.h>
#include <MarketData\include\FilterIfMaturityBeforeThan.h>
#include <Utilities\include\OptionUtilities.h>
#include <Utilities\include\AmericanDividendsCostFunction.h>
#include <Utilities\include\DividendUtilities.h>
#include <MarKetQuote\include\FilterFwdOutliersByPercentile.h>

#include <ql\errors.hpp>
#include <ql\utilities\tracing.hpp>
#include <ql\currency.hpp>
#include <ql\currencies\all.hpp>
#include <ql\money.hpp>
#include <ql\exchangerate.hpp>
#include <ql\time\date.hpp>
#include <ql\time\period.hpp>
#include <ql\time\daycounters\actual360.hpp>
#include <ql\time\daycounters\thirty360.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\settings.hpp>
#include <ql\cashflows\simplecashflow.hpp>
#include <ql\cashflows\coupon.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\termstructures\yield\zeroyieldstructure.hpp>
#include <ql\termstructures\yield\piecewiseyieldcurve.hpp>
#include <ql\termstructures\volatility\equityfx\blackvariancesurface.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\indexes\ibor\eonia.hpp>
#include <ql\math\optimization\all.hpp>
#include <ql\instruments\makeois.hpp>
#include <ql\option.hpp>
#include <ql\instruments\payoffs.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>
#include <ql\processes\hestonprocess.hpp>
#include <ql\models\equity\hestonmodel.hpp>
#include <ql\models\equity\hestonmodelhelper.hpp>
#include <ql\pricingengines\vanilla\analytichestonengine.hpp>
#include <ql\math\optimization\differentialevolution.hpp>
#include <ql\math\optimization\simulatedannealing.hpp>

#include <boost\foreach.hpp>
#include <boost\range\irange.hpp>
#include <boost\tuple\tuple.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\pointer_cast.hpp>

#include <gnuplot-iostream.h>

#define OTL_ODBC
#include <otlv4.h>

#include <vector>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <memory>
#include <ctime>
#include <iostream>
#include <chrono>
#include <regex>

using namespace QuantLib;

int main(void)
{
	//QL_TRACE_ENABLE;
	try {

		std::cout << "evaluation date: " << Settings::instance().evaluationDate() << std::endl;

		auto start = std::chrono::high_resolution_clock::now();

		/*************************
		***** SX5E EXAMPLE ******
		**************************/

		//utilities for reporting
		int width = 112;
		std::string separator = " | ";
		std::string rule(width + 6, '-');
		std::string vspace = "\n\n";

		/**********
		MARKET DATA
		**********/

		//spot
		BaseMarketDataRequest sRequest = BaseMarketDataRequest({ "SX5E Index" }, { "LAST_PRICE" });
		sRequest.run();
		boost::shared_ptr<Quote> spot(new SimpleQuote(std::stod(sRequest.result()[0].fields["LAST_PRICE"])));
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << std::left << "SPOT" << separator << std::endl;
		std::cout << separator << std::setw(width) << std::left << spot->value() << separator << std::endl;
		std::cout << rule << std::endl;
		
		//options for calibration
		OptionsMktDataRequest oRequest =
			OptionsMktDataRequest({ "SX5E Index" }, { "LAST_PRICE", "PX_VOLUME", "OPT_UNDL_PX" });
		oRequest.setNumberStrikesPerExpiry(15); //15 strikes per expiry centered at PX_LAST
		oRequest.setFilter(FilterIfMaturityBeforeThan(Date::todaysDate() + Period(5,Days), "OPT_EXPIRE_DT",
			FilterByVolume(10, FilterIfITM()))); //filters out options ITM and with volume < 10
		std::vector<VanillaOptionQuote> oQuotes =
			oRequest.optionQuotes("SX5E Index", PriceType::MID);
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width)
			<< "OPTIONS (" + std::to_string(oQuotes.size()) + ")" << separator << std::endl;
		for (auto quote : oQuotes)
		{
			std::stringstream ss;
			ss << quote;
			std::cout << separator << std::setw(width) << std::left << ss.str() << separator << std::endl;
		}
		std::cout << rule << std::endl;

		//options for synthetic forwards
		oRequest.setFilter(FilterIfMaturityBeforeThan(Date::todaysDate() + Period(5, Days), "OPT_EXPIRE_DT",
			FilterByVolume(10))); //wrt to above it does not filter the ITM options (necessary for put-call parity)
		std::vector<VanillaOptionQuote> oQuotesForSyntheticFwds =
			oRequest.optionQuotes("SX5E Index", PriceType::MID);
		std::vector<std::shared_ptr<VanillaOptionQuote>> oQuotesForSyntheticFwdsPtr;
		for (auto const& quote : oQuotesForSyntheticFwds)
			oQuotesForSyntheticFwdsPtr.push_back(std::shared_ptr<VanillaOptionQuote>(
			new VanillaOptionQuote(quote)
			));

		/************
		OIS BOOTSTRAP
		*************/

		auto startBootstrap = std::chrono::high_resolution_clock::now();

		//ois
		OISCurveMktDataRequest oisRequest = OISCurveMktDataRequest(OISCurveEUR());
		std::vector<OISQuote> oisQuotes = oisRequest.oisQuotes();
		std::vector<boost::shared_ptr<Quote>> quotes;
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "OIS" << separator << std::endl;
		for (auto q : oisQuotes)
		{
			quotes.push_back(boost::shared_ptr<Quote>(new OISQuote(q)));
			std::stringstream ss;
			ss << *boost::dynamic_pointer_cast<OISQuote>(quotes.back());
			std::cout << separator << std::setw(width) << std::left
				<< ss.str() << separator << std::endl;
		}
		std::cout << rule << std::endl;

		//bootstrap
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "Bootstrapping ..." << separator << std::endl;
		std::cout << rule << std::endl;
		boost::shared_ptr<YieldTermStructure> oisCurve(new
			OISCurve<Discount, LogLinear, OISBootstrap>(2, TARGET(), quotes, Actual365Fixed(), 1.0e-12));

		auto endBootstrap = std::chrono::high_resolution_clock::now();

		/****************
		IMPLIED DIVIDENDS
		*****************/

		//Handles
		Handle<Quote> spotH(spot);
		Handle<YieldTermStructure> oisCurveH(oisCurve);

		//implied dividends
		auto syntheticFwds = utilities::calculateSyntheticFwds(spotH, oisCurveH, oQuotesForSyntheticFwdsPtr,
			FilterFwdOutliersByPercentile(0.20));
		auto dividendTSH = Handle<YieldTermStructure>(
			utilities::stripDividendsFromFutures(spotH, oisCurveH, syntheticFwds,
			oisCurveH->dayCounter(),
			oisCurveH->calendar()
			));

		/**********
		CALIBRATION
		**********/

		//stochastic process
		Real volatility = 0.20;
		Real v0 = volatility*volatility;
		Real kappa = 1.0;
		Real theta = volatility*volatility;
		Real sigma = 0.50;
		Real rho = -0.70;
		boost::shared_ptr<HestonProcess> process(
			new HestonProcess(oisCurveH, dividendTSH, spotH, v0, kappa, theta, sigma, rho));

		//calibrated model
		boost::shared_ptr<HestonModel> model(new HestonModel(process));

		//calibration helpers
		boost::shared_ptr<PricingEngine> hestonEngine(new AnalyticHestonEngine(model));
		std::vector<boost::shared_ptr<CalibrationHelper>> helpers;
		std::vector<boost::shared_ptr<VanillaOption>> options;
		auto volsAndOptions = utilities::calculateImpliedVolatilities(oQuotes, spotH, dividendTSH, oisCurveH);
		auto volatilities = std::get<0>(volsAndOptions);
		oQuotes = std::get<1>(volsAndOptions);
		for (int i = 0; i < oQuotes.size(); ++i)
		{
			options.push_back(boost::shared_ptr<VanillaOption>(oQuotes[i].option()));
			helpers.push_back(boost::shared_ptr<CalibrationHelper>(
				new HestonModelHelper(
				Period(oQuotes[i].expiry()-oisCurveH->referenceDate(),Days),
				oisCurveH->calendar(),
				spotH->value(),
				oQuotes[i].strike(),
				Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(volatilities[i]))),
				oisCurveH,
				dividendTSH
				)));
			helpers[i]->setPricingEngine(hestonEngine);
		}

		//launch calibration
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "Calibrating ..." << separator << std::endl;
		std::cout << rule << std::endl;
		Simplex optimizer(0.001);
		EndCriteria endCriteria(10000, 200, 1.0e-9, 1.0e-9, 1.0e-9);
		model->calibrate(helpers, optimizer, endCriteria);

		//results
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "CALIBRATION RESULTS" << separator << std::endl;
		std::cout << rule << std::endl;
		std::stringstream ss;
		ss << model->endCriteria();
		std::cout << separator << std::setw(width / 2) << "End Criteria: "
			<< separator << std::setw(width / 2) << ss.str() << separator << std::endl;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width / 3) << "Market Price"
			<< separator << std::setw(width / 3) << "Model Price"
			<< separator << std::setw(width / 3) << "Difference"
			<< separator << std::endl;
		std::cout << rule << std::endl;
		for (int j = 0; j < oQuotes.size(); ++j)
		{
			options[j]->setPricingEngine(hestonEngine);
			std::cout << separator << std::setw(width / 3) << std::defaultfloat << oQuotes[j].value()
				<< separator << std::setw(width / 3) << std::defaultfloat << options[j]->NPV()
				<< separator << std::setw(width / 3) << std::scientific << oQuotes[j].value() - options[j]->NPV()
				<< separator << std::endl;
		}
		std::cout << rule << std::endl;

		//calibrated parameters
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "CALIBRATED PARAMETERS" << separator << std::endl;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width / 2) << "initial volatility: "
			<< separator << std::setw(width / 2) << std::sqrt(model->v0()) << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "long-term volatility: "
			<< separator << std::setw(width / 2) << std::sqrt(model->theta()) << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "mean reversion: "
			<< separator << std::setw(width / 2) << model->kappa() << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "vol of vol: "
			<< separator << std::setw(width / 2) << model->sigma() << separator << std::endl;
		std::cout << separator << std::setw(width / 2) << "correlation: "
			<< separator << std::setw(width / 2) << model->rho() << separator << std::endl;
		std::cout << rule << std::endl;

		/*************************
		EXTENDED VOLATILITY MATRIX
		**************************/

		//implied vol from market quotes
		Matrix impliedVolatilityMatrix = utilities::buildImpliedVolatilityMatrix(
			oQuotes,
			spotH,
			dividendTSH,
			oisCurveH);

		//prepare expiries: add quoted + desired
		std::vector<Date> expiries = utilities::extractOptionExpiries(oQuotes);
		expiries.insert(expiries.end(), {
			Date::todaysDate() + Period(1, Years),
			Date::todaysDate() + Period(2, Years),
			Date::todaysDate() + Period(3, Years),
			Date::todaysDate() + Period(4, Years) });
		std::sort(expiries.begin(), expiries.end());
		auto lastE = std::unique(expiries.begin(), expiries.end());
		expiries.erase(lastE, expiries.end());
		expiries.erase(std::remove_if(expiries.begin(), expiries.end(), 
			[](Date d){ return d < Date::todaysDate() + Period(1, Months); }),
			expiries.end()); //exclude expiries < 1 Month because heston price might not correspond to a implied vol

		//prepare strikes: add quoted + desired
		std::vector<Real> strikes = utilities::extractOptionStrikes(oQuotes);
		Real semi = 0.1;
		Real lowestStrike = spotH->value()*(1 - semi);
		Real stepStrike = spotH->value()*0.02;
		std::size_t size = static_cast<std::size_t>(((1 + semi)*spotH->value() - lowestStrike) / stepStrike);
		std::vector<Real> desiredStrikes(size);
		std::generate(desiredStrikes.begin(), desiredStrikes.end(),
			[&lowestStrike, stepStrike]{ return lowestStrike + stepStrike; });
		strikes.insert(strikes.end(), desiredStrikes.begin(), desiredStrikes.end());
		std::sort(strikes.begin(), strikes.end());
		auto lastS = std::unique(strikes.begin(), strikes.end());
		strikes.erase(lastS, strikes.end());

		//matrix
		Matrix extendedVolatilityMatrix = utilities::buildExtendedVolatilityMatrix(
			expiries,
			strikes,
			hestonEngine,
			spotH,
			dividendTSH,
			oisCurveH);

		/*************
		OUTPUT TO FILE
		**************/

		//volatility surface
		BlackVarianceSurface volSurface = BlackVarianceSurface(
			oisCurve->referenceDate(),
			oisCurve->calendar(),
			expiries,
			strikes,
			extendedVolatilityMatrix,
			oisCurve->dayCounter());

		//kondor
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "WRITING TO FILES ..." << separator << std::endl;
		std::cout << rule << std::endl;

		VolatilityStream vs(volSurface);
		vs.writeKondor("C:\\volatility.txt",
			oisCurveH,
			dividendTSH,
			spotH,
			{ Date::todaysDate() + Period(4, Years) },
			{ 0.90, 0.95, 1.05, 1.10, 1.10 });
		vs.copyToArchive("C:\\volatility.txt", Date::todaysDate());

		DividendStream ds(dividendTSH);
		ds.write("C:\\dividendstrip.txt", expiries);
		ds.writeKondor("C:\\dividend.txt", Date::todaysDate() + Period(4, Years));
		ds.copyToArchive("C:\\dividend.txt", Date::todaysDate());

		ConfigurationStream cs("C:\\configuration.txt");
		cs.update(
			dividendTSH,
			Date::todaysDate() + Period(4, Years),
			"SX5E",
			"SX5EShortName",
			"SX5EVolatilityShortName"
			);

		/***********
		ELAPSED TIME
		************/

		auto end = std::chrono::high_resolution_clock::now();
		std::cout << vspace;
		std::cout << rule << std::endl;
		std::cout << separator << std::setw(width) << "ELAPSED TIME" << separator << std::endl;
		std::cout << separator << std::setw(width)
			<< "Total: " + std::to_string(std::chrono::duration<double, std::ratio<60>>(end - start).count()) + " min"
			<< separator << std::endl;
		std::cout << separator << std::setw(width)
			<< "Bootstrap: " + std::to_string(std::chrono::duration<double, std::ratio<60>>(endBootstrap - startBootstrap).count()) + " min"
			<< separator << std::endl;
		std::cout << rule << std::endl;

		/****************
		PLOT PREPARATIONS
		****************/

		//smiles
		std::vector<Date> maturities = utilities::extractOptionExpiries(oQuotes);
		std::map<Date, std::vector<VanillaOptionQuote>> smilesQuotes;
		std::map<Date, std::vector<boost::shared_ptr<VanillaOption>>> smilesOptions;
		std::map<Date, std::vector<Real>> smilesMktStrikes;
		std::map<Date, std::vector<Real>> smilesModelStrikes;
		std::map<Date, std::vector<Real>> smilesMktPrices;
		std::map<Date, std::vector<Real>> smilesModelPrices;
		for (auto maturity : maturities)
		{
			//quotes
			std::vector<VanillaOptionQuote> q;
			for (auto const& quote : oQuotes)
			{
				if (quote.expiry() == maturity) { q.push_back(quote); }
			}
			std::sort(q.begin(), q.end(), [](VanillaOptionQuote q1, VanillaOptionQuote q2){
				return q1.strike() < q2.strike();
			});
			smilesQuotes.insert(std::make_pair(maturity, q));

			//options
			std::vector<boost::shared_ptr<VanillaOption>> o;
			for (auto const& option : options)
			{
				if (option->exercise()->lastDate() == maturity) { o.push_back(option); }
			}
			std::sort(o.begin(), o.end(),
				[](boost::shared_ptr<VanillaOption> o1, boost::shared_ptr<VanillaOption> o2){
				return boost::dynamic_pointer_cast<StrikedTypePayoff>(o1->payoff())->strike() <
					boost::dynamic_pointer_cast<StrikedTypePayoff>(o2->payoff())->strike();
			});
			smilesOptions.insert(std::make_pair(maturity, o));

			//containers for plotting
			std::vector<Real> x1;
			std::vector<Real> y1;
			std::transform(q.begin(), q.end(), std::back_inserter(x1),
				[](VanillaOptionQuote q){ return q.strike(); });
			std::transform(q.begin(), q.end(), std::back_inserter(y1),
				[](VanillaOptionQuote q){ return q.value(); });
			std::vector<Real> x2;
			std::vector<Real> y2;
			std::transform(o.begin(), o.end(), std::back_inserter(x2),
				[](boost::shared_ptr<VanillaOption> o){
				return boost::dynamic_pointer_cast<StrikedTypePayoff>(o->payoff())->strike(); });
			std::transform(o.begin(), o.end(), std::back_inserter(y2),
				[](boost::shared_ptr<VanillaOption> o){ return o->NPV(); });

			smilesMktStrikes.insert(std::make_pair(maturity, x1));
			smilesMktPrices.insert(std::make_pair(maturity, y1));
			smilesModelStrikes.insert(std::make_pair(maturity, x2));
			smilesModelPrices.insert(std::make_pair(maturity, y2));
		}

		//ATM term structure
		std::vector<std::string> datesATMTermStructure;
		std::vector<Real> mktPricesATMTermStructure;
		std::vector<Real> modelPricesATMTermStructure;
		for (auto & pair : smilesQuotes)
		{
			//maturities
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << pair.first.dayOfMonth() << "/"
				<< pair.first.month() << "/"
				<< pair.first.year();
			datesATMTermStructure.push_back(ss.str());

			//quotes
			Real fwd = spot->value()*
				dividendTSH->discount(pair.first, true) / oisCurve->discount(pair.first, true);
			std::sort(pair.second.begin(), pair.second.end(),
				[fwd](VanillaOptionQuote q1, VanillaOptionQuote q2){
				return std::abs(fwd - q1.strike()) < std::abs(fwd - q2.strike());
			});
			mktPricesATMTermStructure.push_back(pair.second.front().value());

			//options
			std::vector<boost::shared_ptr<VanillaOption>> o = smilesOptions.at(pair.first);
			for (auto & option : o)
			{
				if (boost::dynamic_pointer_cast<StrikedTypePayoff>(option->payoff())->strike()
					== pair.second.front().strike())
					modelPricesATMTermStructure.push_back(option->NPV());
			}
		}

		//synthetic fwds
		std::sort(syntheticFwds.begin(), syntheticFwds.end(), 
			[](std::shared_ptr<FutureQuote> f1, std::shared_ptr<FutureQuote> f2){
			return f1->expiry() < f2->expiry();
		});
		std::vector<std::string> datesFutures;
		std::vector<Real> mktPricesFutures;
		std::vector<Real> modelFutures;
		for (auto const& quote : syntheticFwds)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << quote->expiry().dayOfMonth() << "/"
				<< quote->expiry().month() << "/"
				<< quote->expiry().year();
			datesFutures.push_back(ss.str());
			mktPricesFutures.push_back(quote->value());
			Real fwd = spot->value()*
				dividendTSH->discount(quote->expiry(), true) / oisCurve->discount(quote->expiry(), true);
			modelFutures.push_back(fwd);
		}

		//implied vs extended surface
		//implied
		std::vector<std::vector<boost::tuple<std::string, Real, Real>>> implied;
		BlackVarianceSurface impliedVolSurface = BlackVarianceSurface(
			oisCurve->referenceDate(),
			oisCurve->calendar(),
			utilities::extractOptionExpiries(oQuotes),
			utilities::extractOptionStrikes(oQuotes),
			impliedVolatilityMatrix,
			oisCurve->dayCounter());
		for (auto & quote : oQuotes)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << quote.expiry().dayOfMonth() << "/"
				<< quote.expiry().month() << "/"
				<< quote.expiry().year();
			implied.push_back({ boost::make_tuple(ss.str(), quote.strike(),
				impliedVolSurface.blackVol(quote.expiry(), quote.strike())) });
		}

		//extended
		std::vector<std::vector<boost::tuple<std::string, Real, Real>>> extended;
		for (auto & expiry : expiries)
		{
			std::stringstream ss;
			ss << std::setfill('0') << std::setw(2) << expiry.dayOfMonth() << "/"
				<< expiry.month() << "/"
				<< expiry.year();
			std::vector<boost::tuple<std::string, Real, Real>> smile;
			for (auto & strike : strikes)
			{
				smile.push_back(boost::make_tuple(ss.str(), strike, volSurface.blackVol(expiry, strike)));
			}
			extended.push_back(smile);
		}

		/****
		PLOTS
		*****/

		//terminal and layout
		Gnuplot gp;
		gp << "set terminal wxt 0 size 1200,800 title 'Calibration Results' enhanced font 'Verdana,8'\n";
		int rows = (int)std::ceil(maturities.size() / 2.0) + 1;
		gp << "set multiplot layout " << rows << ",2\n";

		//Synthetic forwards
		gp << "set title 'Synthetic Forwards'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['" << datesFutures.front() << "':'" << datesFutures.back() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		std::vector<std::pair<std::string, Real>> datesMktPricesFutures;
		std::transform(datesFutures.begin(), datesFutures.end(), mktPricesFutures.begin(),
			std::back_inserter(datesMktPricesFutures),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << "plot" << gp.file1d(datesMktPricesFutures) << " using 1:2 with linespoints pt 6 title 'market',";
		std::vector<std::pair<std::string, Real>> datesModelPricesFutures;
		std::transform(datesFutures.begin(), datesFutures.end(), modelFutures.begin(),
			std::back_inserter(datesModelPricesFutures),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << gp.file1d(datesModelPricesFutures) << " using 1:2 with linespoints pt 6 title 'model'\n";

		//ATM Term Structure
		gp << "set title 'ATM Prices'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		gp << "set xrange ['" << datesATMTermStructure.front() << "':'" << datesATMTermStructure.back() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		std::vector<std::pair<std::string, Real>> datesMktPricesATM;
		std::transform(datesATMTermStructure.begin(), datesATMTermStructure.end(),
			mktPricesATMTermStructure.begin(), std::back_inserter(datesMktPricesATM),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << "plot" << gp.file1d(datesMktPricesATM) << " using 1:2 with linespoints pt 6 title 'market',";

		std::vector<std::pair<std::string, Real>> datesModelPricesATM;
		std::transform(datesATMTermStructure.begin(), datesATMTermStructure.end(),
			modelPricesATMTermStructure.begin(), std::back_inserter(datesModelPricesATM),
			[](std::string d, Real p){ return std::make_pair(d, p); });
		gp << gp.file1d(datesModelPricesATM) << " using 1:2 with linespoints pt 6 title 'model'\n";

		//smiles
		gp << "set xdata\n";
		gp << "set format\n";
		gp << "set xrange [*:*]\n";
		gp << "set xtics norotate\n";
		for (auto maturity : maturities)
		{
			gp << "set title '" << maturity << "'\n";
			std::vector<std::pair<Real, Real>> strikesMktPrices;
			std::transform(smilesMktStrikes.at(maturity).begin(), smilesMktStrikes.at(maturity).end(),
				smilesMktPrices.at(maturity).begin(),
				std::back_inserter(strikesMktPrices),
				[](Real k, Real p){ return std::make_pair(k, p); });
			gp << "plot" << gp.file1d(strikesMktPrices) << " with linespoints pt 6 title 'market',";

			std::vector<std::pair<Real, Real>> strikesModelPrices;
			std::transform(smilesModelStrikes.at(maturity).begin(), smilesModelStrikes.at(maturity).end(),
				smilesModelPrices.at(maturity).begin(),
				std::back_inserter(strikesModelPrices),
				[](Real k, Real p){ return std::make_pair(k, p); });
			gp << gp.file1d(strikesModelPrices) << " with linespoints pt 6 title 'model'\n";
		}
		gp << "unset multiplot\n";

		//surface (new window)
		gp << "set terminal wxt 1 size 1200,800 title 'Volatility Surface' enhanced font 'Verdana,8'\n";
		gp << "set title 'Impled vs Extended'\n";
		gp << "set xdata time\n";
		gp << "set timefmt '%d/%B/%Y'\n";
		std::stringstream start_stream;
		start_stream << std::setfill('0') << std::setw(2) << expiries.front().dayOfMonth() << "/"
			<< expiries.front().month() << "/"
			<< expiries.front().year();
		std::stringstream end_stream;
		end_stream << std::setfill('0') << std::setw(2) << expiries.back().dayOfMonth() << "/"
			<< expiries.back().month() << "/"
			<< expiries.back().year();
		gp << "set xrange ['" << start_stream.str() << "':'" << end_stream.str() << "']\n";
		gp << "set xtics rotate by -45\n";
		gp << "set format x '%b-%y'\n";
		gp << "set key rmargin center\n";
		gp << "splot" << gp.file2d(extended) << " using 1:2:3 with lines title 'Extended Surface',";
		gp << gp.file2d(implied) << " using 1:2:3 with points title 'Implied Surface'\n";  

	}
	catch (std::exception& e) {

		std::cerr << std::endl;
		std::cerr << e.what() << std::endl;
	}
	catch (otl_exception& p){ // intercept OTL exceptions
		std::cerr << p.msg << std::endl; // print out error message
		std::cerr << p.code << std::endl; // print out error code
		std::cerr << p.var_info << std::endl; // print out the variable that caused the error
		std::cerr << p.sqlstate << std::endl; // print out SQLSTATE message
		std::cerr << p.stm_text << std::endl; // print out SQL that caused the error
	}

	// wait for enter key to exit application
	std::cout << "Press ENTER to quit" << std::endl;
	char dummy[2];
	std::cin.getline(dummy, 2);
	return 0;
}