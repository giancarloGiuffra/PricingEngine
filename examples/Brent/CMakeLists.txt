#inspired by https://www.johnlamp.net/
cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
set(CMAKE_LEGACY_CYGWIN_WIN32 0)

#solution name
project("BrentExample")

#set compiler flags based on compiler
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR
    "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(warnings "-Wall -Wextra -Werror -Wno-empty-body")
    	# Disabled warning:
    	#   - empty-body: quantlib has empty body for else statements
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	set(warnings "/W4 /wd4512 /wd4100 /wd4127 /wd4505 /WX /EHsc /D_SCL_SECURE_NO_WARNINGS")
        # Disabled Warnings:
        #   - 4512 "assignment operator could not be generated"
        #        This warning provides no useful information and will occur in
        #        well formed programs.
        #        <http://msdn.microsoft.com/en-us/library/hsyx7kbz.aspx>
		#   - _SCL_SECURE_NO_WARNINGS (4996) for potentially unsafe methods in the Standard C++ Library.
		#				     quantlib uses these methods though.
		#   - 4100 formal parameter is not referenced in the body of the function. quantlib in bootstraptraits.hpp(99)
		#   - 4127 conditional expression is constant. quantlib in iterativebootstrap.hpp(190)
		#   - 4505 unreferenced local function has been removed. 
endif()
if (NOT CONFIGURED_ONCE)
    set(CMAKE_CXX_FLAGS "${warnings}"
        CACHE STRING "Flags used by the compiler during all build types." FORCE)
    set(CMAKE_C_FLAGS   "${warnings}"
        CACHE STRING "Flags used by the compiler during all build types." FORCE)
endif()

#BOOST library - used by many projects ---------------------------------------------------------------------
#define hint locations where to find the library based on os
if(APPLE)
	set(BOOST_ROOT "/usr/local/include/boost")
elseif(WIN32)
	set(BOOST_INCLUDEDIR "C:/Program Files/boost/boost_1_58_0/boost")
	set(BOOST_LYBRARYDIR "C:/Program Files/boost/boost_1_58_0/stage/lib")
endif()
#static use of library
set(Boost_USE_STATIC_LIBS TRUE)
#look for package
set(BOOST_COMPONENTS_FOR_GNUPLOT_IOSTREAM "iostreams system filesystem") #required by gnuplot-iostream
separate_arguments(BOOST_COMPONENTS_FOR_GNUPLOT_IOSTREAM)
find_package(Boost 1.58 REQUIRED COMPONENTS ${BOOST_COMPONENTS_FOR_GNUPLOT_IOSTREAM})
include_directories( SYSTEM ${Boost_INCLUDE_DIRS})

#blpapi library --------------------------------------------------------------------------------------------
set(BLPAPI_DIR "C:/blp/API/blpapi_cpp_3.8.8.1"
	CACHE PATH "The path to the bloomberg api")
include_directories(${BLPAPI_DIR}/include)
add_library(blpapi SHARED IMPORTED)
set_property(TARGET blpapi PROPERTY IMPORTED_LOCATION ${BLPAPI_DIR}/lib/blpapi3_64.dll)
set_property(TARGET blpapi PROPERTY IMPORTED_IMPLIB ${BLPAPI_DIR}/lib/blpapi3_64.lib)

#quantlib library --------------------------------------------------------------------------------------------
set(QUANTLIB_DIR "C:/Program Files/quantlib/QuantLib-1.6"
	CACHE PATH "The path to the quantlib directory")
include_directories(${QUANTLIB_DIR})
add_library(quantlib STATIC IMPORTED)
set_property(TARGET quantlib PROPERTY IMPORTED_LOCATION ${QUANTLIB_DIR}/lib/QuantLib-vc120-x64-mt.lib)
add_library(quantlibDebug STATIC IMPORTED)
set_property(TARGET quantlibDebug PROPERTY IMPORTED_LOCATION ${QUANTLIB_DIR}/lib/QuantLib-vc120-x64-mt-gd.lib)

#gnuplot-iostream - C++ interface to gnuplot, obviously gnuplot must be installed ----------------------------
set(GNUPLOT_IOSTREAM_DIR "C:/Program Files/gnuplot-iostream"
	CACHE PATH "The path where the header gnuplot-iostream.h is")
include_directories(${GNUPLOT_IOSTREAM_DIR})

#OTL library - Oracle, ODBC and DB2-CLI Template Library ------------------------------------------------------
set(OTL_DIR "C:/Program Files/OTL"
	CACHE PATH "The path to the OTL library")
include_directories(${OTL_DIR})

#MySQL ODBC Connector (required by OTL) -----------------------------------------------------------------------
set(MYSQL_ODBC_DIR "C:/Program Files/MySQL/Connector ODBC 5.3"
	CACHE PATH "The path to the MySQL ODBC API")
find_library(mysqlodbc NAMES myodbc5a PATHS ${MYSQL_ODBC_DIR})
set(ENV{PATH} "$ENV{PATH};${MYSQL_ODBC_DIR}")

#include PricingEngine library
set(PRICINGENGINE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../..)
include_directories(${PRICINGENGINE_DIR})
include(${PRICINGENGINE_DIR}/lib/pricingengine.cmake)

#main executable
add_executable(main main.cpp)
target_link_libraries(main ${Boost_LIBRARIES} ${mysqlodbc} PricingEngine optimized quantlib debug quantlibDebug)

set(CONFIGURED_ONCE TRUE CACHE INTERNAL
    "A flag showing that CMake has configured at least once.")
