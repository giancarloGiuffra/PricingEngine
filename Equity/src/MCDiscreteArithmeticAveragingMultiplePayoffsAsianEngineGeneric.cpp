#include "MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric.h"

/*ArithmeticAverageMultiplePayoffsPathPricer*/

/*constructor*/
ArithmeticAverageMultiplePayoffsPathPricer::ArithmeticAverageMultiplePayoffsPathPricer(
	std::vector<boost::shared_ptr<StrikedTypePayoff>> payoffs,
	DiscountFactor discount,
	Real runningSum,
	Size pastFixings,
	Real initialValue)
	: payoffs_(payoffs), discount_(discount), runningSum_(runningSum),
	pastFixings_(pastFixings), initialValue_(initialValue)
{}

/*operator()*/
Array ArithmeticAverageMultiplePayoffsPathPricer::operator()(const MultiPath& multipath) const
{
	const Path& path = multipath[0];
	const Size n = multipath.pathSize();
	QL_REQUIRE(n > 0, "the path cannot be empty");

	//arithmetic average
	Real sum;
	Size fixings;
	auto mandatoryTimes = path.timeGrid().mandatoryTimes();
	std::vector<Real> valuesForMean;
	TimeGrid grid = path.timeGrid();
	for (int i = 0; i < grid.size(); i++)
	{
		if (std::find(mandatoryTimes.begin(), mandatoryTimes.end(), grid[i]) != mandatoryTimes.end())
			valuesForMean.push_back(path[i]);
	}
	fixings = pastFixings_ + valuesForMean.size();
	sum = std::accumulate(valuesForMean.begin(), valuesForMean.end(), runningSum_);
	Real averagePrice = sum / fixings;

	//results
	Array results(payoffs_.size() + 1);
	for (int i = 0; i < payoffs_.size(); i++)
	{
		results[i] = payoffs_[i]->operator()(averagePrice) * discount_; //price
	}
	results[results.size() - 1] = averagePrice;

	return results;
}