#include <string>
  using std::string;

#include <gmock\gmock.h>
  using ::testing::Eq;
  using ::testing::Le;
  using ::testing::Ge;
#include <gtest\gtest.h>
  using ::testing::Test;

#include <Equity\include\MCDiscreteAveragingAsianEngineGeneric.h>
#include <Equity\include\MCDiscreteArithmeticEngineGeneric.h>
#include <Equity\include\MCEuropeanEngineGeneric.h>
#include <Equity\include\MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric.h>

#include <ql\processes\blackscholesprocess.hpp>
#include <ql\termstructures\yield\flatforward.hpp>
#include <ql\termstructures\volatility\equityfx\blackconstantvol.hpp>
#include <ql\time\calendars\target.hpp>
#include <ql\time\daycounters\actual365fixed.hpp>
#include <ql\quote.hpp>
#include <ql\quotes\simplequote.hpp>
#include <ql\pricingengines\asian\mc_discr_arith_av_price.hpp>
#include <ql\processes\hestonprocess.hpp>
#include <ql\instruments\vanillaoption.hpp>

#include <boost\any.hpp>

namespace Equity
{
namespace testing
{
    class EquityTest : public Test
    {
    protected:
        EquityTest(){}
        ~EquityTest(){}
		
		virtual void SetUp(){
			Date today = Date::todaysDate();
			riskFreeTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
				new FlatForward(today, 0.01, Actual365Fixed())));
			volatility_ = Handle<BlackVolTermStructure>(boost::shared_ptr<BlackVolTermStructure>(
				new BlackConstantVol(today, TARGET(), 0.25, Actual365Fixed())));
			spot_ = Handle<Quote>(boost::shared_ptr<Quote>(new SimpleQuote(100)));
			dividendTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
				new FlatForward(today, 0.0, Actual365Fixed())));
		}
        virtual void TearDown(){}

		boost::shared_ptr<StochasticProcess> process_;
		Handle<YieldTermStructure> riskFreeTS_;
		Handle<BlackVolTermStructure> volatility_;
		Handle<YieldTermStructure> dividendTS_;
		Handle<Quote> spot_;
    };

	TEST_F(EquityTest, DISABLED_MCEuropeanEngineGeneric_HaastrechtPelsserCasII)
	{
		//process
		Real v0 = 0.09;
		Real kappa = 1.0;
		Real theta = 0.09;
		Real rho = -0.3;
		Real sigma = 1.0;

		//discount
		Date today = Date::todaysDate();
		riskFreeTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(today, 0.05, Actual365Fixed())));

		process_ = boost::shared_ptr<StochasticProcess>(new HestonProcess(
			riskFreeTS_,
			dividendTS_,
			spot_,
			v0,
			kappa,
			theta,
			sigma,
			rho));

		//option
		boost::shared_ptr<StrikedTypePayoff> payoff(new PlainVanillaPayoff(Option::Call, 100));
		boost::shared_ptr<Exercise> exercise(new EuropeanExercise(today + Period(5, Years)));
		VanillaOption option(payoff, exercise);

		//engine
		boost::shared_ptr<PricingEngine> engine = boost::shared_ptr<PricingEngine>(
			new MCEuropeanEngineGeneric<VanillaOption>(
			process_,
			riskFreeTS_,
			Null<Size>(), //timeSteps
			16, //timeStepsPerYear
			false, //brownianBridge
			true, //anthitetic
			false, //control variate
			1000000, //required samples
			Null<Real>(), //tolerance
			Null<Size>(), //max samples
			17
			));

		//launch simulation
		option.setPricingEngine(engine);
		Real NPV = option.NPV();

		//tests
		Real tolerance = 1.0e-2;
		EXPECT_THAT(std::abs(NPV - 33.597), Le(tolerance));
	}

	TEST_F(EquityTest, MCDiscreteAveragingAsianEngineGeneric_comparisonWithQuantLib)
	{
		//process
		process_ = boost::shared_ptr<StochasticProcess>( new BlackScholesProcess(
			spot_, riskFreeTS_, volatility_));

		//option
		Date today = Date::todaysDate();
		boost::shared_ptr<StrikedTypePayoff> payoff( new PlainVanillaPayoff(Option::Call, spot_->value()));
		boost::shared_ptr<Exercise> exercise( new EuropeanExercise(today + Period(3, Years)));
		std::vector<Date> fixingDates = { 
			today, 
			today + Period(1, Years), 
			today + Period(2, Years),
			today + Period(3, Years)
		};
		DiscreteAveragingAsianOption option(
			Average::Arithmetic, 0, 0, fixingDates, payoff, exercise);

		boost::shared_ptr<PricingEngine> engine = boost::shared_ptr<PricingEngine>(
			new MCDiscreteArithmeticEngineGeneric<>(
			process_,
			riskFreeTS_,
			Null<Size>(), //timeSteps
			Null<Size>(), //timeStepsPerYear
			false, //brownianBridge
			true, //anthitetic
			false, //control variate
			10000, //required samples
			Null<Real>(), //tolerance
			Null<Size>(), //max samples
			17)); //seed

		//launch simulation
		option.setPricingEngine(engine);
		Real NPV = option.NPV();

		//calculate NPV using QuantLib Engine
		boost::shared_ptr<GeneralizedBlackScholesProcess> bprocess =
			boost::dynamic_pointer_cast<GeneralizedBlackScholesProcess>(process_);
		boost::shared_ptr<PricingEngine> qengine(new MCDiscreteArithmeticAPEngine<>(
			bprocess, false, true, false, 10000, Null<Real>(), Null<Size>(), 17));
		option.setPricingEngine(qengine);
		Real NPVQ = option.NPV();
		
		//test
		EXPECT_THAT(NPV, Eq(NPVQ));
	}

	TEST_F(EquityTest, DISABLED_MCDiscreteAveragingAsianEngineGeneric_HaastrechtPelsserCaseIV)
	{
		//process
		Real v0 = 0.0194;
		Real kappa = 1.0407;
		Real theta = 0.0586;
		Real rho = -0.6747;
		Real sigma = 0.5196;

		//discount
		Date today = Date::todaysDate();
		riskFreeTS_ = Handle<YieldTermStructure>(boost::shared_ptr<YieldTermStructure>(
			new FlatForward(today, 0.0, Actual365Fixed())));

		process_ = boost::shared_ptr<StochasticProcess>(new HestonProcess(
			riskFreeTS_,
			dividendTS_,
			spot_,
			v0,
			kappa,
			theta,
			sigma,
			rho));

		//option
		boost::shared_ptr<StrikedTypePayoff> payoff100(new PlainVanillaPayoff(Option::Call, 100));
		boost::shared_ptr<Exercise> exercise(new EuropeanExercise(today + Period(4, Years)));
		std::vector<Date> fixingDates = {
			today + Period(1, Years),
			today + Period(2, Years),
			today + Period(3, Years),
			today + Period(4, Years)
		};
		DiscreteAveragingAsianOption option100(
			Average::Arithmetic, 0, 0, fixingDates, payoff100, exercise);

		boost::shared_ptr<PricingEngine> engine = boost::shared_ptr<PricingEngine>(
			new MCDiscreteArithmeticEngineGeneric<>(
			process_,
			riskFreeTS_,
			Null<Size>(), //timeSteps
			8, //timeStepsPerYear
			false, //brownianBridge
			false, //anthitetic
			false, //control variate
			1000000, //required samples
			Null<Real>(), //tolerance
			Null<Size>(), //max samples
			17)); //seed

		//launch simulation
		option100.setPricingEngine(engine);
		Real NPV100 = option100.NPV();

		std::cout << "NPV100: " << NPV100 << std::endl;

		//tests
		Real tolerance = 1.0e-2;
		EXPECT_THAT(std::abs(NPV100 - 9.712), Le(tolerance));
	}

	TEST_F(EquityTest, DiscreteAveragingMultiplePayoffsAsianOption_BlackScholes3Strikes)
	{
		//process
		process_ = boost::shared_ptr<StochasticProcess>(new BlackScholesProcess(
			spot_, riskFreeTS_, volatility_));

		//options
		Date today = Date::todaysDate();
		boost::shared_ptr<StrikedTypePayoff> payoff80(new PlainVanillaPayoff(Option::Call, 80));
		boost::shared_ptr<StrikedTypePayoff> payoff100(new PlainVanillaPayoff(Option::Call, spot_->value()));
		boost::shared_ptr<StrikedTypePayoff> payoff120(new PlainVanillaPayoff(Option::Call, 120));
		boost::shared_ptr<Exercise> exercise(new EuropeanExercise(today + Period(3, Years)));
		std::vector<Date> fixingDates = {
			today,
			today + Period(1, Years),
			today + Period(2, Years),
			today + Period(3, Years)
		};
		DiscreteAveragingAsianOption option80(
			Average::Arithmetic, 0, 0, fixingDates, payoff80, exercise);
		DiscreteAveragingAsianOption option100(
			Average::Arithmetic, 0, 0, fixingDates, payoff100, exercise);
		DiscreteAveragingAsianOption option120(
			Average::Arithmetic, 0, 0, fixingDates, payoff120, exercise);
		DiscreteAveragingMultiplePayoffsAsianOption optionMultiplePayoffs(
			Average::Arithmetic, 0, 0, fixingDates, { payoff100, payoff80, payoff120 }, exercise);

		//engines
		boost::shared_ptr<PricingEngine> engine = boost::shared_ptr<PricingEngine>(
			new MCDiscreteArithmeticEngineGeneric<>(
			process_,
			riskFreeTS_,
			Null<Size>(), //timeSteps
			Null<Size>(), //timeStepsPerYear
			false, //brownianBridge
			true, //anthitetic
			false, //control variate
			1000, //required samples
			Null<Real>(), //tolerance
			Null<Size>(), //max samples
			17)); //seed

		boost::shared_ptr<PricingEngine> engineMultiplePayoffs = boost::shared_ptr<PricingEngine>(
			new MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<>(
			process_,
			riskFreeTS_,
			Null<Size>(), //timeSteps
			Null<Size>(), //timeStepsPerYear
			false, //brownianBridge
			true, //anthitetic
			false, //control variate
			1000, //required samples
			Null<Real>(), //tolerance
			Null<Size>(), //max samples
			17)); //seed

		//launch simulations
		option100.setPricingEngine(engine);
		Real option100NPV = option100.NPV();
		option80.setPricingEngine(engine);
		Real option80NPV = option80.NPV();
		option120.setPricingEngine(engine);
		Real option120NPV = option120.NPV();

		optionMultiplePayoffs.setPricingEngine(engineMultiplePayoffs);
		Real optionMultiplesNPV = optionMultiplePayoffs.NPV();

		//test
		auto additionalResults = optionMultiplePayoffs.additionalResults();
		auto prices = boost::any_cast<std::vector<Real>>(additionalResults.at("prices"));

		EXPECT_THAT(option100NPV, Eq(optionMultiplesNPV));
		EXPECT_THAT(option80NPV, Eq(prices[0]));
		EXPECT_THAT(option120NPV, Eq(prices[1]));
	}

} //namespace testing
} //namespace MarketData