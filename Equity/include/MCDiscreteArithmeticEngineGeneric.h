#ifndef MC_DISCRETE_ARITHMETIC_ENGINE_GENERIC_H
#define MC_DISCRETE_ARITHMETIC_ENGINE_GENERIC_H

#include "MCDiscreteAveragingAsianEngineGeneric.h"

#include <ql\handle.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\utilities\tracing.hpp>

using namespace QuantLib;

/*! \brief MC Pricing Engine for DiscreteAveragingAsianOption options with arithmetic average.

*/
template <class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCDiscreteArithmeticEngineGeneric : public MCDiscreteAveragingAsianEngineGeneric < RNG, S >
{
public:

	typedef typename MCDiscreteAveragingAsianEngineGeneric<RNG, S>::path_pricer_type
		path_pricer_type;

	/** Constructor.
	@param process
	@param discountCurve
	@param timeSteps
	@param timeStepsPerYear
	@param brownianBridge
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCDiscreteArithmeticEngineGeneric(
		const boost::shared_ptr<StochasticProcess>& process,
		const Handle<YieldTermStructure>& discountCurve,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

protected:

	/** It returns the PathPricer to use in simulations.
	@return boost::shared_ptr<path_pricer_type>
	*/
	boost::shared_ptr<path_pricer_type> pathPricer() const;

	Handle<YieldTermStructure> discountCurve_;
};

/*! \brief Path Pricer for MCDiscreteArithmeticEngineGeneric.

The convention is that the first element of the Array is the price of the instrument.
*/
class ArithmeticAveragePathPricer : public PathPricer < MultiPath, Array > 
{
public:

	/** Constructor.
	@param payoff
	@param discount
	@param runningSum
	@param pastFixings
	@param initialValue
	*/
	ArithmeticAveragePathPricer(
		boost::shared_ptr<Payoff> payoff,
		DiscountFactor discount,
		Real runningSum = 0.0,
		Size pastFixings = 0,
		Real initialValue = Null<Real>());

	/** Operator()
	@param Multipath
	@return Array
	*/
	Array operator()(const MultiPath& Multipath) const;

private:

	boost::shared_ptr<Payoff> payoff_;
	DiscountFactor discount_;
	Real initialValue_; /**< Underlying's Initial Value */
	Real runningSum_;
	Size pastFixings_;
};

//template definitions MCDiscreteArithmeticEngineGeneric

/*constructor*/
template <class RNG, class S>
inline
MCDiscreteArithmeticEngineGeneric<RNG, S>::MCDiscreteArithmeticEngineGeneric(
const boost::shared_ptr<StochasticProcess>& process,
const Handle<YieldTermStructure>& discountCurve,
Size timeSteps,
Size timeStepsPerYear,
bool brownianBridge,
bool antitheticVariate,
bool controlVariate,
Size requiredSamples,
Real requiredTolerance,
Size maxSamples,
BigNatural seed)
: MCDiscreteAveragingAsianEngineGeneric<RNG, S>(
process,
timeSteps,
timeStepsPerYear,
false,
antitheticVariate,
false,
requiredSamples,
requiredTolerance,
maxSamples,
seed), discountCurve_(discountCurve)
{
	registerWith(discountCurve_);
}

/*path pricer*/
template <class RNG, class S>
boost::shared_ptr<
	typename MCDiscreteArithmeticEngineGeneric<RNG, S>::path_pricer_type>
	MCDiscreteArithmeticEngineGeneric<RNG, S>::pathPricer() const {

	return boost::shared_ptr<
		typename MCDiscreteArithmeticEngineGeneric<RNG, S>::path_pricer_type>(
		new ArithmeticAveragePathPricer(
		this->arguments_.payoff,
		this->discountCurve_->discount(this->timeGrid().back()),
		this->arguments_.runningAccumulator,
		this->arguments_.pastFixings
		));
}

//template definitions ArithmeticAveragePathPricer

/*constructor*/
ArithmeticAveragePathPricer::ArithmeticAveragePathPricer(
	boost::shared_ptr<Payoff> payoff,
	DiscountFactor discount,
	Real runningSum,
	Size pastFixings,
	Real initialValue)
	: payoff_(payoff), discount_(discount), initialValue_(initialValue),
	runningSum_(runningSum), pastFixings_(pastFixings)
{}

/*operator()*/
Array ArithmeticAveragePathPricer::operator()(
	const MultiPath& multiPath) const {

	const Path& path = multiPath[0];
	const Size n = multiPath.pathSize();
	QL_REQUIRE(n > 0, "the path cannot be empty");

	//arithmetic average
	Real sum;
	Size fixings;
	auto mandatoryTimes = path.timeGrid().mandatoryTimes();
	std::vector<Real> valuesForMean;
	TimeGrid grid = path.timeGrid();
	for (int i = 0; i < grid.size(); i++)
	{
		if (std::find(mandatoryTimes.begin(), mandatoryTimes.end(), grid[i]) != mandatoryTimes.end())
			valuesForMean.push_back(path[i]);
	}
	fixings = pastFixings_ + valuesForMean.size();
	sum = std::accumulate(valuesForMean.begin(), valuesForMean.end(), runningSum_);
	Real averagePrice = sum / fixings;

	//results
	Array results(1);
	results[0] = payoff_->operator()(averagePrice) * discount_; //price
	return results;

}

#endif // !MC_DISCRETE_ARITHMETIC_ENGINE_GENERIC_H
