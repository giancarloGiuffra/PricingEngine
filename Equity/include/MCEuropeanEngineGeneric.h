#ifndef MC_EUROPEAN_ENGINE_GENERIC_H
#define MC_EUROPEAN_ENGINE_GENERIC_H

#include "MCEngineGeneric.h"
#include <MonteCarlo\include\MonteCarloTraits.h>
#include <MonteCarlo\include\SequenceStatisticsArrayInspectors.h>

#include <ql\stochasticprocess.hpp>
#include <ql\math\statistics\sequencestatistics.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>
#include <ql\handle.hpp>

using namespace QuantLib;

/*! \brief Class to price european instruments with Monte Carlo Engine.

*/
template<class Inst, class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCEuropeanEngineGeneric : public MCEngineGeneric<Inst, RNG, S>
{
public:

	typedef 
		typename MCEngineGeneric<Inst, RNG, S>::path_pricer_type 
		path_pricer_type;

	/** Constructor.
	@param process
	@param discountCurve
	@param timeSteps
	@param timeStepsPerYear
	@param brownianBridge
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCEuropeanEngineGeneric(
		const boost::shared_ptr<StochasticProcess>& process,
		const Handle<YieldTermStructure>& discountCurve,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

protected:

	/** It returns the PathPricer to use in simulations.
	@return boost::shared_ptr<path_pricer_type>
	*/
	boost::shared_ptr<path_pricer_type> pathPricer() const;

	Handle<YieldTermStructure> discountCurve_;
};

/*! \brief Path Pricer for MCEuropeanEngineGeneric.

The convention is that the first element of the Array is the price of the instrument.
*/
class EuropeanGenericPathPricer : public PathPricer < MultiPath, Array >
{
public:

	/** Constructor.
	@param payoff
	@param discount
	@param initialValue
	*/
	EuropeanGenericPathPricer(
		boost::shared_ptr<Payoff> payoff,
		DiscountFactor discount,
		Real initialValue = Null<Real>());

	/** Operator()
	@param Multipath
	@return Array
	*/
	Array operator()(const MultiPath& Multipath) const;

private:

	boost::shared_ptr<Payoff> payoff_;
	DiscountFactor discount_;
	Real initialValue_; /**< Underlying's Initial Value */
};

//template definitions MCEuropeanEngineGeneric

/*constructor*/
template <class I, class RNG, class S>
inline
MCEuropeanEngineGeneric<I, RNG, S>::MCEuropeanEngineGeneric(
const boost::shared_ptr<StochasticProcess>& process,
const Handle<YieldTermStructure>& discountCurve,
Size timeSteps,
Size timeStepsPerYear,
bool brownianBridge,
bool antitheticVariate,
bool controlVariate,
Size requiredSamples,
Real requiredTolerance,
Size maxSamples,
BigNatural seed)
: MCEngineGeneric<I, RNG, S>(
process,
timeSteps,
timeStepsPerYear,
false,
antitheticVariate,
false,
requiredSamples,
requiredTolerance,
maxSamples,
seed), discountCurve_(discountCurve)
{
	registerWith(discountCurve_);
}

/*path pricer*/
template <class I, class RNG, class S>
boost::shared_ptr<
	typename MCEuropeanEngineGeneric<I, RNG, S>::path_pricer_type>
	MCEuropeanEngineGeneric<I, RNG, S>::pathPricer() const {

	return boost::shared_ptr<
		typename MCEuropeanEngineGeneric<I, RNG, S>::path_pricer_type>(
		new EuropeanGenericPathPricer(
		this->arguments_.payoff,
		this->discountCurve_->discount(this->timeGrid().back())
		));
}

//template definitions EuropeanGenericPathPricer

/*constructor*/
EuropeanGenericPathPricer::EuropeanGenericPathPricer(
	boost::shared_ptr<Payoff> payoff,
	DiscountFactor discount,
	Real initialValue)
	: payoff_(payoff), discount_(discount), initialValue_(initialValue)
{}

/*operator()*/
Array EuropeanGenericPathPricer::operator()(
	const MultiPath& multiPath) const {

	const Path& path = multiPath[0];
	const Size n = multiPath.pathSize();
	QL_REQUIRE(n > 0, "the path cannot be empty");

	//results
	Array results(1);
	results[0] = payoff_->operator()(path.back()) * discount_; //price
	return results;

}

#endif // MC_EUROPEAN_ENGINE_GENERIC_H