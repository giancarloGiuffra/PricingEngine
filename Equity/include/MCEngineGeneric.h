#ifndef MC_ENGINE_GENERIC_H
#define MC_ENGINE_GENERIC_H

#include <MonteCarlo\include\MonteCarloTraits.h>
#include <MonteCarlo\include\SequenceStatisticsArrayInspectors.h>

#include <ql\stochasticprocess.hpp>
#include <ql\pricingengines\mcsimulation.hpp>
#include <ql\math\statistics\sequencestatistics.hpp>
#include <ql\exercise.hpp>
#include <ql\instruments\vanillaoption.hpp>

using namespace QuantLib;

/*! \brief Abstract class to price instruments with Monte Carlo Engine.

*/
template<class Inst, class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCEngineGeneric :
	public Inst::engine,
	public McSimulation<MultiVariateWithArrayResults, RNG, S> {
public:

	typedef
		typename McSimulation<MultiVariateWithArrayResults, RNG, S>::path_generator_type
		path_generator_type;

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::path_pricer_type
		path_pricer_type;

	typedef typename McSimulation<MultiVariateWithArrayResults, RNG, S>::stats_type
		stats_type;

	/** Constructor.
	@param process
	@param timeSteps
	@param timeStepsPerYear
	@param brownianBridge
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCEngineGeneric(
		const boost::shared_ptr<StochasticProcess>& process,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	virtual void calculate() const;
	//@}

protected:

	//! \name McSimulation interface
	//@{

	/** Method that constructs the Time Grid.
	@return TimeGrid
	*/
	TimeGrid timeGrid() const;

	/** Method that constructs the Path Generator.
	@return boost::shared_ptr<path_generator_type>
	*/
	boost::shared_ptr<path_generator_type> pathGenerator() const;

	//@}

	boost::shared_ptr<StochasticProcess> process_;
	Size timeSteps_, timeStepsPerYear_;
	Size requiredSamples_, maxSamples_;
	Real requiredTolerance_;
	bool brownianBridge_;
	BigNatural seed_;
};


// template definitions

/*constructor*/
template<class I, class RNG, class S>
inline
MCEngineGeneric<I, RNG, S>::MCEngineGeneric(
const boost::shared_ptr<StochasticProcess>& process,
Size timeSteps,
Size timeStepsPerYear,
bool brownianBridge,
bool antitheticVariate,
bool controlVariate,
Size requiredSamples,
Real requiredTolerance,
Size maxSamples,
BigNatural seed)
: McSimulation<MultiVariateWithArrayResults, RNG, S>(antitheticVariate, controlVariate),
process_(process), timeSteps_(timeSteps),
timeStepsPerYear_(timeStepsPerYear), requiredSamples_(requiredSamples),
maxSamples_(maxSamples), requiredTolerance_(requiredTolerance),
brownianBridge_(brownianBridge), seed_(seed) {
	registerWith(process_);
}

/*calculate*/
template<class I, class RNG, class S>
inline
void MCEngineGeneric<I, RNG, S>::calculate() const {

	McSimulation<MultiVariateWithArrayResults, RNG, S>::calculate(requiredTolerance_,
		requiredSamples_,
		maxSamples_);
	results_.value = this->mcModel_->sampleAccumulator().mean()[0]; //convention first element is price

	if (this->controlVariate_) {
		// control variate might lead to small negative
		// option values for deep OTM options
		this->results_.value = std::max(0.0, this->results_.value);
	}

	if (RNG::allowsErrorEstimate)
		results_.errorEstimate =
		this->mcModel_->sampleAccumulator().errorEstimate()[0]; //convention first element is price
}

/*time grid*/
template <class I, class RNG, class S>
inline TimeGrid MCEngineGeneric<I, RNG, S>::timeGrid() const {

	//control european exercise
	boost::shared_ptr<EuropeanExercise> exercise =
		boost::dynamic_pointer_cast<EuropeanExercise>(this->arguments_.exercise);
	QL_REQUIRE(exercise, "Only european exercise accepted for MCEngineGeneric.");

	//define time steps
	Date lastExerciseDate = exercise->lastDate();
	Time t = process_->time(lastExerciseDate);
	Size steps = 0;
	if (this->timeSteps_ != Null<Size>()) {
		steps = this->timeSteps_;
	}
	else if (this->timeStepsPerYear_ != Null<Size>()) {
		steps = std::max<Size>(static_cast<Size>(this->timeStepsPerYear_*t), 1);
	}

	QL_ENSURE(steps != 0, "please specify timeSteps or timeStepsPerYear");

	return TimeGrid(t, steps);
}

/*path generator*/
template <class I, class RNG, class S>
boost::shared_ptr<typename MCEngineGeneric<I, RNG, S>::path_generator_type>
MCEngineGeneric<I, RNG, S>::pathGenerator() const {

	TimeGrid grid = this->timeGrid();
	Size dimensions = process_->factors();
	typename RNG::rsg_type gen =
		RNG::make_sequence_generator(dimensions*(grid.size() - 1), seed_);
	return boost::shared_ptr<path_generator_type>(
		new path_generator_type(process_, grid,
		gen, brownianBridge_));
}

#endif // !MC_ENGINE_GENERIC_H
