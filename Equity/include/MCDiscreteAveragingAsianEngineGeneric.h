#ifndef MC_DISCRETE_AVERAGING_ASIAN_ENGINE_GENERIC_H
#define MC_DISCRETE_AVERAGING_ASIAN_ENGINE_GENERIC_H

#include "MCEngineGeneric.h"
#include <MonteCarlo\include\MonteCarloTraits.h>
#include <MonteCarlo\include\SequenceStatisticsArrayInspectors.h>

#include <ql\stochasticprocess.hpp>
#include <ql\pricingengines\mcsimulation.hpp>
#include <ql\math\statistics\sequencestatistics.hpp>
#include <ql\instruments\asianoption.hpp>
#include <ql\exercise.hpp>

using namespace QuantLib;

/*! \brief Abstract class to price family of Discrete Averaging Asian Options.

The payoffs that belong to the family are functions of a type of discrete averaging of the underlying values.
The method pathPricer() needs to be implemented in derived classes..
*/
template<class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCDiscreteAveragingAsianEngineGeneric :
	public MCEngineGeneric<DiscreteAveragingAsianOption, RNG, S> {
public:

	typedef
		typename MCEngineGeneric<DiscreteAveragingAsianOption, RNG, S>::path_generator_type
		path_generator_type;

	typedef typename MCEngineGeneric<DiscreteAveragingAsianOption, RNG, S>::path_pricer_type
		path_pricer_type;

	typedef typename MCEngineGeneric<DiscreteAveragingAsianOption, RNG, S>::stats_type
		stats_type;

	/** Constructor.
	@param process
	@param timeSteps
	@param timeStepsPerYear
	@param brownianBridge
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCDiscreteAveragingAsianEngineGeneric(
		const boost::shared_ptr<StochasticProcess>& process,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

protected:

	//! \name McSimulation interface
	//@{

	/** Method that constructs the Time Grid.
	@return TimeGrid
	*/
	TimeGrid timeGrid() const;

	//@}

};


// template definitions

/*constructor*/
template<class RNG, class S>
inline
MCDiscreteAveragingAsianEngineGeneric<RNG, S>::MCDiscreteAveragingAsianEngineGeneric(
const boost::shared_ptr<StochasticProcess>& process,
Size timeSteps,
Size timeStepsPerYear,
bool brownianBridge,
bool antitheticVariate,
bool controlVariate,
Size requiredSamples,
Real requiredTolerance,
Size maxSamples,
BigNatural seed)
: MCEngineGeneric<DiscreteAveragingAsianOption, RNG, S>(
process,
timeSteps,
timeStepsPerYear,
brownianBridge,
antitheticVariate,
controlVariate,
requiredSamples,
requiredTolerance,
maxSamples,
seed)
{}


/*time grid*/
template <class RNG, class S>
inline TimeGrid MCDiscreteAveragingAsianEngineGeneric<RNG, S>::timeGrid() const {

	//control european exercise
	boost::shared_ptr<EuropeanExercise> exercise =
		boost::dynamic_pointer_cast<EuropeanExercise>(this->arguments_.exercise);
	QL_REQUIRE(exercise, "Only european exercise accepted for MCDiscreteAveragingAsianEngineGeneric.");
	
	//get fixing times, excluding past fixings
	std::vector<Time> fixingTimes;
	Size i;
	for (i = 0; i<arguments_.fixingDates.size(); i++) {
		Time t = process_->time(arguments_.fixingDates[i]);
		if (t >= 0) {
			fixingTimes.push_back(t);
		}
	}

	//define time steps
	Date lastExerciseDate = exercise->lastDate();
	Time t = process_->time(lastExerciseDate);
	Size steps = 0;
	if (this->timeSteps_ != Null<Size>()) {
		steps = this->timeSteps_;
	}
	else if (this->timeStepsPerYear_ != Null<Size>()) {
		steps = std::max<Size>(static_cast<Size>(this->timeStepsPerYear_*t), 1);
	}

	return steps == 0 ? 
		TimeGrid(fixingTimes.begin(), fixingTimes.end()) : TimeGrid(fixingTimes.begin(), fixingTimes.end(), steps);
}

#endif // !MC_DISCRETE_AVERAGING_ASIAN_ENGINE_GENERIC_H
