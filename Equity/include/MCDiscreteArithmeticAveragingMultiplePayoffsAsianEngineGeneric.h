#ifndef MC_DISCRETE_ARITHMETIC_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_ENGINE_GENERIC_H
#define MC_DISCRETE_ARITHMETIC_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_ENGINE_GENERIC_H

#include "MCDiscreteAveragingAsianEngineGeneric.h"
#include <Instruments\include\DiscreteAveragingMultiplePayoffsAsianOption.h>

#include <ql\handle.hpp>
#include <ql\termstructures\yieldtermstructure.hpp>
#include <ql\utilities\tracing.hpp>

using namespace QuantLib;

/*! \brief Pricing engine to price multiple discrete arithmetic averaging asian options in one MC run.

*/
template <class RNG = PseudoRandom, class S = SequenceStatisticsArrayInspectors>
class MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric :
	public MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>
{
public:

	typedef
		typename MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>::path_generator_type
		path_generator_type;

	typedef typename MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>::path_pricer_type
		path_pricer_type;

	typedef typename MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>::stats_type
		stats_type;

	/** Constructor.
	@param process
	@param discountCurve
	@param timeSteps
	@param timeStepsPerYear
	@param brownianBridge
	@param antitheticVariate
	@param controlVariate
	@param requiredSamples
	@param requiredTolerance
	@param maxSamples
	@param seed
	*/
	MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric(
		const boost::shared_ptr<StochasticProcess>& process,
		const Handle<YieldTermStructure>& discountCurve,
		Size timeSteps,
		Size timeStepsPerYear,
		bool brownianBridge,
		bool antitheticVariate,
		bool controlVariate,
		Size requiredSamples,
		Real requiredTolerance,
		Size maxSamples,
		BigNatural seed);

	//! \name PricingEngine interface
	//@{
	/** Method that calculates the price of the instrument.
	*/
	void calculate() const;
	//@}

protected:

	//! \name McSimulation interface
	//@{

	/** Method that constructs the Time Grid.
	@return TimeGrid
	*/
	TimeGrid timeGrid() const;

	/** It returns the PathPricer to use in simulations.
	@return boost::shared_ptr<path_pricer_type>
	*/
	boost::shared_ptr<path_pricer_type> pathPricer() const;

	//@}

	Handle<YieldTermStructure> discountCurve_;

};

/*! \brief Path Pricer for MCDiscreteAveragingMultiplePayoffsAsianEngineGeneric.

The convention is that the first element of the Array is the price of the first payoff.
*/
class ArithmeticAverageMultiplePayoffsPathPricer : public PathPricer < MultiPath, Array >
{
public:

	/** Constructor.
	@param payoffs
	@param discount
	@param runningSum
	@param pastFixings
	@param initialValue
	*/
	ArithmeticAverageMultiplePayoffsPathPricer(
		std::vector<boost::shared_ptr<StrikedTypePayoff>> payoffs,
		DiscountFactor discount,
		Real runningSum = 0.0,
		Size pastFixings = 0,
		Real initialValue = Null<Real>());

	/** Operator()
	@param Multipath
	@return Array
	*/
	Array operator()(const MultiPath& multipath) const;

private:

	std::vector<boost::shared_ptr<StrikedTypePayoff>> payoffs_;
	DiscountFactor discount_;
	Real initialValue_; /**< Underlying's Initial Value */
	Real runningSum_;
	Size pastFixings_;
};

//template definitions MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric

/*constructor*/
template <class RNG, class S>
inline
MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric(
const boost::shared_ptr<StochasticProcess>& process,
const Handle<YieldTermStructure>& discountCurve,
Size timeSteps,
Size timeStepsPerYear,
bool brownianBridge,
bool antitheticVariate,
bool controlVariate,
Size requiredSamples,
Real requiredTolerance,
Size maxSamples,
BigNatural seed)
: MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>(
process,
timeSteps,
timeStepsPerYear,
false,
antitheticVariate,
false,
requiredSamples,
requiredTolerance,
maxSamples,
seed), discountCurve_(discountCurve)
{
	registerWith(discountCurve_);
}

/*time grid*/
template <class RNG, class S>
inline TimeGrid MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::timeGrid() const {

	//control european exercise
	boost::shared_ptr<EuropeanExercise> exercise =
		boost::dynamic_pointer_cast<EuropeanExercise>(this->arguments_.exercise);
	QL_REQUIRE(exercise, "Only european exercise accepted for MCDiscreteAveragingAsianEngineGeneric.");

	//get fixing times, excluding past fixings
	std::vector<Time> fixingTimes;
	Size i;
	for (i = 0; i<arguments_.fixingDates.size(); i++) {
		Time t = process_->time(arguments_.fixingDates[i]);
		if (t >= 0) {
			fixingTimes.push_back(t);
		}
	}

	//define time steps
	Date lastExerciseDate = exercise->lastDate();
	Time t = process_->time(lastExerciseDate);
	Size steps = 0;
	if (this->timeSteps_ != Null<Size>()) {
		steps = this->timeSteps_;
	}
	else if (this->timeStepsPerYear_ != Null<Size>()) {
		steps = std::max<Size>(static_cast<Size>(this->timeStepsPerYear_*t), 1);
	}

	return steps == 0 ?
		TimeGrid(fixingTimes.begin(), fixingTimes.end()) : TimeGrid(fixingTimes.begin(), fixingTimes.end(), steps);
}

/*path pricer*/
template <class RNG, class S>
boost::shared_ptr<
	typename MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::path_pricer_type>
	MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::pathPricer() const {

	return boost::shared_ptr<
		typename MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::path_pricer_type>(
		new ArithmeticAverageMultiplePayoffsPathPricer(
		this->arguments_.payoffs,
		this->discountCurve_->discount(this->timeGrid().back()),
		this->arguments_.runningAccumulator,
		this->arguments_.pastFixings
		));
}

/*calculate*/
template < class RNG, class S>
void MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric<RNG, S>::calculate() const
{
	//take care of first payoff (it goes to results_.value)
	MCEngineGeneric<DiscreteAveragingMultiplePayoffsAsianOption, RNG, S>::calculate();

	//check correct size: array must be payoffs + forward
	Array means = this->mcModel_->sampleAccumulator().mean();
	QL_REQUIRE(means.size() == this->arguments_.payoffs.size() + 1,
		"MCDiscreteArithmeticAveragingMultiplePayoffsAsianEngineGeneric: Statistics Array is not correct size. Expected payoffs' size + 1 (for forward)");

	//extract the rest of the payoffs' prices
	std::vector<Real> prices;
	for (int i = 1; i < means.size() - 1; i++)
	{
		prices.push_back(means[i]);
	}
	results_.additionalResults["prices"] = prices;

	//extract forward of arithmetic average - by convention it's last element of array
	results_.additionalResults["forward"] = means.back();
}

#endif // !MC_DISCRETE_AVERAGING_MULTIPLE_PAYOFFS_ASIAN_ENGINE_GENERIC_H
